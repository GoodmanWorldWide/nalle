<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>



<div class="deprecated-alert">
	<div class="dep-alert-inner">
<p>This website is optimized for certain devices and browsers. Sorry about that!</p>
<p>Please, try to visit our website with a more modern browser for full experience!</p>
 <p>nalle@nallesjoblad.com</p>
  <p> <a href="https://vimeo.com/nalleaxel">VIMEO</a></p>
</div>
	</div>
		<script type="text/javascript">
		var supportsES6 = function() {
					try {
					new Function("(a = 0) => a");
					return true;
					}
					catch (err) {
					return false;
					}
					}();

		</script>

<script type="text/javascript">

		if (supportsES6) {

		} else {
			document.querySelector(".deprecated-alert").style.display = "block";
		}


		</script>
    <!-- <script src="https://player.vimeo.com/api/player.js"></script> -->
    <script type="text/javascript">
    const home_url = "<?php echo home_url() ?>"; // "A string here"
    console.log(home_url);
    </script>
<?php wp_footer(); ?>



</body>
</html>
