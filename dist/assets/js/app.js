/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 12);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(4);

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is a Buffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Buffer, otherwise false
 */
function isBuffer(val) {
  return val !== null && !isUndefined(val) && val.constructor !== null && !isUndefined(val.constructor)
    && typeof val.constructor.isBuffer === 'function' && val.constructor.isBuffer(val);
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 * nativescript
 *  navigator.product -> 'NativeScript' or 'NS'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' ||
                                           navigator.product === 'NativeScript' ||
                                           navigator.product === 'NS')) {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Function equal to merge with the difference being that no reference
 * to original objects is kept.
 *
 * @see merge
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function deepMerge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = deepMerge(result[key], val);
    } else if (typeof val === 'object') {
      result[key] = deepMerge({}, val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  deepMerge: deepMerge,
  extend: extend,
  trim: trim
};


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Link = exports.markSwupElements = exports.getCurrentUrl = exports.transitionEnd = exports.fetch = exports.getDataFromHtml = exports.createHistoryRecord = exports.classify = undefined;

var _classify = __webpack_require__(19);

var _classify2 = _interopRequireDefault(_classify);

var _createHistoryRecord = __webpack_require__(20);

var _createHistoryRecord2 = _interopRequireDefault(_createHistoryRecord);

var _getDataFromHtml = __webpack_require__(21);

var _getDataFromHtml2 = _interopRequireDefault(_getDataFromHtml);

var _fetch = __webpack_require__(22);

var _fetch2 = _interopRequireDefault(_fetch);

var _transitionEnd = __webpack_require__(23);

var _transitionEnd2 = _interopRequireDefault(_transitionEnd);

var _getCurrentUrl = __webpack_require__(24);

var _getCurrentUrl2 = _interopRequireDefault(_getCurrentUrl);

var _markSwupElements = __webpack_require__(25);

var _markSwupElements2 = _interopRequireDefault(_markSwupElements);

var _Link = __webpack_require__(26);

var _Link2 = _interopRequireDefault(_Link);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var classify = exports.classify = _classify2.default;
var createHistoryRecord = exports.createHistoryRecord = _createHistoryRecord2.default;
var getDataFromHtml = exports.getDataFromHtml = _getDataFromHtml2.default;
var fetch = exports.fetch = _fetch2.default;
var transitionEnd = exports.transitionEnd = _transitionEnd2.default;
var getCurrentUrl = exports.getCurrentUrl = _getCurrentUrl2.default;
var markSwupElements = exports.markSwupElements = _markSwupElements2.default;
var Link = exports.Link = _Link2.default;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var query = exports.query = function query(selector) {
	var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

	if (typeof selector !== 'string') {
		return selector;
	}

	return context.querySelector(selector);
};

var queryAll = exports.queryAll = function queryAll(selector) {
	var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

	if (typeof selector !== 'string') {
		return selector;
	}

	return Array.prototype.slice.call(context.querySelectorAll(selector));
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Plugin = function () {
    function Plugin() {
        _classCallCheck(this, Plugin);

        this.isSwupPlugin = true;
    }

    _createClass(Plugin, [{
        key: "mount",
        value: function mount() {
            // this is mount method rewritten by class extending
            // and is executed when swup is enabled with plugin
        }
    }, {
        key: "unmount",
        value: function unmount() {
            // this is unmount method rewritten by class extending
            // and is executed when swup with plugin is disabled
        }
    }, {
        key: "_beforeMount",
        value: function _beforeMount() {
            // here for any future hidden auto init
        }
    }, {
        key: "_afterUnmount",
        value: function _afterUnmount() {}
        // here for any future hidden auto-cleanup


        // this is here so we can tell if plugin was created by extending this class

    }]);

    return Plugin;
}();

exports.default = Plugin;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    var hashmarkIndex = url.indexOf('#');
    if (hashmarkIndex !== -1) {
      url = url.slice(0, hashmarkIndex);
    }

    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__(0);
var normalizeHeaderName = __webpack_require__(53);

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(8);
  } else if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
    // For node use HTTP adapter
    adapter = __webpack_require__(8);
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Accept');
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(52)))

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);
var settle = __webpack_require__(54);
var buildURL = __webpack_require__(5);
var buildFullPath = __webpack_require__(56);
var parseHeaders = __webpack_require__(59);
var isURLSameOrigin = __webpack_require__(60);
var createError = __webpack_require__(9);

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    var fullPath = buildFullPath(config.baseURL, config.url);
    request.open(config.method.toUpperCase(), buildURL(fullPath, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request.onreadystatechange = function handleLoad() {
      if (!request || request.readyState !== 4) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        status: request.status,
        statusText: request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle browser request cancellation (as opposed to a manual cancellation)
    request.onabort = function handleAbort() {
      if (!request) {
        return;
      }

      reject(createError('Request aborted', config, 'ECONNABORTED', request));

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      var timeoutErrorMessage = 'timeout of ' + config.timeout + 'ms exceeded';
      if (config.timeoutErrorMessage) {
        timeoutErrorMessage = config.timeoutErrorMessage;
      }
      reject(createError(timeoutErrorMessage, config, 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__(61);

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(fullPath)) && config.xsrfCookieName ?
        cookies.read(config.xsrfCookieName) :
        undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (!utils.isUndefined(config.withCredentials)) {
      request.withCredentials = !!config.withCredentials;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__(55);

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);

/**
 * Config-specific merge-function which creates a new config-object
 * by merging two configuration objects together.
 *
 * @param {Object} config1
 * @param {Object} config2
 * @returns {Object} New object resulting from merging config2 to config1
 */
module.exports = function mergeConfig(config1, config2) {
  // eslint-disable-next-line no-param-reassign
  config2 = config2 || {};
  var config = {};

  var valueFromConfig2Keys = ['url', 'method', 'params', 'data'];
  var mergeDeepPropertiesKeys = ['headers', 'auth', 'proxy'];
  var defaultToConfig2Keys = [
    'baseURL', 'url', 'transformRequest', 'transformResponse', 'paramsSerializer',
    'timeout', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName',
    'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress',
    'maxContentLength', 'validateStatus', 'maxRedirects', 'httpAgent',
    'httpsAgent', 'cancelToken', 'socketPath'
  ];

  utils.forEach(valueFromConfig2Keys, function valueFromConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    }
  });

  utils.forEach(mergeDeepPropertiesKeys, function mergeDeepProperties(prop) {
    if (utils.isObject(config2[prop])) {
      config[prop] = utils.deepMerge(config1[prop], config2[prop]);
    } else if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (utils.isObject(config1[prop])) {
      config[prop] = utils.deepMerge(config1[prop]);
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });

  utils.forEach(defaultToConfig2Keys, function defaultToConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });

  var axiosKeys = valueFromConfig2Keys
    .concat(mergeDeepPropertiesKeys)
    .concat(defaultToConfig2Keys);

  var otherKeys = Object
    .keys(config2)
    .filter(function filterAxiosKeys(key) {
      return axiosKeys.indexOf(key) === -1;
    });

  utils.forEach(otherKeys, function otherKeysDefaultToConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });

  return config;
};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(13);


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _swup = __webpack_require__(14);

var _swup2 = _interopRequireDefault(_swup);

var _bodyClassPlugin = __webpack_require__(35);

var _bodyClassPlugin2 = _interopRequireDefault(_bodyClassPlugin);

var _debugPlugin = __webpack_require__(36);

var _debugPlugin2 = _interopRequireDefault(_debugPlugin);

var _jsPlugin = __webpack_require__(37);

var _jsPlugin2 = _interopRequireDefault(_jsPlugin);

var _preloadPlugin = __webpack_require__(39);

var _preloadPlugin2 = _interopRequireDefault(_preloadPlugin);

var _scrollPlugin = __webpack_require__(42);

var _scrollPlugin2 = _interopRequireDefault(_scrollPlugin);

__webpack_require__(44);

__webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var axios = __webpack_require__(46).default;


// import {
//   headroom
// } from "./modules/headroom";

lazySizes.cfg.expFactor = 4;

document.addEventListener('lazyloaded', function (e) {

  if (e.target.classList.contains('hero-image')) {
    // setTimeout(function(){

    e.target.closest(".js-film-link").classList.remove("image--loading");
    // }, 3000);
  }
});

////HERO MOUSE OVER ///

function hero_hover() {
  var hero_hover_item = document.querySelectorAll(".hero-block-link");

  if (hero_hover_item.length > 0) {
    for (var i = 0, len = hero_hover_item.length; i < len; i++) {
      hero_hover_item[i].addEventListener("mouseenter", function (e) {

        // e.target.appendChild(canvas_item);
        e.target.classList.add("hover--active");
      });
      hero_hover_item[i].addEventListener("mouseleave", function (e) {

        e.target.classList.remove("hover--active");
      });
    }
  }
}

function com_hover() {
  var hero_hover_item = document.querySelectorAll(".com-block-link");

  if (hero_hover_item.length > 0) {
    for (var i = 0, len = hero_hover_item.length; i < len; i++) {
      hero_hover_item[i].addEventListener("mouseenter", function (e) {
        var canvas_item_2 = document.querySelector(".noise-canvas_2");
        // e.target.querySelector('.com-cell-inner').appendChild(canvas_item_2);
        e.target.classList.add("hover--active");
      });
      hero_hover_item[i].addEventListener("mouseleave", function (e) {

        e.target.classList.remove("hover--active");
      });
    }
  }
}

////HERO load more ///


function hero_load_more() {

  var link = document.querySelector(".js-load-second-row");
  var spinner = document.querySelector(".spin-shit");
  link.addEventListener('click', function (event) {
    event.preventDefault();
    var the_url = event.currentTarget.href;
    // console.log(the_url);


    link.style.opacity = "0";
    spinner.style.opacity = "1";

    axios.get(the_url).then(function (response) {
      // handle success
      document.querySelector(".hero-row--2").classList.remove("hide");
      // console.log(response);
      // console.log(response.data);
      var com_home_container = document.querySelector('.hero-row--2');
      var wrapper = document.createElement('div');
      wrapper.innerHTML = response.data;
      var newContent = wrapper.querySelector('.section--commercials-grid');
      com_home_container.appendChild(newContent);
      setTimeout(function () {
        link.style.display = "none";
        spinner.style.opacity = "0";
      }, 500);

      setTimeout(function () {
        spinner.style.opacity = "0";

        setTimeout(function () {
          document.querySelector(".hero-row--2").classList.add("transform-com-block");
        }, 500);
        scrollIt(document.querySelector('.home-scroll-point'), 1000, 'easeInOutQuint', function () {
          spinner.style.display = "none";
          document.querySelector(".hero-row--2").classList.add("transform-com-block");
          // init_second_noise();
          com_hover();
          window.pjax_target = document.querySelectorAll(".com-block-link");
          init_pjax_links();
          document.querySelector(".bottom-link").classList.remove("bottom-link-home");
        });
      }, 500);
    }).catch(function (error) {
      // handle error
      console.log(error);
    }).then(function () {
      // console.log("axios request");
    });

    //  setTimeout(function(){
    // document.querySelector(".hero-row--2").classList.remove("hero-cell-transform-trigger");
    //
    // }, 500);
    //
    // setTimeout(function(){
    //   document.querySelector(".commercials-flex-grid").style.opacity = "1";
    // // document.querySelector(".hero-row--1").classList.add("second-row-loaded");
    // }, 1100);
    return false;
  });
}

//////SWUP//////


var js_options = [{
  from: '(.*)', to: 'film-view-transition',
  out: function out(next) {
    var root = document.getElementsByTagName("HTML")[0];
    document.querySelector('.project-overlay-view').classList.add("project-overlay-view--show");
    // next();
    // console.log("pjax");
  },

  in: function _in(next) {
    next();
  }

}, {
  from: '(.*)', to: 'basic-transition',
  out: function out(next) {
    var root = document.getElementsByTagName("HTML")[0];
    root.classList.add("pjax--fade");
    // root.classList.remove("activate-animation");
    // setTimeout(function(){
    next();
    // }, 500);

  },

  in: function _in(next) {
    window.scrollTo(0, 0);

    var root = document.getElementsByTagName("HTML")[0];
    if (root.classList.contains('activate-animation')) {
      // console.log("logo anim runnning");
    } else {
      root.classList.add("activate-animation");
      setTimeout(function () {
        root.classList.remove("activate-animation");
      }, 2600);
    }

    init_views();
    setTimeout(function () {
      root.classList.remove("pjax--fade");
      next();
    }, 100);
    // next();
  }

}];

var options = {

  containers: ['.pjax-container--main'],
  linkSelector: '.pjax-link',
  animationSelector: '[class*="page-transition"]',
  animateHistoryBrowsing: true,
  plugins: [new _jsPlugin2.default(js_options),
  // new SwupDebugPlugin(),

  new _preloadPlugin2.default(), new _bodyClassPlugin2.default()]
};

if (document.body.classList.contains('single')) {} else {
  var swup = new _swup2.default(options);
  document.addEventListener('swup:samePage', function (event) {
    // console.log("same page");
    document.getElementsByTagName("body")[0].classList.remove("mobile-menu--active");
  });
}

// const link = document.querySelector(".hero-cell-inner");
// link.addEventListener('click', event => {
//   event.preventDefault();
//   var the_url = event.currentTarget.href;
//   console.log(the_url);
//
//   swup.preloadPage(the_url);
//
//
//
//
//
// return false;
// });


// LOAD FILM ///


function create_project_data() {
  var client_name = event.currentTarget.dataset.clientname;
  var project_name = event.currentTarget.dataset.projectname;

  var cName = document.createTextNode(client_name);
  var pName = document.createTextNode(project_name);
  // console.log(cName);
  // console.log(pName);
  document.querySelector(".film-loading-wrapper--client-name").innerHTML = "";
  document.querySelector('.film-loading-wrapper--client-name').appendChild(cName);
  document.querySelector(".film-loading-wrapper--project-name").innerHTML = "";
  document.querySelector('.film-loading-wrapper--project-name').appendChild(pName);
}

///CUSTOM PJAX From GRID////


function init_pjax_links() {
  var film_links = pjax_target;
  // console.log(film_links);
  for (var z = 0; z < film_links.length; z++) {
    var elem = film_links[z];

    elem.addEventListener('click', function (event) {
      event.preventDefault();
      event.currentTarget.classList.add("prevent-click");
      event.currentTarget.classList.remove("hover--active");

      create_project_data();

      ////TRANSITION Before load////


      /////THE FECTH //////

      var the_url = event.currentTarget.href;
      // console.log(the_url);
      var stateObj;
      window.history.pushState(stateObj, "", the_url);
      var oldContent = document.querySelector('.pjax-container--film');
      if (oldContent !== null) {
        oldContent.parentNode.removeChild(oldContent);
      }

      window.gBodyScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
      document.querySelector('.close-film-button-wrapper').style.display = "block";
      var root = document.getElementsByTagName("HTML")[0];

      document.querySelector('.project-overlay-view').removeAttribute("style");

      setTimeout(function () {
        document.querySelector('.project-overlay-view').classList.add("project-overlay-view--show");
      }, 300);

      // Make a request for a user with a given ID
      axios.get(the_url).then(function (response) {
        // handle success

        // console.log(response);
        // console.log(response.data);
        var film_container = document.querySelector('.pjax-parent');
        var film_container = document.querySelector('.pjax-parent');

        var wrapper = document.createElement('div');
        wrapper.innerHTML = response.data;
        var newContent = wrapper.querySelector('.pjax-container--film');
        film_container.appendChild(newContent);
        setTimeout(function () {
          document.querySelector('.main-app').style.opacity = "0";
          document.querySelector('.main-navigation--wrapper').style.visibility = "hidden";
          root.classList.add("body-bg-override");
          setTimeout(function () {
            document.querySelector('#nalle-body').style.display = "none";
            document.querySelector('.project-overlay-view').style.position = "relative";
          }, 10);
          window.scrollTo(0, 0);
          document.querySelector('.prevent-click').classList.remove("prevent-click");
          var next_film_link = document.querySelector(".js-next-film-link");
          if (typeof next_film_link != 'undefined' && next_film_link != null) {
            next_film_trigger();
          } else {
            // console.log("no next film");
          }

          document.querySelector('.film-loading-wrapper').style.opacity = "0";
          setTimeout(function () {
            document.querySelector('.film-loading-wrapper').style.display = "none";
            var root = document.getElementsByTagName("HTML")[0];
            document.querySelector('.close-fim-button').style.opacity = "1";
            document.querySelector('.pjax-container--main').removeAttribute("style");

            document.querySelector('.pjax-container--film').classList.remove("film--hidden");
          }, 500);
        }, 1000);
      }).catch(function (error) {
        // handle error
        console.log(error);
      }).then(function () {
        // console.log("axios request");
        root.classList.add("overlay-view--active");
      });

      return false;
    });
  }
}

///////NNEXT FILM LINKS /////////

//Combine code if needed more edits////

function next_film_trigger() {

  var next_film_link = document.querySelector(".js-next-film-link");

  next_film_link.addEventListener('click', function (event) {
    // console.log("next fiiiiilm");
    event.currentTarget.classList.add("prevent-click");
    event.preventDefault();

    document.querySelector('.film-loading-wrapper').style.removeProperty('display');
    create_project_data();

    setTimeout(function () {
      window.scrollTo(0, 0);
      document.querySelector('.film-loading-wrapper').style.opacity = "1";
      // window.scrollTo(0, 0);
    }, 500);

    ////TRANSITION Before load////


    var root = document.getElementsByTagName("HTML")[0];

    /////THE FECTH //////

    var the_url = event.currentTarget.href;
    var stateObj;
    window.history.replaceState(stateObj, "", the_url);
    // console.log(the_url);
    var oldContent = document.querySelector('.pjax-container--film');
    if (oldContent !== null) {
      oldContent.style.opacity = 0;
    }

    axios.get(the_url).then(function (response) {
      // handle success

      // console.log(response);
      // console.log(response.data);
      var film_container = document.querySelector('.pjax-parent');

      var wrapper = document.createElement('div');
      wrapper.innerHTML = response.data;
      var newContent = wrapper.querySelector('.pjax-container--film');
      film_container.appendChild(newContent);
      setTimeout(function () {
        oldContent.parentNode.removeChild(oldContent);
        next_film_trigger();
        document.querySelector('.film-loading-wrapper').style.opacity = "0";
        setTimeout(function () {

          var root = document.getElementsByTagName("HTML")[0];
          document.querySelector('.close-film-button-wrapper').style.opacity = "1";
          document.querySelector('.film-loading-wrapper').style.display = "none";
          document.querySelector('.project-overlay-view').style.position = "relative";
          window.scrollTo(0, 0);
          document.querySelector('.pjax-container--film').classList.remove("film--hidden");
        }, 300);
      }, 1000);
    }).catch(function (error) {
      // handle error
      console.log(error);
    }).then(function () {
      // console.log("axios request");
    });

    return false;
  });
}

//////LOAD FILM ENDS ///////


//////CLOSE FILM ///////

function close_overlay() {

  var root = document.getElementsByTagName("HTML")[0];
  window.pagesrol = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  var minuspagesrol = -pagesrol;
  // console.log(pagesrol);
  //   if (root.classList.contains('header--not-on-top')) {
  // root.classList.add("bio-button--keep-position");
  //   }
  // document.querySelector('.header-bg-wrap').removeAttribute("style");
  // document.querySelector('.hide-bio-overlay-wrapper').style.display = "block";
  document.querySelector('.close-fim-button').style.opacity = "0";
  document.querySelector('.project-overlay-view').style.position = "fixed";
  document.querySelector(".inner-mover").style.transform = "translateY(" + minuspagesrol + "px)";
  document.querySelector('#nalle-body').style.display = "";

  // setTimeout(function(){
  root.classList.remove("body-bg-override");

  // }, 100);
  // window.scrollTo(0, 0);
  setTimeout(function () {
    window.scrollTo(0, gBodyScroll);
    setTimeout(function () {
      document.querySelector('.main-app').style.opacity = "";
      document.querySelector('.main-navigation--wrapper').style.visibility = "visible";
      document.querySelector('.project-overlay-view').classList.remove("project-overlay-view--show");
      setTimeout(function () {
        document.querySelector(".inner-mover").removeAttribute("style");
        document.querySelector('.film-loading-wrapper').removeAttribute("style");
        document.querySelector('.project-overlay-view').style.display = "none";
        // document.querySelector('.jaymo-bio-section').removeAttribute("style");
        // root.classList.add("filter-sticky--active");
        // root.classList.remove("filter-sticky--super-active");
        // document.querySelector('.jaymo-bio-section').classList.remove("show-shaker");
        document.querySelector('.close-film-button-wrapper').style.display = "none";
        // root.classList.remove("bio-button--keep-position");
        var oldContent = document.querySelector('.pjax-container--film');

        if (document.body.classList.contains('page-template-home')) {
          var stateObj;
          window.history.replaceState(stateObj, "", home_url);
        }
        if (document.body.classList.contains('page-template-commercials')) {
          var stateObj;
          window.history.replaceState(stateObj, "", home_url + "/commercials/");
        }

        if (document.body.classList.contains('page-template-shortfilms')) {
          var stateObj;
          window.history.replaceState(stateObj, "", home_url + "/short-films/");
        }

        root.classList.remove("overlay-view--active");

        if (oldContent !== null) {
          oldContent.parentNode.removeChild(oldContent);
        }
      }, 600);
    }, 100);
  }, 500);
}

document.querySelector('#js-close-film').onclick = function () {
  close_overlay();
  return false;
};

function create_canvas_1() {
  var new_canvas = document.createElement('canvas');
  new_canvas.className = "noise-canvas noise-canvas_1";
  console.log(new_canvas);
  // console.log(canvas_item);
  var canvas_parent = document.querySelector('.hero-block-link');
  var positionInfo = canvas_parent.getBoundingClientRect();
  var c_height = positionInfo.height;
  var c_width = positionInfo.width;
  console.log(c_height);
  console.log(c_width);
  document.querySelector(".hero-block-link").appendChild(new_canvas);
  window.canvas_item = document.querySelector(".noise-canvas_1");
  noise_1();
}

// for (var i = 0, len = canvas_item.length; i < len; i++) {
// noise_1();
// }

function noise_1() {
  var viewWidth,
      viewHeight,
      canvas = canvas_item,
      ctx;

  // change these settings
  var patternSize = 64,
      patternScaleX = 3,
      patternScaleY = 1,
      patternRefreshInterval = 4,
      patternAlpha = 25; // int between 0 and 255,

  var patternPixelDataLength = patternSize * patternSize * 4,
      patternCanvas,
      patternCtx,
      patternData,
      frame = 0;

  // window.onload = function() {
  initCanvas();
  initGrain();
  requestAnimationFrame(loop);
  // };

  // create a canvas which will render the grain
  function initCanvas() {
    viewWidth = canvas.width = canvas.clientWidth;
    viewHeight = canvas.height = canvas.clientHeight;
    ctx = canvas.getContext('2d');
    console.log(ctx);
    ctx.scale(patternScaleX, patternScaleY);
  }

  // create a canvas which will be used as a pattern
  function initGrain() {
    patternCanvas = document.createElement('canvas');
    console.log(patternCanvas);
    patternCanvas.width = patternSize;
    patternCanvas.height = patternSize;
    patternCtx = patternCanvas.getContext('2d');
    patternData = patternCtx.createImageData(patternSize, patternSize);
  }

  // put a random shade of gray into every pixel of the pattern
  function update() {
    var value;

    for (var i = 0; i < patternPixelDataLength; i += 4) {
      value = Math.random() * 205 | 0;

      patternData.data[i] = value;
      patternData.data[i + 1] = value;
      patternData.data[i + 2] = value;
      patternData.data[i + 3] = patternAlpha;
    }

    patternCtx.putImageData(patternData, 0, 0);
  }

  // fill the canvas using the pattern
  function draw() {
    ctx.clearRect(0, 0, viewWidth, viewHeight);

    ctx.fillStyle = ctx.createPattern(patternCanvas, 'repeat');
    ctx.fillRect(0, 0, viewWidth, viewHeight);
  }

  function loop() {
    if (++frame % patternRefreshInterval === 0) {
      update();
      draw();
    }

    requestAnimationFrame(loop);
  }
}

//////MOBILE NAV/////


function mobile_nav() {
  var menu_trigger_open = document.querySelector(".js-mobile-nav-trigger");
  // const menu_trigger_close = document.querySelector(".js-close-mobile-menu");
  menu_trigger_open.addEventListener("click", function () {
    // console.log("MOBILE MENU");
    if (document.body.classList.contains('mobile-menu--active')) {
      document.getElementsByTagName("body")[0].classList.remove("mobile-menu--active");
    } else {
      document.getElementsByTagName("body")[0].classList.add("mobile-menu--active");
    }
  });
}
mobile_nav();

// setTimeout(function(){
// noise_1();
// noise_2();
// }, 2000);


//SECOND NOISE //

function init_second_noise() {
  var new_canvas = document.createElement('canvas');
  new_canvas.className = "noise-canvas noise-canvas_2";
  console.log(new_canvas);

  // console.log(canvas_item);
  // var canvas_parent = document.querySelector('.com-cell-inner');

  var canvas_parents = document.querySelectorAll('.com-cell-inner');
  var last_canvas = canvas_parents[canvas_parents.length - 1];

  var positionInfo = last_canvas.getBoundingClientRect();
  var c_height = positionInfo.height;
  var c_width = positionInfo.width;
  console.log(c_height);
  console.log(c_width);

  var append_block_parents = document.querySelectorAll('.com-block-link');
  var append_block = append_block_parents[append_block_parents.length - 1];

  append_block.appendChild(new_canvas);
  window.canvas_item_2 = document.querySelector(".noise-canvas_2");
  console.log(canvas_item_2);
  noise_2();
  function noise_2() {
    console.log("noise 2 active");
    var viewWidth,
        viewHeight,
        canvas = canvas_item_2,
        ctx;

    // change these settings
    var patternSize = 64,
        patternScaleX = 3,
        patternScaleY = 1,
        patternRefreshInterval = 4,
        patternAlpha = 25; // int between 0 and 255,

    var patternPixelDataLength = patternSize * patternSize * 4,
        patternCanvas,
        patternCtx,
        patternData,
        frame = 0;

    // window.onload = function() {
    initCanvas();
    initGrain();
    requestAnimationFrame(loop);
    // };

    // create a canvas which will render the grain
    function initCanvas() {
      viewWidth = canvas.width = canvas.clientWidth;
      viewHeight = canvas.height = canvas.clientHeight;
      ctx = canvas.getContext('2d');

      ctx.scale(patternScaleX, patternScaleY);
    }

    // create a canvas which will be used as a pattern
    function initGrain() {
      patternCanvas = document.createElement('canvas');
      patternCanvas.width = patternSize;
      patternCanvas.height = patternSize;
      patternCtx = patternCanvas.getContext('2d');
      patternData = patternCtx.createImageData(patternSize, patternSize);
    }

    // put a random shade of gray into every pixel of the pattern
    function update() {
      var value;

      for (var i = 0; i < patternPixelDataLength; i += 4) {
        value = Math.random() * 205 | 0;

        patternData.data[i] = value;
        patternData.data[i + 1] = value;
        patternData.data[i + 2] = value;
        patternData.data[i + 3] = patternAlpha;
      }

      patternCtx.putImageData(patternData, 0, 0);
    }

    // fill the canvas using the pattern
    function draw() {
      ctx.clearRect(0, 0, viewWidth, viewHeight);

      ctx.fillStyle = ctx.createPattern(patternCanvas, 'repeat');
      ctx.fillRect(0, 0, viewWidth, viewHeight);
    }

    function loop() {
      if (++frame % patternRefreshInterval === 0) {
        update();
        draw();
      }

      requestAnimationFrame(loop);
    }
  }
}

//////HEADER SSCROLLLL///////


window.onscroll = function () {
  var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;

  var root = document.getElementsByTagName("HTML")[0];

  if (scrollTop > 20) {
    var root = document.getElementsByTagName("HTML")[0];
    root.classList.add("header--not-on-top");
  } else {
    var root = document.getElementsByTagName("HTML")[0];
    root.classList.remove("header--not-on-top");
  }
};

function video_loader() {
  // window.addEventListener('load', function() {
  var video = document.querySelector('#contact-video-id');

  // var preloader = document.querySelector('.preloader');

  function checkLoad() {
    if (video.readyState === 4) {
      // preloader.parentNode.removeChild(preloader);
      console.log("video loaded");

      video.play();
    } else {
      setTimeout(checkLoad, 100);
    }
  }

  checkLoad();
  // }, false);
}

//////////// View constractor ////////

function init_views() {
  // console.log("init app");
  if (document.body.classList.contains('page-template-home')) {
    // console.log("home init");
    var init_pjax = function init_pjax() {
      window.pjax_target = document.querySelectorAll(".hero-block-link");
      init_pjax_links();
    };

    set_hero_screen_height();
    window.addEventListener("resize", set_hero_screen_height);
    init_pjax();
    hero_load_more();
    // create_canvas_1();
    hero_hover();
  }
  if (document.body.classList.contains('page-template-commercials')) {
    // console.log("commercials init");
    var _init_pjax = function _init_pjax() {
      window.pjax_target = document.querySelectorAll(".com-block-link");
      init_pjax_links();
    };

    _init_pjax();
    // init_second_noise();
    com_hover();
  }

  if (document.body.classList.contains('page-template-shortfilms')) {
    // console.log("commercials init");
    var _init_pjax2 = function _init_pjax2() {
      window.pjax_target = document.querySelectorAll(".com-block-link");
      init_pjax_links();
    };

    _init_pjax2();
    // init_second_noise();
    com_hover();
  }

  if (document.body.classList.contains('page-template-contact')) {
    // console.log("contact init");
    video_loader();
    setTimeout(function () {
      document.querySelector('.contact-video-cell').style.opacity = "1";
    }, 1000);
  }
}

init_views();

var isIphone = navigator.userAgent.indexOf("iPhone") != -1;

if (isIphone) {
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.add("ios-fix");
} else {
  //   var root = document.getElementsByTagName("HTML")[0];
  // root.classList.add("no-ios-fix");
}

var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && navigator.userAgent.indexOf('CriOS') == -1 && navigator.userAgent.indexOf('FxiOS') == -1;

if (isSafari) {
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.add("ios-fix");
} else {
  //   var root = document.getElementsByTagName("HTML")[0];
  // root.classList.add("no-ios-fix");
}

setTimeout(function () {
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.remove("page-is-loading");
  setTimeout(function () {
    root.classList.add("activate-animation");
    setTimeout(function () {
      root.classList.remove("logo-opacity");
      root.classList.remove("activate-animation");
    }, 2700);
  }, 0);
  setTimeout(function () {
    document.querySelector('.loading-smoke').style.display = "none";
    // root.classList.remove("logo-opacity");
  }, 500);
}, 500);

window.onpopstate = function (event) {
  if (event.state) {
    // history changed because of pushState/replaceState
    // console.log("pushstaaate");
    var root = document.getElementsByTagName("HTML")[0];
    if (root.classList.contains('overlay-view--active')) {
      close_overlay();
    }
    var newPageUrl = location.href;
    console.log(newPageUrl);
  } else {
    var root = document.getElementsByTagName("HTML")[0];
    // console.log("pushstaaate but something elseee");
    if (root.classList.contains('overlay-view--active')) {
      close_overlay();
    }

    var newPageUrl = location.href;
    console.log(newPageUrl);
    if (/commercials/.test(window.location.href)) {
      console.log("COMMERICALLLLL");
      location.replace(newPageUrl);
    }
    if (/shortfilms/.test(window.location.href)) {
      console.log("COMMERICALLLLL");
      location.replace(newPageUrl);
    }
  }
};

function set_hero_screen_height() {
  if (window.innerWidth > 750) {
    // console.log("hero set size");
    var inner_height = window.innerHeight;
    var hero_block = document.querySelector('.hero-row--1');
    if (typeof hero_block != 'undefined' && hero_block != null) {
      document.querySelector('.hero-row--1').style.height = inner_height + "px";
    }
  } else {
    var hero_block = document.querySelector('.hero-row--1');
    if (typeof hero_block != 'undefined' && hero_block != null) {
      document.querySelector('.hero-row--1').removeAttribute("style");
    }
    // console.log("hero clear size");
  }
}

if (typeof next_film_link != 'undefined' && next_film_link != null) {
  next_film_trigger();
} else {
  // console.log("no next film");
}

window.addEventListener('touchstart', function onFirstTouch() {
  // we could use a class
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.add('touch-device');

  // we only need to know once that a human touched the screen, so we can stop listening now
  window.removeEventListener('touchstart', onFirstTouch, false);
}, false);

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

// modules


var _delegate = __webpack_require__(15);

var _delegate2 = _interopRequireDefault(_delegate);

var _Cache = __webpack_require__(17);

var _Cache2 = _interopRequireDefault(_Cache);

var _loadPage = __webpack_require__(18);

var _loadPage2 = _interopRequireDefault(_loadPage);

var _renderPage = __webpack_require__(27);

var _renderPage2 = _interopRequireDefault(_renderPage);

var _triggerEvent = __webpack_require__(28);

var _triggerEvent2 = _interopRequireDefault(_triggerEvent);

var _on = __webpack_require__(29);

var _on2 = _interopRequireDefault(_on);

var _off = __webpack_require__(30);

var _off2 = _interopRequireDefault(_off);

var _updateTransition = __webpack_require__(31);

var _updateTransition2 = _interopRequireDefault(_updateTransition);

var _getAnimationPromises = __webpack_require__(32);

var _getAnimationPromises2 = _interopRequireDefault(_getAnimationPromises);

var _getPageData = __webpack_require__(33);

var _getPageData2 = _interopRequireDefault(_getPageData);

var _plugins = __webpack_require__(34);

var _utils = __webpack_require__(2);

var _helpers = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Swup = function () {
	function Swup(setOptions) {
		_classCallCheck(this, Swup);

		// default options
		var defaults = {
			animateHistoryBrowsing: false,
			animationSelector: '[class*="transition-"]',
			linkSelector: 'a[href^="' + window.location.origin + '"]:not([data-no-swup]), a[href^="/"]:not([data-no-swup]), a[href^="#"]:not([data-no-swup])',
			cache: true,
			containers: ['#swup'],
			requestHeaders: {
				'X-Requested-With': 'swup',
				Accept: 'text/html, application/xhtml+xml'
			},
			plugins: [],
			skipPopStateHandling: function skipPopStateHandling(event) {
				return !(event.state && event.state.source === 'swup');
			}
		};

		// merge options
		var options = _extends({}, defaults, setOptions);

		// handler arrays
		this._handlers = {
			animationInDone: [],
			animationInStart: [],
			animationOutDone: [],
			animationOutStart: [],
			animationSkipped: [],
			clickLink: [],
			contentReplaced: [],
			disabled: [],
			enabled: [],
			openPageInNewTab: [],
			pageLoaded: [],
			pageRetrievedFromCache: [],
			pageView: [],
			popState: [],
			samePage: [],
			samePageWithHash: [],
			serverError: [],
			transitionStart: [],
			transitionEnd: [],
			willReplaceContent: []
		};

		// variable for id of element to scroll to after render
		this.scrollToElement = null;
		// variable for promise used for preload, so no new loading of the same page starts while page is loading
		this.preloadPromise = null;
		// variable for save options
		this.options = options;
		// variable for plugins array
		this.plugins = [];
		// variable for current transition object
		this.transition = {};
		// variable for keeping event listeners from "delegate"
		this.delegatedListeners = {};

		// make modules accessible in instance
		this.cache = new _Cache2.default();
		this.cache.swup = this;
		this.loadPage = _loadPage2.default;
		this.renderPage = _renderPage2.default;
		this.triggerEvent = _triggerEvent2.default;
		this.on = _on2.default;
		this.off = _off2.default;
		this.updateTransition = _updateTransition2.default;
		this.getAnimationPromises = _getAnimationPromises2.default;
		this.getPageData = _getPageData2.default;
		this.log = function () {}; // here so it can be used by plugins
		this.use = _plugins.use;
		this.unuse = _plugins.unuse;
		this.findPlugin = _plugins.findPlugin;

		// enable swup
		this.enable();
	}

	_createClass(Swup, [{
		key: 'enable',
		value: function enable() {
			var _this = this;

			// check for Promise support
			if (typeof Promise === 'undefined') {
				console.warn('Promise is not supported');
				return;
			}

			// add event listeners
			this.delegatedListeners.click = (0, _delegate2.default)(document, this.options.linkSelector, 'click', this.linkClickHandler.bind(this));
			window.addEventListener('popstate', this.popStateHandler.bind(this));

			// initial save to cache
			var page = (0, _helpers.getDataFromHtml)(document.documentElement.outerHTML, this.options.containers);
			page.url = page.responseURL = (0, _helpers.getCurrentUrl)();
			if (this.options.cache) {
				this.cache.cacheUrl(page);
			}

			// mark swup blocks in html
			(0, _helpers.markSwupElements)(document.documentElement, this.options.containers);

			// mount plugins
			this.options.plugins.forEach(function (plugin) {
				_this.use(plugin);
			});

			// modify initial history record
			window.history.replaceState(Object.assign({}, window.history.state, {
				url: window.location.href,
				random: Math.random(),
				source: 'swup'
			}), document.title, window.location.href);

			// trigger enabled event
			this.triggerEvent('enabled');

			// add swup-enabled class to html tag
			document.documentElement.classList.add('swup-enabled');

			// trigger page view event
			this.triggerEvent('pageView');
		}
	}, {
		key: 'destroy',
		value: function destroy() {
			var _this2 = this;

			// remove delegated listeners
			this.delegatedListeners.click.destroy();
			this.delegatedListeners.mouseover.destroy();

			// remove popstate listener
			window.removeEventListener('popstate', this.popStateHandler.bind(this));

			// empty cache
			this.cache.empty();

			// unmount plugins
			this.options.plugins.forEach(function (plugin) {
				_this2.unuse(plugin);
			});

			// remove swup data atributes from blocks
			(0, _utils.queryAll)('[data-swup]').forEach(function (element) {
				element.removeAttribute('data-swup');
			});

			// remove handlers
			this.off();

			// trigger disable event
			this.triggerEvent('disabled');

			// remove swup-enabled class from html tag
			document.documentElement.classList.remove('swup-enabled');
		}
	}, {
		key: 'linkClickHandler',
		value: function linkClickHandler(event) {
			// no control key pressed
			if (!event.metaKey && !event.ctrlKey && !event.shiftKey && !event.altKey) {
				// index of pressed button needs to be checked because Firefox triggers click on all mouse buttons
				if (event.button === 0) {
					this.triggerEvent('clickLink', event);
					event.preventDefault();
					var link = new _helpers.Link(event.delegateTarget);
					if (link.getAddress() == (0, _helpers.getCurrentUrl)() || link.getAddress() == '') {
						// link to the same URL
						if (link.getHash() != '') {
							// link to the same URL with hash
							this.triggerEvent('samePageWithHash', event);
							var element = document.querySelector(link.getHash());
							if (element != null) {
								history.replaceState({
									url: link.getAddress() + link.getHash(),
									random: Math.random(),
									source: 'swup'
								}, document.title, link.getAddress() + link.getHash());
							} else {
								// referenced element not found
								console.warn('Element for offset not found (' + link.getHash() + ')');
							}
						} else {
							// link to the same URL without hash
							this.triggerEvent('samePage', event);
						}
					} else {
						// link to different url
						if (link.getHash() != '') {
							this.scrollToElement = link.getHash();
						}

						// get custom transition from data
						var customTransition = event.delegateTarget.getAttribute('data-swup-transition');

						// load page
						this.loadPage({ url: link.getAddress(), customTransition: customTransition }, false);
					}
				}
			} else {
				// open in new tab (do nothing)
				this.triggerEvent('openPageInNewTab', event);
			}
		}
	}, {
		key: 'popStateHandler',
		value: function popStateHandler(event) {
			if (this.options.skipPopStateHandling(event)) return;
			var link = new _helpers.Link(event.state ? event.state.url : window.location.pathname);
			if (link.getHash() !== '') {
				this.scrollToElement = link.getHash();
			} else {
				event.preventDefault();
			}
			this.triggerEvent('popState', event);
			this.loadPage({ url: link.getAddress() }, event);
		}
	}]);

	return Swup;
}();

exports.default = Swup;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

var closest = __webpack_require__(16);

/**
 * Delegates event to a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function delegate(element, selector, type, callback, useCapture) {
    var listenerFn = listener.apply(this, arguments);

    element.addEventListener(type, listenerFn, useCapture);

    return {
        destroy: function() {
            element.removeEventListener(type, listenerFn, useCapture);
        }
    }
}

/**
 * Finds closest match and invokes callback.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @return {Function}
 */
function listener(element, selector, type, callback) {
    return function(e) {
        e.delegateTarget = closest(e.target, selector);

        if (e.delegateTarget) {
            callback.call(element, e);
        }
    }
}

module.exports = delegate;


/***/ }),
/* 16 */
/***/ (function(module, exports) {

var DOCUMENT_NODE_TYPE = 9;

/**
 * A polyfill for Element.matches()
 */
if (typeof Element !== 'undefined' && !Element.prototype.matches) {
    var proto = Element.prototype;

    proto.matches = proto.matchesSelector ||
                    proto.mozMatchesSelector ||
                    proto.msMatchesSelector ||
                    proto.oMatchesSelector ||
                    proto.webkitMatchesSelector;
}

/**
 * Finds the closest parent that matches a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @return {Function}
 */
function closest (element, selector) {
    while (element && element.nodeType !== DOCUMENT_NODE_TYPE) {
        if (typeof element.matches === 'function' &&
            element.matches(selector)) {
          return element;
        }
        element = element.parentNode;
    }
}

module.exports = closest;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Cache = exports.Cache = function () {
	function Cache() {
		_classCallCheck(this, Cache);

		this.pages = {};
		this.last = null;
	}

	_createClass(Cache, [{
		key: 'cacheUrl',
		value: function cacheUrl(page) {
			if (page.url in this.pages === false) {
				this.pages[page.url] = page;
			}
			this.last = this.pages[page.url];
			this.swup.log('Cache (' + Object.keys(this.pages).length + ')', this.pages);
		}
	}, {
		key: 'getPage',
		value: function getPage(url) {
			return this.pages[url];
		}
	}, {
		key: 'getCurrentPage',
		value: function getCurrentPage() {
			return this.getPage(window.location.pathname + window.location.search);
		}
	}, {
		key: 'exists',
		value: function exists(url) {
			return url in this.pages;
		}
	}, {
		key: 'empty',
		value: function empty() {
			this.pages = {};
			this.last = null;
			this.swup.log('Cache cleared');
		}
	}, {
		key: 'remove',
		value: function remove(url) {
			delete this.pages[url];
		}
	}]);

	return Cache;
}();

exports.default = Cache;

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _helpers = __webpack_require__(1);

var loadPage = function loadPage(data, popstate) {
	var _this = this;

	// create array for storing animation promises
	var animationPromises = [],
	    xhrPromise = void 0;
	var animateOut = function animateOut() {
		_this.triggerEvent('animationOutStart');

		// handle classes
		document.documentElement.classList.add('is-changing');
		document.documentElement.classList.add('is-leaving');
		document.documentElement.classList.add('is-animating');
		if (popstate) {
			document.documentElement.classList.add('is-popstate');
		}
		document.documentElement.classList.add('to-' + (0, _helpers.classify)(data.url));

		// animation promise stuff
		animationPromises = _this.getAnimationPromises('out');
		Promise.all(animationPromises).then(function () {
			_this.triggerEvent('animationOutDone');
		});

		// create history record if this is not a popstate call
		if (!popstate) {
			// create pop element with or without anchor
			var state = void 0;
			if (_this.scrollToElement != null) {
				state = data.url + _this.scrollToElement;
			} else {
				state = data.url;
			}

			(0, _helpers.createHistoryRecord)(state);
		}
	};

	this.triggerEvent('transitionStart', popstate);

	// set transition object
	if (data.customTransition != null) {
		this.updateTransition(window.location.pathname, data.url, data.customTransition);
		document.documentElement.classList.add('to-' + (0, _helpers.classify)(data.customTransition));
	} else {
		this.updateTransition(window.location.pathname, data.url);
	}

	// start/skip animation
	if (!popstate || this.options.animateHistoryBrowsing) {
		animateOut();
	} else {
		this.triggerEvent('animationSkipped');
	}

	// start/skip loading of page
	if (this.cache.exists(data.url)) {
		xhrPromise = new Promise(function (resolve) {
			resolve();
		});
		this.triggerEvent('pageRetrievedFromCache');
	} else {
		if (!this.preloadPromise || this.preloadPromise.route != data.url) {
			xhrPromise = new Promise(function (resolve, reject) {
				(0, _helpers.fetch)(_extends({}, data, { headers: _this.options.requestHeaders }), function (response) {
					if (response.status === 500) {
						_this.triggerEvent('serverError');
						reject(data.url);
						return;
					} else {
						// get json data
						var page = _this.getPageData(response);
						if (page != null) {
							page.url = data.url;
						} else {
							reject(data.url);
							return;
						}
						// render page
						_this.cache.cacheUrl(page);
						_this.triggerEvent('pageLoaded');
					}
					resolve();
				});
			});
		} else {
			xhrPromise = this.preloadPromise;
		}
	}

	// when everything is ready, handle the outcome
	Promise.all(animationPromises.concat([xhrPromise])).then(function () {
		// render page
		_this.renderPage(_this.cache.getPage(data.url), popstate);
		_this.preloadPromise = null;
	}).catch(function (errorUrl) {
		// rewrite the skipPopStateHandling function to redirect manually when the history.go is processed
		_this.options.skipPopStateHandling = function () {
			window.location = errorUrl;
			return true;
		};

		// go back to the actual page were still at
		window.history.go(-1);
	});
};

exports.default = loadPage;

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var classify = function classify(text) {
	var output = text.toString().toLowerCase().replace(/\s+/g, '-') // Replace spaces with -
	.replace(/\//g, '-') // Replace / with -
	.replace(/[^\w\-]+/g, '') // Remove all non-word chars
	.replace(/\-\-+/g, '-') // Replace multiple - with single -
	.replace(/^-+/, '') // Trim - from start of text
	.replace(/-+$/, ''); // Trim - from end of text
	if (output[0] === '/') output = output.splice(1);
	if (output === '') output = 'homepage';
	return output;
};

exports.default = classify;

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var createHistoryRecord = function createHistoryRecord(url) {
	window.history.pushState({
		url: url || window.location.href.split(window.location.hostname)[1],
		random: Math.random(),
		source: 'swup'
	}, document.getElementsByTagName('title')[0].innerText, url || window.location.href.split(window.location.hostname)[1]);
};

exports.default = createHistoryRecord;

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _utils = __webpack_require__(2);

var getDataFromHtml = function getDataFromHtml(html, containers) {
	var content = html.replace('<body', '<div id="swupBody"').replace('</body>', '</div>');
	var fakeDom = document.createElement('div');
	fakeDom.innerHTML = content;
	var blocks = [];

	var _loop = function _loop(i) {
		if (fakeDom.querySelector(containers[i]) == null) {
			// page in invalid
			return {
				v: null
			};
		} else {
			(0, _utils.queryAll)(containers[i]).forEach(function (item, index) {
				(0, _utils.queryAll)(containers[i], fakeDom)[index].setAttribute('data-swup', blocks.length); // marks element with data-swup
				blocks.push((0, _utils.queryAll)(containers[i], fakeDom)[index].outerHTML);
			});
		}
	};

	for (var i = 0; i < containers.length; i++) {
		var _ret = _loop(i);

		if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
	}

	var json = {
		title: fakeDom.querySelector('title').innerText,
		pageClass: fakeDom.querySelector('#swupBody').className,
		originalContent: html,
		blocks: blocks
	};

	// to prevent memory leaks
	fakeDom.innerHTML = '';
	fakeDom = null;

	return json;
};

exports.default = getDataFromHtml;

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var fetch = function fetch(setOptions) {
	var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	var defaults = {
		url: window.location.pathname + window.location.search,
		method: 'GET',
		data: null,
		headers: {}
	};

	var options = _extends({}, defaults, setOptions);

	var request = new XMLHttpRequest();

	request.onreadystatechange = function () {
		if (request.readyState === 4) {
			if (request.status !== 500) {
				callback(request);
			} else {
				callback(request);
			}
		}
	};

	request.open(options.method, options.url, true);
	Object.keys(options.headers).forEach(function (key) {
		request.setRequestHeader(key, options.headers[key]);
	});
	request.send(options.data);
	return request;
};

exports.default = fetch;

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var transitionEnd = function transitionEnd() {
	var el = document.createElement('div');

	var transEndEventNames = {
		WebkitTransition: 'webkitTransitionEnd',
		MozTransition: 'transitionend',
		OTransition: 'oTransitionEnd otransitionend',
		transition: 'transitionend'
	};

	for (var name in transEndEventNames) {
		if (el.style[name] !== undefined) {
			return transEndEventNames[name];
		}
	}

	return false;
};

exports.default = transitionEnd;

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var getCurrentUrl = function getCurrentUrl() {
	return window.location.pathname + window.location.search;
};

exports.default = getCurrentUrl;

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _utils = __webpack_require__(2);

var markSwupElements = function markSwupElements(element, containers) {
	var blocks = 0;

	var _loop = function _loop(i) {
		if (element.querySelector(containers[i]) == null) {
			console.warn('Element ' + containers[i] + ' is not in current page.');
		} else {
			(0, _utils.queryAll)(containers[i]).forEach(function (item, index) {
				(0, _utils.queryAll)(containers[i], element)[index].setAttribute('data-swup', blocks);
				blocks++;
			});
		}
	};

	for (var i = 0; i < containers.length; i++) {
		_loop(i);
	}
};

exports.default = markSwupElements;

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Link = function () {
	function Link(elementOrUrl) {
		_classCallCheck(this, Link);

		if (elementOrUrl instanceof Element || elementOrUrl instanceof SVGElement) {
			this.link = elementOrUrl;
		} else {
			this.link = document.createElement('a');
			this.link.href = elementOrUrl;
		}
	}

	_createClass(Link, [{
		key: 'getPath',
		value: function getPath() {
			var path = this.link.pathname;
			if (path[0] !== '/') {
				path = '/' + path;
			}
			return path;
		}
	}, {
		key: 'getAddress',
		value: function getAddress() {
			var path = this.link.pathname + this.link.search;

			if (this.link.getAttribute('xlink:href')) {
				path = this.link.getAttribute('xlink:href');
			}

			if (path[0] !== '/') {
				path = '/' + path;
			}
			return path;
		}
	}, {
		key: 'getHash',
		value: function getHash() {
			return this.link.hash;
		}
	}]);

	return Link;
}();

exports.default = Link;

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _utils = __webpack_require__(2);

var _helpers = __webpack_require__(1);

var renderPage = function renderPage(page, popstate) {
	var _this = this;

	document.documentElement.classList.remove('is-leaving');

	// replace state in case the url was redirected
	var link = new _helpers.Link(page.responseURL);
	if (window.location.pathname !== link.getPath()) {
		window.history.replaceState({
			url: link.getPath(),
			random: Math.random(),
			source: 'swup'
		}, document.title, link.getPath());

		// save new record for redirected url
		this.cache.cacheUrl(_extends({}, page, { url: link.getPath() }));
	}

	// only add for non-popstate transitions
	if (!popstate || this.options.animateHistoryBrowsing) {
		document.documentElement.classList.add('is-rendering');
	}

	this.triggerEvent('willReplaceContent', popstate);

	// replace blocks
	for (var i = 0; i < page.blocks.length; i++) {
		document.body.querySelector('[data-swup="' + i + '"]').outerHTML = page.blocks[i];
	}

	// set title
	document.title = page.title;

	this.triggerEvent('contentReplaced', popstate);
	this.triggerEvent('pageView', popstate);

	// empty cache if it's disabled (because pages could be preloaded and stuff)
	if (!this.options.cache) {
		this.cache.empty();
	}

	// start animation IN
	setTimeout(function () {
		if (!popstate || _this.options.animateHistoryBrowsing) {
			_this.triggerEvent('animationInStart');
			document.documentElement.classList.remove('is-animating');
		}
	}, 10);

	// handle end of animation
	var animationPromises = this.getAnimationPromises('in');
	if (!popstate || this.options.animateHistoryBrowsing) {
		Promise.all(animationPromises).then(function () {
			_this.triggerEvent('animationInDone');
			_this.triggerEvent('transitionEnd', popstate);
			// remove "to-{page}" classes
			document.documentElement.className.split(' ').forEach(function (classItem) {
				if (new RegExp('^to-').test(classItem) || classItem === 'is-changing' || classItem === 'is-rendering' || classItem === 'is-popstate') {
					document.documentElement.classList.remove(classItem);
				}
			});
		});
	} else {
		this.triggerEvent('transitionEnd', popstate);
	}

	// reset scroll-to element
	this.scrollToElement = null;
};

exports.default = renderPage;

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var triggerEvent = function triggerEvent(eventName, originalEvent) {
	// call saved handlers with "on" method and pass originalEvent object if available
	this._handlers[eventName].forEach(function (handler) {
		try {
			handler(originalEvent);
		} catch (error) {
			console.error(error);
		}
	});

	// trigger event on document with prefix "swup:"
	var event = new CustomEvent('swup:' + eventName, { detail: eventName });
	document.dispatchEvent(event);
};

exports.default = triggerEvent;

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var on = function on(event, handler) {
	if (this._handlers[event]) {
		this._handlers[event].push(handler);
	} else {
		console.warn("Unsupported event " + event + ".");
	}
};

exports.default = on;

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var off = function off(event, handler) {
	var _this = this;

	if (event != null) {
		if (handler != null) {
			if (this._handlers[event] && this._handlers[event].filter(function (savedHandler) {
				return savedHandler === handler;
			}).length) {
				var toRemove = this._handlers[event].filter(function (savedHandler) {
					return savedHandler === handler;
				})[0];
				var index = this._handlers[event].indexOf(toRemove);
				if (index > -1) {
					this._handlers[event].splice(index, 1);
				}
			} else {
				console.warn("Handler for event '" + event + "' no found.");
			}
		} else {
			this._handlers[event] = [];
		}
	} else {
		Object.keys(this._handlers).forEach(function (keys) {
			_this._handlers[keys] = [];
		});
	}
};

exports.default = off;

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var updateTransition = function updateTransition(from, to, custom) {
	// transition routes
	this.transition = {
		from: from,
		to: to,
		custom: custom
	};
};

exports.default = updateTransition;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _utils = __webpack_require__(2);

var _helpers = __webpack_require__(1);

var getAnimationPromises = function getAnimationPromises() {
	var promises = [];
	var animatedElements = (0, _utils.queryAll)(this.options.animationSelector);
	animatedElements.forEach(function (element) {
		var promise = new Promise(function (resolve) {
			element.addEventListener((0, _helpers.transitionEnd)(), function (event) {
				if (element == event.target) {
					resolve();
				}
			});
		});
		promises.push(promise);
	});
	return promises;
};

exports.default = getAnimationPromises;

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _helpers = __webpack_require__(1);

var getPageData = function getPageData(request) {
	// this method can be replaced in case other content than html is expected to be received from server
	// this function should always return {title, pageClass, originalContent, blocks, responseURL}
	// in case page has invalid structure - return null
	var html = request.responseText;
	var pageObject = (0, _helpers.getDataFromHtml)(html, this.options.containers);

	if (pageObject) {
		pageObject.responseURL = request.responseURL ? request.responseURL : window.location.href;
	} else {
		console.warn('Received page is invalid.');
		return null;
	}

	return pageObject;
};

exports.default = getPageData;

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
var use = exports.use = function use(plugin) {
	if (!plugin.isSwupPlugin) {
		console.warn('Not swup plugin instance ' + plugin + '.');
		return;
	}

	this.plugins.push(plugin);
	plugin.swup = this;
	if (typeof plugin._beforeMount === 'function') {
		plugin._beforeMount();
	}
	plugin.mount();

	return this.plugins;
};

var unuse = exports.unuse = function unuse(plugin) {
	var pluginReference = void 0;

	if (typeof plugin === 'string') {
		pluginReference = this.plugins.find(function (p) {
			return plugin === p.name;
		});
	} else {
		pluginReference = plugin;
	}

	if (!pluginReference) {
		console.warn('No such plugin.');
		return;
	}

	pluginReference.unmount();

	if (typeof pluginReference._afterUnmount === 'function') {
		pluginReference._afterUnmount();
	}

	var index = this.plugins.indexOf(pluginReference);
	this.plugins.splice(index, 1);

	return this.plugins;
};

var findPlugin = exports.findPlugin = function findPlugin(pluginName) {
	return this.plugins.find(function (p) {
		return pluginName === p.name;
	});
};

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _plugin = __webpack_require__(3);

var _plugin2 = _interopRequireDefault(_plugin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BodyClassPlugin = function (_Plugin) {
	_inherits(BodyClassPlugin, _Plugin);

	function BodyClassPlugin(options) {
		_classCallCheck(this, BodyClassPlugin);

		var _this = _possibleConstructorReturn(this, (BodyClassPlugin.__proto__ || Object.getPrototypeOf(BodyClassPlugin)).call(this));

		_this.name = 'BodyClassPlugin';


		var defaultOptions = {
			prefix: ''
		};

		_this.options = _extends({}, defaultOptions, options);
		return _this;
	}

	_createClass(BodyClassPlugin, [{
		key: 'mount',
		value: function mount() {
			var _this2 = this;

			this.swup.on('contentReplaced', function () {
				var page = _this2.swup.cache.getCurrentPage();

				// remove old classes
				document.body.className.split(' ').forEach(function (className) {
					if (_this2.isValidClassName(className)) {
						document.body.classList.remove(className);
					}
				});

				// add new classes
				if (page.pageClass !== '') {
					page.pageClass.split(' ').forEach(function (className) {
						if (_this2.isValidClassName(className)) {
							document.body.classList.add(className);
						}
					});
				}
			});
		}
	}, {
		key: 'isValidClassName',
		value: function isValidClassName(className) {
			return className !== '' && className.includes(this.options.prefix);
		}
	}]);

	return BodyClassPlugin;
}(_plugin2.default);

exports.default = BodyClassPlugin;

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _plugin = __webpack_require__(3);

var _plugin2 = _interopRequireDefault(_plugin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DebugPlugin = function (_Plugin) {
    _inherits(DebugPlugin, _Plugin);

    function DebugPlugin() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, DebugPlugin);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = DebugPlugin.__proto__ || Object.getPrototypeOf(DebugPlugin)).call.apply(_ref, [this].concat(args))), _this), _this.name = "DebugPlugin", _this.triggerEvent = function (eventName, originalEvent) {
            if (originalEvent) {
                console.groupCollapsed('%cswup:' + '%c' + eventName, 'color: #343434', 'color: #009ACD');
                console.log(originalEvent);
                console.groupEnd();
            } else {
                console.log('%cswup:' + '%c' + eventName, 'color: #343434', 'color: #009ACD');
            }

            _this.swup._triggerEvent(eventName, originalEvent);
        }, _this.log = function (str, object) {
            if (object) {
                console.groupCollapsed(str);
                for (var key in object) {
                    console.log(object[key]);
                }
                console.groupEnd();
            } else {
                console.log(str + '%c', 'color: #009ACD');
            }
        }, _this.debugLog = function (log, type) {
            if (type === 'error') {
                console.error('DEBUG PLUGIN: ' + log);
            } else {
                console.warn('DEBUG PLUGIN: ' + log);
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(DebugPlugin, [{
        key: 'mount',
        value: function mount() {
            var swup = this.swup;

            // set non-empty log method of swup
            swup.log = this.log;

            // set swup instance as a global variable swup
            window.swup = swup;

            // make events appear in console
            swup._triggerEvent = swup.triggerEvent;
            swup.triggerEvent = this.triggerEvent;

            // detect relative links not starting with / or #
            var potentiallyWrongLinksSelector = 'a[href]:not([href^="' + window.location.origin + '"]):not([href^="/"]):not([href^="http"]):not([href^="/"]):not([href^="?"]):not([href^="#"])';

            swup.on('pageView', function () {
                if (document.querySelectorAll(potentiallyWrongLinksSelector).length) {
                    var error = 'It seems there are some links with a href attribute not starting with "#", "/" or current domain, which is potentially a problem.';
                    console.warn('DEBUG PLUGIN: ' + error, document.querySelectorAll(potentiallyWrongLinksSelector));
                }
                if (document.querySelectorAll(potentiallyWrongLinksSelector).length) {
                    var _error = 'It seems there are some links with a href attribute not starting with "#", "/" or current domain, which is potentially a problem.';
                    console.warn('DEBUG PLUGIN: ' + _error, document.querySelectorAll(potentiallyWrongLinksSelector));
                }
            });
        }
    }, {
        key: 'unmount',
        value: function unmount() {
            this.swup.log = function () {};
            this.swup.triggerEvent = this.swup._triggerEvent;
        }
    }]);

    return DebugPlugin;
}(_plugin2.default);

exports.default = DebugPlugin;

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _plugin = __webpack_require__(3);

var _plugin2 = _interopRequireDefault(_plugin);

var _pathToRegexp = __webpack_require__(38);

var _pathToRegexp2 = _interopRequireDefault(_pathToRegexp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var JsPlugin = function (_Plugin) {
	_inherits(JsPlugin, _Plugin);

	function JsPlugin() {
		var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

		_classCallCheck(this, JsPlugin);

		var _this = _possibleConstructorReturn(this, (JsPlugin.__proto__ || Object.getPrototypeOf(JsPlugin)).call(this));

		_this.name = 'JsPlugin';
		_this.currentAnimation = null;

		_this.getAnimationPromises = function (type) {
			var animationIndex = _this.getAnimationIndex(type);
			return [_this.createAnimationPromise(animationIndex, type)];
		};

		_this.createAnimationPromise = function (index, type) {
			var currentTransitionRoutes = _this.swup.transition;
			var animation = _this.options[index];

			return new Promise(function (resolve) {
				animation[type](resolve, {
					paramsFrom: animation.regFrom.exec(currentTransitionRoutes.from),
					paramsTo: animation.regTo.exec(currentTransitionRoutes.to),
					transition: currentTransitionRoutes,
					from: animation.from,
					to: animation.to
				});
			});
		};

		_this.getAnimationIndex = function (type) {
			// already saved from out animation
			if (type === 'in') {
				return _this.currentAnimation;
			}

			var animations = _this.options;
			var animationIndex = 0;
			var topRating = 0;

			Object.keys(animations).forEach(function (key, index) {
				var animation = animations[key];
				var rating = _this.rateAnimation(animation);

				if (rating >= topRating) {
					animationIndex = index;
					topRating = rating;
				}
			});

			_this.currentAnimation = animationIndex;
			return _this.currentAnimation;
		};

		_this.rateAnimation = function (animation) {
			var currentTransitionRoutes = _this.swup.transition;
			var rating = 0;

			// run regex
			var fromMatched = animation.regFrom.test(currentTransitionRoutes.from);
			var toMatched = animation.regTo.test(currentTransitionRoutes.to);

			// check if regex passes
			rating += fromMatched ? 1 : 0;
			rating += toMatched ? 1 : 0;

			// beat all other if custom parameter fits
			rating += fromMatched && animation.to === currentTransitionRoutes.custom ? 2 : 0;

			return rating;
		};

		var defaultOptions = [{
			from: '(.*)',
			to: '(.*)',
			out: function out(next) {
				return next();
			},
			in: function _in(next) {
				return next();
			}
		}];

		_this.options = _extends({}, defaultOptions, options);

		_this.generateRegex();
		return _this;
	}

	_createClass(JsPlugin, [{
		key: 'mount',
		value: function mount() {
			var swup = this.swup;

			swup._getAnimationPromises = swup.getAnimationPromises;
			swup.getAnimationPromises = this.getAnimationPromises;
		}
	}, {
		key: 'unmount',
		value: function unmount() {
			swup.getAnimationPromises = swup._getAnimationPromises;
			swup._getAnimationPromises = null;
		}
	}, {
		key: 'generateRegex',
		value: function generateRegex() {
			var _this2 = this;

			var isRegex = function isRegex(str) {
				return str instanceof RegExp;
			};

			this.options = Object.keys(this.options).map(function (key) {
				return _extends({}, _this2.options[key], {
					regFrom: isRegex(_this2.options[key].from) ? _this2.options[key].from : (0, _pathToRegexp2.default)(_this2.options[key].from),
					regTo: isRegex(_this2.options[key].to) ? _this2.options[key].to : (0, _pathToRegexp2.default)(_this2.options[key].to)
				});
			});
		}
	}]);

	return JsPlugin;
}(_plugin2.default);

exports.default = JsPlugin;

/***/ }),
/* 38 */
/***/ (function(module, exports) {

/**
 * Expose `pathToRegexp`.
 */
module.exports = pathToRegexp
module.exports.parse = parse
module.exports.compile = compile
module.exports.tokensToFunction = tokensToFunction
module.exports.tokensToRegExp = tokensToRegExp

/**
 * Default configs.
 */
var DEFAULT_DELIMITER = '/'

/**
 * The main path matching regexp utility.
 *
 * @type {RegExp}
 */
var PATH_REGEXP = new RegExp([
  // Match escaped characters that would otherwise appear in future matches.
  // This allows the user to escape special characters that won't transform.
  '(\\\\.)',
  // Match Express-style parameters and un-named parameters with a prefix
  // and optional suffixes. Matches appear as:
  //
  // ":test(\\d+)?" => ["test", "\d+", undefined, "?"]
  // "(\\d+)"  => [undefined, undefined, "\d+", undefined]
  '(?:\\:(\\w+)(?:\\(((?:\\\\.|[^\\\\()])+)\\))?|\\(((?:\\\\.|[^\\\\()])+)\\))([+*?])?'
].join('|'), 'g')

/**
 * Parse a string for the raw tokens.
 *
 * @param  {string}  str
 * @param  {Object=} options
 * @return {!Array}
 */
function parse (str, options) {
  var tokens = []
  var key = 0
  var index = 0
  var path = ''
  var defaultDelimiter = (options && options.delimiter) || DEFAULT_DELIMITER
  var whitelist = (options && options.whitelist) || undefined
  var pathEscaped = false
  var res

  while ((res = PATH_REGEXP.exec(str)) !== null) {
    var m = res[0]
    var escaped = res[1]
    var offset = res.index
    path += str.slice(index, offset)
    index = offset + m.length

    // Ignore already escaped sequences.
    if (escaped) {
      path += escaped[1]
      pathEscaped = true
      continue
    }

    var prev = ''
    var name = res[2]
    var capture = res[3]
    var group = res[4]
    var modifier = res[5]

    if (!pathEscaped && path.length) {
      var k = path.length - 1
      var c = path[k]
      var matches = whitelist ? whitelist.indexOf(c) > -1 : true

      if (matches) {
        prev = c
        path = path.slice(0, k)
      }
    }

    // Push the current path onto the tokens.
    if (path) {
      tokens.push(path)
      path = ''
      pathEscaped = false
    }

    var repeat = modifier === '+' || modifier === '*'
    var optional = modifier === '?' || modifier === '*'
    var pattern = capture || group
    var delimiter = prev || defaultDelimiter

    tokens.push({
      name: name || key++,
      prefix: prev,
      delimiter: delimiter,
      optional: optional,
      repeat: repeat,
      pattern: pattern
        ? escapeGroup(pattern)
        : '[^' + escapeString(delimiter === defaultDelimiter ? delimiter : (delimiter + defaultDelimiter)) + ']+?'
    })
  }

  // Push any remaining characters.
  if (path || index < str.length) {
    tokens.push(path + str.substr(index))
  }

  return tokens
}

/**
 * Compile a string to a template function for the path.
 *
 * @param  {string}             str
 * @param  {Object=}            options
 * @return {!function(Object=, Object=)}
 */
function compile (str, options) {
  return tokensToFunction(parse(str, options))
}

/**
 * Expose a method for transforming tokens into the path function.
 */
function tokensToFunction (tokens) {
  // Compile all the tokens into regexps.
  var matches = new Array(tokens.length)

  // Compile all the patterns before compilation.
  for (var i = 0; i < tokens.length; i++) {
    if (typeof tokens[i] === 'object') {
      matches[i] = new RegExp('^(?:' + tokens[i].pattern + ')$')
    }
  }

  return function (data, options) {
    var path = ''
    var encode = (options && options.encode) || encodeURIComponent

    for (var i = 0; i < tokens.length; i++) {
      var token = tokens[i]

      if (typeof token === 'string') {
        path += token
        continue
      }

      var value = data ? data[token.name] : undefined
      var segment

      if (Array.isArray(value)) {
        if (!token.repeat) {
          throw new TypeError('Expected "' + token.name + '" to not repeat, but got array')
        }

        if (value.length === 0) {
          if (token.optional) continue

          throw new TypeError('Expected "' + token.name + '" to not be empty')
        }

        for (var j = 0; j < value.length; j++) {
          segment = encode(value[j], token)

          if (!matches[i].test(segment)) {
            throw new TypeError('Expected all "' + token.name + '" to match "' + token.pattern + '"')
          }

          path += (j === 0 ? token.prefix : token.delimiter) + segment
        }

        continue
      }

      if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
        segment = encode(String(value), token)

        if (!matches[i].test(segment)) {
          throw new TypeError('Expected "' + token.name + '" to match "' + token.pattern + '", but got "' + segment + '"')
        }

        path += token.prefix + segment
        continue
      }

      if (token.optional) continue

      throw new TypeError('Expected "' + token.name + '" to be ' + (token.repeat ? 'an array' : 'a string'))
    }

    return path
  }
}

/**
 * Escape a regular expression string.
 *
 * @param  {string} str
 * @return {string}
 */
function escapeString (str) {
  return str.replace(/([.+*?=^!:${}()[\]|/\\])/g, '\\$1')
}

/**
 * Escape the capturing group by escaping special characters and meaning.
 *
 * @param  {string} group
 * @return {string}
 */
function escapeGroup (group) {
  return group.replace(/([=!:$/()])/g, '\\$1')
}

/**
 * Get the flags for a regexp from the options.
 *
 * @param  {Object} options
 * @return {string}
 */
function flags (options) {
  return options && options.sensitive ? '' : 'i'
}

/**
 * Pull out keys from a regexp.
 *
 * @param  {!RegExp} path
 * @param  {Array=}  keys
 * @return {!RegExp}
 */
function regexpToRegexp (path, keys) {
  if (!keys) return path

  // Use a negative lookahead to match only capturing groups.
  var groups = path.source.match(/\((?!\?)/g)

  if (groups) {
    for (var i = 0; i < groups.length; i++) {
      keys.push({
        name: i,
        prefix: null,
        delimiter: null,
        optional: false,
        repeat: false,
        pattern: null
      })
    }
  }

  return path
}

/**
 * Transform an array into a regexp.
 *
 * @param  {!Array}  path
 * @param  {Array=}  keys
 * @param  {Object=} options
 * @return {!RegExp}
 */
function arrayToRegexp (path, keys, options) {
  var parts = []

  for (var i = 0; i < path.length; i++) {
    parts.push(pathToRegexp(path[i], keys, options).source)
  }

  return new RegExp('(?:' + parts.join('|') + ')', flags(options))
}

/**
 * Create a path regexp from string input.
 *
 * @param  {string}  path
 * @param  {Array=}  keys
 * @param  {Object=} options
 * @return {!RegExp}
 */
function stringToRegexp (path, keys, options) {
  return tokensToRegExp(parse(path, options), keys, options)
}

/**
 * Expose a function for taking tokens and returning a RegExp.
 *
 * @param  {!Array}  tokens
 * @param  {Array=}  keys
 * @param  {Object=} options
 * @return {!RegExp}
 */
function tokensToRegExp (tokens, keys, options) {
  options = options || {}

  var strict = options.strict
  var start = options.start !== false
  var end = options.end !== false
  var delimiter = options.delimiter || DEFAULT_DELIMITER
  var endsWith = [].concat(options.endsWith || []).map(escapeString).concat('$').join('|')
  var route = start ? '^' : ''

  // Iterate over the tokens and create our regexp string.
  for (var i = 0; i < tokens.length; i++) {
    var token = tokens[i]

    if (typeof token === 'string') {
      route += escapeString(token)
    } else {
      var capture = token.repeat
        ? '(?:' + token.pattern + ')(?:' + escapeString(token.delimiter) + '(?:' + token.pattern + '))*'
        : token.pattern

      if (keys) keys.push(token)

      if (token.optional) {
        if (!token.prefix) {
          route += '(' + capture + ')?'
        } else {
          route += '(?:' + escapeString(token.prefix) + '(' + capture + '))?'
        }
      } else {
        route += escapeString(token.prefix) + '(' + capture + ')'
      }
    }
  }

  if (end) {
    if (!strict) route += '(?:' + escapeString(delimiter) + ')?'

    route += endsWith === '$' ? '$' : '(?=' + endsWith + ')'
  } else {
    var endToken = tokens[tokens.length - 1]
    var isEndDelimited = typeof endToken === 'string'
      ? endToken[endToken.length - 1] === delimiter
      : endToken === undefined

    if (!strict) route += '(?:' + escapeString(delimiter) + '(?=' + endsWith + '))?'
    if (!isEndDelimited) route += '(?=' + escapeString(delimiter) + '|' + endsWith + ')'
  }

  return new RegExp(route, flags(options))
}

/**
 * Normalize the given path string, returning a regular expression.
 *
 * An empty array can be passed in for the keys, which will hold the
 * placeholder key descriptions. For example, using `/user/:id`, `keys` will
 * contain `[{ name: 'id', delimiter: '/', optional: false, repeat: false }]`.
 *
 * @param  {(string|RegExp|Array)} path
 * @param  {Array=}                keys
 * @param  {Object=}               options
 * @return {!RegExp}
 */
function pathToRegexp (path, keys, options) {
  if (path instanceof RegExp) {
    return regexpToRegexp(path, keys)
  }

  if (Array.isArray(path)) {
    return arrayToRegexp(/** @type {!Array} */ (path), keys, options)
  }

  return stringToRegexp(/** @type {string} */ (path), keys, options)
}


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _plugin = __webpack_require__(3);

var _plugin2 = _interopRequireDefault(_plugin);

var _delegate = __webpack_require__(40);

var _delegate2 = _interopRequireDefault(_delegate);

var _utils = __webpack_require__(2);

var _helpers = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PreloadPlugin = function (_Plugin) {
    _inherits(PreloadPlugin, _Plugin);

    function PreloadPlugin() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, PreloadPlugin);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = PreloadPlugin.__proto__ || Object.getPrototypeOf(PreloadPlugin)).call.apply(_ref, [this].concat(args))), _this), _this.name = "PreloadPlugin", _this.onContentReplaced = function () {
            _this.swup.preloadPages();
        }, _this.onMouseover = function (event) {
            var swup = _this.swup;

            swup.triggerEvent('hoverLink', event);

            var link = new _helpers.Link(event.delegateTarget);
            if (link.getAddress() !== (0, _helpers.getCurrentUrl)() && !swup.cache.exists(link.getAddress()) && swup.preloadPromise == null) {
                swup.preloadPromise = swup.preloadPage(link.getAddress());
                swup.preloadPromise.route = link.getAddress();
                swup.preloadPromise.finally(function () {
                    swup.preloadPromise = null;
                });
            }
        }, _this.preloadPage = function (pathname) {
            var swup = _this.swup;

            var link = new _helpers.Link(pathname);
            return new Promise(function (resolve, reject) {
                if (link.getAddress() != (0, _helpers.getCurrentUrl)() && !swup.cache.exists(link.getAddress())) {
                    (0, _helpers.fetch)({ url: link.getAddress(), headers: swup.options.requestHeaders }, function (response) {
                        if (response.status === 500) {
                            swup.triggerEvent('serverError');
                            reject();
                        } else {
                            // get json data
                            var page = swup.getPageData(response);
                            if (page != null) {
                                page.url = link.getAddress();
                                swup.cache.cacheUrl(page, swup.options.debugMode);
                                swup.triggerEvent('pagePreloaded');
                            } else {
                                reject(link.getAddress());
                                return;
                            }
                            resolve(swup.cache.getPage(link.getAddress()));
                        }
                    });
                } else {
                    resolve(swup.cache.getPage(link.getAddress()));
                }
            });
        }, _this.preloadPages = function () {
            (0, _utils.queryAll)('[data-swup-preload]').forEach(function (element) {
                _this.swup.preloadPage(element.href);
            });
        }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(PreloadPlugin, [{
        key: 'mount',
        value: function mount() {
            var swup = this.swup;

            swup._handlers.pagePreloaded = [];
            swup._handlers.hoverLink = [];

            swup.preloadPage = this.preloadPage;
            swup.preloadPages = this.preloadPages;

            // register mouseover handler
            swup.delegatedListeners.mouseover = (0, _delegate2.default)(document.body, swup.options.linkSelector, 'mouseover', this.onMouseover.bind(this));

            // initial preload of page form links with [data-swup-preload]
            swup.preloadPages();

            // do the same on every content replace
            swup.on('contentReplaced', this.onContentReplaced);
        }
    }, {
        key: 'unmount',
        value: function unmount() {
            var swup = this.swup;

            swup._handlers.pagePreloaded = null;
            swup._handlers.hoverLink = null;

            swup.preloadPage = null;
            swup.preloadPages = null;

            swup.delegatedListeners.mouseover.destroy();

            swup.off('contentReplaced', this.onContentReplaced);
        }
    }]);

    return PreloadPlugin;
}(_plugin2.default);

exports.default = PreloadPlugin;

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

var closest = __webpack_require__(41);

/**
 * Delegates event to a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function _delegate(element, selector, type, callback, useCapture) {
    var listenerFn = listener.apply(this, arguments);

    element.addEventListener(type, listenerFn, useCapture);

    return {
        destroy: function() {
            element.removeEventListener(type, listenerFn, useCapture);
        }
    }
}

/**
 * Delegates event to a selector.
 *
 * @param {Element|String|Array} [elements]
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function delegate(elements, selector, type, callback, useCapture) {
    // Handle the regular Element usage
    if (typeof elements.addEventListener === 'function') {
        return _delegate.apply(null, arguments);
    }

    // Handle Element-less usage, it defaults to global delegation
    if (typeof type === 'function') {
        // Use `document` as the first parameter, then apply arguments
        // This is a short way to .unshift `arguments` without running into deoptimizations
        return _delegate.bind(null, document).apply(null, arguments);
    }

    // Handle Selector-based usage
    if (typeof elements === 'string') {
        elements = document.querySelectorAll(elements);
    }

    // Handle Array-like based usage
    return Array.prototype.map.call(elements, function (element) {
        return _delegate(element, selector, type, callback, useCapture);
    });
}

/**
 * Finds closest match and invokes callback.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @return {Function}
 */
function listener(element, selector, type, callback) {
    return function(e) {
        e.delegateTarget = closest(e.target, selector);

        if (e.delegateTarget) {
            callback.call(element, e);
        }
    }
}

module.exports = delegate;


/***/ }),
/* 41 */
/***/ (function(module, exports) {

var DOCUMENT_NODE_TYPE = 9;

/**
 * A polyfill for Element.matches()
 */
if (typeof Element !== 'undefined' && !Element.prototype.matches) {
    var proto = Element.prototype;

    proto.matches = proto.matchesSelector ||
                    proto.mozMatchesSelector ||
                    proto.msMatchesSelector ||
                    proto.oMatchesSelector ||
                    proto.webkitMatchesSelector;
}

/**
 * Finds the closest parent that matches a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @return {Function}
 */
function closest (element, selector) {
    while (element && element.nodeType !== DOCUMENT_NODE_TYPE) {
        if (typeof element.matches === 'function' &&
            element.matches(selector)) {
          return element;
        }
        element = element.parentNode;
    }
}

module.exports = closest;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _plugin = __webpack_require__(3);

var _plugin2 = _interopRequireDefault(_plugin);

var _scrl = __webpack_require__(43);

var _scrl2 = _interopRequireDefault(_scrl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ScrollPlugin = function (_Plugin) {
    _inherits(ScrollPlugin, _Plugin);

    function ScrollPlugin(options) {
        _classCallCheck(this, ScrollPlugin);

        var _this = _possibleConstructorReturn(this, (ScrollPlugin.__proto__ || Object.getPrototypeOf(ScrollPlugin)).call(this));

        _this.name = "ScrollPlugin";

        _this.onSamePage = function () {
            _this.swup.scrollTo(0);
        };

        _this.onSamePageWithHash = function (event) {
            var link = event.delegateTarget;
            var element = document.querySelector(link.hash);
            var top = element.getBoundingClientRect().top + window.pageYOffset;
            _this.swup.scrollTo(top);
        };

        _this.onTransitionStart = function (popstate) {
            if (_this.options.doScrollingRightAway && !_this.swup.scrollToElement) {
                _this.doScrolling(popstate);
            }
        };

        _this.onContentReplaced = function (popstate) {
            if (!_this.options.doScrollingRightAway || _this.swup.scrollToElement) {
                _this.doScrolling(popstate);
            }
        };

        _this.doScrolling = function (popstate) {
            var swup = _this.swup;

            if (!popstate || swup.options.animateHistoryBrowsing) {
                if (swup.scrollToElement != null) {
                    var element = document.querySelector(swup.scrollToElement);
                    if (element != null) {
                        var top = element.getBoundingClientRect().top + window.pageYOffset;
                        swup.scrollTo(top);
                    } else {
                        console.warn('Element ' + swup.scrollToElement + ' not found');
                    }
                    swup.scrollToElement = null;
                } else {
                    swup.scrollTo(0);
                }
            }
        };

        var defaultOptions = {
            doScrollingRightAway: false,
            animateScroll: true,
            scrollFriction: 0.3,
            scrollAcceleration: 0.04
        };

        _this.options = _extends({}, defaultOptions, options);
        return _this;
    }

    _createClass(ScrollPlugin, [{
        key: 'mount',
        value: function mount() {
            var _this2 = this;

            var swup = this.swup;

            // add empty handlers array for submitForm event
            swup._handlers.scrollDone = [];
            swup._handlers.scrollStart = [];

            this.scrl = new _scrl2.default({
                onStart: function onStart() {
                    return swup.triggerEvent('scrollStart');
                },
                onEnd: function onEnd() {
                    return swup.triggerEvent('scrollDone');
                },
                onCancel: function onCancel() {
                    return swup.triggerEvent('scrollDone');
                },
                friction: this.options.scrollFriction,
                acceleration: this.options.scrollAcceleration
            });

            // set scrollTo method of swup and animate based on current animateScroll option
            swup.scrollTo = function (offset) {
                if (_this2.options.animateScroll) {
                    _this2.scrl.scrollTo(offset);
                } else {
                    swup.triggerEvent('scrollStart');
                    window.scrollTo(0, offset);
                    swup.triggerEvent('scrollDone');
                }
            };

            // disable browser scroll control on popstates when
            // animateHistoryBrowsing option is enabled in swup
            if (swup.options.animateHistoryBrowsing) {
                window.history.scrollRestoration = 'manual';
            }

            // scroll to the top of the page
            swup.on('samePage', this.onSamePage);

            // scroll to referenced element on the same page
            swup.on('samePageWithHash', this.onSamePageWithHash);

            // scroll to the referenced element
            swup.on('transitionStart', this.onTransitionStart);

            // scroll to the referenced element when it's in the page (after render)
            swup.on('contentReplaced', this.onContentReplaced);
        }
    }, {
        key: 'unmount',
        value: function unmount() {
            this.swup.scrollTo = null;

            delete this.scrl;
            this.scrl = null;

            this.swup.off('samePage', this.onSamePage);
            this.swup.off('samePageWithHash', this.onSamePageWithHash);
            this.swup.off('transitionStart', this.onTransitionStart);
            this.swup.off('contentReplaced', this.onContentReplaced);

            this.swup._handlers.scrollDone = null;
            this.swup._handlers.scrollStart = null;

            window.history.scrollRestoration = 'auto';
        }
    }]);

    return ScrollPlugin;
}(_plugin2.default);

exports.default = ScrollPlugin;

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Scrl = function Scrl(options) {
    var _this = this;

    _classCallCheck(this, Scrl);

    this._raf = null;
    this._positionY = 0;
    this._velocityY = 0;
    this._targetPositionY = 0;
    this._targetPositionYWithOffset = 0;
    this._direction = 0;

    this.scrollTo = function (offset) {
        if (offset && offset.nodeType) {
            // the offset is element
            _this._targetPositionY = Math.round(offset.getBoundingClientRect().top + window.pageYOffset);
        } else if (parseInt(_this._targetPositionY) === _this._targetPositionY) {
            // the offset is a number
            _this._targetPositionY = Math.round(offset);
        } else {
            console.error('Argument must be a number or an element.');
            return;
        }

        // don't animate beyond the document height
        if (_this._targetPositionY > document.documentElement.scrollHeight - window.innerHeight) {
            _this._targetPositionY = document.documentElement.scrollHeight - window.innerHeight;
        }

        // calculated required values
        _this._positionY = document.body.scrollTop || document.documentElement.scrollTop;
        _this._direction = _this._positionY > _this._targetPositionY ? -1 : 1;
        _this._targetPositionYWithOffset = _this._targetPositionY + _this._direction;
        _this._velocityY = 0;

        if (_this._positionY !== _this._targetPositionY) {
            // start animation
            _this.options.onStart();
            _this._animate();
        } else {
            // page is already at the position
            _this.options.onAlreadyAtPositions();
        }
    };

    this._animate = function () {
        var distance = _this._update();
        _this._render();

        if (_this._direction === 1 && _this._targetPositionY > _this._positionY || _this._direction === -1 && _this._targetPositionY < _this._positionY) {
            // calculate next position
            _this._raf = requestAnimationFrame(_this._animate);
            _this.options.onTick();
        } else {
            // finish and set position to the final position
            _this._positionY = _this._targetPositionY;
            _this._render();
            _this._raf = null;
            _this.options.onTick();
            _this.options.onEnd();
            // this.triggerEvent('scrollDone')
        }
    };

    this._update = function () {
        var distance = _this._targetPositionYWithOffset - _this._positionY;
        var attraction = distance * _this.options.acceleration;

        _this._velocityY += attraction;

        _this._velocityY *= _this.options.friction;
        _this._positionY += _this._velocityY;

        return Math.abs(distance);
    };

    this._render = function () {
        window.scrollTo(0, _this._positionY);
    };

    // default options
    var defaults = {
        onAlreadyAtPositions: function onAlreadyAtPositions() {},
        onCancel: function onCancel() {},
        onEnd: function onEnd() {},
        onStart: function onStart() {},
        onTick: function onTick() {},
        friction: .7, // 1 - .3
        acceleration: .04

        // merge options
    };this.options = _extends({}, defaults, options);

    // set reverse friction
    if (options && options.friction) {
        this.options.friction = 1 - options.friction;
    }

    // register listener for cancel on wheel event
    window.addEventListener('mousewheel', function (event) {
        if (_this._raf) {
            _this.options.onCancel();
            cancelAnimationFrame(_this._raf);
            _this._raf = null;
        }
    }, {
        passive: true
    });
};

exports.default = Scrl;

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// console.log("Scrollmodule-loaded");


function scrollIt(destination) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 200;
  var easing = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'linear';
  var callback = arguments[3];


  var easings = {
    linear: function linear(t) {
      return t;
    },
    easeInQuad: function easeInQuad(t) {
      return t * t;
    },
    easeOutQuad: function easeOutQuad(t) {
      return t * (2 - t);
    },
    easeInOutQuad: function easeInOutQuad(t) {
      return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
    },
    easeInCubic: function easeInCubic(t) {
      return t * t * t;
    },
    easeOutCubic: function easeOutCubic(t) {
      return --t * t * t + 1;
    },
    easeInOutCubic: function easeInOutCubic(t) {
      return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
    },
    easeInQuart: function easeInQuart(t) {
      return t * t * t * t;
    },
    easeOutQuart: function easeOutQuart(t) {
      return 1 - --t * t * t * t;
    },
    easeInOutQuart: function easeInOutQuart(t) {
      return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t;
    },
    easeInQuint: function easeInQuint(t) {
      return t * t * t * t * t;
    },
    easeOutQuint: function easeOutQuint(t) {
      return 1 + --t * t * t * t * t;
    },
    easeInOutQuint: function easeInOutQuint(t) {
      return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * --t * t * t * t * t;
    }
  };

  var start = window.pageYOffset;
  var startTime = 'now' in window.performance ? performance.now() : new Date().getTime();

  var documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
  var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
  var destinationOffset = typeof destination === 'number' ? destination : destination.offsetTop;
  var destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset);

  if ('requestAnimationFrame' in window === false) {
    window.scroll(0, destinationOffsetToScroll);
    if (callback) {
      callback();
    }
    return;
  }

  function scroll() {
    var now = 'now' in window.performance ? performance.now() : new Date().getTime();
    var time = Math.min(1, (now - startTime) / duration);
    var timeFunction = easings[easing](time);
    window.scroll(0, Math.ceil(timeFunction * (destinationOffsetToScroll - start) + start));

    if (window.pageYOffset === destinationOffsetToScroll) {
      if (callback) {
        callback();
      }
      return;
    }

    requestAnimationFrame(scroll);
  }

  scroll();
}

window.scrollIt = scrollIt;

/***/ }),
/* 45 */
/***/ (function(module, exports) {

(function(window, factory) {
	var lazySizes = factory(window, window.document);
	window.lazySizes = lazySizes;
	if(typeof module == 'object' && module.exports){
		module.exports = lazySizes;
	}
}(window, function l(window, document) {
	'use strict';
	/*jshint eqnull:true */
	if(!document.getElementsByClassName){return;}

	var lazysizes, lazySizesCfg;

	var docElem = document.documentElement;

	var Date = window.Date;

	var supportPicture = window.HTMLPictureElement;

	var _addEventListener = 'addEventListener';

	var _getAttribute = 'getAttribute';

	var addEventListener = window[_addEventListener];

	var setTimeout = window.setTimeout;

	var requestAnimationFrame = window.requestAnimationFrame || setTimeout;

	var requestIdleCallback = window.requestIdleCallback;

	var regPicture = /^picture$/i;

	var loadEvents = ['load', 'error', 'lazyincluded', '_lazyloaded'];

	var regClassCache = {};

	var forEach = Array.prototype.forEach;

	var hasClass = function(ele, cls) {
		if(!regClassCache[cls]){
			regClassCache[cls] = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		}
		return regClassCache[cls].test(ele[_getAttribute]('class') || '') && regClassCache[cls];
	};

	var addClass = function(ele, cls) {
		if (!hasClass(ele, cls)){
			ele.setAttribute('class', (ele[_getAttribute]('class') || '').trim() + ' ' + cls);
		}
	};

	var removeClass = function(ele, cls) {
		var reg;
		if ((reg = hasClass(ele,cls))) {
			ele.setAttribute('class', (ele[_getAttribute]('class') || '').replace(reg, ' '));
		}
	};

	var addRemoveLoadEvents = function(dom, fn, add){
		var action = add ? _addEventListener : 'removeEventListener';
		if(add){
			addRemoveLoadEvents(dom, fn);
		}
		loadEvents.forEach(function(evt){
			dom[action](evt, fn);
		});
	};

	var triggerEvent = function(elem, name, detail, noBubbles, noCancelable){
		var event = document.createEvent('Event');

		if(!detail){
			detail = {};
		}

		detail.instance = lazysizes;

		event.initEvent(name, !noBubbles, !noCancelable);

		event.detail = detail;

		elem.dispatchEvent(event);
		return event;
	};

	var updatePolyfill = function (el, full){
		var polyfill;
		if( !supportPicture && ( polyfill = (window.picturefill || lazySizesCfg.pf) ) ){
			if(full && full.src && !el[_getAttribute]('srcset')){
				el.setAttribute('srcset', full.src);
			}
			polyfill({reevaluate: true, elements: [el]});
		} else if(full && full.src){
			el.src = full.src;
		}
	};

	var getCSS = function (elem, style){
		return (getComputedStyle(elem, null) || {})[style];
	};

	var getWidth = function(elem, parent, width){
		width = width || elem.offsetWidth;

		while(width < lazySizesCfg.minSize && parent && !elem._lazysizesWidth){
			width =  parent.offsetWidth;
			parent = parent.parentNode;
		}

		return width;
	};

	var rAF = (function(){
		var running, waiting;
		var firstFns = [];
		var secondFns = [];
		var fns = firstFns;

		var run = function(){
			var runFns = fns;

			fns = firstFns.length ? secondFns : firstFns;

			running = true;
			waiting = false;

			while(runFns.length){
				runFns.shift()();
			}

			running = false;
		};

		var rafBatch = function(fn, queue){
			if(running && !queue){
				fn.apply(this, arguments);
			} else {
				fns.push(fn);

				if(!waiting){
					waiting = true;
					(document.hidden ? setTimeout : requestAnimationFrame)(run);
				}
			}
		};

		rafBatch._lsFlush = run;

		return rafBatch;
	})();

	var rAFIt = function(fn, simple){
		return simple ?
			function() {
				rAF(fn);
			} :
			function(){
				var that = this;
				var args = arguments;
				rAF(function(){
					fn.apply(that, args);
				});
			}
		;
	};

	var throttle = function(fn){
		var running;
		var lastTime = 0;
		var gDelay = lazySizesCfg.throttleDelay;
		var rICTimeout = lazySizesCfg.ricTimeout;
		var run = function(){
			running = false;
			lastTime = Date.now();
			fn();
		};
		var idleCallback = requestIdleCallback && rICTimeout > 49 ?
			function(){
				requestIdleCallback(run, {timeout: rICTimeout});

				if(rICTimeout !== lazySizesCfg.ricTimeout){
					rICTimeout = lazySizesCfg.ricTimeout;
				}
			} :
			rAFIt(function(){
				setTimeout(run);
			}, true)
		;

		return function(isPriority){
			var delay;

			if((isPriority = isPriority === true)){
				rICTimeout = 33;
			}

			if(running){
				return;
			}

			running =  true;

			delay = gDelay - (Date.now() - lastTime);

			if(delay < 0){
				delay = 0;
			}

			if(isPriority || delay < 9){
				idleCallback();
			} else {
				setTimeout(idleCallback, delay);
			}
		};
	};

	//based on http://modernjavascript.blogspot.de/2013/08/building-better-debounce.html
	var debounce = function(func) {
		var timeout, timestamp;
		var wait = 99;
		var run = function(){
			timeout = null;
			func();
		};
		var later = function() {
			var last = Date.now() - timestamp;

			if (last < wait) {
				setTimeout(later, wait - last);
			} else {
				(requestIdleCallback || run)(run);
			}
		};

		return function() {
			timestamp = Date.now();

			if (!timeout) {
				timeout = setTimeout(later, wait);
			}
		};
	};

	(function(){
		var prop;

		var lazySizesDefaults = {
			lazyClass: 'lazyload',
			loadedClass: 'lazyloaded',
			loadingClass: 'lazyloading',
			preloadClass: 'lazypreload',
			errorClass: 'lazyerror',
			//strictClass: 'lazystrict',
			autosizesClass: 'lazyautosizes',
			srcAttr: 'data-src',
			srcsetAttr: 'data-srcset',
			sizesAttr: 'data-sizes',
			//preloadAfterLoad: false,
			minSize: 40,
			customMedia: {},
			init: true,
			expFactor: 1.5,
			hFac: 0.8,
			loadMode: 2,
			loadHidden: true,
			ricTimeout: 0,
			throttleDelay: 125,
		};

		lazySizesCfg = window.lazySizesConfig || window.lazysizesConfig || {};

		for(prop in lazySizesDefaults){
			if(!(prop in lazySizesCfg)){
				lazySizesCfg[prop] = lazySizesDefaults[prop];
			}
		}

		setTimeout(function(){
			if(lazySizesCfg.init){
				init();
			}
		});
	})();

	var loader = (function(){
		var preloadElems, isCompleted, resetPreloadingTimer, loadMode, started;

		var eLvW, elvH, eLtop, eLleft, eLright, eLbottom, isBodyHidden;

		var regImg = /^img$/i;
		var regIframe = /^iframe$/i;

		var supportScroll = ('onscroll' in window) && !(/(gle|ing)bot/.test(navigator.userAgent));

		var shrinkExpand = 0;
		var currentExpand = 0;

		var isLoading = 0;
		var lowRuns = -1;

		var resetPreloading = function(e){
			isLoading--;
			if(!e || isLoading < 0 || !e.target){
				isLoading = 0;
			}
		};

		var isVisible = function (elem) {
			if (isBodyHidden == null) {
				isBodyHidden = getCSS(document.body, 'visibility') == 'hidden';
			}

			return isBodyHidden || (getCSS(elem.parentNode, 'visibility') != 'hidden' && getCSS(elem, 'visibility') != 'hidden');
		};

		var isNestedVisible = function(elem, elemExpand){
			var outerRect;
			var parent = elem;
			var visible = isVisible(elem);

			eLtop -= elemExpand;
			eLbottom += elemExpand;
			eLleft -= elemExpand;
			eLright += elemExpand;

			while(visible && (parent = parent.offsetParent) && parent != document.body && parent != docElem){
				visible = ((getCSS(parent, 'opacity') || 1) > 0);

				if(visible && getCSS(parent, 'overflow') != 'visible'){
					outerRect = parent.getBoundingClientRect();
					visible = eLright > outerRect.left &&
						eLleft < outerRect.right &&
						eLbottom > outerRect.top - 1 &&
						eLtop < outerRect.bottom + 1
					;
				}
			}

			return visible;
		};

		var checkElements = function() {
			var eLlen, i, rect, autoLoadElem, loadedSomething, elemExpand, elemNegativeExpand, elemExpandVal,
				beforeExpandVal, defaultExpand, preloadExpand, hFac;
			var lazyloadElems = lazysizes.elements;

			if((loadMode = lazySizesCfg.loadMode) && isLoading < 8 && (eLlen = lazyloadElems.length)){

				i = 0;

				lowRuns++;

				for(; i < eLlen; i++){

					if(!lazyloadElems[i] || lazyloadElems[i]._lazyRace){continue;}

					if(!supportScroll || (lazysizes.prematureUnveil && lazysizes.prematureUnveil(lazyloadElems[i]))){unveilElement(lazyloadElems[i]);continue;}

					if(!(elemExpandVal = lazyloadElems[i][_getAttribute]('data-expand')) || !(elemExpand = elemExpandVal * 1)){
						elemExpand = currentExpand;
					}

					if (!defaultExpand) {
						defaultExpand = (!lazySizesCfg.expand || lazySizesCfg.expand < 1) ?
							docElem.clientHeight > 500 && docElem.clientWidth > 500 ? 500 : 370 :
							lazySizesCfg.expand;

						lazysizes._defEx = defaultExpand;

						preloadExpand = defaultExpand * lazySizesCfg.expFactor;
						hFac = lazySizesCfg.hFac;
						isBodyHidden = null;

						if(currentExpand < preloadExpand && isLoading < 1 && lowRuns > 2 && loadMode > 2 && !document.hidden){
							currentExpand = preloadExpand;
							lowRuns = 0;
						} else if(loadMode > 1 && lowRuns > 1 && isLoading < 6){
							currentExpand = defaultExpand;
						} else {
							currentExpand = shrinkExpand;
						}
					}

					if(beforeExpandVal !== elemExpand){
						eLvW = innerWidth + (elemExpand * hFac);
						elvH = innerHeight + elemExpand;
						elemNegativeExpand = elemExpand * -1;
						beforeExpandVal = elemExpand;
					}

					rect = lazyloadElems[i].getBoundingClientRect();

					if ((eLbottom = rect.bottom) >= elemNegativeExpand &&
						(eLtop = rect.top) <= elvH &&
						(eLright = rect.right) >= elemNegativeExpand * hFac &&
						(eLleft = rect.left) <= eLvW &&
						(eLbottom || eLright || eLleft || eLtop) &&
						(lazySizesCfg.loadHidden || isVisible(lazyloadElems[i])) &&
						((isCompleted && isLoading < 3 && !elemExpandVal && (loadMode < 3 || lowRuns < 4)) || isNestedVisible(lazyloadElems[i], elemExpand))){
						unveilElement(lazyloadElems[i]);
						loadedSomething = true;
						if(isLoading > 9){break;}
					} else if(!loadedSomething && isCompleted && !autoLoadElem &&
						isLoading < 4 && lowRuns < 4 && loadMode > 2 &&
						(preloadElems[0] || lazySizesCfg.preloadAfterLoad) &&
						(preloadElems[0] || (!elemExpandVal && ((eLbottom || eLright || eLleft || eLtop) || lazyloadElems[i][_getAttribute](lazySizesCfg.sizesAttr) != 'auto')))){
						autoLoadElem = preloadElems[0] || lazyloadElems[i];
					}
				}

				if(autoLoadElem && !loadedSomething){
					unveilElement(autoLoadElem);
				}
			}
		};

		var throttledCheckElements = throttle(checkElements);

		var switchLoadingClass = function(e){
			var elem = e.target;

			if (elem._lazyCache) {
				delete elem._lazyCache;
				return;
			}

			resetPreloading(e);
			addClass(elem, lazySizesCfg.loadedClass);
			removeClass(elem, lazySizesCfg.loadingClass);
			addRemoveLoadEvents(elem, rafSwitchLoadingClass);
			triggerEvent(elem, 'lazyloaded');
		};
		var rafedSwitchLoadingClass = rAFIt(switchLoadingClass);
		var rafSwitchLoadingClass = function(e){
			rafedSwitchLoadingClass({target: e.target});
		};

		var changeIframeSrc = function(elem, src){
			try {
				elem.contentWindow.location.replace(src);
			} catch(e){
				elem.src = src;
			}
		};

		var handleSources = function(source){
			var customMedia;

			var sourceSrcset = source[_getAttribute](lazySizesCfg.srcsetAttr);

			if( (customMedia = lazySizesCfg.customMedia[source[_getAttribute]('data-media') || source[_getAttribute]('media')]) ){
				source.setAttribute('media', customMedia);
			}

			if(sourceSrcset){
				source.setAttribute('srcset', sourceSrcset);
			}
		};

		var lazyUnveil = rAFIt(function (elem, detail, isAuto, sizes, isImg){
			var src, srcset, parent, isPicture, event, firesLoad;

			if(!(event = triggerEvent(elem, 'lazybeforeunveil', detail)).defaultPrevented){

				if(sizes){
					if(isAuto){
						addClass(elem, lazySizesCfg.autosizesClass);
					} else {
						elem.setAttribute('sizes', sizes);
					}
				}

				srcset = elem[_getAttribute](lazySizesCfg.srcsetAttr);
				src = elem[_getAttribute](lazySizesCfg.srcAttr);

				if(isImg) {
					parent = elem.parentNode;
					isPicture = parent && regPicture.test(parent.nodeName || '');
				}

				firesLoad = detail.firesLoad || (('src' in elem) && (srcset || src || isPicture));

				event = {target: elem};

				addClass(elem, lazySizesCfg.loadingClass);

				if(firesLoad){
					clearTimeout(resetPreloadingTimer);
					resetPreloadingTimer = setTimeout(resetPreloading, 2500);
					addRemoveLoadEvents(elem, rafSwitchLoadingClass, true);
				}

				if(isPicture){
					forEach.call(parent.getElementsByTagName('source'), handleSources);
				}

				if(srcset){
					elem.setAttribute('srcset', srcset);
				} else if(src && !isPicture){
					if(regIframe.test(elem.nodeName)){
						changeIframeSrc(elem, src);
					} else {
						elem.src = src;
					}
				}

				if(isImg && (srcset || isPicture)){
					updatePolyfill(elem, {src: src});
				}
			}

			if(elem._lazyRace){
				delete elem._lazyRace;
			}
			removeClass(elem, lazySizesCfg.lazyClass);

			rAF(function(){
				// Part of this can be removed as soon as this fix is older: https://bugs.chromium.org/p/chromium/issues/detail?id=7731 (2015)
				var isLoaded = elem.complete && elem.naturalWidth > 1;

				if( !firesLoad || isLoaded){
					if (isLoaded) {
						addClass(elem, 'ls-is-cached');
					}
					switchLoadingClass(event);
					elem._lazyCache = true;
					setTimeout(function(){
						if ('_lazyCache' in elem) {
							delete elem._lazyCache;
						}
					}, 9);
				}
				if (elem.loading == 'lazy') {
					isLoading--;
				}
			}, true);
		});

		var unveilElement = function (elem){
			if (elem._lazyRace) {return;}
			var detail;

			var isImg = regImg.test(elem.nodeName);

			//allow using sizes="auto", but don't use. it's invalid. Use data-sizes="auto" or a valid value for sizes instead (i.e.: sizes="80vw")
			var sizes = isImg && (elem[_getAttribute](lazySizesCfg.sizesAttr) || elem[_getAttribute]('sizes'));
			var isAuto = sizes == 'auto';

			if( (isAuto || !isCompleted) && isImg && (elem[_getAttribute]('src') || elem.srcset) && !elem.complete && !hasClass(elem, lazySizesCfg.errorClass) && hasClass(elem, lazySizesCfg.lazyClass)){return;}

			detail = triggerEvent(elem, 'lazyunveilread').detail;

			if(isAuto){
				 autoSizer.updateElem(elem, true, elem.offsetWidth);
			}

			elem._lazyRace = true;
			isLoading++;

			lazyUnveil(elem, detail, isAuto, sizes, isImg);
		};

		var afterScroll = debounce(function(){
			lazySizesCfg.loadMode = 3;
			throttledCheckElements();
		});

		var altLoadmodeScrollListner = function(){
			if(lazySizesCfg.loadMode == 3){
				lazySizesCfg.loadMode = 2;
			}
			afterScroll();
		};

		var onload = function(){
			if(isCompleted){return;}
			if(Date.now() - started < 999){
				setTimeout(onload, 999);
				return;
			}


			isCompleted = true;

			lazySizesCfg.loadMode = 3;

			throttledCheckElements();

			addEventListener('scroll', altLoadmodeScrollListner, true);
		};

		return {
			_: function(){
				started = Date.now();

				lazysizes.elements = document.getElementsByClassName(lazySizesCfg.lazyClass);
				preloadElems = document.getElementsByClassName(lazySizesCfg.lazyClass + ' ' + lazySizesCfg.preloadClass);

				addEventListener('scroll', throttledCheckElements, true);

				addEventListener('resize', throttledCheckElements, true);

				if(window.MutationObserver){
					new MutationObserver( throttledCheckElements ).observe( docElem, {childList: true, subtree: true, attributes: true} );
				} else {
					docElem[_addEventListener]('DOMNodeInserted', throttledCheckElements, true);
					docElem[_addEventListener]('DOMAttrModified', throttledCheckElements, true);
					setInterval(throttledCheckElements, 999);
				}

				addEventListener('hashchange', throttledCheckElements, true);

				//, 'fullscreenchange'
				['focus', 'mouseover', 'click', 'load', 'transitionend', 'animationend'].forEach(function(name){
					document[_addEventListener](name, throttledCheckElements, true);
				});

				if((/d$|^c/.test(document.readyState))){
					onload();
				} else {
					addEventListener('load', onload);
					document[_addEventListener]('DOMContentLoaded', throttledCheckElements);
					setTimeout(onload, 20000);
				}

				if(lazysizes.elements.length){
					checkElements();
					rAF._lsFlush();
				} else {
					throttledCheckElements();
				}
			},
			checkElems: throttledCheckElements,
			unveil: unveilElement,
			_aLSL: altLoadmodeScrollListner,
		};
	})();


	var autoSizer = (function(){
		var autosizesElems;

		var sizeElement = rAFIt(function(elem, parent, event, width){
			var sources, i, len;
			elem._lazysizesWidth = width;
			width += 'px';

			elem.setAttribute('sizes', width);

			if(regPicture.test(parent.nodeName || '')){
				sources = parent.getElementsByTagName('source');
				for(i = 0, len = sources.length; i < len; i++){
					sources[i].setAttribute('sizes', width);
				}
			}

			if(!event.detail.dataAttr){
				updatePolyfill(elem, event.detail);
			}
		});
		var getSizeElement = function (elem, dataAttr, width){
			var event;
			var parent = elem.parentNode;

			if(parent){
				width = getWidth(elem, parent, width);
				event = triggerEvent(elem, 'lazybeforesizes', {width: width, dataAttr: !!dataAttr});

				if(!event.defaultPrevented){
					width = event.detail.width;

					if(width && width !== elem._lazysizesWidth){
						sizeElement(elem, parent, event, width);
					}
				}
			}
		};

		var updateElementsSizes = function(){
			var i;
			var len = autosizesElems.length;
			if(len){
				i = 0;

				for(; i < len; i++){
					getSizeElement(autosizesElems[i]);
				}
			}
		};

		var debouncedUpdateElementsSizes = debounce(updateElementsSizes);

		return {
			_: function(){
				autosizesElems = document.getElementsByClassName(lazySizesCfg.autosizesClass);
				addEventListener('resize', debouncedUpdateElementsSizes);
			},
			checkElems: debouncedUpdateElementsSizes,
			updateElem: getSizeElement
		};
	})();

	var init = function(){
		if(!init.i){
			init.i = true;
			autoSizer._();
			loader._();
		}
	};

	lazysizes = {
		cfg: lazySizesCfg,
		autoSizer: autoSizer,
		loader: loader,
		init: init,
		uP: updatePolyfill,
		aC: addClass,
		rC: removeClass,
		hC: hasClass,
		fire: triggerEvent,
		gW: getWidth,
		rAF: rAF,
	};

	return lazysizes;
}
));


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(47);

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);
var bind = __webpack_require__(4);
var Axios = __webpack_require__(48);
var mergeConfig = __webpack_require__(10);
var defaults = __webpack_require__(7);

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(mergeConfig(axios.defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__(11);
axios.CancelToken = __webpack_require__(62);
axios.isCancel = __webpack_require__(6);

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__(63);

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);
var buildURL = __webpack_require__(5);
var InterceptorManager = __webpack_require__(49);
var dispatchRequest = __webpack_require__(50);
var mergeConfig = __webpack_require__(10);

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = arguments[1] || {};
    config.url = arguments[0];
  } else {
    config = config || {};
  }

  config = mergeConfig(this.defaults, config);

  // Set config.method
  if (config.method) {
    config.method = config.method.toLowerCase();
  } else if (this.defaults.method) {
    config.method = this.defaults.method.toLowerCase();
  } else {
    config.method = 'get';
  }

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

Axios.prototype.getUri = function getUri(config) {
  config = mergeConfig(this.defaults, config);
  return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);
var transformData = __webpack_require__(51);
var isCancel = __webpack_require__(6);
var defaults = __webpack_require__(7);

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};


/***/ }),
/* 52 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__(9);

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  if (!validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }

  error.request = request;
  error.response = response;
  error.isAxiosError = true;

  error.toJSON = function() {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: this.config,
      code: this.code
    };
  };
  return error;
};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isAbsoluteURL = __webpack_require__(57);
var combineURLs = __webpack_require__(58);

/**
 * Creates a new URL by combining the baseURL with the requestedURL,
 * only when the requestedURL is not already an absolute URL.
 * If the requestURL is absolute, this function returns the requestedURL untouched.
 *
 * @param {string} baseURL The base URL
 * @param {string} requestedURL Absolute or relative URL to combine
 * @returns {string} The combined full path
 */
module.exports = function buildFullPath(baseURL, requestedURL) {
  if (baseURL && !isAbsoluteURL(requestedURL)) {
    return combineURLs(baseURL, requestedURL);
  }
  return requestedURL;
};


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
    (function standardBrowserEnv() {
      var msie = /(msie|trident)/i.test(navigator.userAgent);
      var urlParsingNode = document.createElement('a');
      var originURL;

      /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
      function resolveURL(url) {
        var href = url;

        if (msie) {
        // IE needs attribute set twice to normalize properties
          urlParsingNode.setAttribute('href', href);
          href = urlParsingNode.href;
        }

        urlParsingNode.setAttribute('href', href);

        // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
        return {
          href: urlParsingNode.href,
          protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
          host: urlParsingNode.host,
          search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
          hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
          hostname: urlParsingNode.hostname,
          port: urlParsingNode.port,
          pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
            urlParsingNode.pathname :
            '/' + urlParsingNode.pathname
        };
      }

      originURL = resolveURL(window.location.href);

      /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
      return function isURLSameOrigin(requestURL) {
        var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
        return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
      };
    })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return function isURLSameOrigin() {
        return true;
      };
    })()
);


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(0);

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
    (function standardBrowserEnv() {
      return {
        write: function write(name, value, expires, path, domain, secure) {
          var cookie = [];
          cookie.push(name + '=' + encodeURIComponent(value));

          if (utils.isNumber(expires)) {
            cookie.push('expires=' + new Date(expires).toGMTString());
          }

          if (utils.isString(path)) {
            cookie.push('path=' + path);
          }

          if (utils.isString(domain)) {
            cookie.push('domain=' + domain);
          }

          if (secure === true) {
            cookie.push('secure');
          }

          document.cookie = cookie.join('; ');
        },

        read: function read(name) {
          var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
          return (match ? decodeURIComponent(match[3]) : null);
        },

        remove: function remove(name) {
          this.write(name, '', Date.now() - 86400000);
        }
      };
    })() :

  // Non standard browser env (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return {
        write: function write() {},
        read: function read() { return null; },
        remove: function remove() {}
      };
    })()
);


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__(11);

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgMzgxOGY2ZTk2NDcyZjFlZTNjYTQiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi91dGlscy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvaGVscGVycy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvdXRpbHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0Bzd3VwL3BsdWdpbi9saWIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2JpbmQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2J1aWxkVVJMLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY2FuY2VsL2lzQ2FuY2VsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvZGVmYXVsdHMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9hZGFwdGVycy94aHIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jb3JlL2NyZWF0ZUVycm9yLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9tZXJnZUNvbmZpZy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2NhbmNlbC9DYW5jZWwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9qcy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9kZWxlZ2F0ZS9zcmMvZGVsZWdhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2RlbGVnYXRlL3NyYy9jbG9zZXN0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9tb2R1bGVzL0NhY2hlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9tb2R1bGVzL2xvYWRQYWdlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL2NsYXNzaWZ5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL2NyZWF0ZUhpc3RvcnlSZWNvcmQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL2hlbHBlcnMvZ2V0RGF0YUZyb21IdG1sLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL2ZldGNoLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL3RyYW5zaXRpb25FbmQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL2hlbHBlcnMvZ2V0Q3VycmVudFVybC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvaGVscGVycy9tYXJrU3d1cEVsZW1lbnRzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL0xpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvcmVuZGVyUGFnZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvbW9kdWxlcy90cmlnZ2VyRXZlbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvb2ZmLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9tb2R1bGVzL3VwZGF0ZVRyYW5zaXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvZ2V0QW5pbWF0aW9uUHJvbWlzZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvZ2V0UGFnZURhdGEuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvcGx1Z2lucy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHN3dXAvYm9keS1jbGFzcy1wbHVnaW4vbGliL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac3d1cC9kZWJ1Zy1wbHVnaW4vbGliL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac3d1cC9qcy1wbHVnaW4vbGliL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wYXRoLXRvLXJlZ2V4cC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHN3dXAvcHJlbG9hZC1wbHVnaW4vbGliL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac3d1cC9wcmVsb2FkLXBsdWdpbi9ub2RlX21vZHVsZXMvZGVsZWdhdGUvc3JjL2RlbGVnYXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac3d1cC9wcmVsb2FkLXBsdWdpbi9ub2RlX21vZHVsZXMvZGVsZWdhdGUvc3JjL2Nsb3Nlc3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0Bzd3VwL3Njcm9sbC1wbHVnaW4vbGliL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zY3JsL2xpYi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2pzL21vZHVsZXMvc2Nyb2xsLW1vZHVsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbGF6eXNpemVzL2xhenlzaXplcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9heGlvcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2NvcmUvQXhpb3MuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jb3JlL0ludGVyY2VwdG9yTWFuYWdlci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2NvcmUvZGlzcGF0Y2hSZXF1ZXN0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS90cmFuc2Zvcm1EYXRhLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9wcm9jZXNzL2Jyb3dzZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL25vcm1hbGl6ZUhlYWRlck5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jb3JlL3NldHRsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2NvcmUvZW5oYW5jZUVycm9yLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9idWlsZEZ1bGxQYXRoLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9pc0Fic29sdXRlVVJMLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9jb21iaW5lVVJMcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvcGFyc2VIZWFkZXJzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9pc1VSTFNhbWVPcmlnaW4uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2Nvb2tpZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jYW5jZWwvQ2FuY2VsVG9rZW4uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL3NwcmVhZC5qcyJdLCJuYW1lcyI6WyJheGlvcyIsInJlcXVpcmUiLCJkZWZhdWx0IiwibGF6eVNpemVzIiwiY2ZnIiwiZXhwRmFjdG9yIiwiZG9jdW1lbnQiLCJhZGRFdmVudExpc3RlbmVyIiwiZSIsInRhcmdldCIsImNsYXNzTGlzdCIsImNvbnRhaW5zIiwiY2xvc2VzdCIsInJlbW92ZSIsImhlcm9faG92ZXIiLCJoZXJvX2hvdmVyX2l0ZW0iLCJxdWVyeVNlbGVjdG9yQWxsIiwibGVuZ3RoIiwiaSIsImxlbiIsImFkZCIsImNvbV9ob3ZlciIsImNhbnZhc19pdGVtXzIiLCJxdWVyeVNlbGVjdG9yIiwiaGVyb19sb2FkX21vcmUiLCJsaW5rIiwic3Bpbm5lciIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJ0aGVfdXJsIiwiY3VycmVudFRhcmdldCIsImhyZWYiLCJzdHlsZSIsIm9wYWNpdHkiLCJnZXQiLCJ0aGVuIiwicmVzcG9uc2UiLCJjb21faG9tZV9jb250YWluZXIiLCJ3cmFwcGVyIiwiY3JlYXRlRWxlbWVudCIsImlubmVySFRNTCIsImRhdGEiLCJuZXdDb250ZW50IiwiYXBwZW5kQ2hpbGQiLCJzZXRUaW1lb3V0IiwiZGlzcGxheSIsInNjcm9sbEl0Iiwid2luZG93IiwicGpheF90YXJnZXQiLCJpbml0X3BqYXhfbGlua3MiLCJjYXRjaCIsImVycm9yIiwiY29uc29sZSIsImxvZyIsImpzX29wdGlvbnMiLCJmcm9tIiwidG8iLCJvdXQiLCJuZXh0Iiwicm9vdCIsImdldEVsZW1lbnRzQnlUYWdOYW1lIiwiaW4iLCJzY3JvbGxUbyIsImluaXRfdmlld3MiLCJvcHRpb25zIiwiY29udGFpbmVycyIsImxpbmtTZWxlY3RvciIsImFuaW1hdGlvblNlbGVjdG9yIiwiYW5pbWF0ZUhpc3RvcnlCcm93c2luZyIsInBsdWdpbnMiLCJTd3VwSnNQbHVnaW4iLCJTd3VwUHJlbG9hZFBsdWdpbiIsIlN3dXBCb2R5Q2xhc3NQbHVnaW4iLCJib2R5Iiwic3d1cCIsIlN3dXAiLCJjcmVhdGVfcHJvamVjdF9kYXRhIiwiY2xpZW50X25hbWUiLCJkYXRhc2V0IiwiY2xpZW50bmFtZSIsInByb2plY3RfbmFtZSIsInByb2plY3RuYW1lIiwiY05hbWUiLCJjcmVhdGVUZXh0Tm9kZSIsInBOYW1lIiwiZmlsbV9saW5rcyIsInoiLCJlbGVtIiwic3RhdGVPYmoiLCJoaXN0b3J5IiwicHVzaFN0YXRlIiwib2xkQ29udGVudCIsInBhcmVudE5vZGUiLCJyZW1vdmVDaGlsZCIsImdCb2R5U2Nyb2xsIiwicGFnZVlPZmZzZXQiLCJkb2N1bWVudEVsZW1lbnQiLCJzY3JvbGxUb3AiLCJyZW1vdmVBdHRyaWJ1dGUiLCJmaWxtX2NvbnRhaW5lciIsInZpc2liaWxpdHkiLCJwb3NpdGlvbiIsIm5leHRfZmlsbV9saW5rIiwibmV4dF9maWxtX3RyaWdnZXIiLCJyZW1vdmVQcm9wZXJ0eSIsInJlcGxhY2VTdGF0ZSIsImNsb3NlX292ZXJsYXkiLCJwYWdlc3JvbCIsIm1pbnVzcGFnZXNyb2wiLCJ0cmFuc2Zvcm0iLCJob21lX3VybCIsIm9uY2xpY2siLCJjcmVhdGVfY2FudmFzXzEiLCJuZXdfY2FudmFzIiwiY2xhc3NOYW1lIiwiY2FudmFzX3BhcmVudCIsInBvc2l0aW9uSW5mbyIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsImNfaGVpZ2h0IiwiaGVpZ2h0IiwiY193aWR0aCIsIndpZHRoIiwiY2FudmFzX2l0ZW0iLCJub2lzZV8xIiwidmlld1dpZHRoIiwidmlld0hlaWdodCIsImNhbnZhcyIsImN0eCIsInBhdHRlcm5TaXplIiwicGF0dGVyblNjYWxlWCIsInBhdHRlcm5TY2FsZVkiLCJwYXR0ZXJuUmVmcmVzaEludGVydmFsIiwicGF0dGVybkFscGhhIiwicGF0dGVyblBpeGVsRGF0YUxlbmd0aCIsInBhdHRlcm5DYW52YXMiLCJwYXR0ZXJuQ3R4IiwicGF0dGVybkRhdGEiLCJmcmFtZSIsImluaXRDYW52YXMiLCJpbml0R3JhaW4iLCJyZXF1ZXN0QW5pbWF0aW9uRnJhbWUiLCJsb29wIiwiY2xpZW50V2lkdGgiLCJjbGllbnRIZWlnaHQiLCJnZXRDb250ZXh0Iiwic2NhbGUiLCJjcmVhdGVJbWFnZURhdGEiLCJ1cGRhdGUiLCJ2YWx1ZSIsIk1hdGgiLCJyYW5kb20iLCJwdXRJbWFnZURhdGEiLCJkcmF3IiwiY2xlYXJSZWN0IiwiZmlsbFN0eWxlIiwiY3JlYXRlUGF0dGVybiIsImZpbGxSZWN0IiwibW9iaWxlX25hdiIsIm1lbnVfdHJpZ2dlcl9vcGVuIiwiaW5pdF9zZWNvbmRfbm9pc2UiLCJjYW52YXNfcGFyZW50cyIsImxhc3RfY2FudmFzIiwiYXBwZW5kX2Jsb2NrX3BhcmVudHMiLCJhcHBlbmRfYmxvY2siLCJub2lzZV8yIiwib25zY3JvbGwiLCJ2aWRlb19sb2FkZXIiLCJ2aWRlbyIsImNoZWNrTG9hZCIsInJlYWR5U3RhdGUiLCJwbGF5IiwiaW5pdF9wamF4Iiwic2V0X2hlcm9fc2NyZWVuX2hlaWdodCIsImlzSXBob25lIiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwiaW5kZXhPZiIsImlzU2FmYXJpIiwidmVuZG9yIiwib25wb3BzdGF0ZSIsInN0YXRlIiwibmV3UGFnZVVybCIsImxvY2F0aW9uIiwidGVzdCIsInJlcGxhY2UiLCJpbm5lcldpZHRoIiwiaW5uZXJfaGVpZ2h0IiwiaW5uZXJIZWlnaHQiLCJoZXJvX2Jsb2NrIiwib25GaXJzdFRvdWNoIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImRlc3RpbmF0aW9uIiwiZHVyYXRpb24iLCJlYXNpbmciLCJjYWxsYmFjayIsImVhc2luZ3MiLCJsaW5lYXIiLCJ0IiwiZWFzZUluUXVhZCIsImVhc2VPdXRRdWFkIiwiZWFzZUluT3V0UXVhZCIsImVhc2VJbkN1YmljIiwiZWFzZU91dEN1YmljIiwiZWFzZUluT3V0Q3ViaWMiLCJlYXNlSW5RdWFydCIsImVhc2VPdXRRdWFydCIsImVhc2VJbk91dFF1YXJ0IiwiZWFzZUluUXVpbnQiLCJlYXNlT3V0UXVpbnQiLCJlYXNlSW5PdXRRdWludCIsInN0YXJ0Iiwic3RhcnRUaW1lIiwicGVyZm9ybWFuY2UiLCJub3ciLCJEYXRlIiwiZ2V0VGltZSIsImRvY3VtZW50SGVpZ2h0IiwibWF4Iiwic2Nyb2xsSGVpZ2h0Iiwib2Zmc2V0SGVpZ2h0Iiwid2luZG93SGVpZ2h0IiwiZGVzdGluYXRpb25PZmZzZXQiLCJvZmZzZXRUb3AiLCJkZXN0aW5hdGlvbk9mZnNldFRvU2Nyb2xsIiwicm91bmQiLCJzY3JvbGwiLCJ0aW1lIiwibWluIiwidGltZUZ1bmN0aW9uIiwiY2VpbCJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQzdEQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsYUFBYTtBQUN4QixXQUFXLFNBQVM7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1DQUFtQyxPQUFPO0FBQzFDO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixTQUFTLEdBQUcsU0FBUztBQUM1QywyQkFBMkI7QUFDM0I7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQSx1Q0FBdUMsT0FBTztBQUM5QztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsZ0NBQWdDO0FBQ2hDLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUEsdUNBQXVDLE9BQU87QUFDOUM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsWUFBWSxPQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDdlZBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUM7Ozs7Ozs7QUNoREE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRTs7Ozs7OztBQ3ZCQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdko7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQSxLQUFLOztBQUVMO0FBQ0EsQ0FBQzs7QUFFRCx5Qjs7Ozs7OztBQy9DQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsaUJBQWlCO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7O0FDdEVBOztBQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7K0NDSkE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdFQUF3RTtBQUN4RTtBQUNBO0FBQ0E7QUFDQSx1REFBdUQ7QUFDdkQ7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTyxZQUFZO0FBQ25CO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQSxDQUFDOztBQUVEOzs7Ozs7Ozs7QUNoR0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw0Q0FBNEM7QUFDNUM7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7Ozs7Ozs7O0FDbkxBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsYUFBYSxNQUFNO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDakJBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7Ozs7Ozs7QUN4RUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7OztBQ2JBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOztBQU9BOzs7O0FBZEEsSUFBTUEsUUFBUSxtQkFBQUMsQ0FBUSxFQUFSLEVBQWlCQyxPQUEvQjs7O0FBU0E7QUFDQTtBQUNBOztBQUVBQyxVQUFVQyxHQUFWLENBQWNDLFNBQWQsR0FBMEIsQ0FBMUI7O0FBRUFDLFNBQVNDLGdCQUFULENBQTBCLFlBQTFCLEVBQXdDLFVBQVNDLENBQVQsRUFBVzs7QUFFbkQsTUFBSUEsRUFBRUMsTUFBRixDQUFTQyxTQUFULENBQW1CQyxRQUFuQixDQUE0QixZQUE1QixDQUFKLEVBQStDO0FBQzdDOztBQUVGSCxNQUFFQyxNQUFGLENBQVNHLE9BQVQsQ0FBaUIsZUFBakIsRUFBa0NGLFNBQWxDLENBQTRDRyxNQUE1QyxDQUFtRCxnQkFBbkQ7QUFDQTtBQUVDO0FBRUEsQ0FWRDs7QUFjQTs7QUFFQSxTQUFTQyxVQUFULEdBQXFCO0FBQ3JCLE1BQUlDLGtCQUFrQlQsU0FBU1UsZ0JBQVQsQ0FBMEIsa0JBQTFCLENBQXRCOztBQUVBLE1BQUdELGdCQUFnQkUsTUFBaEIsR0FBeUIsQ0FBNUIsRUFBK0I7QUFDN0IsU0FBSyxJQUFJQyxJQUFJLENBQVIsRUFBV0MsTUFBTUosZ0JBQWdCRSxNQUF0QyxFQUE4Q0MsSUFBSUMsR0FBbEQsRUFBdURELEdBQXZELEVBQTREO0FBQzFESCxzQkFBZ0JHLENBQWhCLEVBQW1CWCxnQkFBbkIsQ0FBb0MsWUFBcEMsRUFBa0QsVUFBU0MsQ0FBVCxFQUFXOztBQUVqRTtBQUNFQSxVQUFFQyxNQUFGLENBQVNDLFNBQVQsQ0FBbUJVLEdBQW5CLENBQXVCLGVBQXZCO0FBQ0QsT0FKRztBQUtKTCxzQkFBZ0JHLENBQWhCLEVBQW1CWCxnQkFBbkIsQ0FBb0MsWUFBcEMsRUFBa0QsVUFBU0MsQ0FBVCxFQUFXOztBQUc3REEsVUFBRUMsTUFBRixDQUFTQyxTQUFULENBQW1CRyxNQUFuQixDQUEwQixlQUExQjtBQUNDLE9BSkQ7QUFNQztBQUVBO0FBRUE7O0FBRUQsU0FBU1EsU0FBVCxHQUFvQjtBQUNwQixNQUFJTixrQkFBa0JULFNBQVNVLGdCQUFULENBQTBCLGlCQUExQixDQUF0Qjs7QUFFQSxNQUFHRCxnQkFBZ0JFLE1BQWhCLEdBQXlCLENBQTVCLEVBQStCO0FBQzdCLFNBQUssSUFBSUMsSUFBSSxDQUFSLEVBQVdDLE1BQU1KLGdCQUFnQkUsTUFBdEMsRUFBOENDLElBQUlDLEdBQWxELEVBQXVERCxHQUF2RCxFQUE0RDtBQUMxREgsc0JBQWdCRyxDQUFoQixFQUFtQlgsZ0JBQW5CLENBQW9DLFlBQXBDLEVBQWtELFVBQVNDLENBQVQsRUFBVztBQUNqRSxZQUFJYyxnQkFBZ0JoQixTQUFTaUIsYUFBVCxDQUF1QixpQkFBdkIsQ0FBcEI7QUFDQTtBQUNFZixVQUFFQyxNQUFGLENBQVNDLFNBQVQsQ0FBbUJVLEdBQW5CLENBQXVCLGVBQXZCO0FBQ0QsT0FKRztBQUtKTCxzQkFBZ0JHLENBQWhCLEVBQW1CWCxnQkFBbkIsQ0FBb0MsWUFBcEMsRUFBa0QsVUFBU0MsQ0FBVCxFQUFXOztBQUc3REEsVUFBRUMsTUFBRixDQUFTQyxTQUFULENBQW1CRyxNQUFuQixDQUEwQixlQUExQjtBQUNDLE9BSkQ7QUFNQztBQUVBO0FBRUE7O0FBUUQ7OztBQUdBLFNBQVNXLGNBQVQsR0FBMEI7O0FBSTFCLE1BQU1DLE9BQU9uQixTQUFTaUIsYUFBVCxDQUF1QixxQkFBdkIsQ0FBYjtBQUNBLE1BQU1HLFVBQVVwQixTQUFTaUIsYUFBVCxDQUF1QixZQUF2QixDQUFoQjtBQUNBRSxPQUFLbEIsZ0JBQUwsQ0FBc0IsT0FBdEIsRUFBK0IsaUJBQVM7QUFDdENvQixVQUFNQyxjQUFOO0FBQ0EsUUFBSUMsVUFBVUYsTUFBTUcsYUFBTixDQUFvQkMsSUFBbEM7QUFDQTs7O0FBSUFOLFNBQUtPLEtBQUwsQ0FBV0MsT0FBWCxHQUFxQixHQUFyQjtBQUNBUCxZQUFRTSxLQUFSLENBQWNDLE9BQWQsR0FBd0IsR0FBeEI7O0FBSUFqQyxVQUFNa0MsR0FBTixDQUFVTCxPQUFWLEVBQ0dNLElBREgsQ0FDUSxVQUFVQyxRQUFWLEVBQW9CO0FBQ3hCO0FBQ0E5QixlQUFTaUIsYUFBVCxDQUF1QixjQUF2QixFQUF1Q2IsU0FBdkMsQ0FBaURHLE1BQWpELENBQXdELE1BQXhEO0FBQ0E7QUFDQTtBQUNBLFVBQUl3QixxQkFBcUIvQixTQUFTaUIsYUFBVCxDQUF1QixjQUF2QixDQUF6QjtBQUNFLFVBQUllLFVBQVVoQyxTQUFTaUMsYUFBVCxDQUF1QixLQUF2QixDQUFkO0FBQ0lELGNBQVFFLFNBQVIsR0FBb0JKLFNBQVNLLElBQTdCO0FBQ0EsVUFBSUMsYUFBYUosUUFBUWYsYUFBUixDQUFzQiw0QkFBdEIsQ0FBakI7QUFDQWMseUJBQW1CTSxXQUFuQixDQUErQkQsVUFBL0I7QUFDQUUsaUJBQVcsWUFBVTtBQUNqQm5CLGFBQUtPLEtBQUwsQ0FBV2EsT0FBWCxHQUFxQixNQUFyQjtBQUNKbkIsZ0JBQVFNLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixHQUF4QjtBQUNELE9BSEMsRUFHQyxHQUhEOztBQUtGVyxpQkFBVyxZQUFZO0FBQ2pDbEIsZ0JBQVFNLEtBQVIsQ0FBY0MsT0FBZCxHQUF3QixHQUF4Qjs7QUFFQVcsbUJBQVcsWUFBVTtBQUNyQnRDLG1CQUFTaUIsYUFBVCxDQUF1QixjQUF2QixFQUF1Q2IsU0FBdkMsQ0FBaURVLEdBQWpELENBQXFELHFCQUFyRDtBQUNDLFNBRkQsRUFFRyxHQUZIO0FBR0EwQixpQkFBU3hDLFNBQVNpQixhQUFULENBQXVCLG9CQUF2QixDQUFULEVBQXVELElBQXZELEVBQTZELGdCQUE3RCxFQUErRSxZQUFZO0FBQzNGRyxrQkFBUU0sS0FBUixDQUFjYSxPQUFkLEdBQXdCLE1BQXhCO0FBQ0F2QyxtQkFBU2lCLGFBQVQsQ0FBdUIsY0FBdkIsRUFBdUNiLFNBQXZDLENBQWlEVSxHQUFqRCxDQUFxRCxxQkFBckQ7QUFDQTtBQUNBQztBQUNBMEIsaUJBQU9DLFdBQVAsR0FBcUIxQyxTQUFTVSxnQkFBVCxDQUEwQixpQkFBMUIsQ0FBckI7QUFDQWlDO0FBQ0EzQyxtQkFBU2lCLGFBQVQsQ0FBdUIsY0FBdkIsRUFBdUNiLFNBQXZDLENBQWlERyxNQUFqRCxDQUF3RCxrQkFBeEQ7QUFDQyxTQVJEO0FBU0MsT0FmUyxFQWVQLEdBZk87QUFtQkwsS0FuQ0gsRUFvQ0dxQyxLQXBDSCxDQW9DUyxVQUFVQyxLQUFWLEVBQWlCO0FBQ3RCO0FBQ0FDLGNBQVFDLEdBQVIsQ0FBWUYsS0FBWjtBQUNELEtBdkNILEVBd0NHaEIsSUF4Q0gsQ0F3Q1EsWUFBWTtBQUNoQjtBQUNELEtBMUNIOztBQWlERjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFPLEtBQVA7QUFDRyxHQXZFSDtBQXlFQzs7QUFXRDs7O0FBR0EsSUFBTW1CLGFBQWEsQ0FHakI7QUFDRUMsUUFBTSxNQURSLEVBQ2dCQyxJQUFJLHNCQURwQjtBQUVFQyxPQUFLLGFBQUNDLElBQUQsRUFBVTtBQUNiLFFBQUlDLE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDtBQUNBdEQsYUFBU2lCLGFBQVQsQ0FBdUIsdUJBQXZCLEVBQWdEYixTQUFoRCxDQUEwRFUsR0FBMUQsQ0FBOEQsNEJBQTlEO0FBQ0E7QUFDQTtBQUVELEdBUkg7O0FBVUV5QyxNQUFJLGFBQVNILElBQVQsRUFBZTtBQUNqQkE7QUFDRDs7QUFaSCxDQUhpQixFQXNCbkI7QUFDRUgsUUFBTSxNQURSLEVBQ2dCQyxJQUFJLGtCQURwQjtBQUVFQyxPQUFLLGFBQUNDLElBQUQsRUFBVTtBQUNiLFFBQUlDLE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDtBQUNBRCxTQUFLakQsU0FBTCxDQUFlVSxHQUFmLENBQW1CLFlBQW5CO0FBQ0E7QUFDQTtBQUNJc0M7QUFDSjs7QUFHRCxHQVhIOztBQWFFRyxNQUFJLGFBQVNILElBQVQsRUFBZTtBQUNmWCxXQUFPZSxRQUFQLENBQWdCLENBQWhCLEVBQW1CLENBQW5COztBQUVGLFFBQUlILE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDtBQUNFLFFBQUlELEtBQUtqRCxTQUFMLENBQWVDLFFBQWYsQ0FBd0Isb0JBQXhCLENBQUosRUFBbUQ7QUFDakQ7QUFDRCxLQUZELE1BR0s7QUFDRGdELFdBQUtqRCxTQUFMLENBQWVVLEdBQWYsQ0FBbUIsb0JBQW5CO0FBQ0F3QixpQkFBVyxZQUFVO0FBQ25CZSxhQUFLakQsU0FBTCxDQUFlRyxNQUFmLENBQXNCLG9CQUF0QjtBQUNELE9BRkQsRUFFRyxJQUZIO0FBR0g7O0FBRURrRDtBQUNBbkIsZUFBVyxZQUFVO0FBQ3ZCZSxXQUFLakQsU0FBTCxDQUFlRyxNQUFmLENBQXNCLFlBQXRCO0FBQ0E2QztBQUNELEtBSEcsRUFHRCxHQUhDO0FBSUY7QUFFRDs7QUFsQ0gsQ0F0Qm1CLENBQW5COztBQWlFQSxJQUFNTSxVQUFVOztBQUVkQyxjQUFZLENBQUMsdUJBQUQsQ0FGRTtBQUdkQyxnQkFDRSxZQUpZO0FBS2RDLHFCQUFtQiw0QkFMTDtBQU1kQywwQkFBd0IsSUFOVjtBQU9kQyxXQUFTLENBQ1AsSUFBSUMsa0JBQUosQ0FBaUJoQixVQUFqQixDQURPO0FBRVA7O0FBRUEsTUFBSWlCLHVCQUFKLEVBSk8sRUFLUCxJQUFJQyx5QkFBSixFQUxPO0FBUEssQ0FBaEI7O0FBd0JBLElBQUlsRSxTQUFTbUUsSUFBVCxDQUFjL0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUMsUUFBakMsQ0FBSixFQUFnRCxDQUUvQyxDQUZELE1BR0s7QUFDSCxNQUFNK0QsT0FBTyxJQUFJQyxjQUFKLENBQVNYLE9BQVQsQ0FBYjtBQUNBMUQsV0FBU0MsZ0JBQVQsQ0FBMEIsZUFBMUIsRUFBMkMsVUFBQ29CLEtBQUQsRUFBVztBQUN0RDtBQUNBckIsYUFBU3NELG9CQUFULENBQThCLE1BQTlCLEVBQXNDLENBQXRDLEVBQXlDbEQsU0FBekMsQ0FBbURHLE1BQW5ELENBQTBELHFCQUExRDtBQUNDLEdBSEQ7QUFJRDs7QUFJRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFLQTs7O0FBS0EsU0FBUytELG1CQUFULEdBQThCO0FBQzlCLE1BQUlDLGNBQWNsRCxNQUFNRyxhQUFOLENBQW9CZ0QsT0FBcEIsQ0FBNEJDLFVBQTlDO0FBQ0UsTUFBSUMsZUFBZXJELE1BQU1HLGFBQU4sQ0FBb0JnRCxPQUFwQixDQUE0QkcsV0FBL0M7O0FBRUYsTUFBSUMsUUFBUTVFLFNBQVM2RSxjQUFULENBQXdCTixXQUF4QixDQUFaO0FBQ0EsTUFBSU8sUUFBUTlFLFNBQVM2RSxjQUFULENBQXdCSCxZQUF4QixDQUFaO0FBQ0E7QUFDQTtBQUNBMUUsV0FBU2lCLGFBQVQsQ0FBdUIsb0NBQXZCLEVBQTZEaUIsU0FBN0QsR0FBeUUsRUFBekU7QUFDQWxDLFdBQVNpQixhQUFULENBQXVCLG9DQUF2QixFQUE2RG9CLFdBQTdELENBQXlFdUMsS0FBekU7QUFDQTVFLFdBQVNpQixhQUFULENBQXVCLHFDQUF2QixFQUE4RGlCLFNBQTlELEdBQTBFLEVBQTFFO0FBQ0FsQyxXQUFTaUIsYUFBVCxDQUF1QixxQ0FBdkIsRUFBOERvQixXQUE5RCxDQUEwRXlDLEtBQTFFO0FBQ0M7O0FBSUQ7OztBQVFBLFNBQVNuQyxlQUFULEdBQTJCO0FBQzNCLE1BQUlvQyxhQUFhckMsV0FBakI7QUFDQTtBQUNBLE9BQUksSUFBSXNDLElBQUksQ0FBWixFQUFlQSxJQUFJRCxXQUFXcEUsTUFBOUIsRUFBc0NxRSxHQUF0QyxFQUEyQztBQUN6QyxRQUFJQyxPQUFPRixXQUFXQyxDQUFYLENBQVg7O0FBRUFDLFNBQUtoRixnQkFBTCxDQUFzQixPQUF0QixFQUErQixpQkFBUztBQUNwQ29CLFlBQU1DLGNBQU47QUFDRkQsWUFBTUcsYUFBTixDQUFvQnBCLFNBQXBCLENBQThCVSxHQUE5QixDQUFrQyxlQUFsQztBQUNKTyxZQUFNRyxhQUFOLENBQW9CcEIsU0FBcEIsQ0FBOEJHLE1BQTlCLENBQXFDLGVBQXJDOztBQUtBK0Q7O0FBRUE7OztBQU1BOztBQUVFLFVBQUkvQyxVQUFVRixNQUFNRyxhQUFOLENBQW9CQyxJQUFsQztBQUNBO0FBQ0EsVUFBSXlELFFBQUo7QUFDQXpDLGFBQU8wQyxPQUFQLENBQWVDLFNBQWYsQ0FBeUJGLFFBQXpCLEVBQW1DLEVBQW5DLEVBQXVDM0QsT0FBdkM7QUFDQSxVQUFJOEQsYUFBYXJGLFNBQVNpQixhQUFULENBQXVCLHVCQUF2QixDQUFqQjtBQUNGLFVBQUlvRSxlQUFlLElBQW5CLEVBQXlCO0FBQ3ZCQSxtQkFBV0MsVUFBWCxDQUFzQkMsV0FBdEIsQ0FBa0NGLFVBQWxDO0FBQ0Q7O0FBRUQ1QyxhQUFPK0MsV0FBUCxHQUFxQi9DLE9BQU9nRCxXQUFQLElBQXNCekYsU0FBUzBGLGVBQVQsQ0FBeUJDLFNBQS9DLElBQTREM0YsU0FBU21FLElBQVQsQ0FBY3dCLFNBQTFFLElBQXVGLENBQTVHO0FBQ0EzRixlQUFTaUIsYUFBVCxDQUF1Qiw0QkFBdkIsRUFBcURTLEtBQXJELENBQTJEYSxPQUEzRCxHQUFxRSxPQUFyRTtBQUNFLFVBQUljLE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDs7QUFFQXRELGVBQVNpQixhQUFULENBQXVCLHVCQUF2QixFQUFnRDJFLGVBQWhELENBQWdFLE9BQWhFOztBQUVGdEQsaUJBQVcsWUFBVTtBQUNyQnRDLGlCQUFTaUIsYUFBVCxDQUF1Qix1QkFBdkIsRUFBZ0RiLFNBQWhELENBQTBEVSxHQUExRCxDQUE4RCw0QkFBOUQ7QUFDQyxPQUZELEVBRUcsR0FGSDs7QUFLQTtBQUNBcEIsWUFBTWtDLEdBQU4sQ0FBVUwsT0FBVixFQUNHTSxJQURILENBQ1EsVUFBVUMsUUFBVixFQUFvQjtBQUN4Qjs7QUFFQTtBQUNBO0FBQ0EsWUFBSStELGlCQUFpQjdGLFNBQVNpQixhQUFULENBQXVCLGNBQXZCLENBQXJCO0FBQ0UsWUFBSTRFLGlCQUFpQjdGLFNBQVNpQixhQUFULENBQXVCLGNBQXZCLENBQXJCOztBQUVBLFlBQUllLFVBQVVoQyxTQUFTaUMsYUFBVCxDQUF1QixLQUF2QixDQUFkO0FBQ0lELGdCQUFRRSxTQUFSLEdBQW9CSixTQUFTSyxJQUE3QjtBQUNBLFlBQUlDLGFBQWFKLFFBQVFmLGFBQVIsQ0FBc0IsdUJBQXRCLENBQWpCO0FBQ0E0RSx1QkFBZXhELFdBQWYsQ0FBMkJELFVBQTNCO0FBQ0VFLG1CQUFXLFlBQVU7QUFDbkJ0QyxtQkFBU2lCLGFBQVQsQ0FBdUIsV0FBdkIsRUFBb0NTLEtBQXBDLENBQTBDQyxPQUExQyxHQUFvRCxHQUFwRDtBQUNBM0IsbUJBQVNpQixhQUFULENBQXVCLDJCQUF2QixFQUFvRFMsS0FBcEQsQ0FBMERvRSxVQUExRCxHQUF1RSxRQUF2RTtBQUNBekMsZUFBS2pELFNBQUwsQ0FBZVUsR0FBZixDQUFtQixrQkFBbkI7QUFDQXdCLHFCQUFXLFlBQVU7QUFDdkJ0QyxxQkFBU2lCLGFBQVQsQ0FBdUIsYUFBdkIsRUFBc0NTLEtBQXRDLENBQTRDYSxPQUE1QyxHQUFzRCxNQUF0RDtBQUNBdkMscUJBQVNpQixhQUFULENBQXVCLHVCQUF2QixFQUFnRFMsS0FBaEQsQ0FBc0RxRSxRQUF0RCxHQUFpRSxVQUFqRTtBQUNELFdBSEcsRUFHRCxFQUhDO0FBSUZ0RCxpQkFBT2UsUUFBUCxDQUFnQixDQUFoQixFQUFtQixDQUFuQjtBQUNBeEQsbUJBQVNpQixhQUFULENBQXVCLGdCQUF2QixFQUF5Q2IsU0FBekMsQ0FBbURHLE1BQW5ELENBQTBELGVBQTFEO0FBQ0UsY0FBSXlGLGlCQUFpQmhHLFNBQVNpQixhQUFULENBQXVCLG9CQUF2QixDQUFyQjtBQUNBLGNBQUcsT0FBTytFLGNBQVAsSUFBMEIsV0FBMUIsSUFBeUNBLGtCQUFrQixJQUE5RCxFQUFtRTtBQUNqRUM7QUFDRCxXQUZELE1BRU07QUFDSjtBQUNEOztBQUVEakcsbUJBQVNpQixhQUFULENBQXVCLHVCQUF2QixFQUFnRFMsS0FBaEQsQ0FBc0RDLE9BQXRELEdBQWdFLEdBQWhFO0FBQ0FXLHFCQUFXLFlBQVU7QUFDbkJ0QyxxQkFBU2lCLGFBQVQsQ0FBdUIsdUJBQXZCLEVBQWdEUyxLQUFoRCxDQUFzRGEsT0FBdEQsR0FBZ0UsTUFBaEU7QUFDQSxnQkFBSWMsT0FBT3JELFNBQVNzRCxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxDQUFYO0FBQ0F0RCxxQkFBU2lCLGFBQVQsQ0FBdUIsbUJBQXZCLEVBQTRDUyxLQUE1QyxDQUFrREMsT0FBbEQsR0FBNEQsR0FBNUQ7QUFDQTNCLHFCQUFTaUIsYUFBVCxDQUF1Qix1QkFBdkIsRUFBZ0QyRSxlQUFoRCxDQUFnRSxPQUFoRTs7QUFFTDVGLHFCQUFTaUIsYUFBVCxDQUF1Qix1QkFBdkIsRUFBZ0RiLFNBQWhELENBQTBERyxNQUExRCxDQUFpRSxjQUFqRTtBQUlELFdBVkksRUFVRixHQVZFO0FBV04sU0E3QkksRUE2QkYsSUE3QkU7QUFnQ1QsT0E3Q0gsRUE4Q0dxQyxLQTlDSCxDQThDUyxVQUFVQyxLQUFWLEVBQWlCO0FBQ3RCO0FBQ0FDLGdCQUFRQyxHQUFSLENBQVlGLEtBQVo7QUFDRCxPQWpESCxFQWtER2hCLElBbERILENBa0RRLFlBQVk7QUFDaEI7QUFDQXdCLGFBQUtqRCxTQUFMLENBQWVVLEdBQWYsQ0FBbUIsc0JBQW5CO0FBQ0QsT0FyREg7O0FBOERBLGFBQU8sS0FBUDtBQUNDLEtBdEdDO0FBdUdEO0FBRUE7O0FBSUQ7O0FBRUE7O0FBRUEsU0FBU21GLGlCQUFULEdBQTZCOztBQUc3QixNQUFJRCxpQkFBaUJoRyxTQUFTaUIsYUFBVCxDQUF1QixvQkFBdkIsQ0FBckI7O0FBR0ErRSxpQkFBZS9GLGdCQUFmLENBQWdDLE9BQWhDLEVBQXlDLGlCQUFTO0FBQ2xEO0FBQ0lvQixVQUFNRyxhQUFOLENBQW9CcEIsU0FBcEIsQ0FBOEJVLEdBQTlCLENBQWtDLGVBQWxDO0FBQ0ZPLFVBQU1DLGNBQU47O0FBSUZ0QixhQUFTaUIsYUFBVCxDQUF1Qix1QkFBdkIsRUFBZ0RTLEtBQWhELENBQXNEd0UsY0FBdEQsQ0FBcUUsU0FBckU7QUFDQTVCOztBQUVHaEMsZUFBVyxZQUFVO0FBQ25CRyxhQUFPZSxRQUFQLENBQWdCLENBQWhCLEVBQW1CLENBQW5CO0FBQ0F4RCxlQUFTaUIsYUFBVCxDQUF1Qix1QkFBdkIsRUFBZ0RTLEtBQWhELENBQXNEQyxPQUF0RCxHQUFnRSxHQUFoRTtBQUNJO0FBQ0QsS0FKTCxFQUlPLEdBSlA7O0FBTUg7OztBQUdFLFFBQUkwQixPQUFPckQsU0FBU3NELG9CQUFULENBQThCLE1BQTlCLEVBQXNDLENBQXRDLENBQVg7O0FBSUY7O0FBRUUsUUFBSS9CLFVBQVVGLE1BQU1HLGFBQU4sQ0FBb0JDLElBQWxDO0FBQ0EsUUFBSXlELFFBQUo7QUFDQXpDLFdBQU8wQyxPQUFQLENBQWVnQixZQUFmLENBQTRCakIsUUFBNUIsRUFBc0MsRUFBdEMsRUFBMEMzRCxPQUExQztBQUNBO0FBQ0EsUUFBSThELGFBQWFyRixTQUFTaUIsYUFBVCxDQUF1Qix1QkFBdkIsQ0FBakI7QUFDRixRQUFJb0UsZUFBZSxJQUFuQixFQUF5QjtBQUN2QkEsaUJBQVczRCxLQUFYLENBQWlCQyxPQUFqQixHQUEyQixDQUEzQjtBQUNEOztBQUlEakMsVUFBTWtDLEdBQU4sQ0FBVUwsT0FBVixFQUNHTSxJQURILENBQ1EsVUFBVUMsUUFBVixFQUFvQjtBQUN4Qjs7QUFFQTtBQUNBO0FBQ0EsVUFBSStELGlCQUFpQjdGLFNBQVNpQixhQUFULENBQXVCLGNBQXZCLENBQXJCOztBQUdFLFVBQUllLFVBQVVoQyxTQUFTaUMsYUFBVCxDQUF1QixLQUF2QixDQUFkO0FBQ0lELGNBQVFFLFNBQVIsR0FBb0JKLFNBQVNLLElBQTdCO0FBQ0EsVUFBSUMsYUFBYUosUUFBUWYsYUFBUixDQUFzQix1QkFBdEIsQ0FBakI7QUFDQTRFLHFCQUFleEQsV0FBZixDQUEyQkQsVUFBM0I7QUFDRUUsaUJBQVcsWUFBVTtBQUNuQitDLG1CQUFXQyxVQUFYLENBQXNCQyxXQUF0QixDQUFrQ0YsVUFBbEM7QUFDQVk7QUFDQWpHLGlCQUFTaUIsYUFBVCxDQUF1Qix1QkFBdkIsRUFBZ0RTLEtBQWhELENBQXNEQyxPQUF0RCxHQUFnRSxHQUFoRTtBQUNBVyxtQkFBVyxZQUFVOztBQUVuQixjQUFJZSxPQUFPckQsU0FBU3NELG9CQUFULENBQThCLE1BQTlCLEVBQXNDLENBQXRDLENBQVg7QUFDQXRELG1CQUFTaUIsYUFBVCxDQUF1Qiw0QkFBdkIsRUFBcURTLEtBQXJELENBQTJEQyxPQUEzRCxHQUFxRSxHQUFyRTtBQUNBM0IsbUJBQVNpQixhQUFULENBQXVCLHVCQUF2QixFQUFnRFMsS0FBaEQsQ0FBc0RhLE9BQXRELEdBQWdFLE1BQWhFO0FBQ0Z2QyxtQkFBU2lCLGFBQVQsQ0FBdUIsdUJBQXZCLEVBQWdEUyxLQUFoRCxDQUFzRHFFLFFBQXRELEdBQWlFLFVBQWpFO0FBQ0F0RCxpQkFBT2UsUUFBUCxDQUFnQixDQUFoQixFQUFtQixDQUFuQjtBQUNIeEQsbUJBQVNpQixhQUFULENBQXVCLHVCQUF2QixFQUFnRGIsU0FBaEQsQ0FBMERHLE1BQTFELENBQWlFLGNBQWpFO0FBR0EsU0FWRyxFQVVELEdBVkM7QUFXTCxPQWZHLEVBZUQsSUFmQztBQWtCVCxLQS9CSCxFQWdDR3FDLEtBaENILENBZ0NTLFVBQVVDLEtBQVYsRUFBaUI7QUFDdEI7QUFDQUMsY0FBUUMsR0FBUixDQUFZRixLQUFaO0FBQ0QsS0FuQ0gsRUFvQ0doQixJQXBDSCxDQW9DUSxZQUFZO0FBQ2hCO0FBQ0QsS0F0Q0g7O0FBNENBLFdBQU8sS0FBUDtBQUNDLEdBakZEO0FBbUZDOztBQUlEOzs7QUFLQTs7QUFFQSxTQUFTdUUsYUFBVCxHQUF5Qjs7QUFFekIsTUFBSS9DLE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDtBQUNBYixTQUFPNEQsUUFBUCxHQUFrQjVELE9BQU9nRCxXQUFQLElBQXNCekYsU0FBUzBGLGVBQVQsQ0FBeUJDLFNBQS9DLElBQTREM0YsU0FBU21FLElBQVQsQ0FBY3dCLFNBQTFFLElBQXVGLENBQXpHO0FBQ0EsTUFBSVcsZ0JBQWdCLENBQUdELFFBQXZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0VyRyxXQUFTaUIsYUFBVCxDQUF1QixtQkFBdkIsRUFBNENTLEtBQTVDLENBQWtEQyxPQUFsRCxHQUE0RCxHQUE1RDtBQUNBM0IsV0FBU2lCLGFBQVQsQ0FBdUIsdUJBQXZCLEVBQWdEUyxLQUFoRCxDQUFzRHFFLFFBQXRELEdBQWlFLE9BQWpFO0FBQ0YvRixXQUFTaUIsYUFBVCxDQUF1QixjQUF2QixFQUF1Q1MsS0FBdkMsQ0FBNkM2RSxTQUE3QyxHQUF5RCxnQkFBZ0JELGFBQWhCLEdBQWdDLEtBQXpGO0FBQ0F0RyxXQUFTaUIsYUFBVCxDQUF1QixhQUF2QixFQUFzQ1MsS0FBdEMsQ0FBNENhLE9BQTVDLEdBQXNELEVBQXREOztBQUdFO0FBQ0ljLE9BQUtqRCxTQUFMLENBQWVHLE1BQWYsQ0FBc0Isa0JBQXRCOztBQUdKO0FBQ0Y7QUFDRStCLGFBQVcsWUFBVTtBQUNuQkcsV0FBT2UsUUFBUCxDQUFnQixDQUFoQixFQUFtQmdDLFdBQW5CO0FBQ0psRCxlQUFXLFlBQVU7QUFDakJ0QyxlQUFTaUIsYUFBVCxDQUF1QixXQUF2QixFQUFvQ1MsS0FBcEMsQ0FBMENDLE9BQTFDLEdBQW9ELEVBQXBEO0FBQ0EzQixlQUFTaUIsYUFBVCxDQUF1QiwyQkFBdkIsRUFBb0RTLEtBQXBELENBQTBEb0UsVUFBMUQsR0FBdUUsU0FBdkU7QUFDQTlGLGVBQVNpQixhQUFULENBQXVCLHVCQUF2QixFQUFnRGIsU0FBaEQsQ0FBMERHLE1BQTFELENBQWlFLDRCQUFqRTtBQUNBK0IsaUJBQVcsWUFBVTtBQUNuQnRDLGlCQUFTaUIsYUFBVCxDQUF1QixjQUF2QixFQUF1QzJFLGVBQXZDLENBQXVELE9BQXZEO0FBQ0E1RixpQkFBU2lCLGFBQVQsQ0FBdUIsdUJBQXZCLEVBQWdEMkUsZUFBaEQsQ0FBZ0UsT0FBaEU7QUFDQTVGLGlCQUFTaUIsYUFBVCxDQUF1Qix1QkFBdkIsRUFBZ0RTLEtBQWhELENBQXNEYSxPQUF0RCxHQUFnRSxNQUFoRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0F2QyxpQkFBU2lCLGFBQVQsQ0FBdUIsNEJBQXZCLEVBQXFEUyxLQUFyRCxDQUEyRGEsT0FBM0QsR0FBcUUsTUFBckU7QUFDQTtBQUNBLFlBQUk4QyxhQUFhckYsU0FBU2lCLGFBQVQsQ0FBdUIsdUJBQXZCLENBQWpCOztBQUlBLFlBQUlqQixTQUFTbUUsSUFBVCxDQUFjL0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUMsb0JBQWpDLENBQUosRUFBNEQ7QUFDMUQsY0FBSTZFLFFBQUo7QUFDQXpDLGlCQUFPMEMsT0FBUCxDQUFlZ0IsWUFBZixDQUE0QmpCLFFBQTVCLEVBQXNDLEVBQXRDLEVBQTBDc0IsUUFBMUM7QUFFRDtBQUNELFlBQUl4RyxTQUFTbUUsSUFBVCxDQUFjL0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUMsMkJBQWpDLENBQUosRUFBbUU7QUFDakUsY0FBSTZFLFFBQUo7QUFDQXpDLGlCQUFPMEMsT0FBUCxDQUFlZ0IsWUFBZixDQUE0QmpCLFFBQTVCLEVBQXNDLEVBQXRDLEVBQTBDc0IsV0FBVyxlQUFyRDtBQUNEOztBQUVELFlBQUl4RyxTQUFTbUUsSUFBVCxDQUFjL0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUMsMEJBQWpDLENBQUosRUFBa0U7QUFDaEUsY0FBSTZFLFFBQUo7QUFDQXpDLGlCQUFPMEMsT0FBUCxDQUFlZ0IsWUFBZixDQUE0QmpCLFFBQTVCLEVBQXNDLEVBQXRDLEVBQTBDc0IsV0FBVyxlQUFyRDtBQUNEOztBQUVEbkQsYUFBS2pELFNBQUwsQ0FBZUcsTUFBZixDQUFzQixzQkFBdEI7O0FBRUYsWUFBSThFLGVBQWUsSUFBbkIsRUFBeUI7QUFDdkJBLHFCQUFXQyxVQUFYLENBQXNCQyxXQUF0QixDQUFrQ0YsVUFBbEM7QUFDRDtBQUdBLE9BcENELEVBb0NHLEdBcENIO0FBcUNELEtBekNILEVBeUNLLEdBekNMO0FBMENDLEdBNUNDLEVBNENDLEdBNUNEO0FBNkNEOztBQUlEckYsU0FBU2lCLGFBQVQsQ0FBdUIsZ0JBQXZCLEVBQXlDd0YsT0FBekMsR0FBbUQsWUFBVztBQUM5REw7QUFDQSxTQUFPLEtBQVA7QUFDQyxDQUhEOztBQU1BLFNBQVNNLGVBQVQsR0FBNEI7QUFDNUIsTUFBSUMsYUFBYTNHLFNBQVNpQyxhQUFULENBQXVCLFFBQXZCLENBQWpCO0FBQ0EwRSxhQUFXQyxTQUFYLEdBQXVCLDZCQUF2QjtBQUNBOUQsVUFBUUMsR0FBUixDQUFZNEQsVUFBWjtBQUNBO0FBQ0EsTUFBSUUsZ0JBQWdCN0csU0FBU2lCLGFBQVQsQ0FBdUIsa0JBQXZCLENBQXBCO0FBQ0EsTUFBSTZGLGVBQWVELGNBQWNFLHFCQUFkLEVBQW5CO0FBQ0EsTUFBSUMsV0FBV0YsYUFBYUcsTUFBNUI7QUFDQSxNQUFJQyxVQUFVSixhQUFhSyxLQUEzQjtBQUNBckUsVUFBUUMsR0FBUixDQUFZaUUsUUFBWjtBQUNBbEUsVUFBUUMsR0FBUixDQUFZbUUsT0FBWjtBQUNBbEgsV0FBU2lCLGFBQVQsQ0FBdUIsa0JBQXZCLEVBQTJDb0IsV0FBM0MsQ0FBdURzRSxVQUF2RDtBQUNBbEUsU0FBTzJFLFdBQVAsR0FBcUJwSCxTQUFTaUIsYUFBVCxDQUF1QixpQkFBdkIsQ0FBckI7QUFDQW9HO0FBQ0M7O0FBR0M7QUFDRTtBQUNGOztBQUVGLFNBQVNBLE9BQVQsR0FBbUI7QUFDbkIsTUFBSUMsU0FBSjtBQUFBLE1BQ0lDLFVBREo7QUFBQSxNQUVJQyxTQUFTSixXQUZiO0FBQUEsTUFHSUssR0FISjs7QUFLQTtBQUNBLE1BQUlDLGNBQWMsRUFBbEI7QUFBQSxNQUNJQyxnQkFBZ0IsQ0FEcEI7QUFBQSxNQUVJQyxnQkFBZ0IsQ0FGcEI7QUFBQSxNQUdJQyx5QkFBeUIsQ0FIN0I7QUFBQSxNQUlJQyxlQUFlLEVBSm5CLENBUG1CLENBV0k7O0FBRXZCLE1BQUlDLHlCQUF5QkwsY0FBY0EsV0FBZCxHQUE0QixDQUF6RDtBQUFBLE1BQ0lNLGFBREo7QUFBQSxNQUVJQyxVQUZKO0FBQUEsTUFHSUMsV0FISjtBQUFBLE1BSUlDLFFBQVEsQ0FKWjs7QUFNQTtBQUNJQztBQUNBQztBQUNBQyx3QkFBc0JDLElBQXRCO0FBQ0o7O0FBRUE7QUFDQSxXQUFTSCxVQUFULEdBQXNCO0FBQ2xCZCxnQkFBWUUsT0FBT0wsS0FBUCxHQUFlSyxPQUFPZ0IsV0FBbEM7QUFDQWpCLGlCQUFhQyxPQUFPUCxNQUFQLEdBQWdCTyxPQUFPaUIsWUFBcEM7QUFDQWhCLFVBQU1ELE9BQU9rQixVQUFQLENBQWtCLElBQWxCLENBQU47QUFDQTVGLFlBQVFDLEdBQVIsQ0FBWTBFLEdBQVo7QUFDQUEsUUFBSWtCLEtBQUosQ0FBVWhCLGFBQVYsRUFBeUJDLGFBQXpCO0FBQ0g7O0FBRUQ7QUFDQSxXQUFTUyxTQUFULEdBQXFCO0FBQ2pCTCxvQkFBZ0JoSSxTQUFTaUMsYUFBVCxDQUF1QixRQUF2QixDQUFoQjtBQUNBYSxZQUFRQyxHQUFSLENBQVlpRixhQUFaO0FBQ0FBLGtCQUFjYixLQUFkLEdBQXNCTyxXQUF0QjtBQUNBTSxrQkFBY2YsTUFBZCxHQUF1QlMsV0FBdkI7QUFDQU8saUJBQWFELGNBQWNVLFVBQWQsQ0FBeUIsSUFBekIsQ0FBYjtBQUNBUixrQkFBY0QsV0FBV1csZUFBWCxDQUEyQmxCLFdBQTNCLEVBQXdDQSxXQUF4QyxDQUFkO0FBQ0g7O0FBRUQ7QUFDQSxXQUFTbUIsTUFBVCxHQUFrQjtBQUNkLFFBQUlDLEtBQUo7O0FBRUEsU0FBSyxJQUFJbEksSUFBSSxDQUFiLEVBQWdCQSxJQUFJbUgsc0JBQXBCLEVBQTRDbkgsS0FBSyxDQUFqRCxFQUFvRDtBQUNoRGtJLGNBQVNDLEtBQUtDLE1BQUwsS0FBZ0IsR0FBakIsR0FBd0IsQ0FBaEM7O0FBRUFkLGtCQUFZL0YsSUFBWixDQUFpQnZCLENBQWpCLElBQTBCa0ksS0FBMUI7QUFDQVosa0JBQVkvRixJQUFaLENBQWlCdkIsSUFBSSxDQUFyQixJQUEwQmtJLEtBQTFCO0FBQ0FaLGtCQUFZL0YsSUFBWixDQUFpQnZCLElBQUksQ0FBckIsSUFBMEJrSSxLQUExQjtBQUNBWixrQkFBWS9GLElBQVosQ0FBaUJ2QixJQUFJLENBQXJCLElBQTBCa0gsWUFBMUI7QUFDSDs7QUFFREcsZUFBV2dCLFlBQVgsQ0FBd0JmLFdBQXhCLEVBQXFDLENBQXJDLEVBQXdDLENBQXhDO0FBQ0g7O0FBRUQ7QUFDQSxXQUFTZ0IsSUFBVCxHQUFnQjtBQUNaekIsUUFBSTBCLFNBQUosQ0FBYyxDQUFkLEVBQWlCLENBQWpCLEVBQW9CN0IsU0FBcEIsRUFBK0JDLFVBQS9COztBQUVBRSxRQUFJMkIsU0FBSixHQUFnQjNCLElBQUk0QixhQUFKLENBQWtCckIsYUFBbEIsRUFBaUMsUUFBakMsQ0FBaEI7QUFDQVAsUUFBSTZCLFFBQUosQ0FBYSxDQUFiLEVBQWdCLENBQWhCLEVBQW1CaEMsU0FBbkIsRUFBOEJDLFVBQTlCO0FBQ0g7O0FBRUQsV0FBU2dCLElBQVQsR0FBZ0I7QUFDWixRQUFJLEVBQUVKLEtBQUYsR0FBVU4sc0JBQVYsS0FBcUMsQ0FBekMsRUFBNEM7QUFDeENnQjtBQUNBSztBQUNIOztBQUVEWiwwQkFBc0JDLElBQXRCO0FBQ0g7QUFFQTs7QUFPRDs7O0FBR0EsU0FBU2dCLFVBQVQsR0FBc0I7QUFDdEIsTUFBTUMsb0JBQW9CeEosU0FBU2lCLGFBQVQsQ0FBdUIsd0JBQXZCLENBQTFCO0FBQ0E7QUFDRXVJLG9CQUFrQnZKLGdCQUFsQixDQUFtQyxPQUFuQyxFQUE0QyxZQUFVO0FBQ3BEO0FBQ0EsUUFBSUQsU0FBU21FLElBQVQsQ0FBYy9ELFNBQWQsQ0FBd0JDLFFBQXhCLENBQWlDLHFCQUFqQyxDQUFKLEVBQTZEO0FBQzNETCxlQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsRUFBeUNsRCxTQUF6QyxDQUFtREcsTUFBbkQsQ0FBMEQscUJBQTFEO0FBQ0QsS0FGRCxNQUdLO0FBQ0hQLGVBQVNzRCxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxFQUF5Q2xELFNBQXpDLENBQW1EVSxHQUFuRCxDQUF1RCxxQkFBdkQ7QUFDRDtBQU1GLEdBYkQ7QUFlRDtBQUNEeUk7O0FBR0U7QUFDRjtBQUNBO0FBQ0E7OztBQU9BOztBQUVBLFNBQVNFLGlCQUFULEdBQTZCO0FBQzNCLE1BQUk5QyxhQUFhM0csU0FBU2lDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBakI7QUFDQTBFLGFBQVdDLFNBQVgsR0FBdUIsNkJBQXZCO0FBQ0E5RCxVQUFRQyxHQUFSLENBQVk0RCxVQUFaOztBQUdBO0FBQ0E7O0FBRUEsTUFBSStDLGlCQUFpQjFKLFNBQVNVLGdCQUFULENBQTBCLGlCQUExQixDQUFyQjtBQUNBLE1BQUlpSixjQUFjRCxlQUFlQSxlQUFlL0ksTUFBZixHQUF1QixDQUF0QyxDQUFsQjs7QUFFQSxNQUFJbUcsZUFBZTZDLFlBQVk1QyxxQkFBWixFQUFuQjtBQUNBLE1BQUlDLFdBQVdGLGFBQWFHLE1BQTVCO0FBQ0EsTUFBSUMsVUFBVUosYUFBYUssS0FBM0I7QUFDQXJFLFVBQVFDLEdBQVIsQ0FBWWlFLFFBQVo7QUFDQWxFLFVBQVFDLEdBQVIsQ0FBWW1FLE9BQVo7O0FBRUEsTUFBSTBDLHVCQUF1QjVKLFNBQVNVLGdCQUFULENBQTBCLGlCQUExQixDQUEzQjtBQUNBLE1BQUltSixlQUFlRCxxQkFBcUJBLHFCQUFxQmpKLE1BQXJCLEdBQTZCLENBQWxELENBQW5COztBQUVBa0osZUFBYXhILFdBQWIsQ0FBeUJzRSxVQUF6QjtBQUNBbEUsU0FBT3pCLGFBQVAsR0FBdUJoQixTQUFTaUIsYUFBVCxDQUF1QixpQkFBdkIsQ0FBdkI7QUFDQTZCLFVBQVFDLEdBQVIsQ0FBWS9CLGFBQVo7QUFDRjhJO0FBQ0UsV0FBU0EsT0FBVCxHQUFtQjtBQUNqQmhILFlBQVFDLEdBQVIsQ0FBWSxnQkFBWjtBQUNGLFFBQUl1RSxTQUFKO0FBQUEsUUFDSUMsVUFESjtBQUFBLFFBRUlDLFNBQVN4RyxhQUZiO0FBQUEsUUFHSXlHLEdBSEo7O0FBS0E7QUFDQSxRQUFJQyxjQUFjLEVBQWxCO0FBQUEsUUFDSUMsZ0JBQWdCLENBRHBCO0FBQUEsUUFFSUMsZ0JBQWdCLENBRnBCO0FBQUEsUUFHSUMseUJBQXlCLENBSDdCO0FBQUEsUUFJSUMsZUFBZSxFQUpuQixDQVJtQixDQVlJOztBQUV2QixRQUFJQyx5QkFBeUJMLGNBQWNBLFdBQWQsR0FBNEIsQ0FBekQ7QUFBQSxRQUNJTSxhQURKO0FBQUEsUUFFSUMsVUFGSjtBQUFBLFFBR0lDLFdBSEo7QUFBQSxRQUlJQyxRQUFRLENBSlo7O0FBTUE7QUFDSUM7QUFDQUM7QUFDQUMsMEJBQXNCQyxJQUF0QjtBQUNKOztBQUVBO0FBQ0EsYUFBU0gsVUFBVCxHQUFzQjtBQUNsQmQsa0JBQVlFLE9BQU9MLEtBQVAsR0FBZUssT0FBT2dCLFdBQWxDO0FBQ0FqQixtQkFBYUMsT0FBT1AsTUFBUCxHQUFnQk8sT0FBT2lCLFlBQXBDO0FBQ0FoQixZQUFNRCxPQUFPa0IsVUFBUCxDQUFrQixJQUFsQixDQUFOOztBQUVBakIsVUFBSWtCLEtBQUosQ0FBVWhCLGFBQVYsRUFBeUJDLGFBQXpCO0FBQ0g7O0FBRUQ7QUFDQSxhQUFTUyxTQUFULEdBQXFCO0FBQ2pCTCxzQkFBZ0JoSSxTQUFTaUMsYUFBVCxDQUF1QixRQUF2QixDQUFoQjtBQUNBK0Ysb0JBQWNiLEtBQWQsR0FBc0JPLFdBQXRCO0FBQ0FNLG9CQUFjZixNQUFkLEdBQXVCUyxXQUF2QjtBQUNBTyxtQkFBYUQsY0FBY1UsVUFBZCxDQUF5QixJQUF6QixDQUFiO0FBQ0FSLG9CQUFjRCxXQUFXVyxlQUFYLENBQTJCbEIsV0FBM0IsRUFBd0NBLFdBQXhDLENBQWQ7QUFDSDs7QUFFRDtBQUNBLGFBQVNtQixNQUFULEdBQWtCO0FBQ2QsVUFBSUMsS0FBSjs7QUFFQSxXQUFLLElBQUlsSSxJQUFJLENBQWIsRUFBZ0JBLElBQUltSCxzQkFBcEIsRUFBNENuSCxLQUFLLENBQWpELEVBQW9EO0FBQ2hEa0ksZ0JBQVNDLEtBQUtDLE1BQUwsS0FBZ0IsR0FBakIsR0FBd0IsQ0FBaEM7O0FBRUFkLG9CQUFZL0YsSUFBWixDQUFpQnZCLENBQWpCLElBQTBCa0ksS0FBMUI7QUFDQVosb0JBQVkvRixJQUFaLENBQWlCdkIsSUFBSSxDQUFyQixJQUEwQmtJLEtBQTFCO0FBQ0FaLG9CQUFZL0YsSUFBWixDQUFpQnZCLElBQUksQ0FBckIsSUFBMEJrSSxLQUExQjtBQUNBWixvQkFBWS9GLElBQVosQ0FBaUJ2QixJQUFJLENBQXJCLElBQTBCa0gsWUFBMUI7QUFDSDs7QUFFREcsaUJBQVdnQixZQUFYLENBQXdCZixXQUF4QixFQUFxQyxDQUFyQyxFQUF3QyxDQUF4QztBQUNIOztBQUVEO0FBQ0EsYUFBU2dCLElBQVQsR0FBZ0I7QUFDWnpCLFVBQUkwQixTQUFKLENBQWMsQ0FBZCxFQUFpQixDQUFqQixFQUFvQjdCLFNBQXBCLEVBQStCQyxVQUEvQjs7QUFFQUUsVUFBSTJCLFNBQUosR0FBZ0IzQixJQUFJNEIsYUFBSixDQUFrQnJCLGFBQWxCLEVBQWlDLFFBQWpDLENBQWhCO0FBQ0FQLFVBQUk2QixRQUFKLENBQWEsQ0FBYixFQUFnQixDQUFoQixFQUFtQmhDLFNBQW5CLEVBQThCQyxVQUE5QjtBQUNIOztBQUVELGFBQVNnQixJQUFULEdBQWdCO0FBQ1osVUFBSSxFQUFFSixLQUFGLEdBQVVOLHNCQUFWLEtBQXFDLENBQXpDLEVBQTRDO0FBQ3hDZ0I7QUFDQUs7QUFDSDs7QUFFRFosNEJBQXNCQyxJQUF0QjtBQUNIO0FBRUE7QUFDQTs7QUFPRDs7O0FBR0E5RixPQUFPc0gsUUFBUCxHQUFrQixZQUFXO0FBQzdCLE1BQUlwRSxZQUFZbEQsT0FBT2dELFdBQVAsSUFBc0IsQ0FBQ3pGLFNBQVMwRixlQUFULElBQTRCMUYsU0FBU21FLElBQVQsQ0FBY21CLFVBQTFDLElBQXdEdEYsU0FBU21FLElBQWxFLEVBQXdFd0IsU0FBOUc7O0FBRUEsTUFBSXRDLE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDs7QUFFQSxNQUFJcUMsWUFBWSxFQUFoQixFQUFvQjtBQUNsQixRQUFJdEMsT0FBT3JELFNBQVNzRCxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxDQUFYO0FBQ0FELFNBQUtqRCxTQUFMLENBQWVVLEdBQWYsQ0FBbUIsb0JBQW5CO0FBR0QsR0FMRCxNQU1LO0FBQ0gsUUFBSXVDLE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDtBQUNBRCxTQUFLakQsU0FBTCxDQUFlRyxNQUFmLENBQXNCLG9CQUF0QjtBQUVEO0FBRUEsQ0FqQkQ7O0FBdUJGLFNBQVN5SixZQUFULEdBQXdCO0FBQ3RCO0FBQ0ksTUFBSUMsUUFBUWpLLFNBQVNpQixhQUFULENBQXVCLG1CQUF2QixDQUFaOztBQUVBOztBQUVBLFdBQVNpSixTQUFULEdBQXFCO0FBQ2pCLFFBQUlELE1BQU1FLFVBQU4sS0FBcUIsQ0FBekIsRUFBNEI7QUFDeEI7QUFDQXJILGNBQVFDLEdBQVIsQ0FBWSxjQUFaOztBQUVBa0gsWUFBTUcsSUFBTjtBQUNILEtBTEQsTUFLTztBQUNIOUgsaUJBQVc0SCxTQUFYLEVBQXNCLEdBQXRCO0FBQ0g7QUFDSjs7QUFFREE7QUFDSjtBQUNEOztBQU1DOztBQUVBLFNBQVN6RyxVQUFULEdBQXNCO0FBQ3hCO0FBQ0EsTUFBSXpELFNBQVNtRSxJQUFULENBQWMvRCxTQUFkLENBQXdCQyxRQUF4QixDQUFpQyxvQkFBakMsQ0FBSixFQUE0RDtBQUUxRDtBQUYwRCxRQUdqRGdLLFNBSGlELEdBRzFELFNBQVNBLFNBQVQsR0FBcUI7QUFDckI1SCxhQUFPQyxXQUFQLEdBQXFCMUMsU0FBU1UsZ0JBQVQsQ0FBMEIsa0JBQTFCLENBQXJCO0FBQ0FpQztBQUNDLEtBTnlEOztBQUMxRDJIO0FBTUE3SCxXQUFPeEMsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0NxSyxzQkFBbEM7QUFDQUQ7QUFDQW5KO0FBQ0E7QUFDQVY7QUFDRDtBQUNELE1BQUlSLFNBQVNtRSxJQUFULENBQWMvRCxTQUFkLENBQXdCQyxRQUF4QixDQUFpQywyQkFBakMsQ0FBSixFQUFtRTtBQUMvRDtBQUQrRCxRQUV4RGdLLFVBRndELEdBRWpFLFNBQVNBLFVBQVQsR0FBcUI7QUFDckI1SCxhQUFPQyxXQUFQLEdBQXFCMUMsU0FBU1UsZ0JBQVQsQ0FBMEIsaUJBQTFCLENBQXJCO0FBQ0FpQztBQUNDLEtBTGdFOztBQU9qRTBIO0FBQ0E7QUFDQXRKO0FBRUQ7O0FBRUQsTUFBSWYsU0FBU21FLElBQVQsQ0FBYy9ELFNBQWQsQ0FBd0JDLFFBQXhCLENBQWlDLDBCQUFqQyxDQUFKLEVBQWtFO0FBQzlEO0FBRDhELFFBRXZEZ0ssV0FGdUQsR0FFaEUsU0FBU0EsV0FBVCxHQUFxQjtBQUNyQjVILGFBQU9DLFdBQVAsR0FBcUIxQyxTQUFTVSxnQkFBVCxDQUEwQixpQkFBMUIsQ0FBckI7QUFDQWlDO0FBQ0MsS0FMK0Q7O0FBT2hFMEg7QUFDRjtBQUNFdEo7QUFFRDs7QUFFRCxNQUFJZixTQUFTbUUsSUFBVCxDQUFjL0QsU0FBZCxDQUF3QkMsUUFBeEIsQ0FBaUMsdUJBQWpDLENBQUosRUFBK0Q7QUFDM0Q7QUFDQTJKO0FBQ0ExSCxlQUFXLFlBQVU7QUFDakJ0QyxlQUFTaUIsYUFBVCxDQUF1QixxQkFBdkIsRUFBOENTLEtBQTlDLENBQW9EQyxPQUFwRCxHQUE4RCxHQUE5RDtBQUNMLEtBRkMsRUFFQyxJQUZEO0FBS0g7QUFLRTs7QUFHRDhCOztBQUlBLElBQUk4RyxXQUFXQyxVQUFVQyxTQUFWLENBQW9CQyxPQUFwQixDQUE0QixRQUE1QixLQUF5QyxDQUFDLENBQXpEOztBQUVBLElBQUlILFFBQUosRUFBYztBQUNaLE1BQUlsSCxPQUFPckQsU0FBU3NELG9CQUFULENBQThCLE1BQTlCLEVBQXNDLENBQXRDLENBQVg7QUFDRUQsT0FBS2pELFNBQUwsQ0FBZVUsR0FBZixDQUFtQixTQUFuQjtBQUNILENBSEQsTUFJSztBQUNIO0FBQ0E7QUFDRDs7QUFFRCxJQUFJNkosV0FBV0gsVUFBVUksTUFBVixJQUFvQkosVUFBVUksTUFBVixDQUFpQkYsT0FBakIsQ0FBeUIsT0FBekIsSUFBb0MsQ0FBQyxDQUF6RCxJQUNBRixVQUFVQyxTQURWLElBRUFELFVBQVVDLFNBQVYsQ0FBb0JDLE9BQXBCLENBQTRCLE9BQTVCLEtBQXdDLENBQUMsQ0FGekMsSUFHQUYsVUFBVUMsU0FBVixDQUFvQkMsT0FBcEIsQ0FBNEIsT0FBNUIsS0FBd0MsQ0FBQyxDQUh4RDs7QUFNZSxJQUFJQyxRQUFKLEVBQWM7QUFDWixNQUFJdEgsT0FBT3JELFNBQVNzRCxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxDQUFYO0FBQ0VELE9BQUtqRCxTQUFMLENBQWVVLEdBQWYsQ0FBbUIsU0FBbkI7QUFFSCxDQUpELE1BS0s7QUFDTDtBQUNBO0FBQ0M7O0FBSWhCd0IsV0FBVyxZQUFVO0FBQ3ZCLE1BQUllLE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDtBQUNBRCxPQUFLakQsU0FBTCxDQUFlRyxNQUFmLENBQXNCLGlCQUF0QjtBQUNFK0IsYUFBVyxZQUFVO0FBQ25CZSxTQUFLakQsU0FBTCxDQUFlVSxHQUFmLENBQW1CLG9CQUFuQjtBQUNFd0IsZUFBVyxZQUFVO0FBQ2ZlLFdBQUtqRCxTQUFMLENBQWVHLE1BQWYsQ0FBc0IsY0FBdEI7QUFDQThDLFdBQUtqRCxTQUFMLENBQWVHLE1BQWYsQ0FBc0Isb0JBQXRCO0FBQ0wsS0FIRCxFQUdHLElBSEg7QUFJSCxHQU5ELEVBTUcsQ0FOSDtBQU9BK0IsYUFBVyxZQUFVO0FBQ3ZCdEMsYUFBU2lCLGFBQVQsQ0FBdUIsZ0JBQXZCLEVBQXlDUyxLQUF6QyxDQUErQ2EsT0FBL0MsR0FBeUQsTUFBekQ7QUFDQTtBQUNHLEdBSEQsRUFHRyxHQUhIO0FBSUQsQ0FkQyxFQWNDLEdBZEQ7O0FBc0JGRSxPQUFPb0ksVUFBUCxHQUFvQixVQUFVeEosS0FBVixFQUFpQjtBQUNuQyxNQUFJQSxNQUFNeUosS0FBVixFQUFpQjtBQUNmO0FBQ0E7QUFDTSxRQUFJekgsT0FBT3JELFNBQVNzRCxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxDQUFYO0FBQ04sUUFBSUQsS0FBS2pELFNBQUwsQ0FBZUMsUUFBZixDQUF3QixzQkFBeEIsQ0FBSixFQUFxRDtBQUN6RCtGO0FBQ0s7QUFDRCxRQUFJMkUsYUFBYUMsU0FBU3ZKLElBQTFCO0FBQ0FxQixZQUFRQyxHQUFSLENBQVlnSSxVQUFaO0FBQ0QsR0FURCxNQVNPO0FBQ0wsUUFBSTFILE9BQU9yRCxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDtBQUNBO0FBQ0EsUUFBSUQsS0FBS2pELFNBQUwsQ0FBZUMsUUFBZixDQUF3QixzQkFBeEIsQ0FBSixFQUFxRDtBQUN6RCtGO0FBQ0s7O0FBRUQsUUFBSTJFLGFBQWFDLFNBQVN2SixJQUExQjtBQUNBcUIsWUFBUUMsR0FBUixDQUFZZ0ksVUFBWjtBQUNBLFFBQUksY0FBY0UsSUFBZCxDQUFtQnhJLE9BQU91SSxRQUFQLENBQWdCdkosSUFBbkMsQ0FBSixFQUE4QztBQUM1Q3FCLGNBQVFDLEdBQVIsQ0FBWSxnQkFBWjtBQUNBaUksZUFBU0UsT0FBVCxDQUFpQkgsVUFBakI7QUFDRDtBQUNELFFBQUksYUFBYUUsSUFBYixDQUFrQnhJLE9BQU91SSxRQUFQLENBQWdCdkosSUFBbEMsQ0FBSixFQUE2QztBQUMzQ3FCLGNBQVFDLEdBQVIsQ0FBWSxnQkFBWjtBQUNBaUksZUFBU0UsT0FBVCxDQUFpQkgsVUFBakI7QUFDRDtBQUVGO0FBQ0YsQ0E3QkQ7O0FBaUNBLFNBQVNULHNCQUFULEdBQWtDO0FBQ2hDLE1BQUk3SCxPQUFPMEksVUFBUCxHQUFvQixHQUF4QixFQUE2QjtBQUMzQjtBQUNGLFFBQUlDLGVBQWUzSSxPQUFPNEksV0FBMUI7QUFDQSxRQUFJQyxhQUFhdEwsU0FBU2lCLGFBQVQsQ0FBdUIsY0FBdkIsQ0FBakI7QUFDQSxRQUFHLE9BQU9xSyxVQUFQLElBQXNCLFdBQXRCLElBQXFDQSxjQUFjLElBQXRELEVBQTJEO0FBQzNEdEwsZUFBU2lCLGFBQVQsQ0FBdUIsY0FBdkIsRUFBdUNTLEtBQXZDLENBQTZDdUYsTUFBN0MsR0FBdURtRSxlQUFlLElBQXRFO0FBQ0c7QUFDRixHQVBELE1BUUs7QUFDSCxRQUFJRSxhQUFhdEwsU0FBU2lCLGFBQVQsQ0FBdUIsY0FBdkIsQ0FBakI7QUFDQSxRQUFHLE9BQU9xSyxVQUFQLElBQXNCLFdBQXRCLElBQXFDQSxjQUFjLElBQXRELEVBQTJEO0FBQzNEdEwsZUFBU2lCLGFBQVQsQ0FBdUIsY0FBdkIsRUFBdUMyRSxlQUF2QyxDQUF1RCxPQUF2RDtBQUNHO0FBQ0Q7QUFDSDtBQUNGOztBQUlELElBQUcsT0FBT0ksY0FBUCxJQUEwQixXQUExQixJQUF5Q0Esa0JBQWtCLElBQTlELEVBQW1FO0FBQ2pFQztBQUNELENBRkQsTUFFTTtBQUNKO0FBQ0Q7O0FBT0R4RCxPQUFPeEMsZ0JBQVAsQ0FBd0IsWUFBeEIsRUFBc0MsU0FBU3NMLFlBQVQsR0FBd0I7QUFDNUQ7QUFDQSxNQUFJbEksT0FBT3JELFNBQVNzRCxvQkFBVCxDQUE4QixNQUE5QixFQUFzQyxDQUF0QyxDQUFYO0FBQ0FELE9BQUtqRCxTQUFMLENBQWVVLEdBQWYsQ0FBbUIsY0FBbkI7O0FBTUE7QUFDQTJCLFNBQU8rSSxtQkFBUCxDQUEyQixZQUEzQixFQUF5Q0QsWUFBekMsRUFBdUQsS0FBdkQ7QUFDRCxDQVhELEVBV0csS0FYSCxFOzs7Ozs7O0FDdGxDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakI7OztBQUdBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDJCQUEyQjs7QUFFM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QjtBQUM1QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBQUk7O0FBRUo7QUFDQSwrQ0FBK0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0EsSUFBSTs7QUFFSjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBQUk7O0FBRUo7QUFDQTtBQUNBO0FBQ0EsSUFBSTs7QUFFSjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLHFCQUFxQiw2REFBNkQ7QUFDbEY7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLHlCQUF5QjtBQUMzQztBQUNBLEVBQUU7O0FBRUY7QUFDQSxDQUFDOztBQUVELHVCOzs7Ozs7QUNsU0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxTQUFTO0FBQ3BCLFdBQVcsUUFBUTtBQUNuQixZQUFZO0FBQ1o7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLFNBQVM7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUMzQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsT0FBTztBQUNsQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FDaENBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVELGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0EsQ0FBQzs7QUFFRCx3Qjs7Ozs7OztBQzNEQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLG1DQUFtQyxTQUFTLHdDQUF3QztBQUNwRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLElBQUk7QUFDSixHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGOztBQUVBLDJCOzs7Ozs7O0FDckhBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDJCOzs7Ozs7O0FDakJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjs7QUFFQSxzQzs7Ozs7OztBQ2JBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVELG9HQUFvRyxtQkFBbUIsRUFBRSxtQkFBbUIsOEhBQThIOztBQUUxUTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGlHQUFpRztBQUNqRztBQUNBLElBQUk7QUFDSjtBQUNBOztBQUVBLGdCQUFnQix1QkFBdUI7QUFDdkM7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsa0M7Ozs7Ozs7QUNsREE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsMEJBQTBCOztBQUUxQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBLHdCOzs7Ozs7O0FDeENBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGdDOzs7Ozs7O0FDeEJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBOztBQUVBLGdDOzs7Ozs7O0FDVEE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7O0FBRUEsZ0JBQWdCLHVCQUF1QjtBQUN2QztBQUNBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7QUMzQkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0EsQ0FBQzs7QUFFRCx1Qjs7Ozs7OztBQ3ZEQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLGlDQUFpQyxTQUFTLHNCQUFzQjtBQUNoRTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLGdCQUFnQix3QkFBd0I7QUFDeEM7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsS0FBSztBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSixHQUFHO0FBQ0gsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLDZCOzs7Ozs7O0FDbEZBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBLG1EQUFtRCxvQkFBb0I7QUFDdkU7QUFDQTs7QUFFQSwrQjs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7O0FBRUEscUI7Ozs7Ozs7QUNiQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBLHNCOzs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxtQzs7Ozs7OztBQ2RBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSixHQUFHO0FBQ0g7QUFDQSxFQUFFO0FBQ0Y7QUFDQTs7QUFFQSx1Qzs7Ozs7OztBQzFCQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0Esd0NBQXdDO0FBQ3hDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDhCOzs7Ozs7O0FDekJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxFQUFFO0FBQ0Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRixFOzs7Ozs7O0FDckRBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVELG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQLGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQjs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQSw2QkFBNkI7QUFDN0I7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBLElBQUk7QUFDSjtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQSxDQUFDOztBQUVELGtDOzs7Ozs7O0FDNUVBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVELGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQjs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsdUVBQXVFLGFBQWE7QUFDcEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQSxDQUFDOztBQUVELDhCOzs7Ozs7O0FDdkdBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVELG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQLGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsSUFBSTtBQUNKOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7O0FBRUo7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUgsNkJBQTZCOztBQUU3QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0EsS0FBSztBQUNMLElBQUk7QUFDSjtBQUNBLEVBQUU7O0FBRUY7QUFDQSxDQUFDOztBQUVELDJCOzs7Ozs7QUN6SkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZLFFBQVE7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkIsWUFBWSxRQUFRO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUIsbUJBQW1CO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUIsbUJBQW1CO0FBQ3RDOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCQUF1QixrQkFBa0I7QUFDekM7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLFFBQVE7QUFDcEIsWUFBWSxPQUFPO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZLE9BQU87QUFDbkIsWUFBWSxRQUFRO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7O0FBRUEsaUJBQWlCLGlCQUFpQjtBQUNsQztBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVksT0FBTztBQUNuQixZQUFZLFFBQVE7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZLE9BQU87QUFDbkIsWUFBWSxRQUFRO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCLG1CQUFtQjtBQUNwQzs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLDZEQUE2RDtBQUMzRTtBQUNBLFlBQVksc0JBQXNCO0FBQ2xDLFlBQVksT0FBTztBQUNuQixZQUFZLFFBQVE7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxvQ0FBb0MsT0FBTztBQUMzQzs7QUFFQSxtQ0FBbUMsT0FBTztBQUMxQzs7Ozs7Ozs7QUM5V0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSx1RUFBdUUsYUFBYTtBQUNwRjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QywrREFBK0Q7QUFDeEc7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0EsQ0FBQzs7QUFFRCxnQzs7Ozs7O0FDcElBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsU0FBUztBQUNwQixXQUFXLFFBQVE7QUFDbkIsWUFBWTtBQUNaO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxxQkFBcUI7QUFDaEMsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLFNBQVM7QUFDcEIsV0FBVyxRQUFRO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLFNBQVM7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7QUM3RUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsT0FBTztBQUNsQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7O0FDaENBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVELG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQLGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxtQ0FBbUM7QUFDbkM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsYUFBYTs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQSxDQUFDOztBQUVELCtCOzs7Ozs7O0FDdEtBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVELG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZ0VBQWdFO0FBQ2hFLHdDQUF3QztBQUN4QyxrQ0FBa0M7QUFDbEMsc0NBQXNDO0FBQ3RDLG9DQUFvQztBQUNwQztBQUNBOztBQUVBO0FBQ0EsTUFBTSwwQkFBMEI7O0FBRWhDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMOztBQUVBLHVCOzs7Ozs7Ozs7QUN4SEE7OztBQUlBLFNBQVMvSSxRQUFULENBQWtCaUosV0FBbEIsRUFBNEU7QUFBQSxNQUE3Q0MsUUFBNkMsdUVBQWxDLEdBQWtDO0FBQUEsTUFBN0JDLE1BQTZCLHVFQUFwQixRQUFvQjtBQUFBLE1BQVZDLFFBQVU7OztBQUUxRSxNQUFNQyxVQUFVO0FBQ2RDLFVBRGMsa0JBQ1BDLENBRE8sRUFDSjtBQUNSLGFBQU9BLENBQVA7QUFDRCxLQUhhO0FBSWRDLGNBSmMsc0JBSUhELENBSkcsRUFJQTtBQUNaLGFBQU9BLElBQUlBLENBQVg7QUFDRCxLQU5hO0FBT2RFLGVBUGMsdUJBT0ZGLENBUEUsRUFPQztBQUNiLGFBQU9BLEtBQUssSUFBSUEsQ0FBVCxDQUFQO0FBQ0QsS0FUYTtBQVVkRyxpQkFWYyx5QkFVQUgsQ0FWQSxFQVVHO0FBQ2YsYUFBT0EsSUFBSSxHQUFKLEdBQVUsSUFBSUEsQ0FBSixHQUFRQSxDQUFsQixHQUFzQixDQUFDLENBQUQsR0FBSyxDQUFDLElBQUksSUFBSUEsQ0FBVCxJQUFjQSxDQUFoRDtBQUNELEtBWmE7QUFhZEksZUFiYyx1QkFhRkosQ0FiRSxFQWFDO0FBQ2IsYUFBT0EsSUFBSUEsQ0FBSixHQUFRQSxDQUFmO0FBQ0QsS0FmYTtBQWdCZEssZ0JBaEJjLHdCQWdCREwsQ0FoQkMsRUFnQkU7QUFDZCxhQUFRLEVBQUVBLENBQUgsR0FBUUEsQ0FBUixHQUFZQSxDQUFaLEdBQWdCLENBQXZCO0FBQ0QsS0FsQmE7QUFtQmRNLGtCQW5CYywwQkFtQkNOLENBbkJELEVBbUJJO0FBQ2hCLGFBQU9BLElBQUksR0FBSixHQUFVLElBQUlBLENBQUosR0FBUUEsQ0FBUixHQUFZQSxDQUF0QixHQUEwQixDQUFDQSxJQUFJLENBQUwsS0FBVyxJQUFJQSxDQUFKLEdBQVEsQ0FBbkIsS0FBeUIsSUFBSUEsQ0FBSixHQUFRLENBQWpDLElBQXNDLENBQXZFO0FBQ0QsS0FyQmE7QUFzQmRPLGVBdEJjLHVCQXNCRlAsQ0F0QkUsRUFzQkM7QUFDYixhQUFPQSxJQUFJQSxDQUFKLEdBQVFBLENBQVIsR0FBWUEsQ0FBbkI7QUFDRCxLQXhCYTtBQXlCZFEsZ0JBekJjLHdCQXlCRFIsQ0F6QkMsRUF5QkU7QUFDZCxhQUFPLElBQUssRUFBRUEsQ0FBSCxHQUFRQSxDQUFSLEdBQVlBLENBQVosR0FBZ0JBLENBQTNCO0FBQ0QsS0EzQmE7QUE0QmRTLGtCQTVCYywwQkE0QkNULENBNUJELEVBNEJJO0FBQ2hCLGFBQU9BLElBQUksR0FBSixHQUFVLElBQUlBLENBQUosR0FBUUEsQ0FBUixHQUFZQSxDQUFaLEdBQWdCQSxDQUExQixHQUE4QixJQUFJLElBQUssRUFBRUEsQ0FBUCxHQUFZQSxDQUFaLEdBQWdCQSxDQUFoQixHQUFvQkEsQ0FBN0Q7QUFDRCxLQTlCYTtBQStCZFUsZUEvQmMsdUJBK0JGVixDQS9CRSxFQStCQztBQUNiLGFBQU9BLElBQUlBLENBQUosR0FBUUEsQ0FBUixHQUFZQSxDQUFaLEdBQWdCQSxDQUF2QjtBQUNELEtBakNhO0FBa0NkVyxnQkFsQ2Msd0JBa0NEWCxDQWxDQyxFQWtDRTtBQUNkLGFBQU8sSUFBSyxFQUFFQSxDQUFILEdBQVFBLENBQVIsR0FBWUEsQ0FBWixHQUFnQkEsQ0FBaEIsR0FBb0JBLENBQS9CO0FBQ0QsS0FwQ2E7QUFxQ2RZLGtCQXJDYywwQkFxQ0NaLENBckNELEVBcUNJO0FBQ2hCLGFBQU9BLElBQUksR0FBSixHQUFVLEtBQUtBLENBQUwsR0FBU0EsQ0FBVCxHQUFhQSxDQUFiLEdBQWlCQSxDQUFqQixHQUFxQkEsQ0FBL0IsR0FBbUMsSUFBSSxLQUFNLEVBQUVBLENBQVIsR0FBYUEsQ0FBYixHQUFpQkEsQ0FBakIsR0FBcUJBLENBQXJCLEdBQXlCQSxDQUF2RTtBQUNEO0FBdkNhLEdBQWhCOztBQTBDQSxNQUFNYSxRQUFRbkssT0FBT2dELFdBQXJCO0FBQ0EsTUFBTW9ILFlBQVksU0FBU3BLLE9BQU9xSyxXQUFoQixHQUE4QkEsWUFBWUMsR0FBWixFQUE5QixHQUFrRCxJQUFJQyxJQUFKLEdBQVdDLE9BQVgsRUFBcEU7O0FBRUEsTUFBTUMsaUJBQWlCbkUsS0FBS29FLEdBQUwsQ0FBU25OLFNBQVNtRSxJQUFULENBQWNpSixZQUF2QixFQUFxQ3BOLFNBQVNtRSxJQUFULENBQWNrSixZQUFuRCxFQUFpRXJOLFNBQVMwRixlQUFULENBQXlCK0MsWUFBMUYsRUFBd0d6SSxTQUFTMEYsZUFBVCxDQUF5QjBILFlBQWpJLEVBQStJcE4sU0FBUzBGLGVBQVQsQ0FBeUIySCxZQUF4SyxDQUF2QjtBQUNBLE1BQU1DLGVBQWU3SyxPQUFPNEksV0FBUCxJQUFzQnJMLFNBQVMwRixlQUFULENBQXlCK0MsWUFBL0MsSUFBK0R6SSxTQUFTc0Qsb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsRUFBeUNtRixZQUE3SDtBQUNBLE1BQU04RSxvQkFBb0IsT0FBTzlCLFdBQVAsS0FBdUIsUUFBdkIsR0FBa0NBLFdBQWxDLEdBQWdEQSxZQUFZK0IsU0FBdEY7QUFDQSxNQUFNQyw0QkFBNEIxRSxLQUFLMkUsS0FBTCxDQUFXUixpQkFBaUJLLGlCQUFqQixHQUFxQ0QsWUFBckMsR0FBb0RKLGlCQUFpQkksWUFBckUsR0FBb0ZDLGlCQUEvRixDQUFsQzs7QUFFQSxNQUFJLDJCQUEyQjlLLE1BQTNCLEtBQXNDLEtBQTFDLEVBQWlEO0FBQy9DQSxXQUFPa0wsTUFBUCxDQUFjLENBQWQsRUFBaUJGLHlCQUFqQjtBQUNBLFFBQUk3QixRQUFKLEVBQWM7QUFDWkE7QUFDRDtBQUNEO0FBQ0Q7O0FBRUQsV0FBUytCLE1BQVQsR0FBa0I7QUFDaEIsUUFBTVosTUFBTSxTQUFTdEssT0FBT3FLLFdBQWhCLEdBQThCQSxZQUFZQyxHQUFaLEVBQTlCLEdBQWtELElBQUlDLElBQUosR0FBV0MsT0FBWCxFQUE5RDtBQUNBLFFBQU1XLE9BQU83RSxLQUFLOEUsR0FBTCxDQUFTLENBQVQsRUFBYSxDQUFDZCxNQUFNRixTQUFQLElBQW9CbkIsUUFBakMsQ0FBYjtBQUNBLFFBQU1vQyxlQUFlakMsUUFBUUYsTUFBUixFQUFnQmlDLElBQWhCLENBQXJCO0FBQ0FuTCxXQUFPa0wsTUFBUCxDQUFjLENBQWQsRUFBaUI1RSxLQUFLZ0YsSUFBTCxDQUFXRCxnQkFBZ0JMLDRCQUE0QmIsS0FBNUMsQ0FBRCxHQUF1REEsS0FBakUsQ0FBakI7O0FBRUEsUUFBSW5LLE9BQU9nRCxXQUFQLEtBQXVCZ0kseUJBQTNCLEVBQXNEO0FBQ3BELFVBQUk3QixRQUFKLEVBQWM7QUFDWkE7QUFDRDtBQUNEO0FBQ0Q7O0FBRUR0RCwwQkFBc0JxRixNQUF0QjtBQUNEOztBQUVEQTtBQUNEOztBQUdEbEwsT0FBT0QsUUFBUCxHQUFrQkEsUUFBbEIsQzs7Ozs7O0FDcEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0Esc0NBQXNDOztBQUV0Qzs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsaUNBQWlDO0FBQzlDLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw0Q0FBNEM7QUFDNUM7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4QixvQkFBb0I7O0FBRWxEO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsRUFBRTs7QUFFRjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsU0FBUyxXQUFXOztBQUVwQix5REFBeUQ7O0FBRXpELHNHQUFzRyxnQ0FBZ0M7O0FBRXRJO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCO0FBQ3hCLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsaUJBQWlCO0FBQzdDOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsYUFBYTs7QUFFYjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0EsMkJBQTJCLFNBQVM7QUFDcEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0osR0FBRzs7QUFFSDtBQUNBLHdCQUF3QjtBQUN4Qjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsaU1BQWlNOztBQUVqTTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0EsdUVBQXVFLGlEQUFpRDtBQUN4SCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7O0FBR0Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0Esb0NBQW9DLFNBQVM7QUFDN0M7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1EQUFtRCxtQ0FBbUM7O0FBRXRGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBUyxTQUFTO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FDdnRCQSx5Qzs7Ozs7OztBQ0FBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFlBQVksTUFBTTtBQUNsQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOzs7Ozs7OztBQ3BEQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdEQUFnRDtBQUNoRDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxnREFBZ0Q7QUFDaEQ7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7QUM3RkE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsU0FBUztBQUNwQixXQUFXLFNBQVM7QUFDcEI7QUFDQSxZQUFZLE9BQU87QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxTQUFTO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTs7Ozs7Ozs7QUNuREE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CLHVDQUF1QztBQUN2QztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7QUFDSDs7Ozs7Ozs7QUM5RUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxjQUFjO0FBQ3pCLFdBQVcsTUFBTTtBQUNqQixXQUFXLGVBQWU7QUFDMUIsYUFBYSxFQUFFO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7Ozs7OztBQ25CQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLHNCQUFzQjtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUNBQXFDOztBQUVyQztBQUNBO0FBQ0E7O0FBRUEsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QixVQUFVOzs7Ozs7OztBQ3ZMdEM7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7Ozs7OztBQ1hBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsU0FBUztBQUNwQixXQUFXLFNBQVM7QUFDcEIsV0FBVyxPQUFPO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ3hCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE1BQU07QUFDakIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsTUFBTTtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FDekNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ25CQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQ2JBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUNiQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsaUJBQWlCLGVBQWU7O0FBRWhDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7Ozs7Ozs7O0FDcERBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsT0FBTztBQUNyQixnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLE9BQU87QUFDckIsZ0JBQWdCLFFBQVE7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOzs7Ozs7OztBQ25FQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsMENBQTBDO0FBQzFDLFNBQVM7O0FBRVQ7QUFDQSw0REFBNEQsd0JBQXdCO0FBQ3BGO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDO0FBQ2xDLCtCQUErQixhQUFhLEVBQUU7QUFDOUM7QUFDQTtBQUNBLEtBQUs7QUFDTDs7Ozs7Ozs7QUNwREE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFNBQVM7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7OztBQ3hEQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQSxXQUFXLFNBQVM7QUFDcEIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAxMik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgMzgxOGY2ZTk2NDcyZjFlZTNjYTQiLCIndXNlIHN0cmljdCc7XG5cbnZhciBiaW5kID0gcmVxdWlyZSgnLi9oZWxwZXJzL2JpbmQnKTtcblxuLypnbG9iYWwgdG9TdHJpbmc6dHJ1ZSovXG5cbi8vIHV0aWxzIGlzIGEgbGlicmFyeSBvZiBnZW5lcmljIGhlbHBlciBmdW5jdGlvbnMgbm9uLXNwZWNpZmljIHRvIGF4aW9zXG5cbnZhciB0b1N0cmluZyA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmc7XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYW4gQXJyYXlcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhbiBBcnJheSwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzQXJyYXkodmFsKSB7XG4gIHJldHVybiB0b1N0cmluZy5jYWxsKHZhbCkgPT09ICdbb2JqZWN0IEFycmF5XSc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgdW5kZWZpbmVkXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdGhlIHZhbHVlIGlzIHVuZGVmaW5lZCwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzVW5kZWZpbmVkKHZhbCkge1xuICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ3VuZGVmaW5lZCc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBCdWZmZXJcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIEJ1ZmZlciwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzQnVmZmVyKHZhbCkge1xuICByZXR1cm4gdmFsICE9PSBudWxsICYmICFpc1VuZGVmaW5lZCh2YWwpICYmIHZhbC5jb25zdHJ1Y3RvciAhPT0gbnVsbCAmJiAhaXNVbmRlZmluZWQodmFsLmNvbnN0cnVjdG9yKVxuICAgICYmIHR5cGVvZiB2YWwuY29uc3RydWN0b3IuaXNCdWZmZXIgPT09ICdmdW5jdGlvbicgJiYgdmFsLmNvbnN0cnVjdG9yLmlzQnVmZmVyKHZhbCk7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYW4gQXJyYXlCdWZmZXJcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhbiBBcnJheUJ1ZmZlciwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzQXJyYXlCdWZmZXIodmFsKSB7XG4gIHJldHVybiB0b1N0cmluZy5jYWxsKHZhbCkgPT09ICdbb2JqZWN0IEFycmF5QnVmZmVyXSc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBGb3JtRGF0YVxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGFuIEZvcm1EYXRhLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNGb3JtRGF0YSh2YWwpIHtcbiAgcmV0dXJuICh0eXBlb2YgRm9ybURhdGEgIT09ICd1bmRlZmluZWQnKSAmJiAodmFsIGluc3RhbmNlb2YgRm9ybURhdGEpO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgdmlldyBvbiBhbiBBcnJheUJ1ZmZlclxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgdmlldyBvbiBhbiBBcnJheUJ1ZmZlciwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzQXJyYXlCdWZmZXJWaWV3KHZhbCkge1xuICB2YXIgcmVzdWx0O1xuICBpZiAoKHR5cGVvZiBBcnJheUJ1ZmZlciAhPT0gJ3VuZGVmaW5lZCcpICYmIChBcnJheUJ1ZmZlci5pc1ZpZXcpKSB7XG4gICAgcmVzdWx0ID0gQXJyYXlCdWZmZXIuaXNWaWV3KHZhbCk7XG4gIH0gZWxzZSB7XG4gICAgcmVzdWx0ID0gKHZhbCkgJiYgKHZhbC5idWZmZXIpICYmICh2YWwuYnVmZmVyIGluc3RhbmNlb2YgQXJyYXlCdWZmZXIpO1xuICB9XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBTdHJpbmdcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIFN0cmluZywgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzU3RyaW5nKHZhbCkge1xuICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ3N0cmluZyc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBOdW1iZXJcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdmFsIFRoZSB2YWx1ZSB0byB0ZXN0XG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gVHJ1ZSBpZiB2YWx1ZSBpcyBhIE51bWJlciwgb3RoZXJ3aXNlIGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzTnVtYmVyKHZhbCkge1xuICByZXR1cm4gdHlwZW9mIHZhbCA9PT0gJ251bWJlcic7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYW4gT2JqZWN0XG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYW4gT2JqZWN0LCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNPYmplY3QodmFsKSB7XG4gIHJldHVybiB2YWwgIT09IG51bGwgJiYgdHlwZW9mIHZhbCA9PT0gJ29iamVjdCc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBEYXRlXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBEYXRlLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNEYXRlKHZhbCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbCh2YWwpID09PSAnW29iamVjdCBEYXRlXSc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBGaWxlXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBGaWxlLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNGaWxlKHZhbCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbCh2YWwpID09PSAnW29iamVjdCBGaWxlXSc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBCbG9iXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBCbG9iLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNCbG9iKHZhbCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbCh2YWwpID09PSAnW29iamVjdCBCbG9iXSc7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBGdW5jdGlvblxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB2YWwgVGhlIHZhbHVlIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHZhbHVlIGlzIGEgRnVuY3Rpb24sIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc0Z1bmN0aW9uKHZhbCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbCh2YWwpID09PSAnW29iamVjdCBGdW5jdGlvbl0nO1xufVxuXG4vKipcbiAqIERldGVybWluZSBpZiBhIHZhbHVlIGlzIGEgU3RyZWFtXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBTdHJlYW0sIG90aGVyd2lzZSBmYWxzZVxuICovXG5mdW5jdGlvbiBpc1N0cmVhbSh2YWwpIHtcbiAgcmV0dXJuIGlzT2JqZWN0KHZhbCkgJiYgaXNGdW5jdGlvbih2YWwucGlwZSk7XG59XG5cbi8qKlxuICogRGV0ZXJtaW5lIGlmIGEgdmFsdWUgaXMgYSBVUkxTZWFyY2hQYXJhbXMgb2JqZWN0XG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHZhbCBUaGUgdmFsdWUgdG8gdGVzdFxuICogQHJldHVybnMge2Jvb2xlYW59IFRydWUgaWYgdmFsdWUgaXMgYSBVUkxTZWFyY2hQYXJhbXMgb2JqZWN0LCBvdGhlcndpc2UgZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNVUkxTZWFyY2hQYXJhbXModmFsKSB7XG4gIHJldHVybiB0eXBlb2YgVVJMU2VhcmNoUGFyYW1zICE9PSAndW5kZWZpbmVkJyAmJiB2YWwgaW5zdGFuY2VvZiBVUkxTZWFyY2hQYXJhbXM7XG59XG5cbi8qKlxuICogVHJpbSBleGNlc3Mgd2hpdGVzcGFjZSBvZmYgdGhlIGJlZ2lubmluZyBhbmQgZW5kIG9mIGEgc3RyaW5nXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IHN0ciBUaGUgU3RyaW5nIHRvIHRyaW1cbiAqIEByZXR1cm5zIHtTdHJpbmd9IFRoZSBTdHJpbmcgZnJlZWQgb2YgZXhjZXNzIHdoaXRlc3BhY2VcbiAqL1xuZnVuY3Rpb24gdHJpbShzdHIpIHtcbiAgcmV0dXJuIHN0ci5yZXBsYWNlKC9eXFxzKi8sICcnKS5yZXBsYWNlKC9cXHMqJC8sICcnKTtcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgaWYgd2UncmUgcnVubmluZyBpbiBhIHN0YW5kYXJkIGJyb3dzZXIgZW52aXJvbm1lbnRcbiAqXG4gKiBUaGlzIGFsbG93cyBheGlvcyB0byBydW4gaW4gYSB3ZWIgd29ya2VyLCBhbmQgcmVhY3QtbmF0aXZlLlxuICogQm90aCBlbnZpcm9ubWVudHMgc3VwcG9ydCBYTUxIdHRwUmVxdWVzdCwgYnV0IG5vdCBmdWxseSBzdGFuZGFyZCBnbG9iYWxzLlxuICpcbiAqIHdlYiB3b3JrZXJzOlxuICogIHR5cGVvZiB3aW5kb3cgLT4gdW5kZWZpbmVkXG4gKiAgdHlwZW9mIGRvY3VtZW50IC0+IHVuZGVmaW5lZFxuICpcbiAqIHJlYWN0LW5hdGl2ZTpcbiAqICBuYXZpZ2F0b3IucHJvZHVjdCAtPiAnUmVhY3ROYXRpdmUnXG4gKiBuYXRpdmVzY3JpcHRcbiAqICBuYXZpZ2F0b3IucHJvZHVjdCAtPiAnTmF0aXZlU2NyaXB0JyBvciAnTlMnXG4gKi9cbmZ1bmN0aW9uIGlzU3RhbmRhcmRCcm93c2VyRW52KCkge1xuICBpZiAodHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiYgKG5hdmlnYXRvci5wcm9kdWN0ID09PSAnUmVhY3ROYXRpdmUnIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLnByb2R1Y3QgPT09ICdOYXRpdmVTY3JpcHQnIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLnByb2R1Y3QgPT09ICdOUycpKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHJldHVybiAoXG4gICAgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG4gICk7XG59XG5cbi8qKlxuICogSXRlcmF0ZSBvdmVyIGFuIEFycmF5IG9yIGFuIE9iamVjdCBpbnZva2luZyBhIGZ1bmN0aW9uIGZvciBlYWNoIGl0ZW0uXG4gKlxuICogSWYgYG9iamAgaXMgYW4gQXJyYXkgY2FsbGJhY2sgd2lsbCBiZSBjYWxsZWQgcGFzc2luZ1xuICogdGhlIHZhbHVlLCBpbmRleCwgYW5kIGNvbXBsZXRlIGFycmF5IGZvciBlYWNoIGl0ZW0uXG4gKlxuICogSWYgJ29iaicgaXMgYW4gT2JqZWN0IGNhbGxiYWNrIHdpbGwgYmUgY2FsbGVkIHBhc3NpbmdcbiAqIHRoZSB2YWx1ZSwga2V5LCBhbmQgY29tcGxldGUgb2JqZWN0IGZvciBlYWNoIHByb3BlcnR5LlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fEFycmF5fSBvYmogVGhlIG9iamVjdCB0byBpdGVyYXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmbiBUaGUgY2FsbGJhY2sgdG8gaW52b2tlIGZvciBlYWNoIGl0ZW1cbiAqL1xuZnVuY3Rpb24gZm9yRWFjaChvYmosIGZuKSB7XG4gIC8vIERvbid0IGJvdGhlciBpZiBubyB2YWx1ZSBwcm92aWRlZFxuICBpZiAob2JqID09PSBudWxsIHx8IHR5cGVvZiBvYmogPT09ICd1bmRlZmluZWQnKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLy8gRm9yY2UgYW4gYXJyYXkgaWYgbm90IGFscmVhZHkgc29tZXRoaW5nIGl0ZXJhYmxlXG4gIGlmICh0eXBlb2Ygb2JqICE9PSAnb2JqZWN0Jykge1xuICAgIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICAgIG9iaiA9IFtvYmpdO1xuICB9XG5cbiAgaWYgKGlzQXJyYXkob2JqKSkge1xuICAgIC8vIEl0ZXJhdGUgb3ZlciBhcnJheSB2YWx1ZXNcbiAgICBmb3IgKHZhciBpID0gMCwgbCA9IG9iai5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgIGZuLmNhbGwobnVsbCwgb2JqW2ldLCBpLCBvYmopO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICAvLyBJdGVyYXRlIG92ZXIgb2JqZWN0IGtleXNcbiAgICBmb3IgKHZhciBrZXkgaW4gb2JqKSB7XG4gICAgICBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwga2V5KSkge1xuICAgICAgICBmbi5jYWxsKG51bGwsIG9ialtrZXldLCBrZXksIG9iaik7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi8qKlxuICogQWNjZXB0cyB2YXJhcmdzIGV4cGVjdGluZyBlYWNoIGFyZ3VtZW50IHRvIGJlIGFuIG9iamVjdCwgdGhlblxuICogaW1tdXRhYmx5IG1lcmdlcyB0aGUgcHJvcGVydGllcyBvZiBlYWNoIG9iamVjdCBhbmQgcmV0dXJucyByZXN1bHQuXG4gKlxuICogV2hlbiBtdWx0aXBsZSBvYmplY3RzIGNvbnRhaW4gdGhlIHNhbWUga2V5IHRoZSBsYXRlciBvYmplY3QgaW5cbiAqIHRoZSBhcmd1bWVudHMgbGlzdCB3aWxsIHRha2UgcHJlY2VkZW5jZS5cbiAqXG4gKiBFeGFtcGxlOlxuICpcbiAqIGBgYGpzXG4gKiB2YXIgcmVzdWx0ID0gbWVyZ2Uoe2ZvbzogMTIzfSwge2ZvbzogNDU2fSk7XG4gKiBjb25zb2xlLmxvZyhyZXN1bHQuZm9vKTsgLy8gb3V0cHV0cyA0NTZcbiAqIGBgYFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmoxIE9iamVjdCB0byBtZXJnZVxuICogQHJldHVybnMge09iamVjdH0gUmVzdWx0IG9mIGFsbCBtZXJnZSBwcm9wZXJ0aWVzXG4gKi9cbmZ1bmN0aW9uIG1lcmdlKC8qIG9iajEsIG9iajIsIG9iajMsIC4uLiAqLykge1xuICB2YXIgcmVzdWx0ID0ge307XG4gIGZ1bmN0aW9uIGFzc2lnblZhbHVlKHZhbCwga2V5KSB7XG4gICAgaWYgKHR5cGVvZiByZXN1bHRba2V5XSA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIHZhbCA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJlc3VsdFtrZXldID0gbWVyZ2UocmVzdWx0W2tleV0sIHZhbCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3VsdFtrZXldID0gdmFsO1xuICAgIH1cbiAgfVxuXG4gIGZvciAodmFyIGkgPSAwLCBsID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgIGZvckVhY2goYXJndW1lbnRzW2ldLCBhc3NpZ25WYWx1ZSk7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuLyoqXG4gKiBGdW5jdGlvbiBlcXVhbCB0byBtZXJnZSB3aXRoIHRoZSBkaWZmZXJlbmNlIGJlaW5nIHRoYXQgbm8gcmVmZXJlbmNlXG4gKiB0byBvcmlnaW5hbCBvYmplY3RzIGlzIGtlcHQuXG4gKlxuICogQHNlZSBtZXJnZVxuICogQHBhcmFtIHtPYmplY3R9IG9iajEgT2JqZWN0IHRvIG1lcmdlXG4gKiBAcmV0dXJucyB7T2JqZWN0fSBSZXN1bHQgb2YgYWxsIG1lcmdlIHByb3BlcnRpZXNcbiAqL1xuZnVuY3Rpb24gZGVlcE1lcmdlKC8qIG9iajEsIG9iajIsIG9iajMsIC4uLiAqLykge1xuICB2YXIgcmVzdWx0ID0ge307XG4gIGZ1bmN0aW9uIGFzc2lnblZhbHVlKHZhbCwga2V5KSB7XG4gICAgaWYgKHR5cGVvZiByZXN1bHRba2V5XSA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIHZhbCA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJlc3VsdFtrZXldID0gZGVlcE1lcmdlKHJlc3VsdFtrZXldLCB2YWwpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbCA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJlc3VsdFtrZXldID0gZGVlcE1lcmdlKHt9LCB2YWwpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXN1bHRba2V5XSA9IHZhbDtcbiAgICB9XG4gIH1cblxuICBmb3IgKHZhciBpID0gMCwgbCA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICBmb3JFYWNoKGFyZ3VtZW50c1tpXSwgYXNzaWduVmFsdWUpO1xuICB9XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbi8qKlxuICogRXh0ZW5kcyBvYmplY3QgYSBieSBtdXRhYmx5IGFkZGluZyB0byBpdCB0aGUgcHJvcGVydGllcyBvZiBvYmplY3QgYi5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gYSBUaGUgb2JqZWN0IHRvIGJlIGV4dGVuZGVkXG4gKiBAcGFyYW0ge09iamVjdH0gYiBUaGUgb2JqZWN0IHRvIGNvcHkgcHJvcGVydGllcyBmcm9tXG4gKiBAcGFyYW0ge09iamVjdH0gdGhpc0FyZyBUaGUgb2JqZWN0IHRvIGJpbmQgZnVuY3Rpb24gdG9cbiAqIEByZXR1cm4ge09iamVjdH0gVGhlIHJlc3VsdGluZyB2YWx1ZSBvZiBvYmplY3QgYVxuICovXG5mdW5jdGlvbiBleHRlbmQoYSwgYiwgdGhpc0FyZykge1xuICBmb3JFYWNoKGIsIGZ1bmN0aW9uIGFzc2lnblZhbHVlKHZhbCwga2V5KSB7XG4gICAgaWYgKHRoaXNBcmcgJiYgdHlwZW9mIHZhbCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgYVtrZXldID0gYmluZCh2YWwsIHRoaXNBcmcpO1xuICAgIH0gZWxzZSB7XG4gICAgICBhW2tleV0gPSB2YWw7XG4gICAgfVxuICB9KTtcbiAgcmV0dXJuIGE7XG59XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICBpc0FycmF5OiBpc0FycmF5LFxuICBpc0FycmF5QnVmZmVyOiBpc0FycmF5QnVmZmVyLFxuICBpc0J1ZmZlcjogaXNCdWZmZXIsXG4gIGlzRm9ybURhdGE6IGlzRm9ybURhdGEsXG4gIGlzQXJyYXlCdWZmZXJWaWV3OiBpc0FycmF5QnVmZmVyVmlldyxcbiAgaXNTdHJpbmc6IGlzU3RyaW5nLFxuICBpc051bWJlcjogaXNOdW1iZXIsXG4gIGlzT2JqZWN0OiBpc09iamVjdCxcbiAgaXNVbmRlZmluZWQ6IGlzVW5kZWZpbmVkLFxuICBpc0RhdGU6IGlzRGF0ZSxcbiAgaXNGaWxlOiBpc0ZpbGUsXG4gIGlzQmxvYjogaXNCbG9iLFxuICBpc0Z1bmN0aW9uOiBpc0Z1bmN0aW9uLFxuICBpc1N0cmVhbTogaXNTdHJlYW0sXG4gIGlzVVJMU2VhcmNoUGFyYW1zOiBpc1VSTFNlYXJjaFBhcmFtcyxcbiAgaXNTdGFuZGFyZEJyb3dzZXJFbnY6IGlzU3RhbmRhcmRCcm93c2VyRW52LFxuICBmb3JFYWNoOiBmb3JFYWNoLFxuICBtZXJnZTogbWVyZ2UsXG4gIGRlZXBNZXJnZTogZGVlcE1lcmdlLFxuICBleHRlbmQ6IGV4dGVuZCxcbiAgdHJpbTogdHJpbVxufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi91dGlscy5qc1xuLy8gbW9kdWxlIGlkID0gMFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLkxpbmsgPSBleHBvcnRzLm1hcmtTd3VwRWxlbWVudHMgPSBleHBvcnRzLmdldEN1cnJlbnRVcmwgPSBleHBvcnRzLnRyYW5zaXRpb25FbmQgPSBleHBvcnRzLmZldGNoID0gZXhwb3J0cy5nZXREYXRhRnJvbUh0bWwgPSBleHBvcnRzLmNyZWF0ZUhpc3RvcnlSZWNvcmQgPSBleHBvcnRzLmNsYXNzaWZ5ID0gdW5kZWZpbmVkO1xuXG52YXIgX2NsYXNzaWZ5ID0gcmVxdWlyZSgnLi9jbGFzc2lmeScpO1xuXG52YXIgX2NsYXNzaWZ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzaWZ5KTtcblxudmFyIF9jcmVhdGVIaXN0b3J5UmVjb3JkID0gcmVxdWlyZSgnLi9jcmVhdGVIaXN0b3J5UmVjb3JkJyk7XG5cbnZhciBfY3JlYXRlSGlzdG9yeVJlY29yZDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVIaXN0b3J5UmVjb3JkKTtcblxudmFyIF9nZXREYXRhRnJvbUh0bWwgPSByZXF1aXJlKCcuL2dldERhdGFGcm9tSHRtbCcpO1xuXG52YXIgX2dldERhdGFGcm9tSHRtbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXREYXRhRnJvbUh0bWwpO1xuXG52YXIgX2ZldGNoID0gcmVxdWlyZSgnLi9mZXRjaCcpO1xuXG52YXIgX2ZldGNoMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2ZldGNoKTtcblxudmFyIF90cmFuc2l0aW9uRW5kID0gcmVxdWlyZSgnLi90cmFuc2l0aW9uRW5kJyk7XG5cbnZhciBfdHJhbnNpdGlvbkVuZDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90cmFuc2l0aW9uRW5kKTtcblxudmFyIF9nZXRDdXJyZW50VXJsID0gcmVxdWlyZSgnLi9nZXRDdXJyZW50VXJsJyk7XG5cbnZhciBfZ2V0Q3VycmVudFVybDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRDdXJyZW50VXJsKTtcblxudmFyIF9tYXJrU3d1cEVsZW1lbnRzID0gcmVxdWlyZSgnLi9tYXJrU3d1cEVsZW1lbnRzJyk7XG5cbnZhciBfbWFya1N3dXBFbGVtZW50czIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9tYXJrU3d1cEVsZW1lbnRzKTtcblxudmFyIF9MaW5rID0gcmVxdWlyZSgnLi9MaW5rJyk7XG5cbnZhciBfTGluazIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9MaW5rKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNsYXNzaWZ5ID0gZXhwb3J0cy5jbGFzc2lmeSA9IF9jbGFzc2lmeTIuZGVmYXVsdDtcbnZhciBjcmVhdGVIaXN0b3J5UmVjb3JkID0gZXhwb3J0cy5jcmVhdGVIaXN0b3J5UmVjb3JkID0gX2NyZWF0ZUhpc3RvcnlSZWNvcmQyLmRlZmF1bHQ7XG52YXIgZ2V0RGF0YUZyb21IdG1sID0gZXhwb3J0cy5nZXREYXRhRnJvbUh0bWwgPSBfZ2V0RGF0YUZyb21IdG1sMi5kZWZhdWx0O1xudmFyIGZldGNoID0gZXhwb3J0cy5mZXRjaCA9IF9mZXRjaDIuZGVmYXVsdDtcbnZhciB0cmFuc2l0aW9uRW5kID0gZXhwb3J0cy50cmFuc2l0aW9uRW5kID0gX3RyYW5zaXRpb25FbmQyLmRlZmF1bHQ7XG52YXIgZ2V0Q3VycmVudFVybCA9IGV4cG9ydHMuZ2V0Q3VycmVudFVybCA9IF9nZXRDdXJyZW50VXJsMi5kZWZhdWx0O1xudmFyIG1hcmtTd3VwRWxlbWVudHMgPSBleHBvcnRzLm1hcmtTd3VwRWxlbWVudHMgPSBfbWFya1N3dXBFbGVtZW50czIuZGVmYXVsdDtcbnZhciBMaW5rID0gZXhwb3J0cy5MaW5rID0gX0xpbmsyLmRlZmF1bHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvaGVscGVycy9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG52YXIgcXVlcnkgPSBleHBvcnRzLnF1ZXJ5ID0gZnVuY3Rpb24gcXVlcnkoc2VsZWN0b3IpIHtcblx0dmFyIGNvbnRleHQgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IGRvY3VtZW50O1xuXG5cdGlmICh0eXBlb2Ygc2VsZWN0b3IgIT09ICdzdHJpbmcnKSB7XG5cdFx0cmV0dXJuIHNlbGVjdG9yO1xuXHR9XG5cblx0cmV0dXJuIGNvbnRleHQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XG59O1xuXG52YXIgcXVlcnlBbGwgPSBleHBvcnRzLnF1ZXJ5QWxsID0gZnVuY3Rpb24gcXVlcnlBbGwoc2VsZWN0b3IpIHtcblx0dmFyIGNvbnRleHQgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IGRvY3VtZW50O1xuXG5cdGlmICh0eXBlb2Ygc2VsZWN0b3IgIT09ICdzdHJpbmcnKSB7XG5cdFx0cmV0dXJuIHNlbGVjdG9yO1xuXHR9XG5cblx0cmV0dXJuIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGNvbnRleHQucXVlcnlTZWxlY3RvckFsbChzZWxlY3RvcikpO1xufTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi91dGlscy9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gICAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG52YXIgUGx1Z2luID0gZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIFBsdWdpbigpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFBsdWdpbik7XG5cbiAgICAgICAgdGhpcy5pc1N3dXBQbHVnaW4gPSB0cnVlO1xuICAgIH1cblxuICAgIF9jcmVhdGVDbGFzcyhQbHVnaW4sIFt7XG4gICAgICAgIGtleTogXCJtb3VudFwiLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gbW91bnQoKSB7XG4gICAgICAgICAgICAvLyB0aGlzIGlzIG1vdW50IG1ldGhvZCByZXdyaXR0ZW4gYnkgY2xhc3MgZXh0ZW5kaW5nXG4gICAgICAgICAgICAvLyBhbmQgaXMgZXhlY3V0ZWQgd2hlbiBzd3VwIGlzIGVuYWJsZWQgd2l0aCBwbHVnaW5cbiAgICAgICAgfVxuICAgIH0sIHtcbiAgICAgICAga2V5OiBcInVubW91bnRcIixcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIHVubW91bnQoKSB7XG4gICAgICAgICAgICAvLyB0aGlzIGlzIHVubW91bnQgbWV0aG9kIHJld3JpdHRlbiBieSBjbGFzcyBleHRlbmRpbmdcbiAgICAgICAgICAgIC8vIGFuZCBpcyBleGVjdXRlZCB3aGVuIHN3dXAgd2l0aCBwbHVnaW4gaXMgZGlzYWJsZWRcbiAgICAgICAgfVxuICAgIH0sIHtcbiAgICAgICAga2V5OiBcIl9iZWZvcmVNb3VudFwiLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gX2JlZm9yZU1vdW50KCkge1xuICAgICAgICAgICAgLy8gaGVyZSBmb3IgYW55IGZ1dHVyZSBoaWRkZW4gYXV0byBpbml0XG4gICAgICAgIH1cbiAgICB9LCB7XG4gICAgICAgIGtleTogXCJfYWZ0ZXJVbm1vdW50XCIsXG4gICAgICAgIHZhbHVlOiBmdW5jdGlvbiBfYWZ0ZXJVbm1vdW50KCkge31cbiAgICAgICAgLy8gaGVyZSBmb3IgYW55IGZ1dHVyZSBoaWRkZW4gYXV0by1jbGVhbnVwXG5cblxuICAgICAgICAvLyB0aGlzIGlzIGhlcmUgc28gd2UgY2FuIHRlbGwgaWYgcGx1Z2luIHdhcyBjcmVhdGVkIGJ5IGV4dGVuZGluZyB0aGlzIGNsYXNzXG5cbiAgICB9XSk7XG5cbiAgICByZXR1cm4gUGx1Z2luO1xufSgpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBQbHVnaW47XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvQHN3dXAvcGx1Z2luL2xpYi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gM1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYmluZChmbiwgdGhpc0FyZykge1xuICByZXR1cm4gZnVuY3Rpb24gd3JhcCgpIHtcbiAgICB2YXIgYXJncyA9IG5ldyBBcnJheShhcmd1bWVudHMubGVuZ3RoKTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICAgIGFyZ3NbaV0gPSBhcmd1bWVudHNbaV07XG4gICAgfVxuICAgIHJldHVybiBmbi5hcHBseSh0aGlzQXJnLCBhcmdzKTtcbiAgfTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9iaW5kLmpzXG4vLyBtb2R1bGUgaWQgPSA0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi8uLi91dGlscycpO1xuXG5mdW5jdGlvbiBlbmNvZGUodmFsKSB7XG4gIHJldHVybiBlbmNvZGVVUklDb21wb25lbnQodmFsKS5cbiAgICByZXBsYWNlKC8lNDAvZ2ksICdAJykuXG4gICAgcmVwbGFjZSgvJTNBL2dpLCAnOicpLlxuICAgIHJlcGxhY2UoLyUyNC9nLCAnJCcpLlxuICAgIHJlcGxhY2UoLyUyQy9naSwgJywnKS5cbiAgICByZXBsYWNlKC8lMjAvZywgJysnKS5cbiAgICByZXBsYWNlKC8lNUIvZ2ksICdbJykuXG4gICAgcmVwbGFjZSgvJTVEL2dpLCAnXScpO1xufVxuXG4vKipcbiAqIEJ1aWxkIGEgVVJMIGJ5IGFwcGVuZGluZyBwYXJhbXMgdG8gdGhlIGVuZFxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSB1cmwgVGhlIGJhc2Ugb2YgdGhlIHVybCAoZS5nLiwgaHR0cDovL3d3dy5nb29nbGUuY29tKVxuICogQHBhcmFtIHtvYmplY3R9IFtwYXJhbXNdIFRoZSBwYXJhbXMgdG8gYmUgYXBwZW5kZWRcbiAqIEByZXR1cm5zIHtzdHJpbmd9IFRoZSBmb3JtYXR0ZWQgdXJsXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYnVpbGRVUkwodXJsLCBwYXJhbXMsIHBhcmFtc1NlcmlhbGl6ZXIpIHtcbiAgLyplc2xpbnQgbm8tcGFyYW0tcmVhc3NpZ246MCovXG4gIGlmICghcGFyYW1zKSB7XG4gICAgcmV0dXJuIHVybDtcbiAgfVxuXG4gIHZhciBzZXJpYWxpemVkUGFyYW1zO1xuICBpZiAocGFyYW1zU2VyaWFsaXplcikge1xuICAgIHNlcmlhbGl6ZWRQYXJhbXMgPSBwYXJhbXNTZXJpYWxpemVyKHBhcmFtcyk7XG4gIH0gZWxzZSBpZiAodXRpbHMuaXNVUkxTZWFyY2hQYXJhbXMocGFyYW1zKSkge1xuICAgIHNlcmlhbGl6ZWRQYXJhbXMgPSBwYXJhbXMudG9TdHJpbmcoKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgcGFydHMgPSBbXTtcblxuICAgIHV0aWxzLmZvckVhY2gocGFyYW1zLCBmdW5jdGlvbiBzZXJpYWxpemUodmFsLCBrZXkpIHtcbiAgICAgIGlmICh2YWwgPT09IG51bGwgfHwgdHlwZW9mIHZhbCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAodXRpbHMuaXNBcnJheSh2YWwpKSB7XG4gICAgICAgIGtleSA9IGtleSArICdbXSc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YWwgPSBbdmFsXTtcbiAgICAgIH1cblxuICAgICAgdXRpbHMuZm9yRWFjaCh2YWwsIGZ1bmN0aW9uIHBhcnNlVmFsdWUodikge1xuICAgICAgICBpZiAodXRpbHMuaXNEYXRlKHYpKSB7XG4gICAgICAgICAgdiA9IHYudG9JU09TdHJpbmcoKTtcbiAgICAgICAgfSBlbHNlIGlmICh1dGlscy5pc09iamVjdCh2KSkge1xuICAgICAgICAgIHYgPSBKU09OLnN0cmluZ2lmeSh2KTtcbiAgICAgICAgfVxuICAgICAgICBwYXJ0cy5wdXNoKGVuY29kZShrZXkpICsgJz0nICsgZW5jb2RlKHYpKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgc2VyaWFsaXplZFBhcmFtcyA9IHBhcnRzLmpvaW4oJyYnKTtcbiAgfVxuXG4gIGlmIChzZXJpYWxpemVkUGFyYW1zKSB7XG4gICAgdmFyIGhhc2htYXJrSW5kZXggPSB1cmwuaW5kZXhPZignIycpO1xuICAgIGlmIChoYXNobWFya0luZGV4ICE9PSAtMSkge1xuICAgICAgdXJsID0gdXJsLnNsaWNlKDAsIGhhc2htYXJrSW5kZXgpO1xuICAgIH1cblxuICAgIHVybCArPSAodXJsLmluZGV4T2YoJz8nKSA9PT0gLTEgPyAnPycgOiAnJicpICsgc2VyaWFsaXplZFBhcmFtcztcbiAgfVxuXG4gIHJldHVybiB1cmw7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvYnVpbGRVUkwuanNcbi8vIG1vZHVsZSBpZCA9IDVcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGlzQ2FuY2VsKHZhbHVlKSB7XG4gIHJldHVybiAhISh2YWx1ZSAmJiB2YWx1ZS5fX0NBTkNFTF9fKTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY2FuY2VsL2lzQ2FuY2VsLmpzXG4vLyBtb2R1bGUgaWQgPSA2XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi91dGlscycpO1xudmFyIG5vcm1hbGl6ZUhlYWRlck5hbWUgPSByZXF1aXJlKCcuL2hlbHBlcnMvbm9ybWFsaXplSGVhZGVyTmFtZScpO1xuXG52YXIgREVGQVVMVF9DT05URU5UX1RZUEUgPSB7XG4gICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJ1xufTtcblxuZnVuY3Rpb24gc2V0Q29udGVudFR5cGVJZlVuc2V0KGhlYWRlcnMsIHZhbHVlKSB7XG4gIGlmICghdXRpbHMuaXNVbmRlZmluZWQoaGVhZGVycykgJiYgdXRpbHMuaXNVbmRlZmluZWQoaGVhZGVyc1snQ29udGVudC1UeXBlJ10pKSB7XG4gICAgaGVhZGVyc1snQ29udGVudC1UeXBlJ10gPSB2YWx1ZTtcbiAgfVxufVxuXG5mdW5jdGlvbiBnZXREZWZhdWx0QWRhcHRlcigpIHtcbiAgdmFyIGFkYXB0ZXI7XG4gIGlmICh0eXBlb2YgWE1MSHR0cFJlcXVlc3QgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgLy8gRm9yIGJyb3dzZXJzIHVzZSBYSFIgYWRhcHRlclxuICAgIGFkYXB0ZXIgPSByZXF1aXJlKCcuL2FkYXB0ZXJzL3hocicpO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBwcm9jZXNzICE9PSAndW5kZWZpbmVkJyAmJiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwocHJvY2VzcykgPT09ICdbb2JqZWN0IHByb2Nlc3NdJykge1xuICAgIC8vIEZvciBub2RlIHVzZSBIVFRQIGFkYXB0ZXJcbiAgICBhZGFwdGVyID0gcmVxdWlyZSgnLi9hZGFwdGVycy9odHRwJyk7XG4gIH1cbiAgcmV0dXJuIGFkYXB0ZXI7XG59XG5cbnZhciBkZWZhdWx0cyA9IHtcbiAgYWRhcHRlcjogZ2V0RGVmYXVsdEFkYXB0ZXIoKSxcblxuICB0cmFuc2Zvcm1SZXF1ZXN0OiBbZnVuY3Rpb24gdHJhbnNmb3JtUmVxdWVzdChkYXRhLCBoZWFkZXJzKSB7XG4gICAgbm9ybWFsaXplSGVhZGVyTmFtZShoZWFkZXJzLCAnQWNjZXB0Jyk7XG4gICAgbm9ybWFsaXplSGVhZGVyTmFtZShoZWFkZXJzLCAnQ29udGVudC1UeXBlJyk7XG4gICAgaWYgKHV0aWxzLmlzRm9ybURhdGEoZGF0YSkgfHxcbiAgICAgIHV0aWxzLmlzQXJyYXlCdWZmZXIoZGF0YSkgfHxcbiAgICAgIHV0aWxzLmlzQnVmZmVyKGRhdGEpIHx8XG4gICAgICB1dGlscy5pc1N0cmVhbShkYXRhKSB8fFxuICAgICAgdXRpbHMuaXNGaWxlKGRhdGEpIHx8XG4gICAgICB1dGlscy5pc0Jsb2IoZGF0YSlcbiAgICApIHtcbiAgICAgIHJldHVybiBkYXRhO1xuICAgIH1cbiAgICBpZiAodXRpbHMuaXNBcnJheUJ1ZmZlclZpZXcoZGF0YSkpIHtcbiAgICAgIHJldHVybiBkYXRhLmJ1ZmZlcjtcbiAgICB9XG4gICAgaWYgKHV0aWxzLmlzVVJMU2VhcmNoUGFyYW1zKGRhdGEpKSB7XG4gICAgICBzZXRDb250ZW50VHlwZUlmVW5zZXQoaGVhZGVycywgJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZDtjaGFyc2V0PXV0Zi04Jyk7XG4gICAgICByZXR1cm4gZGF0YS50b1N0cmluZygpO1xuICAgIH1cbiAgICBpZiAodXRpbHMuaXNPYmplY3QoZGF0YSkpIHtcbiAgICAgIHNldENvbnRlbnRUeXBlSWZVbnNldChoZWFkZXJzLCAnYXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04Jyk7XG4gICAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkoZGF0YSk7XG4gICAgfVxuICAgIHJldHVybiBkYXRhO1xuICB9XSxcblxuICB0cmFuc2Zvcm1SZXNwb25zZTogW2Z1bmN0aW9uIHRyYW5zZm9ybVJlc3BvbnNlKGRhdGEpIHtcbiAgICAvKmVzbGludCBuby1wYXJhbS1yZWFzc2lnbjowKi9cbiAgICBpZiAodHlwZW9mIGRhdGEgPT09ICdzdHJpbmcnKSB7XG4gICAgICB0cnkge1xuICAgICAgICBkYXRhID0gSlNPTi5wYXJzZShkYXRhKTtcbiAgICAgIH0gY2F0Y2ggKGUpIHsgLyogSWdub3JlICovIH1cbiAgICB9XG4gICAgcmV0dXJuIGRhdGE7XG4gIH1dLFxuXG4gIC8qKlxuICAgKiBBIHRpbWVvdXQgaW4gbWlsbGlzZWNvbmRzIHRvIGFib3J0IGEgcmVxdWVzdC4gSWYgc2V0IHRvIDAgKGRlZmF1bHQpIGFcbiAgICogdGltZW91dCBpcyBub3QgY3JlYXRlZC5cbiAgICovXG4gIHRpbWVvdXQ6IDAsXG5cbiAgeHNyZkNvb2tpZU5hbWU6ICdYU1JGLVRPS0VOJyxcbiAgeHNyZkhlYWRlck5hbWU6ICdYLVhTUkYtVE9LRU4nLFxuXG4gIG1heENvbnRlbnRMZW5ndGg6IC0xLFxuXG4gIHZhbGlkYXRlU3RhdHVzOiBmdW5jdGlvbiB2YWxpZGF0ZVN0YXR1cyhzdGF0dXMpIHtcbiAgICByZXR1cm4gc3RhdHVzID49IDIwMCAmJiBzdGF0dXMgPCAzMDA7XG4gIH1cbn07XG5cbmRlZmF1bHRzLmhlYWRlcnMgPSB7XG4gIGNvbW1vbjoge1xuICAgICdBY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbiwgdGV4dC9wbGFpbiwgKi8qJ1xuICB9XG59O1xuXG51dGlscy5mb3JFYWNoKFsnZGVsZXRlJywgJ2dldCcsICdoZWFkJ10sIGZ1bmN0aW9uIGZvckVhY2hNZXRob2ROb0RhdGEobWV0aG9kKSB7XG4gIGRlZmF1bHRzLmhlYWRlcnNbbWV0aG9kXSA9IHt9O1xufSk7XG5cbnV0aWxzLmZvckVhY2goWydwb3N0JywgJ3B1dCcsICdwYXRjaCddLCBmdW5jdGlvbiBmb3JFYWNoTWV0aG9kV2l0aERhdGEobWV0aG9kKSB7XG4gIGRlZmF1bHRzLmhlYWRlcnNbbWV0aG9kXSA9IHV0aWxzLm1lcmdlKERFRkFVTFRfQ09OVEVOVF9UWVBFKTtcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGRlZmF1bHRzO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2RlZmF1bHRzLmpzXG4vLyBtb2R1bGUgaWQgPSA3XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi8uLi91dGlscycpO1xudmFyIHNldHRsZSA9IHJlcXVpcmUoJy4vLi4vY29yZS9zZXR0bGUnKTtcbnZhciBidWlsZFVSTCA9IHJlcXVpcmUoJy4vLi4vaGVscGVycy9idWlsZFVSTCcpO1xudmFyIGJ1aWxkRnVsbFBhdGggPSByZXF1aXJlKCcuLi9jb3JlL2J1aWxkRnVsbFBhdGgnKTtcbnZhciBwYXJzZUhlYWRlcnMgPSByZXF1aXJlKCcuLy4uL2hlbHBlcnMvcGFyc2VIZWFkZXJzJyk7XG52YXIgaXNVUkxTYW1lT3JpZ2luID0gcmVxdWlyZSgnLi8uLi9oZWxwZXJzL2lzVVJMU2FtZU9yaWdpbicpO1xudmFyIGNyZWF0ZUVycm9yID0gcmVxdWlyZSgnLi4vY29yZS9jcmVhdGVFcnJvcicpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHhockFkYXB0ZXIoY29uZmlnKSB7XG4gIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiBkaXNwYXRjaFhoclJlcXVlc3QocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgdmFyIHJlcXVlc3REYXRhID0gY29uZmlnLmRhdGE7XG4gICAgdmFyIHJlcXVlc3RIZWFkZXJzID0gY29uZmlnLmhlYWRlcnM7XG5cbiAgICBpZiAodXRpbHMuaXNGb3JtRGF0YShyZXF1ZXN0RGF0YSkpIHtcbiAgICAgIGRlbGV0ZSByZXF1ZXN0SGVhZGVyc1snQ29udGVudC1UeXBlJ107IC8vIExldCB0aGUgYnJvd3NlciBzZXQgaXRcbiAgICB9XG5cbiAgICB2YXIgcmVxdWVzdCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXG4gICAgLy8gSFRUUCBiYXNpYyBhdXRoZW50aWNhdGlvblxuICAgIGlmIChjb25maWcuYXV0aCkge1xuICAgICAgdmFyIHVzZXJuYW1lID0gY29uZmlnLmF1dGgudXNlcm5hbWUgfHwgJyc7XG4gICAgICB2YXIgcGFzc3dvcmQgPSBjb25maWcuYXV0aC5wYXNzd29yZCB8fCAnJztcbiAgICAgIHJlcXVlc3RIZWFkZXJzLkF1dGhvcml6YXRpb24gPSAnQmFzaWMgJyArIGJ0b2EodXNlcm5hbWUgKyAnOicgKyBwYXNzd29yZCk7XG4gICAgfVxuXG4gICAgdmFyIGZ1bGxQYXRoID0gYnVpbGRGdWxsUGF0aChjb25maWcuYmFzZVVSTCwgY29uZmlnLnVybCk7XG4gICAgcmVxdWVzdC5vcGVuKGNvbmZpZy5tZXRob2QudG9VcHBlckNhc2UoKSwgYnVpbGRVUkwoZnVsbFBhdGgsIGNvbmZpZy5wYXJhbXMsIGNvbmZpZy5wYXJhbXNTZXJpYWxpemVyKSwgdHJ1ZSk7XG5cbiAgICAvLyBTZXQgdGhlIHJlcXVlc3QgdGltZW91dCBpbiBNU1xuICAgIHJlcXVlc3QudGltZW91dCA9IGNvbmZpZy50aW1lb3V0O1xuXG4gICAgLy8gTGlzdGVuIGZvciByZWFkeSBzdGF0ZVxuICAgIHJlcXVlc3Qub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24gaGFuZGxlTG9hZCgpIHtcbiAgICAgIGlmICghcmVxdWVzdCB8fCByZXF1ZXN0LnJlYWR5U3RhdGUgIT09IDQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBUaGUgcmVxdWVzdCBlcnJvcmVkIG91dCBhbmQgd2UgZGlkbid0IGdldCBhIHJlc3BvbnNlLCB0aGlzIHdpbGwgYmVcbiAgICAgIC8vIGhhbmRsZWQgYnkgb25lcnJvciBpbnN0ZWFkXG4gICAgICAvLyBXaXRoIG9uZSBleGNlcHRpb246IHJlcXVlc3QgdGhhdCB1c2luZyBmaWxlOiBwcm90b2NvbCwgbW9zdCBicm93c2Vyc1xuICAgICAgLy8gd2lsbCByZXR1cm4gc3RhdHVzIGFzIDAgZXZlbiB0aG91Z2ggaXQncyBhIHN1Y2Nlc3NmdWwgcmVxdWVzdFxuICAgICAgaWYgKHJlcXVlc3Quc3RhdHVzID09PSAwICYmICEocmVxdWVzdC5yZXNwb25zZVVSTCAmJiByZXF1ZXN0LnJlc3BvbnNlVVJMLmluZGV4T2YoJ2ZpbGU6JykgPT09IDApKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgLy8gUHJlcGFyZSB0aGUgcmVzcG9uc2VcbiAgICAgIHZhciByZXNwb25zZUhlYWRlcnMgPSAnZ2V0QWxsUmVzcG9uc2VIZWFkZXJzJyBpbiByZXF1ZXN0ID8gcGFyc2VIZWFkZXJzKHJlcXVlc3QuZ2V0QWxsUmVzcG9uc2VIZWFkZXJzKCkpIDogbnVsbDtcbiAgICAgIHZhciByZXNwb25zZURhdGEgPSAhY29uZmlnLnJlc3BvbnNlVHlwZSB8fCBjb25maWcucmVzcG9uc2VUeXBlID09PSAndGV4dCcgPyByZXF1ZXN0LnJlc3BvbnNlVGV4dCA6IHJlcXVlc3QucmVzcG9uc2U7XG4gICAgICB2YXIgcmVzcG9uc2UgPSB7XG4gICAgICAgIGRhdGE6IHJlc3BvbnNlRGF0YSxcbiAgICAgICAgc3RhdHVzOiByZXF1ZXN0LnN0YXR1cyxcbiAgICAgICAgc3RhdHVzVGV4dDogcmVxdWVzdC5zdGF0dXNUZXh0LFxuICAgICAgICBoZWFkZXJzOiByZXNwb25zZUhlYWRlcnMsXG4gICAgICAgIGNvbmZpZzogY29uZmlnLFxuICAgICAgICByZXF1ZXN0OiByZXF1ZXN0XG4gICAgICB9O1xuXG4gICAgICBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCByZXNwb25zZSk7XG5cbiAgICAgIC8vIENsZWFuIHVwIHJlcXVlc3RcbiAgICAgIHJlcXVlc3QgPSBudWxsO1xuICAgIH07XG5cbiAgICAvLyBIYW5kbGUgYnJvd3NlciByZXF1ZXN0IGNhbmNlbGxhdGlvbiAoYXMgb3Bwb3NlZCB0byBhIG1hbnVhbCBjYW5jZWxsYXRpb24pXG4gICAgcmVxdWVzdC5vbmFib3J0ID0gZnVuY3Rpb24gaGFuZGxlQWJvcnQoKSB7XG4gICAgICBpZiAoIXJlcXVlc3QpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICByZWplY3QoY3JlYXRlRXJyb3IoJ1JlcXVlc3QgYWJvcnRlZCcsIGNvbmZpZywgJ0VDT05OQUJPUlRFRCcsIHJlcXVlc3QpKTtcblxuICAgICAgLy8gQ2xlYW4gdXAgcmVxdWVzdFxuICAgICAgcmVxdWVzdCA9IG51bGw7XG4gICAgfTtcblxuICAgIC8vIEhhbmRsZSBsb3cgbGV2ZWwgbmV0d29yayBlcnJvcnNcbiAgICByZXF1ZXN0Lm9uZXJyb3IgPSBmdW5jdGlvbiBoYW5kbGVFcnJvcigpIHtcbiAgICAgIC8vIFJlYWwgZXJyb3JzIGFyZSBoaWRkZW4gZnJvbSB1cyBieSB0aGUgYnJvd3NlclxuICAgICAgLy8gb25lcnJvciBzaG91bGQgb25seSBmaXJlIGlmIGl0J3MgYSBuZXR3b3JrIGVycm9yXG4gICAgICByZWplY3QoY3JlYXRlRXJyb3IoJ05ldHdvcmsgRXJyb3InLCBjb25maWcsIG51bGwsIHJlcXVlc3QpKTtcblxuICAgICAgLy8gQ2xlYW4gdXAgcmVxdWVzdFxuICAgICAgcmVxdWVzdCA9IG51bGw7XG4gICAgfTtcblxuICAgIC8vIEhhbmRsZSB0aW1lb3V0XG4gICAgcmVxdWVzdC5vbnRpbWVvdXQgPSBmdW5jdGlvbiBoYW5kbGVUaW1lb3V0KCkge1xuICAgICAgdmFyIHRpbWVvdXRFcnJvck1lc3NhZ2UgPSAndGltZW91dCBvZiAnICsgY29uZmlnLnRpbWVvdXQgKyAnbXMgZXhjZWVkZWQnO1xuICAgICAgaWYgKGNvbmZpZy50aW1lb3V0RXJyb3JNZXNzYWdlKSB7XG4gICAgICAgIHRpbWVvdXRFcnJvck1lc3NhZ2UgPSBjb25maWcudGltZW91dEVycm9yTWVzc2FnZTtcbiAgICAgIH1cbiAgICAgIHJlamVjdChjcmVhdGVFcnJvcih0aW1lb3V0RXJyb3JNZXNzYWdlLCBjb25maWcsICdFQ09OTkFCT1JURUQnLFxuICAgICAgICByZXF1ZXN0KSk7XG5cbiAgICAgIC8vIENsZWFuIHVwIHJlcXVlc3RcbiAgICAgIHJlcXVlc3QgPSBudWxsO1xuICAgIH07XG5cbiAgICAvLyBBZGQgeHNyZiBoZWFkZXJcbiAgICAvLyBUaGlzIGlzIG9ubHkgZG9uZSBpZiBydW5uaW5nIGluIGEgc3RhbmRhcmQgYnJvd3NlciBlbnZpcm9ubWVudC5cbiAgICAvLyBTcGVjaWZpY2FsbHkgbm90IGlmIHdlJ3JlIGluIGEgd2ViIHdvcmtlciwgb3IgcmVhY3QtbmF0aXZlLlxuICAgIGlmICh1dGlscy5pc1N0YW5kYXJkQnJvd3NlckVudigpKSB7XG4gICAgICB2YXIgY29va2llcyA9IHJlcXVpcmUoJy4vLi4vaGVscGVycy9jb29raWVzJyk7XG5cbiAgICAgIC8vIEFkZCB4c3JmIGhlYWRlclxuICAgICAgdmFyIHhzcmZWYWx1ZSA9IChjb25maWcud2l0aENyZWRlbnRpYWxzIHx8IGlzVVJMU2FtZU9yaWdpbihmdWxsUGF0aCkpICYmIGNvbmZpZy54c3JmQ29va2llTmFtZSA/XG4gICAgICAgIGNvb2tpZXMucmVhZChjb25maWcueHNyZkNvb2tpZU5hbWUpIDpcbiAgICAgICAgdW5kZWZpbmVkO1xuXG4gICAgICBpZiAoeHNyZlZhbHVlKSB7XG4gICAgICAgIHJlcXVlc3RIZWFkZXJzW2NvbmZpZy54c3JmSGVhZGVyTmFtZV0gPSB4c3JmVmFsdWU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gQWRkIGhlYWRlcnMgdG8gdGhlIHJlcXVlc3RcbiAgICBpZiAoJ3NldFJlcXVlc3RIZWFkZXInIGluIHJlcXVlc3QpIHtcbiAgICAgIHV0aWxzLmZvckVhY2gocmVxdWVzdEhlYWRlcnMsIGZ1bmN0aW9uIHNldFJlcXVlc3RIZWFkZXIodmFsLCBrZXkpIHtcbiAgICAgICAgaWYgKHR5cGVvZiByZXF1ZXN0RGF0YSA9PT0gJ3VuZGVmaW5lZCcgJiYga2V5LnRvTG93ZXJDYXNlKCkgPT09ICdjb250ZW50LXR5cGUnKSB7XG4gICAgICAgICAgLy8gUmVtb3ZlIENvbnRlbnQtVHlwZSBpZiBkYXRhIGlzIHVuZGVmaW5lZFxuICAgICAgICAgIGRlbGV0ZSByZXF1ZXN0SGVhZGVyc1trZXldO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIE90aGVyd2lzZSBhZGQgaGVhZGVyIHRvIHRoZSByZXF1ZXN0XG4gICAgICAgICAgcmVxdWVzdC5zZXRSZXF1ZXN0SGVhZGVyKGtleSwgdmFsKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gQWRkIHdpdGhDcmVkZW50aWFscyB0byByZXF1ZXN0IGlmIG5lZWRlZFxuICAgIGlmICghdXRpbHMuaXNVbmRlZmluZWQoY29uZmlnLndpdGhDcmVkZW50aWFscykpIHtcbiAgICAgIHJlcXVlc3Qud2l0aENyZWRlbnRpYWxzID0gISFjb25maWcud2l0aENyZWRlbnRpYWxzO1xuICAgIH1cblxuICAgIC8vIEFkZCByZXNwb25zZVR5cGUgdG8gcmVxdWVzdCBpZiBuZWVkZWRcbiAgICBpZiAoY29uZmlnLnJlc3BvbnNlVHlwZSkge1xuICAgICAgdHJ5IHtcbiAgICAgICAgcmVxdWVzdC5yZXNwb25zZVR5cGUgPSBjb25maWcucmVzcG9uc2VUeXBlO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAvLyBFeHBlY3RlZCBET01FeGNlcHRpb24gdGhyb3duIGJ5IGJyb3dzZXJzIG5vdCBjb21wYXRpYmxlIFhNTEh0dHBSZXF1ZXN0IExldmVsIDIuXG4gICAgICAgIC8vIEJ1dCwgdGhpcyBjYW4gYmUgc3VwcHJlc3NlZCBmb3IgJ2pzb24nIHR5cGUgYXMgaXQgY2FuIGJlIHBhcnNlZCBieSBkZWZhdWx0ICd0cmFuc2Zvcm1SZXNwb25zZScgZnVuY3Rpb24uXG4gICAgICAgIGlmIChjb25maWcucmVzcG9uc2VUeXBlICE9PSAnanNvbicpIHtcbiAgICAgICAgICB0aHJvdyBlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gSGFuZGxlIHByb2dyZXNzIGlmIG5lZWRlZFxuICAgIGlmICh0eXBlb2YgY29uZmlnLm9uRG93bmxvYWRQcm9ncmVzcyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgcmVxdWVzdC5hZGRFdmVudExpc3RlbmVyKCdwcm9ncmVzcycsIGNvbmZpZy5vbkRvd25sb2FkUHJvZ3Jlc3MpO1xuICAgIH1cblxuICAgIC8vIE5vdCBhbGwgYnJvd3NlcnMgc3VwcG9ydCB1cGxvYWQgZXZlbnRzXG4gICAgaWYgKHR5cGVvZiBjb25maWcub25VcGxvYWRQcm9ncmVzcyA9PT0gJ2Z1bmN0aW9uJyAmJiByZXF1ZXN0LnVwbG9hZCkge1xuICAgICAgcmVxdWVzdC51cGxvYWQuYWRkRXZlbnRMaXN0ZW5lcigncHJvZ3Jlc3MnLCBjb25maWcub25VcGxvYWRQcm9ncmVzcyk7XG4gICAgfVxuXG4gICAgaWYgKGNvbmZpZy5jYW5jZWxUb2tlbikge1xuICAgICAgLy8gSGFuZGxlIGNhbmNlbGxhdGlvblxuICAgICAgY29uZmlnLmNhbmNlbFRva2VuLnByb21pc2UudGhlbihmdW5jdGlvbiBvbkNhbmNlbGVkKGNhbmNlbCkge1xuICAgICAgICBpZiAoIXJlcXVlc3QpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICByZXF1ZXN0LmFib3J0KCk7XG4gICAgICAgIHJlamVjdChjYW5jZWwpO1xuICAgICAgICAvLyBDbGVhbiB1cCByZXF1ZXN0XG4gICAgICAgIHJlcXVlc3QgPSBudWxsO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKHJlcXVlc3REYXRhID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHJlcXVlc3REYXRhID0gbnVsbDtcbiAgICB9XG5cbiAgICAvLyBTZW5kIHRoZSByZXF1ZXN0XG4gICAgcmVxdWVzdC5zZW5kKHJlcXVlc3REYXRhKTtcbiAgfSk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2FkYXB0ZXJzL3hoci5qc1xuLy8gbW9kdWxlIGlkID0gOFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbnZhciBlbmhhbmNlRXJyb3IgPSByZXF1aXJlKCcuL2VuaGFuY2VFcnJvcicpO1xuXG4vKipcbiAqIENyZWF0ZSBhbiBFcnJvciB3aXRoIHRoZSBzcGVjaWZpZWQgbWVzc2FnZSwgY29uZmlnLCBlcnJvciBjb2RlLCByZXF1ZXN0IGFuZCByZXNwb25zZS5cbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gbWVzc2FnZSBUaGUgZXJyb3IgbWVzc2FnZS5cbiAqIEBwYXJhbSB7T2JqZWN0fSBjb25maWcgVGhlIGNvbmZpZy5cbiAqIEBwYXJhbSB7c3RyaW5nfSBbY29kZV0gVGhlIGVycm9yIGNvZGUgKGZvciBleGFtcGxlLCAnRUNPTk5BQk9SVEVEJykuXG4gKiBAcGFyYW0ge09iamVjdH0gW3JlcXVlc3RdIFRoZSByZXF1ZXN0LlxuICogQHBhcmFtIHtPYmplY3R9IFtyZXNwb25zZV0gVGhlIHJlc3BvbnNlLlxuICogQHJldHVybnMge0Vycm9yfSBUaGUgY3JlYXRlZCBlcnJvci5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBjcmVhdGVFcnJvcihtZXNzYWdlLCBjb25maWcsIGNvZGUsIHJlcXVlc3QsIHJlc3BvbnNlKSB7XG4gIHZhciBlcnJvciA9IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgcmV0dXJuIGVuaGFuY2VFcnJvcihlcnJvciwgY29uZmlnLCBjb2RlLCByZXF1ZXN0LCByZXNwb25zZSk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2NvcmUvY3JlYXRlRXJyb3IuanNcbi8vIG1vZHVsZSBpZCA9IDlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLi91dGlscycpO1xuXG4vKipcbiAqIENvbmZpZy1zcGVjaWZpYyBtZXJnZS1mdW5jdGlvbiB3aGljaCBjcmVhdGVzIGEgbmV3IGNvbmZpZy1vYmplY3RcbiAqIGJ5IG1lcmdpbmcgdHdvIGNvbmZpZ3VyYXRpb24gb2JqZWN0cyB0b2dldGhlci5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gY29uZmlnMVxuICogQHBhcmFtIHtPYmplY3R9IGNvbmZpZzJcbiAqIEByZXR1cm5zIHtPYmplY3R9IE5ldyBvYmplY3QgcmVzdWx0aW5nIGZyb20gbWVyZ2luZyBjb25maWcyIHRvIGNvbmZpZzFcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBtZXJnZUNvbmZpZyhjb25maWcxLCBjb25maWcyKSB7XG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wYXJhbS1yZWFzc2lnblxuICBjb25maWcyID0gY29uZmlnMiB8fCB7fTtcbiAgdmFyIGNvbmZpZyA9IHt9O1xuXG4gIHZhciB2YWx1ZUZyb21Db25maWcyS2V5cyA9IFsndXJsJywgJ21ldGhvZCcsICdwYXJhbXMnLCAnZGF0YSddO1xuICB2YXIgbWVyZ2VEZWVwUHJvcGVydGllc0tleXMgPSBbJ2hlYWRlcnMnLCAnYXV0aCcsICdwcm94eSddO1xuICB2YXIgZGVmYXVsdFRvQ29uZmlnMktleXMgPSBbXG4gICAgJ2Jhc2VVUkwnLCAndXJsJywgJ3RyYW5zZm9ybVJlcXVlc3QnLCAndHJhbnNmb3JtUmVzcG9uc2UnLCAncGFyYW1zU2VyaWFsaXplcicsXG4gICAgJ3RpbWVvdXQnLCAnd2l0aENyZWRlbnRpYWxzJywgJ2FkYXB0ZXInLCAncmVzcG9uc2VUeXBlJywgJ3hzcmZDb29raWVOYW1lJyxcbiAgICAneHNyZkhlYWRlck5hbWUnLCAnb25VcGxvYWRQcm9ncmVzcycsICdvbkRvd25sb2FkUHJvZ3Jlc3MnLFxuICAgICdtYXhDb250ZW50TGVuZ3RoJywgJ3ZhbGlkYXRlU3RhdHVzJywgJ21heFJlZGlyZWN0cycsICdodHRwQWdlbnQnLFxuICAgICdodHRwc0FnZW50JywgJ2NhbmNlbFRva2VuJywgJ3NvY2tldFBhdGgnXG4gIF07XG5cbiAgdXRpbHMuZm9yRWFjaCh2YWx1ZUZyb21Db25maWcyS2V5cywgZnVuY3Rpb24gdmFsdWVGcm9tQ29uZmlnMihwcm9wKSB7XG4gICAgaWYgKHR5cGVvZiBjb25maWcyW3Byb3BdICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgY29uZmlnW3Byb3BdID0gY29uZmlnMltwcm9wXTtcbiAgICB9XG4gIH0pO1xuXG4gIHV0aWxzLmZvckVhY2gobWVyZ2VEZWVwUHJvcGVydGllc0tleXMsIGZ1bmN0aW9uIG1lcmdlRGVlcFByb3BlcnRpZXMocHJvcCkge1xuICAgIGlmICh1dGlscy5pc09iamVjdChjb25maWcyW3Byb3BdKSkge1xuICAgICAgY29uZmlnW3Byb3BdID0gdXRpbHMuZGVlcE1lcmdlKGNvbmZpZzFbcHJvcF0sIGNvbmZpZzJbcHJvcF0pO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGNvbmZpZzJbcHJvcF0gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICBjb25maWdbcHJvcF0gPSBjb25maWcyW3Byb3BdO1xuICAgIH0gZWxzZSBpZiAodXRpbHMuaXNPYmplY3QoY29uZmlnMVtwcm9wXSkpIHtcbiAgICAgIGNvbmZpZ1twcm9wXSA9IHV0aWxzLmRlZXBNZXJnZShjb25maWcxW3Byb3BdKTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBjb25maWcxW3Byb3BdICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgY29uZmlnW3Byb3BdID0gY29uZmlnMVtwcm9wXTtcbiAgICB9XG4gIH0pO1xuXG4gIHV0aWxzLmZvckVhY2goZGVmYXVsdFRvQ29uZmlnMktleXMsIGZ1bmN0aW9uIGRlZmF1bHRUb0NvbmZpZzIocHJvcCkge1xuICAgIGlmICh0eXBlb2YgY29uZmlnMltwcm9wXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIGNvbmZpZ1twcm9wXSA9IGNvbmZpZzJbcHJvcF07XG4gICAgfSBlbHNlIGlmICh0eXBlb2YgY29uZmlnMVtwcm9wXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIGNvbmZpZ1twcm9wXSA9IGNvbmZpZzFbcHJvcF07XG4gICAgfVxuICB9KTtcblxuICB2YXIgYXhpb3NLZXlzID0gdmFsdWVGcm9tQ29uZmlnMktleXNcbiAgICAuY29uY2F0KG1lcmdlRGVlcFByb3BlcnRpZXNLZXlzKVxuICAgIC5jb25jYXQoZGVmYXVsdFRvQ29uZmlnMktleXMpO1xuXG4gIHZhciBvdGhlcktleXMgPSBPYmplY3RcbiAgICAua2V5cyhjb25maWcyKVxuICAgIC5maWx0ZXIoZnVuY3Rpb24gZmlsdGVyQXhpb3NLZXlzKGtleSkge1xuICAgICAgcmV0dXJuIGF4aW9zS2V5cy5pbmRleE9mKGtleSkgPT09IC0xO1xuICAgIH0pO1xuXG4gIHV0aWxzLmZvckVhY2gob3RoZXJLZXlzLCBmdW5jdGlvbiBvdGhlcktleXNEZWZhdWx0VG9Db25maWcyKHByb3ApIHtcbiAgICBpZiAodHlwZW9mIGNvbmZpZzJbcHJvcF0gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICBjb25maWdbcHJvcF0gPSBjb25maWcyW3Byb3BdO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGNvbmZpZzFbcHJvcF0gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICBjb25maWdbcHJvcF0gPSBjb25maWcxW3Byb3BdO1xuICAgIH1cbiAgfSk7XG5cbiAgcmV0dXJuIGNvbmZpZztcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9tZXJnZUNvbmZpZy5qc1xuLy8gbW9kdWxlIGlkID0gMTBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIEEgYENhbmNlbGAgaXMgYW4gb2JqZWN0IHRoYXQgaXMgdGhyb3duIHdoZW4gYW4gb3BlcmF0aW9uIGlzIGNhbmNlbGVkLlxuICpcbiAqIEBjbGFzc1xuICogQHBhcmFtIHtzdHJpbmc9fSBtZXNzYWdlIFRoZSBtZXNzYWdlLlxuICovXG5mdW5jdGlvbiBDYW5jZWwobWVzc2FnZSkge1xuICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xufVxuXG5DYW5jZWwucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gIHJldHVybiAnQ2FuY2VsJyArICh0aGlzLm1lc3NhZ2UgPyAnOiAnICsgdGhpcy5tZXNzYWdlIDogJycpO1xufTtcblxuQ2FuY2VsLnByb3RvdHlwZS5fX0NBTkNFTF9fID0gdHJ1ZTtcblxubW9kdWxlLmV4cG9ydHMgPSBDYW5jZWw7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY2FuY2VsL0NhbmNlbC5qc1xuLy8gbW9kdWxlIGlkID0gMTFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiXG5cblxuXG5cbmltcG9ydCBTd3VwIGZyb20gJ3N3dXAnO1xuY29uc3QgYXhpb3MgPSByZXF1aXJlKCdheGlvcycpLmRlZmF1bHQ7XG5pbXBvcnQgU3d1cEJvZHlDbGFzc1BsdWdpbiBmcm9tICdAc3d1cC9ib2R5LWNsYXNzLXBsdWdpbic7XG5pbXBvcnQgU3d1cERlYnVnUGx1Z2luIGZyb20gJ0Bzd3VwL2RlYnVnLXBsdWdpbic7XG5pbXBvcnQgU3d1cEpzUGx1Z2luIGZyb20gJ0Bzd3VwL2pzLXBsdWdpbic7XG5pbXBvcnQgU3d1cFByZWxvYWRQbHVnaW4gZnJvbSAnQHN3dXAvcHJlbG9hZC1wbHVnaW4nO1xuaW1wb3J0IFN3dXBTY3JvbGxQbHVnaW4gZnJvbSAnQHN3dXAvc2Nyb2xsLXBsdWdpbic7XG5cbmltcG9ydCAgXCIuL21vZHVsZXMvc2Nyb2xsLW1vZHVsZVwiO1xuXG4vLyBpbXBvcnQge1xuLy8gICBoZWFkcm9vbVxuLy8gfSBmcm9tIFwiLi9tb2R1bGVzL2hlYWRyb29tXCI7XG5cbmxhenlTaXplcy5jZmcuZXhwRmFjdG9yID0gNDtcbmltcG9ydCAnbGF6eXNpemVzJztcbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2xhenlsb2FkZWQnLCBmdW5jdGlvbihlKXtcblxuaWYgKGUudGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygnaGVyby1pbWFnZScpKSB7XG4gIC8vIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblxuZS50YXJnZXQuY2xvc2VzdChcIi5qcy1maWxtLWxpbmtcIikuY2xhc3NMaXN0LnJlbW92ZShcImltYWdlLS1sb2FkaW5nXCIpO1xuLy8gfSwgMzAwMCk7XG5cbn1cblxufSk7XG5cblxuXG4vLy8vSEVSTyBNT1VTRSBPVkVSIC8vL1xuXG5mdW5jdGlvbiBoZXJvX2hvdmVyKCl7XG52YXIgaGVyb19ob3Zlcl9pdGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5oZXJvLWJsb2NrLWxpbmtcIik7XG5cbmlmKGhlcm9faG92ZXJfaXRlbS5sZW5ndGggPiAwKSB7XG4gIGZvciAodmFyIGkgPSAwLCBsZW4gPSBoZXJvX2hvdmVyX2l0ZW0ubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICBoZXJvX2hvdmVyX2l0ZW1baV0uYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZW50ZXJcIiwgZnVuY3Rpb24oZSl7XG5cbi8vIGUudGFyZ2V0LmFwcGVuZENoaWxkKGNhbnZhc19pdGVtKTtcbiAgZS50YXJnZXQuY2xhc3NMaXN0LmFkZChcImhvdmVyLS1hY3RpdmVcIik7XG59KTtcbmhlcm9faG92ZXJfaXRlbVtpXS5hZGRFdmVudExpc3RlbmVyKFwibW91c2VsZWF2ZVwiLCBmdW5jdGlvbihlKXtcblxuXG5lLnRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKFwiaG92ZXItLWFjdGl2ZVwiKTtcbn0pO1xuXG59XG5cbn1cblxufVxuXG5mdW5jdGlvbiBjb21faG92ZXIoKXtcbnZhciBoZXJvX2hvdmVyX2l0ZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmNvbS1ibG9jay1saW5rXCIpO1xuXG5pZihoZXJvX2hvdmVyX2l0ZW0ubGVuZ3RoID4gMCkge1xuICBmb3IgKHZhciBpID0gMCwgbGVuID0gaGVyb19ob3Zlcl9pdGVtLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgaGVyb19ob3Zlcl9pdGVtW2ldLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWVudGVyXCIsIGZ1bmN0aW9uKGUpe1xudmFyIGNhbnZhc19pdGVtXzIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLm5vaXNlLWNhbnZhc18yXCIpO1xuLy8gZS50YXJnZXQucXVlcnlTZWxlY3RvcignLmNvbS1jZWxsLWlubmVyJykuYXBwZW5kQ2hpbGQoY2FudmFzX2l0ZW1fMik7XG4gIGUudGFyZ2V0LmNsYXNzTGlzdC5hZGQoXCJob3Zlci0tYWN0aXZlXCIpO1xufSk7XG5oZXJvX2hvdmVyX2l0ZW1baV0uYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbGVhdmVcIiwgZnVuY3Rpb24oZSl7XG5cblxuZS50YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImhvdmVyLS1hY3RpdmVcIik7XG59KTtcblxufVxuXG59XG5cbn1cblxuXG5cblxuXG5cblxuLy8vL0hFUk8gbG9hZCBtb3JlIC8vL1xuXG5cbmZ1bmN0aW9uIGhlcm9fbG9hZF9tb3JlKCkge1xuXG5cblxuY29uc3QgbGluayA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuanMtbG9hZC1zZWNvbmQtcm93XCIpO1xuY29uc3Qgc3Bpbm5lciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuc3Bpbi1zaGl0XCIpO1xubGluay5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IHtcbiAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgdmFyIHRoZV91cmwgPSBldmVudC5jdXJyZW50VGFyZ2V0LmhyZWY7XG4gIC8vIGNvbnNvbGUubG9nKHRoZV91cmwpO1xuXG5cblxuICBsaW5rLnN0eWxlLm9wYWNpdHkgPSBcIjBcIjtcbiAgc3Bpbm5lci5zdHlsZS5vcGFjaXR5ID0gXCIxXCI7XG5cblxuXG4gIGF4aW9zLmdldCh0aGVfdXJsKVxuICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgLy8gaGFuZGxlIHN1Y2Nlc3NcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuaGVyby1yb3ctLTJcIikuY2xhc3NMaXN0LnJlbW92ZShcImhpZGVcIik7XG4gICAgICAvLyBjb25zb2xlLmxvZyhyZXNwb25zZSk7XG4gICAgICAvLyBjb25zb2xlLmxvZyhyZXNwb25zZS5kYXRhKTtcbiAgICAgIHZhciBjb21faG9tZV9jb250YWluZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaGVyby1yb3ctLTInKTtcbiAgICAgICAgdmFyIHdyYXBwZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgICAgIHdyYXBwZXIuaW5uZXJIVE1MID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgIHZhciBuZXdDb250ZW50ID0gd3JhcHBlci5xdWVyeVNlbGVjdG9yKCcuc2VjdGlvbi0tY29tbWVyY2lhbHMtZ3JpZCcpO1xuICAgICAgICAgICAgY29tX2hvbWVfY29udGFpbmVyLmFwcGVuZENoaWxkKG5ld0NvbnRlbnQpO1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgIGxpbmsuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgc3Bpbm5lci5zdHlsZS5vcGFjaXR5ID0gXCIwXCI7XG4gICAgICAgICAgfSwgNTAwKTtcblxuICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuc3Bpbm5lci5zdHlsZS5vcGFjaXR5ID0gXCIwXCI7XG5cbnNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuaGVyby1yb3ctLTJcIikuY2xhc3NMaXN0LmFkZChcInRyYW5zZm9ybS1jb20tYmxvY2tcIik7XG59LCA1MDApO1xuc2Nyb2xsSXQoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmhvbWUtc2Nyb2xsLXBvaW50JyksIDEwMDAsICdlYXNlSW5PdXRRdWludCcsIGZ1bmN0aW9uICgpIHtcbnNwaW5uZXIuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5oZXJvLXJvdy0tMlwiKS5jbGFzc0xpc3QuYWRkKFwidHJhbnNmb3JtLWNvbS1ibG9ja1wiKTtcbi8vIGluaXRfc2Vjb25kX25vaXNlKCk7XG5jb21faG92ZXIoKTtcbndpbmRvdy5wamF4X3RhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIuY29tLWJsb2NrLWxpbmtcIik7XG5pbml0X3BqYXhfbGlua3MoKTtcbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuYm90dG9tLWxpbmtcIikuY2xhc3NMaXN0LnJlbW92ZShcImJvdHRvbS1saW5rLWhvbWVcIik7XG59KTtcbn0sIDUwMCk7XG5cblxuXG4gICAgfSlcbiAgICAuY2F0Y2goZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAvLyBoYW5kbGUgZXJyb3JcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9KVxuICAgIC50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKFwiYXhpb3MgcmVxdWVzdFwiKTtcbiAgICB9KTtcblxuXG5cblxuXG5cbi8vICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4vLyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmhlcm8tcm93LS0yXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJoZXJvLWNlbGwtdHJhbnNmb3JtLXRyaWdnZXJcIik7XG4vL1xuLy8gfSwgNTAwKTtcbi8vXG4vLyBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4vLyAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuY29tbWVyY2lhbHMtZmxleC1ncmlkXCIpLnN0eWxlLm9wYWNpdHkgPSBcIjFcIjtcbi8vIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuaGVyby1yb3ctLTFcIikuY2xhc3NMaXN0LmFkZChcInNlY29uZC1yb3ctbG9hZGVkXCIpO1xuLy8gfSwgMTEwMCk7XG5yZXR1cm4gZmFsc2U7XG4gIH0pO1xuXG59XG5cblxuXG5cblxuXG5cblxuXG5cbi8vLy8vL1NXVVAvLy8vLy9cblxuXG5jb25zdCBqc19vcHRpb25zID0gW1xuXG5cbiAge1xuICAgIGZyb206ICcoLiopJywgdG86ICdmaWxtLXZpZXctdHJhbnNpdGlvbicsXG4gICAgb3V0OiAobmV4dCkgPT4ge1xuICAgICAgdmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucHJvamVjdC1vdmVybGF5LXZpZXcnKS5jbGFzc0xpc3QuYWRkKFwicHJvamVjdC1vdmVybGF5LXZpZXctLXNob3dcIik7XG4gICAgICAvLyBuZXh0KCk7XG4gICAgICAvLyBjb25zb2xlLmxvZyhcInBqYXhcIik7XG5cbiAgICB9LFxuXG4gICAgaW46IGZ1bmN0aW9uKG5leHQpIHtcbiAgICAgIG5leHQoKTtcbiAgICB9LFxuXG4gIH0sXG5cblxuXG5cbntcbiAgZnJvbTogJyguKiknLCB0bzogJ2Jhc2ljLXRyYW5zaXRpb24nLFxuICBvdXQ6IChuZXh0KSA9PiB7XG4gICAgdmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG4gICAgcm9vdC5jbGFzc0xpc3QuYWRkKFwicGpheC0tZmFkZVwiKTtcbiAgICAvLyByb290LmNsYXNzTGlzdC5yZW1vdmUoXCJhY3RpdmF0ZS1hbmltYXRpb25cIik7XG4gICAgLy8gc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICBuZXh0KCk7XG4gICAgLy8gfSwgNTAwKTtcblxuXG4gIH0sXG5cbiAgaW46IGZ1bmN0aW9uKG5leHQpIHtcbiAgICAgIHdpbmRvdy5zY3JvbGxUbygwLCAwKTtcblxuICAgIHZhciByb290ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJIVE1MXCIpWzBdO1xuICAgICAgaWYgKHJvb3QuY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmF0ZS1hbmltYXRpb24nKSkge1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhcImxvZ28gYW5pbSBydW5ubmluZ1wiKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICAgIHJvb3QuY2xhc3NMaXN0LmFkZChcImFjdGl2YXRlLWFuaW1hdGlvblwiKTtcbiAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICByb290LmNsYXNzTGlzdC5yZW1vdmUoXCJhY3RpdmF0ZS1hbmltYXRpb25cIik7XG4gICAgICAgICAgfSwgMjYwMCk7XG4gICAgICB9XG5cbiAgICAgIGluaXRfdmlld3MoKTtcbiAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICByb290LmNsYXNzTGlzdC5yZW1vdmUoXCJwamF4LS1mYWRlXCIpO1xuICAgIG5leHQoKTtcbiAgfSwgMTAwKTtcbiAgICAvLyBuZXh0KCk7XG5cbiAgfSxcblxufSxcblxuXG5dO1xuXG5cblxuY29uc3Qgb3B0aW9ucyA9IHtcblxuICBjb250YWluZXJzOiBbJy5wamF4LWNvbnRhaW5lci0tbWFpbiddLFxuICBsaW5rU2VsZWN0b3I6XG4gICAgJy5wamF4LWxpbmsnLFxuICBhbmltYXRpb25TZWxlY3RvcjogJ1tjbGFzcyo9XCJwYWdlLXRyYW5zaXRpb25cIl0nLFxuICBhbmltYXRlSGlzdG9yeUJyb3dzaW5nOiB0cnVlLFxuICBwbHVnaW5zOiBbXG4gICAgbmV3IFN3dXBKc1BsdWdpbihqc19vcHRpb25zKSxcbiAgICAvLyBuZXcgU3d1cERlYnVnUGx1Z2luKCksXG5cbiAgICBuZXcgU3d1cFByZWxvYWRQbHVnaW4oKSxcbiAgICBuZXcgU3d1cEJvZHlDbGFzc1BsdWdpbigpLFxuLy8gICAgIG5ldyBTd3VwU2Nyb2xsUGx1Z2luKHtcbi8vICAgICBkb1Njcm9sbGluZ1JpZ2h0QXdheTogZmFsc2UsXG4vLyAgICAgYW5pbWF0ZVNjcm9sbDogZmFsc2UsXG4vLyAgICAgc2Nyb2xsRnJpY3Rpb246IDAuMyxcbi8vICAgICBjYWNoZTogZmFsc2UsXG4vLyAgICAgc2Nyb2xsQWNjZWxlcmF0aW9uOiAwLjA0LFxuLy8gfSksXG4gIF0sXG59O1xuXG5cbmlmIChkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5jb250YWlucygnc2luZ2xlJykpIHtcblxufVxuZWxzZSB7XG4gIGNvbnN0IHN3dXAgPSBuZXcgU3d1cChvcHRpb25zKTtcbiAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignc3d1cDpzYW1lUGFnZScsIChldmVudCkgPT4ge1xuICAvLyBjb25zb2xlLmxvZyhcInNhbWUgcGFnZVwiKTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJib2R5XCIpWzBdLmNsYXNzTGlzdC5yZW1vdmUoXCJtb2JpbGUtbWVudS0tYWN0aXZlXCIpO1xuICB9KTtcbn1cblxuXG5cbi8vIGNvbnN0IGxpbmsgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmhlcm8tY2VsbC1pbm5lclwiKTtcbi8vIGxpbmsuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB7XG4vLyAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4vLyAgIHZhciB0aGVfdXJsID0gZXZlbnQuY3VycmVudFRhcmdldC5ocmVmO1xuLy8gICBjb25zb2xlLmxvZyh0aGVfdXJsKTtcbi8vXG4vLyAgIHN3dXAucHJlbG9hZFBhZ2UodGhlX3VybCk7XG4vL1xuLy9cbi8vXG4vL1xuLy9cbi8vIHJldHVybiBmYWxzZTtcbi8vIH0pO1xuXG5cblxuXG4vLyBMT0FEIEZJTE0gLy8vXG5cblxuXG5cbmZ1bmN0aW9uIGNyZWF0ZV9wcm9qZWN0X2RhdGEoKXtcbnZhciBjbGllbnRfbmFtZSA9IGV2ZW50LmN1cnJlbnRUYXJnZXQuZGF0YXNldC5jbGllbnRuYW1lO1xuICB2YXIgcHJvamVjdF9uYW1lID0gZXZlbnQuY3VycmVudFRhcmdldC5kYXRhc2V0LnByb2plY3RuYW1lO1xuXG52YXIgY05hbWUgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjbGllbnRfbmFtZSk7XG52YXIgcE5hbWUgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShwcm9qZWN0X25hbWUpO1xuLy8gY29uc29sZS5sb2coY05hbWUpO1xuLy8gY29uc29sZS5sb2cocE5hbWUpO1xuZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5maWxtLWxvYWRpbmctd3JhcHBlci0tY2xpZW50LW5hbWVcIikuaW5uZXJIVE1MID0gXCJcIjtcbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5maWxtLWxvYWRpbmctd3JhcHBlci0tY2xpZW50LW5hbWUnKS5hcHBlbmRDaGlsZChjTmFtZSk7XG5kb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmZpbG0tbG9hZGluZy13cmFwcGVyLS1wcm9qZWN0LW5hbWVcIikuaW5uZXJIVE1MID0gXCJcIjtcbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5maWxtLWxvYWRpbmctd3JhcHBlci0tcHJvamVjdC1uYW1lJykuYXBwZW5kQ2hpbGQocE5hbWUpO1xufVxuXG5cblxuLy8vQ1VTVE9NIFBKQVggRnJvbSBHUklELy8vL1xuXG5cblxuXG5cblxuXG5mdW5jdGlvbiBpbml0X3BqYXhfbGlua3MoKSB7XG52YXIgZmlsbV9saW5rcyA9IHBqYXhfdGFyZ2V0O1xuLy8gY29uc29sZS5sb2coZmlsbV9saW5rcyk7XG5mb3IodmFyIHogPSAwOyB6IDwgZmlsbV9saW5rcy5sZW5ndGg7IHorKykge1xuICB2YXIgZWxlbSA9IGZpbG1fbGlua3Nbel07XG5cbiAgZWxlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgZXZlbnQuY3VycmVudFRhcmdldC5jbGFzc0xpc3QuYWRkKFwicHJldmVudC1jbGlja1wiKTtcbmV2ZW50LmN1cnJlbnRUYXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImhvdmVyLS1hY3RpdmVcIik7XG5cblxuXG5cbmNyZWF0ZV9wcm9qZWN0X2RhdGEoKTtcblxuLy8vL1RSQU5TSVRJT04gQmVmb3JlIGxvYWQvLy8vXG5cblxuXG5cblxuLy8vLy9USEUgRkVDVEggLy8vLy8vXG5cbiAgdmFyIHRoZV91cmwgPSBldmVudC5jdXJyZW50VGFyZ2V0LmhyZWY7XG4gIC8vIGNvbnNvbGUubG9nKHRoZV91cmwpO1xuICB2YXIgc3RhdGVPYmo7XG4gIHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZShzdGF0ZU9iaiwgXCJcIiwgdGhlX3VybCk7XG4gIHZhciBvbGRDb250ZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnBqYXgtY29udGFpbmVyLS1maWxtJyk7XG5pZiAob2xkQ29udGVudCAhPT0gbnVsbCkge1xuICBvbGRDb250ZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQob2xkQ29udGVudCk7XG59XG5cbndpbmRvdy5nQm9keVNjcm9sbCA9IHdpbmRvdy5wYWdlWU9mZnNldCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wIHx8IGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wIHx8IDA7XG5kb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY2xvc2UtZmlsbS1idXR0b24td3JhcHBlcicpLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XG4gIHZhciByb290ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJIVE1MXCIpWzBdO1xuXG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wcm9qZWN0LW92ZXJsYXktdmlldycpLnJlbW92ZUF0dHJpYnV0ZShcInN0eWxlXCIpO1xuXG5zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5kb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucHJvamVjdC1vdmVybGF5LXZpZXcnKS5jbGFzc0xpc3QuYWRkKFwicHJvamVjdC1vdmVybGF5LXZpZXctLXNob3dcIik7XG59LCAzMDApO1xuXG5cbi8vIE1ha2UgYSByZXF1ZXN0IGZvciBhIHVzZXIgd2l0aCBhIGdpdmVuIElEXG5heGlvcy5nZXQodGhlX3VybClcbiAgLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgLy8gaGFuZGxlIHN1Y2Nlc3NcblxuICAgIC8vIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcbiAgICAvLyBjb25zb2xlLmxvZyhyZXNwb25zZS5kYXRhKTtcbiAgICB2YXIgZmlsbV9jb250YWluZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucGpheC1wYXJlbnQnKTtcbiAgICAgIHZhciBmaWxtX2NvbnRhaW5lciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wamF4LXBhcmVudCcpO1xuXG4gICAgICB2YXIgd3JhcHBlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICAgIHdyYXBwZXIuaW5uZXJIVE1MID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICB2YXIgbmV3Q29udGVudCA9IHdyYXBwZXIucXVlcnlTZWxlY3RvcignLnBqYXgtY29udGFpbmVyLS1maWxtJyk7XG4gICAgICAgICAgZmlsbV9jb250YWluZXIuYXBwZW5kQ2hpbGQobmV3Q29udGVudCk7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tYWluLWFwcCcpLnN0eWxlLm9wYWNpdHkgPSBcIjBcIjtcbiAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm1haW4tbmF2aWdhdGlvbi0td3JhcHBlcicpLnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xuICAgICAgICAgICAgICByb290LmNsYXNzTGlzdC5hZGQoXCJib2R5LWJnLW92ZXJyaWRlXCIpO1xuICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbmFsbGUtYm9keScpLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wcm9qZWN0LW92ZXJsYXktdmlldycpLnN0eWxlLnBvc2l0aW9uID0gXCJyZWxhdGl2ZVwiO1xuICAgICAgICAgIH0sIDEwKTtcbiAgICAgICAgICAgIHdpbmRvdy5zY3JvbGxUbygwLCAwKTtcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wcmV2ZW50LWNsaWNrJykuY2xhc3NMaXN0LnJlbW92ZShcInByZXZlbnQtY2xpY2tcIik7XG4gICAgICAgICAgICAgIHZhciBuZXh0X2ZpbG1fbGluayA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuanMtbmV4dC1maWxtLWxpbmtcIik7XG4gICAgICAgICAgICAgIGlmKHR5cGVvZihuZXh0X2ZpbG1fbGluaykgIT0gJ3VuZGVmaW5lZCcgJiYgbmV4dF9maWxtX2xpbmsgIT0gbnVsbCl7XG4gICAgICAgICAgICAgICAgbmV4dF9maWxtX3RyaWdnZXIoKTtcbiAgICAgICAgICAgICAgfSBlbHNle1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwibm8gbmV4dCBmaWxtXCIpO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZpbG0tbG9hZGluZy13cmFwcGVyJykuc3R5bGUub3BhY2l0eSA9IFwiMFwiO1xuICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZpbG0tbG9hZGluZy13cmFwcGVyJykuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgICAgIHZhciByb290ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJIVE1MXCIpWzBdO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jbG9zZS1maW0tYnV0dG9uJykuc3R5bGUub3BhY2l0eSA9IFwiMVwiO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wamF4LWNvbnRhaW5lci0tbWFpbicpLnJlbW92ZUF0dHJpYnV0ZShcInN0eWxlXCIpO1xuXG4gICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wamF4LWNvbnRhaW5lci0tZmlsbScpLmNsYXNzTGlzdC5yZW1vdmUoXCJmaWxtLS1oaWRkZW5cIik7XG5cblxuXG4gICAgICAgICB9LCA1MDApO1xuICAgICAgIH0sIDEwMDApO1xuXG5cbiAgfSlcbiAgLmNhdGNoKGZ1bmN0aW9uIChlcnJvcikge1xuICAgIC8vIGhhbmRsZSBlcnJvclxuICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgfSlcbiAgLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgIC8vIGNvbnNvbGUubG9nKFwiYXhpb3MgcmVxdWVzdFwiKTtcbiAgICByb290LmNsYXNzTGlzdC5hZGQoXCJvdmVybGF5LXZpZXctLWFjdGl2ZVwiKTtcbiAgfSk7XG5cblxuXG5cblxuXG5cblxucmV0dXJuIGZhbHNlO1xufSk7XG59XG5cbn1cblxuXG5cbi8vLy8vLy9OTkVYVCBGSUxNIExJTktTIC8vLy8vLy8vL1xuXG4vL0NvbWJpbmUgY29kZSBpZiBuZWVkZWQgbW9yZSBlZGl0cy8vLy9cblxuZnVuY3Rpb24gbmV4dF9maWxtX3RyaWdnZXIoKSB7XG5cblxudmFyIG5leHRfZmlsbV9saW5rID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5qcy1uZXh0LWZpbG0tbGlua1wiKTtcblxuXG5uZXh0X2ZpbG1fbGluay5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IHtcbi8vIGNvbnNvbGUubG9nKFwibmV4dCBmaWlpaWlsbVwiKTtcbiAgICBldmVudC5jdXJyZW50VGFyZ2V0LmNsYXNzTGlzdC5hZGQoXCJwcmV2ZW50LWNsaWNrXCIpO1xuICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cblxuZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZpbG0tbG9hZGluZy13cmFwcGVyJykuc3R5bGUucmVtb3ZlUHJvcGVydHkoJ2Rpc3BsYXknKTtcbmNyZWF0ZV9wcm9qZWN0X2RhdGEoKTtcblxuICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgMCk7XG4gICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5maWxtLWxvYWRpbmctd3JhcHBlcicpLnN0eWxlLm9wYWNpdHkgPSBcIjFcIjtcbiAgICAgICAgIC8vIHdpbmRvdy5zY3JvbGxUbygwLCAwKTtcbiAgICAgICB9LCA1MDApO1xuXG4vLy8vVFJBTlNJVElPTiBCZWZvcmUgbG9hZC8vLy9cblxuXG4gIHZhciByb290ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJIVE1MXCIpWzBdO1xuXG5cblxuLy8vLy9USEUgRkVDVEggLy8vLy8vXG5cbiAgdmFyIHRoZV91cmwgPSBldmVudC5jdXJyZW50VGFyZ2V0LmhyZWY7XG4gIHZhciBzdGF0ZU9iajtcbiAgd2luZG93Lmhpc3RvcnkucmVwbGFjZVN0YXRlKHN0YXRlT2JqLCBcIlwiLCB0aGVfdXJsKTtcbiAgLy8gY29uc29sZS5sb2codGhlX3VybCk7XG4gIHZhciBvbGRDb250ZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnBqYXgtY29udGFpbmVyLS1maWxtJyk7XG5pZiAob2xkQ29udGVudCAhPT0gbnVsbCkge1xuICBvbGRDb250ZW50LnN0eWxlLm9wYWNpdHkgPSAwO1xufVxuXG5cblxuYXhpb3MuZ2V0KHRoZV91cmwpXG4gIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgIC8vIGhhbmRsZSBzdWNjZXNzXG5cbiAgICAvLyBjb25zb2xlLmxvZyhyZXNwb25zZSk7XG4gICAgLy8gY29uc29sZS5sb2cocmVzcG9uc2UuZGF0YSk7XG4gICAgdmFyIGZpbG1fY29udGFpbmVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnBqYXgtcGFyZW50Jyk7XG5cblxuICAgICAgdmFyIHdyYXBwZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgICB3cmFwcGVyLmlubmVySFRNTCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgdmFyIG5ld0NvbnRlbnQgPSB3cmFwcGVyLnF1ZXJ5U2VsZWN0b3IoJy5wamF4LWNvbnRhaW5lci0tZmlsbScpO1xuICAgICAgICAgIGZpbG1fY29udGFpbmVyLmFwcGVuZENoaWxkKG5ld0NvbnRlbnQpO1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICBvbGRDb250ZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQob2xkQ29udGVudCk7XG4gICAgICAgICAgICAgIG5leHRfZmlsbV90cmlnZ2VyKCk7XG4gICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5maWxtLWxvYWRpbmctd3JhcHBlcicpLnN0eWxlLm9wYWNpdHkgPSBcIjBcIjtcbiAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuXG4gICAgICAgICAgICAgICAgdmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNsb3NlLWZpbG0tYnV0dG9uLXdyYXBwZXInKS5zdHlsZS5vcGFjaXR5ID0gXCIxXCI7XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZpbG0tbG9hZGluZy13cmFwcGVyJykuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucHJvamVjdC1vdmVybGF5LXZpZXcnKS5zdHlsZS5wb3NpdGlvbiA9IFwicmVsYXRpdmVcIjtcbiAgICAgICAgICAgICAgd2luZG93LnNjcm9sbFRvKDAsIDApO1xuICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucGpheC1jb250YWluZXItLWZpbG0nKS5jbGFzc0xpc3QucmVtb3ZlKFwiZmlsbS0taGlkZGVuXCIpO1xuXG5cbiAgICAgICAgICB9LCAzMDApO1xuICAgICAgICB9LCAxMDAwKTtcblxuXG4gIH0pXG4gIC5jYXRjaChmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAvLyBoYW5kbGUgZXJyb3JcbiAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gIH0pXG4gIC50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAvLyBjb25zb2xlLmxvZyhcImF4aW9zIHJlcXVlc3RcIik7XG4gIH0pO1xuXG5cblxuXG5cbnJldHVybiBmYWxzZTtcbn0pO1xuXG59XG5cblxuXG4vLy8vLy9MT0FEIEZJTE0gRU5EUyAvLy8vLy8vXG5cblxuXG5cbi8vLy8vL0NMT1NFIEZJTE0gLy8vLy8vL1xuXG5mdW5jdGlvbiBjbG9zZV9vdmVybGF5KCkge1xuXG52YXIgcm9vdCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiSFRNTFwiKVswXTtcbndpbmRvdy5wYWdlc3JvbCA9IHdpbmRvdy5wYWdlWU9mZnNldCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wIHx8IGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wIHx8IDA7XG52YXIgbWludXNwYWdlc3JvbCA9IC0gKHBhZ2Vzcm9sKTtcbi8vIGNvbnNvbGUubG9nKHBhZ2Vzcm9sKTtcbi8vICAgaWYgKHJvb3QuY2xhc3NMaXN0LmNvbnRhaW5zKCdoZWFkZXItLW5vdC1vbi10b3AnKSkge1xuLy8gcm9vdC5jbGFzc0xpc3QuYWRkKFwiYmlvLWJ1dHRvbi0ta2VlcC1wb3NpdGlvblwiKTtcbi8vICAgfVxuLy8gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmhlYWRlci1iZy13cmFwJykucmVtb3ZlQXR0cmlidXRlKFwic3R5bGVcIik7XG4vLyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaGlkZS1iaW8tb3ZlcmxheS13cmFwcGVyJykuc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcbiAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNsb3NlLWZpbS1idXR0b24nKS5zdHlsZS5vcGFjaXR5ID0gXCIwXCI7XG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wcm9qZWN0LW92ZXJsYXktdmlldycpLnN0eWxlLnBvc2l0aW9uID0gXCJmaXhlZFwiO1xuZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5pbm5lci1tb3ZlclwiKS5zdHlsZS50cmFuc2Zvcm0gPSBcInRyYW5zbGF0ZVkoXCIgKyBtaW51c3BhZ2Vzcm9sICsgXCJweClcIjtcbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNuYWxsZS1ib2R5Jykuc3R5bGUuZGlzcGxheSA9IFwiXCI7XG5cblxuICAvLyBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICByb290LmNsYXNzTGlzdC5yZW1vdmUoXCJib2R5LWJnLW92ZXJyaWRlXCIpO1xuXG5cbiAgLy8gfSwgMTAwKTtcbi8vIHdpbmRvdy5zY3JvbGxUbygwLCAwKTtcbiAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgIHdpbmRvdy5zY3JvbGxUbygwLCBnQm9keVNjcm9sbCk7XG5zZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm1haW4tYXBwJykuc3R5bGUub3BhY2l0eSA9IFwiXCI7XG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm1haW4tbmF2aWdhdGlvbi0td3JhcHBlcicpLnN0eWxlLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucHJvamVjdC1vdmVybGF5LXZpZXcnKS5jbGFzc0xpc3QucmVtb3ZlKFwicHJvamVjdC1vdmVybGF5LXZpZXctLXNob3dcIik7XG4gICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5pbm5lci1tb3ZlclwiKS5yZW1vdmVBdHRyaWJ1dGUoXCJzdHlsZVwiKTtcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5maWxtLWxvYWRpbmctd3JhcHBlcicpLnJlbW92ZUF0dHJpYnV0ZShcInN0eWxlXCIpO1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnByb2plY3Qtb3ZlcmxheS12aWV3Jykuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgLy8gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmpheW1vLWJpby1zZWN0aW9uJykucmVtb3ZlQXR0cmlidXRlKFwic3R5bGVcIik7XG4gICAgICAvLyByb290LmNsYXNzTGlzdC5hZGQoXCJmaWx0ZXItc3RpY2t5LS1hY3RpdmVcIik7XG4gICAgICAvLyByb290LmNsYXNzTGlzdC5yZW1vdmUoXCJmaWx0ZXItc3RpY2t5LS1zdXBlci1hY3RpdmVcIik7XG4gICAgICAvLyBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuamF5bW8tYmlvLXNlY3Rpb24nKS5jbGFzc0xpc3QucmVtb3ZlKFwic2hvdy1zaGFrZXJcIik7XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY2xvc2UtZmlsbS1idXR0b24td3JhcHBlcicpLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICAgIC8vIHJvb3QuY2xhc3NMaXN0LnJlbW92ZShcImJpby1idXR0b24tLWtlZXAtcG9zaXRpb25cIik7XG4gICAgICB2YXIgb2xkQ29udGVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wamF4LWNvbnRhaW5lci0tZmlsbScpO1xuXG5cblxuICAgICAgaWYgKGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmNvbnRhaW5zKCdwYWdlLXRlbXBsYXRlLWhvbWUnKSkge1xuICAgICAgICB2YXIgc3RhdGVPYmo7XG4gICAgICAgIHdpbmRvdy5oaXN0b3J5LnJlcGxhY2VTdGF0ZShzdGF0ZU9iaiwgXCJcIiwgaG9tZV91cmwpO1xuXG4gICAgICB9XG4gICAgICBpZiAoZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuY29udGFpbnMoJ3BhZ2UtdGVtcGxhdGUtY29tbWVyY2lhbHMnKSkge1xuICAgICAgICB2YXIgc3RhdGVPYmo7XG4gICAgICAgIHdpbmRvdy5oaXN0b3J5LnJlcGxhY2VTdGF0ZShzdGF0ZU9iaiwgXCJcIiwgaG9tZV91cmwgKyBcIi9jb21tZXJjaWFscy9cIik7XG4gICAgICB9XG5cbiAgICAgIGlmIChkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5jb250YWlucygncGFnZS10ZW1wbGF0ZS1zaG9ydGZpbG1zJykpIHtcbiAgICAgICAgdmFyIHN0YXRlT2JqO1xuICAgICAgICB3aW5kb3cuaGlzdG9yeS5yZXBsYWNlU3RhdGUoc3RhdGVPYmosIFwiXCIsIGhvbWVfdXJsICsgXCIvc2hvcnQtZmlsbXMvXCIpO1xuICAgICAgfVxuXG4gICAgICByb290LmNsYXNzTGlzdC5yZW1vdmUoXCJvdmVybGF5LXZpZXctLWFjdGl2ZVwiKTtcblxuICAgIGlmIChvbGRDb250ZW50ICE9PSBudWxsKSB7XG4gICAgICBvbGRDb250ZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQob2xkQ29udGVudCk7XG4gICAgfVxuXG5cbiAgICB9LCA2MDApO1xuICB9LCAxMDApO1xufSwgNTAwKTtcbn1cblxuXG5cbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNqcy1jbG9zZS1maWxtJykub25jbGljayA9IGZ1bmN0aW9uKCkge1xuY2xvc2Vfb3ZlcmxheSgpO1xucmV0dXJuIGZhbHNlO1xufTtcblxuXG5mdW5jdGlvbiBjcmVhdGVfY2FudmFzXzEgKCkge1xudmFyIG5ld19jYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTtcbm5ld19jYW52YXMuY2xhc3NOYW1lID0gXCJub2lzZS1jYW52YXMgbm9pc2UtY2FudmFzXzFcIjtcbmNvbnNvbGUubG9nKG5ld19jYW52YXMpO1xuLy8gY29uc29sZS5sb2coY2FudmFzX2l0ZW0pO1xudmFyIGNhbnZhc19wYXJlbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaGVyby1ibG9jay1saW5rJyk7XG52YXIgcG9zaXRpb25JbmZvID0gY2FudmFzX3BhcmVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbnZhciBjX2hlaWdodCA9IHBvc2l0aW9uSW5mby5oZWlnaHQ7XG52YXIgY193aWR0aCA9IHBvc2l0aW9uSW5mby53aWR0aDtcbmNvbnNvbGUubG9nKGNfaGVpZ2h0KTtcbmNvbnNvbGUubG9nKGNfd2lkdGgpO1xuZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5oZXJvLWJsb2NrLWxpbmtcIikuYXBwZW5kQ2hpbGQobmV3X2NhbnZhcyk7XG53aW5kb3cuY2FudmFzX2l0ZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLm5vaXNlLWNhbnZhc18xXCIpO1xubm9pc2VfMSgpO1xufVxuXG5cbiAgLy8gZm9yICh2YXIgaSA9IDAsIGxlbiA9IGNhbnZhc19pdGVtLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgLy8gbm9pc2VfMSgpO1xuICAvLyB9XG5cbmZ1bmN0aW9uIG5vaXNlXzEoKSB7XG52YXIgdmlld1dpZHRoLFxuICAgIHZpZXdIZWlnaHQsXG4gICAgY2FudmFzID0gY2FudmFzX2l0ZW0sXG4gICAgY3R4O1xuXG4vLyBjaGFuZ2UgdGhlc2Ugc2V0dGluZ3NcbnZhciBwYXR0ZXJuU2l6ZSA9IDY0LFxuICAgIHBhdHRlcm5TY2FsZVggPSAzLFxuICAgIHBhdHRlcm5TY2FsZVkgPSAxLFxuICAgIHBhdHRlcm5SZWZyZXNoSW50ZXJ2YWwgPSA0LFxuICAgIHBhdHRlcm5BbHBoYSA9IDI1OyAvLyBpbnQgYmV0d2VlbiAwIGFuZCAyNTUsXG5cbnZhciBwYXR0ZXJuUGl4ZWxEYXRhTGVuZ3RoID0gcGF0dGVyblNpemUgKiBwYXR0ZXJuU2l6ZSAqIDQsXG4gICAgcGF0dGVybkNhbnZhcyxcbiAgICBwYXR0ZXJuQ3R4LFxuICAgIHBhdHRlcm5EYXRhLFxuICAgIGZyYW1lID0gMDtcblxuLy8gd2luZG93Lm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAgIGluaXRDYW52YXMoKTtcbiAgICBpbml0R3JhaW4oKTtcbiAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUobG9vcCk7XG4vLyB9O1xuXG4vLyBjcmVhdGUgYSBjYW52YXMgd2hpY2ggd2lsbCByZW5kZXIgdGhlIGdyYWluXG5mdW5jdGlvbiBpbml0Q2FudmFzKCkge1xuICAgIHZpZXdXaWR0aCA9IGNhbnZhcy53aWR0aCA9IGNhbnZhcy5jbGllbnRXaWR0aDtcbiAgICB2aWV3SGVpZ2h0ID0gY2FudmFzLmhlaWdodCA9IGNhbnZhcy5jbGllbnRIZWlnaHQ7XG4gICAgY3R4ID0gY2FudmFzLmdldENvbnRleHQoJzJkJyk7XG4gICAgY29uc29sZS5sb2coY3R4KTtcbiAgICBjdHguc2NhbGUocGF0dGVyblNjYWxlWCwgcGF0dGVyblNjYWxlWSk7XG59XG5cbi8vIGNyZWF0ZSBhIGNhbnZhcyB3aGljaCB3aWxsIGJlIHVzZWQgYXMgYSBwYXR0ZXJuXG5mdW5jdGlvbiBpbml0R3JhaW4oKSB7XG4gICAgcGF0dGVybkNhbnZhcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2NhbnZhcycpO1xuICAgIGNvbnNvbGUubG9nKHBhdHRlcm5DYW52YXMpO1xuICAgIHBhdHRlcm5DYW52YXMud2lkdGggPSBwYXR0ZXJuU2l6ZTtcbiAgICBwYXR0ZXJuQ2FudmFzLmhlaWdodCA9IHBhdHRlcm5TaXplO1xuICAgIHBhdHRlcm5DdHggPSBwYXR0ZXJuQ2FudmFzLmdldENvbnRleHQoJzJkJyk7XG4gICAgcGF0dGVybkRhdGEgPSBwYXR0ZXJuQ3R4LmNyZWF0ZUltYWdlRGF0YShwYXR0ZXJuU2l6ZSwgcGF0dGVyblNpemUpO1xufVxuXG4vLyBwdXQgYSByYW5kb20gc2hhZGUgb2YgZ3JheSBpbnRvIGV2ZXJ5IHBpeGVsIG9mIHRoZSBwYXR0ZXJuXG5mdW5jdGlvbiB1cGRhdGUoKSB7XG4gICAgdmFyIHZhbHVlO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXR0ZXJuUGl4ZWxEYXRhTGVuZ3RoOyBpICs9IDQpIHtcbiAgICAgICAgdmFsdWUgPSAoTWF0aC5yYW5kb20oKSAqIDIwNSkgfCAwO1xuXG4gICAgICAgIHBhdHRlcm5EYXRhLmRhdGFbaSAgICBdID0gdmFsdWU7XG4gICAgICAgIHBhdHRlcm5EYXRhLmRhdGFbaSArIDFdID0gdmFsdWU7XG4gICAgICAgIHBhdHRlcm5EYXRhLmRhdGFbaSArIDJdID0gdmFsdWU7XG4gICAgICAgIHBhdHRlcm5EYXRhLmRhdGFbaSArIDNdID0gcGF0dGVybkFscGhhO1xuICAgIH1cblxuICAgIHBhdHRlcm5DdHgucHV0SW1hZ2VEYXRhKHBhdHRlcm5EYXRhLCAwLCAwKTtcbn1cblxuLy8gZmlsbCB0aGUgY2FudmFzIHVzaW5nIHRoZSBwYXR0ZXJuXG5mdW5jdGlvbiBkcmF3KCkge1xuICAgIGN0eC5jbGVhclJlY3QoMCwgMCwgdmlld1dpZHRoLCB2aWV3SGVpZ2h0KTtcblxuICAgIGN0eC5maWxsU3R5bGUgPSBjdHguY3JlYXRlUGF0dGVybihwYXR0ZXJuQ2FudmFzLCAncmVwZWF0Jyk7XG4gICAgY3R4LmZpbGxSZWN0KDAsIDAsIHZpZXdXaWR0aCwgdmlld0hlaWdodCk7XG59XG5cbmZ1bmN0aW9uIGxvb3AoKSB7XG4gICAgaWYgKCsrZnJhbWUgJSBwYXR0ZXJuUmVmcmVzaEludGVydmFsID09PSAwKSB7XG4gICAgICAgIHVwZGF0ZSgpO1xuICAgICAgICBkcmF3KCk7XG4gICAgfVxuXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKGxvb3ApO1xufVxuXG59XG5cblxuXG5cblxuXG4vLy8vLy9NT0JJTEUgTkFWLy8vLy9cblxuXG5mdW5jdGlvbiBtb2JpbGVfbmF2KCkge1xuY29uc3QgbWVudV90cmlnZ2VyX29wZW4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmpzLW1vYmlsZS1uYXYtdHJpZ2dlclwiKTtcbi8vIGNvbnN0IG1lbnVfdHJpZ2dlcl9jbG9zZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuanMtY2xvc2UtbW9iaWxlLW1lbnVcIik7XG4gIG1lbnVfdHJpZ2dlcl9vcGVuLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbigpe1xuICAgIC8vIGNvbnNvbGUubG9nKFwiTU9CSUxFIE1FTlVcIik7XG4gICAgaWYgKGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmNvbnRhaW5zKCdtb2JpbGUtbWVudS0tYWN0aXZlJykpIHtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiYm9keVwiKVswXS5jbGFzc0xpc3QucmVtb3ZlKFwibW9iaWxlLW1lbnUtLWFjdGl2ZVwiKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImJvZHlcIilbMF0uY2xhc3NMaXN0LmFkZChcIm1vYmlsZS1tZW51LS1hY3RpdmVcIik7XG4gICAgfVxuXG5cblxuXG5cbiAgfSk7XG5cbn1cbm1vYmlsZV9uYXYoKTtcblxuXG4gIC8vIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbi8vIG5vaXNlXzEoKTtcbi8vIG5vaXNlXzIoKTtcbi8vIH0sIDIwMDApO1xuXG5cblxuXG5cblxuLy9TRUNPTkQgTk9JU0UgLy9cblxuZnVuY3Rpb24gaW5pdF9zZWNvbmRfbm9pc2UoKSB7XG4gIHZhciBuZXdfY2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJyk7XG4gIG5ld19jYW52YXMuY2xhc3NOYW1lID0gXCJub2lzZS1jYW52YXMgbm9pc2UtY2FudmFzXzJcIjtcbiAgY29uc29sZS5sb2cobmV3X2NhbnZhcyk7XG5cblxuICAvLyBjb25zb2xlLmxvZyhjYW52YXNfaXRlbSk7XG4gIC8vIHZhciBjYW52YXNfcGFyZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNvbS1jZWxsLWlubmVyJyk7XG5cbiAgdmFyIGNhbnZhc19wYXJlbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNvbS1jZWxsLWlubmVyJyk7XG4gIHZhciBsYXN0X2NhbnZhcyA9IGNhbnZhc19wYXJlbnRzW2NhbnZhc19wYXJlbnRzLmxlbmd0aC0gMV07XG5cbiAgdmFyIHBvc2l0aW9uSW5mbyA9IGxhc3RfY2FudmFzLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICB2YXIgY19oZWlnaHQgPSBwb3NpdGlvbkluZm8uaGVpZ2h0O1xuICB2YXIgY193aWR0aCA9IHBvc2l0aW9uSW5mby53aWR0aDtcbiAgY29uc29sZS5sb2coY19oZWlnaHQpO1xuICBjb25zb2xlLmxvZyhjX3dpZHRoKTtcblxuICB2YXIgYXBwZW5kX2Jsb2NrX3BhcmVudHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuY29tLWJsb2NrLWxpbmsnKTtcbiAgdmFyIGFwcGVuZF9ibG9jayA9IGFwcGVuZF9ibG9ja19wYXJlbnRzW2FwcGVuZF9ibG9ja19wYXJlbnRzLmxlbmd0aC0gMV07XG5cbiAgYXBwZW5kX2Jsb2NrLmFwcGVuZENoaWxkKG5ld19jYW52YXMpO1xuICB3aW5kb3cuY2FudmFzX2l0ZW1fMiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubm9pc2UtY2FudmFzXzJcIik7XG4gIGNvbnNvbGUubG9nKGNhbnZhc19pdGVtXzIpO1xubm9pc2VfMigpO1xuICBmdW5jdGlvbiBub2lzZV8yKCkge1xuICAgIGNvbnNvbGUubG9nKFwibm9pc2UgMiBhY3RpdmVcIik7XG4gIHZhciB2aWV3V2lkdGgsXG4gICAgICB2aWV3SGVpZ2h0LFxuICAgICAgY2FudmFzID0gY2FudmFzX2l0ZW1fMixcbiAgICAgIGN0eDtcblxuICAvLyBjaGFuZ2UgdGhlc2Ugc2V0dGluZ3NcbiAgdmFyIHBhdHRlcm5TaXplID0gNjQsXG4gICAgICBwYXR0ZXJuU2NhbGVYID0gMyxcbiAgICAgIHBhdHRlcm5TY2FsZVkgPSAxLFxuICAgICAgcGF0dGVyblJlZnJlc2hJbnRlcnZhbCA9IDQsXG4gICAgICBwYXR0ZXJuQWxwaGEgPSAyNTsgLy8gaW50IGJldHdlZW4gMCBhbmQgMjU1LFxuXG4gIHZhciBwYXR0ZXJuUGl4ZWxEYXRhTGVuZ3RoID0gcGF0dGVyblNpemUgKiBwYXR0ZXJuU2l6ZSAqIDQsXG4gICAgICBwYXR0ZXJuQ2FudmFzLFxuICAgICAgcGF0dGVybkN0eCxcbiAgICAgIHBhdHRlcm5EYXRhLFxuICAgICAgZnJhbWUgPSAwO1xuXG4gIC8vIHdpbmRvdy5vbmxvYWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIGluaXRDYW52YXMoKTtcbiAgICAgIGluaXRHcmFpbigpO1xuICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKGxvb3ApO1xuICAvLyB9O1xuXG4gIC8vIGNyZWF0ZSBhIGNhbnZhcyB3aGljaCB3aWxsIHJlbmRlciB0aGUgZ3JhaW5cbiAgZnVuY3Rpb24gaW5pdENhbnZhcygpIHtcbiAgICAgIHZpZXdXaWR0aCA9IGNhbnZhcy53aWR0aCA9IGNhbnZhcy5jbGllbnRXaWR0aDtcbiAgICAgIHZpZXdIZWlnaHQgPSBjYW52YXMuaGVpZ2h0ID0gY2FudmFzLmNsaWVudEhlaWdodDtcbiAgICAgIGN0eCA9IGNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuXG4gICAgICBjdHguc2NhbGUocGF0dGVyblNjYWxlWCwgcGF0dGVyblNjYWxlWSk7XG4gIH1cblxuICAvLyBjcmVhdGUgYSBjYW52YXMgd2hpY2ggd2lsbCBiZSB1c2VkIGFzIGEgcGF0dGVyblxuICBmdW5jdGlvbiBpbml0R3JhaW4oKSB7XG4gICAgICBwYXR0ZXJuQ2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJyk7XG4gICAgICBwYXR0ZXJuQ2FudmFzLndpZHRoID0gcGF0dGVyblNpemU7XG4gICAgICBwYXR0ZXJuQ2FudmFzLmhlaWdodCA9IHBhdHRlcm5TaXplO1xuICAgICAgcGF0dGVybkN0eCA9IHBhdHRlcm5DYW52YXMuZ2V0Q29udGV4dCgnMmQnKTtcbiAgICAgIHBhdHRlcm5EYXRhID0gcGF0dGVybkN0eC5jcmVhdGVJbWFnZURhdGEocGF0dGVyblNpemUsIHBhdHRlcm5TaXplKTtcbiAgfVxuXG4gIC8vIHB1dCBhIHJhbmRvbSBzaGFkZSBvZiBncmF5IGludG8gZXZlcnkgcGl4ZWwgb2YgdGhlIHBhdHRlcm5cbiAgZnVuY3Rpb24gdXBkYXRlKCkge1xuICAgICAgdmFyIHZhbHVlO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBhdHRlcm5QaXhlbERhdGFMZW5ndGg7IGkgKz0gNCkge1xuICAgICAgICAgIHZhbHVlID0gKE1hdGgucmFuZG9tKCkgKiAyMDUpIHwgMDtcblxuICAgICAgICAgIHBhdHRlcm5EYXRhLmRhdGFbaSAgICBdID0gdmFsdWU7XG4gICAgICAgICAgcGF0dGVybkRhdGEuZGF0YVtpICsgMV0gPSB2YWx1ZTtcbiAgICAgICAgICBwYXR0ZXJuRGF0YS5kYXRhW2kgKyAyXSA9IHZhbHVlO1xuICAgICAgICAgIHBhdHRlcm5EYXRhLmRhdGFbaSArIDNdID0gcGF0dGVybkFscGhhO1xuICAgICAgfVxuXG4gICAgICBwYXR0ZXJuQ3R4LnB1dEltYWdlRGF0YShwYXR0ZXJuRGF0YSwgMCwgMCk7XG4gIH1cblxuICAvLyBmaWxsIHRoZSBjYW52YXMgdXNpbmcgdGhlIHBhdHRlcm5cbiAgZnVuY3Rpb24gZHJhdygpIHtcbiAgICAgIGN0eC5jbGVhclJlY3QoMCwgMCwgdmlld1dpZHRoLCB2aWV3SGVpZ2h0KTtcblxuICAgICAgY3R4LmZpbGxTdHlsZSA9IGN0eC5jcmVhdGVQYXR0ZXJuKHBhdHRlcm5DYW52YXMsICdyZXBlYXQnKTtcbiAgICAgIGN0eC5maWxsUmVjdCgwLCAwLCB2aWV3V2lkdGgsIHZpZXdIZWlnaHQpO1xuICB9XG5cbiAgZnVuY3Rpb24gbG9vcCgpIHtcbiAgICAgIGlmICgrK2ZyYW1lICUgcGF0dGVyblJlZnJlc2hJbnRlcnZhbCA9PT0gMCkge1xuICAgICAgICAgIHVwZGF0ZSgpO1xuICAgICAgICAgIGRyYXcoKTtcbiAgICAgIH1cblxuICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKGxvb3ApO1xuICB9XG5cbiAgfVxuICB9XG5cblxuXG5cblxuXG4gIC8vLy8vL0hFQURFUiBTU0NST0xMTEwvLy8vLy8vXG5cblxuICB3aW5kb3cub25zY3JvbGwgPSBmdW5jdGlvbigpIHtcbiAgdmFyIHNjcm9sbFRvcCA9IHdpbmRvdy5wYWdlWU9mZnNldCB8fCAoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50IHx8IGRvY3VtZW50LmJvZHkucGFyZW50Tm9kZSB8fCBkb2N1bWVudC5ib2R5KS5zY3JvbGxUb3BcblxuICB2YXIgcm9vdCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiSFRNTFwiKVswXTtcblxuICBpZiAoc2Nyb2xsVG9wID4gMjApIHtcbiAgICB2YXIgcm9vdCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiSFRNTFwiKVswXTtcbiAgICByb290LmNsYXNzTGlzdC5hZGQoXCJoZWFkZXItLW5vdC1vbi10b3BcIik7XG5cblxuICB9XG4gIGVsc2Uge1xuICAgIHZhciByb290ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJIVE1MXCIpWzBdO1xuICAgIHJvb3QuY2xhc3NMaXN0LnJlbW92ZShcImhlYWRlci0tbm90LW9uLXRvcFwiKTtcblxuICB9XG5cbiAgfTtcblxuXG5cblxuXG5mdW5jdGlvbiB2aWRlb19sb2FkZXIoKSB7XG4gIC8vIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgdmlkZW8gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjY29udGFjdC12aWRlby1pZCcpO1xuXG4gICAgICAvLyB2YXIgcHJlbG9hZGVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnByZWxvYWRlcicpO1xuXG4gICAgICBmdW5jdGlvbiBjaGVja0xvYWQoKSB7XG4gICAgICAgICAgaWYgKHZpZGVvLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgICAgLy8gcHJlbG9hZGVyLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQocHJlbG9hZGVyKTtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJ2aWRlbyBsb2FkZWRcIik7XG5cbiAgICAgICAgICAgICAgdmlkZW8ucGxheSgpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHNldFRpbWVvdXQoY2hlY2tMb2FkLCAxMDApO1xuICAgICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgY2hlY2tMb2FkKCk7XG4gIC8vIH0sIGZhbHNlKTtcbn1cblxuXG5cblxuXG4gIC8vLy8vLy8vLy8vLyBWaWV3IGNvbnN0cmFjdG9yIC8vLy8vLy8vXG5cbiAgZnVuY3Rpb24gaW5pdF92aWV3cygpIHtcbi8vIGNvbnNvbGUubG9nKFwiaW5pdCBhcHBcIik7XG5pZiAoZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuY29udGFpbnMoJ3BhZ2UtdGVtcGxhdGUtaG9tZScpKSB7XG4gIHNldF9oZXJvX3NjcmVlbl9oZWlnaHQoKTtcbiAgLy8gY29uc29sZS5sb2coXCJob21lIGluaXRcIik7XG4gIGZ1bmN0aW9uIGluaXRfcGpheCgpIHtcbiAgd2luZG93LnBqYXhfdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5oZXJvLWJsb2NrLWxpbmtcIik7XG4gIGluaXRfcGpheF9saW5rcygpO1xuICB9XG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIHNldF9oZXJvX3NjcmVlbl9oZWlnaHQpO1xuICBpbml0X3BqYXgoKTtcbiAgaGVyb19sb2FkX21vcmUoKTtcbiAgLy8gY3JlYXRlX2NhbnZhc18xKCk7XG4gIGhlcm9faG92ZXIoKTtcbn1cbmlmIChkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5jb250YWlucygncGFnZS10ZW1wbGF0ZS1jb21tZXJjaWFscycpKSB7XG4gICAgLy8gY29uc29sZS5sb2coXCJjb21tZXJjaWFscyBpbml0XCIpO1xuICBmdW5jdGlvbiBpbml0X3BqYXgoKSB7XG4gIHdpbmRvdy5wamF4X3RhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIuY29tLWJsb2NrLWxpbmtcIik7XG4gIGluaXRfcGpheF9saW5rcygpO1xuICB9XG5cbiAgaW5pdF9wamF4KCk7XG4gIC8vIGluaXRfc2Vjb25kX25vaXNlKCk7XG4gIGNvbV9ob3ZlcigpO1xuXG59XG5cbmlmIChkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5jb250YWlucygncGFnZS10ZW1wbGF0ZS1zaG9ydGZpbG1zJykpIHtcbiAgICAvLyBjb25zb2xlLmxvZyhcImNvbW1lcmNpYWxzIGluaXRcIik7XG4gIGZ1bmN0aW9uIGluaXRfcGpheCgpIHtcbiAgd2luZG93LnBqYXhfdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5jb20tYmxvY2stbGlua1wiKTtcbiAgaW5pdF9wamF4X2xpbmtzKCk7XG4gIH1cblxuICBpbml0X3BqYXgoKTtcbi8vIGluaXRfc2Vjb25kX25vaXNlKCk7XG4gIGNvbV9ob3ZlcigpO1xuXG59XG5cbmlmIChkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5jb250YWlucygncGFnZS10ZW1wbGF0ZS1jb250YWN0JykpIHtcbiAgICAvLyBjb25zb2xlLmxvZyhcImNvbnRhY3QgaW5pdFwiKTtcbiAgICB2aWRlb19sb2FkZXIoKTtcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jb250YWN0LXZpZGVvLWNlbGwnKS5zdHlsZS5vcGFjaXR5ID0gXCIxXCI7XG4gIH0sIDEwMDApO1xuXG5cbn1cblxuXG5cblxuICB9XG5cblxuICBpbml0X3ZpZXdzKCk7XG5cblxuXG4gIHZhciBpc0lwaG9uZSA9IG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZihcImlQaG9uZVwiKSAhPSAtMSA7XG5cbiAgaWYgKGlzSXBob25lKSB7XG4gICAgdmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG4gICAgICByb290LmNsYXNzTGlzdC5hZGQoXCJpb3MtZml4XCIpO1xuICB9XG4gIGVsc2Uge1xuICAgIC8vICAgdmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG4gICAgLy8gcm9vdC5jbGFzc0xpc3QuYWRkKFwibm8taW9zLWZpeFwiKTtcbiAgfVxuXG4gIHZhciBpc1NhZmFyaSA9IG5hdmlnYXRvci52ZW5kb3IgJiYgbmF2aWdhdG9yLnZlbmRvci5pbmRleE9mKCdBcHBsZScpID4gLTEgJiZcbiAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLnVzZXJBZ2VudCAmJlxuICAgICAgICAgICAgICAgICBuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoJ0NyaU9TJykgPT0gLTEgJiZcbiAgICAgICAgICAgICAgICAgbmF2aWdhdG9yLnVzZXJBZ2VudC5pbmRleE9mKCdGeGlPUycpID09IC0xO1xuXG5cbiAgICAgICAgICAgICAgICAgaWYgKGlzU2FmYXJpKSB7XG4gICAgICAgICAgICAgICAgICAgdmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG4gICAgICAgICAgICAgICAgICAgICByb290LmNsYXNzTGlzdC5hZGQoXCJpb3MtZml4XCIpO1xuXG4gICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgIC8vICAgdmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG4gICAgICAgICAgICAgICAgIC8vIHJvb3QuY2xhc3NMaXN0LmFkZChcIm5vLWlvcy1maXhcIik7XG4gICAgICAgICAgICAgICAgIH1cblxuXG5cbiAgc2V0VGltZW91dChmdW5jdGlvbigpe1xudmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG5yb290LmNsYXNzTGlzdC5yZW1vdmUoXCJwYWdlLWlzLWxvYWRpbmdcIik7XG4gIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICByb290LmNsYXNzTGlzdC5hZGQoXCJhY3RpdmF0ZS1hbmltYXRpb25cIik7XG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICByb290LmNsYXNzTGlzdC5yZW1vdmUoXCJsb2dvLW9wYWNpdHlcIik7XG4gICAgICAgICAgICByb290LmNsYXNzTGlzdC5yZW1vdmUoXCJhY3RpdmF0ZS1hbmltYXRpb25cIik7XG4gICAgICB9LCAyNzAwKTtcbiAgfSwgMCk7XG4gIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5sb2FkaW5nLXNtb2tlJykuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuLy8gcm9vdC5jbGFzc0xpc3QucmVtb3ZlKFwibG9nby1vcGFjaXR5XCIpO1xuICB9LCA1MDApO1xufSwgNTAwKTtcblxuXG5cblxuXG5cblxud2luZG93Lm9ucG9wc3RhdGUgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgaWYgKGV2ZW50LnN0YXRlKSB7XG4gICAgLy8gaGlzdG9yeSBjaGFuZ2VkIGJlY2F1c2Ugb2YgcHVzaFN0YXRlL3JlcGxhY2VTdGF0ZVxuICAgIC8vIGNvbnNvbGUubG9nKFwicHVzaHN0YWFhdGVcIik7XG4gICAgICAgICAgdmFyIHJvb3QgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcIkhUTUxcIilbMF07XG4gICAgaWYgKHJvb3QuY2xhc3NMaXN0LmNvbnRhaW5zKCdvdmVybGF5LXZpZXctLWFjdGl2ZScpKSB7XG5jbG9zZV9vdmVybGF5KCk7XG4gICAgfVxuICAgIHZhciBuZXdQYWdlVXJsID0gbG9jYXRpb24uaHJlZjtcbiAgICBjb25zb2xlLmxvZyhuZXdQYWdlVXJsKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgcm9vdCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKFwiSFRNTFwiKVswXTtcbiAgICAvLyBjb25zb2xlLmxvZyhcInB1c2hzdGFhYXRlIGJ1dCBzb21ldGhpbmcgZWxzZWVlXCIpO1xuICAgIGlmIChyb290LmNsYXNzTGlzdC5jb250YWlucygnb3ZlcmxheS12aWV3LS1hY3RpdmUnKSkge1xuY2xvc2Vfb3ZlcmxheSgpO1xuICAgIH1cblxuICAgIHZhciBuZXdQYWdlVXJsID0gbG9jYXRpb24uaHJlZjtcbiAgICBjb25zb2xlLmxvZyhuZXdQYWdlVXJsKTtcbiAgICBpZiAoL2NvbW1lcmNpYWxzLy50ZXN0KHdpbmRvdy5sb2NhdGlvbi5ocmVmKSkge1xuICAgICAgY29uc29sZS5sb2coXCJDT01NRVJJQ0FMTExMTFwiKTtcbiAgICAgIGxvY2F0aW9uLnJlcGxhY2UobmV3UGFnZVVybCk7XG4gICAgfVxuICAgIGlmICgvc2hvcnRmaWxtcy8udGVzdCh3aW5kb3cubG9jYXRpb24uaHJlZikpIHtcbiAgICAgIGNvbnNvbGUubG9nKFwiQ09NTUVSSUNBTExMTExcIik7XG4gICAgICBsb2NhdGlvbi5yZXBsYWNlKG5ld1BhZ2VVcmwpO1xuICAgIH1cblxuICB9XG59XG5cblxuXG5mdW5jdGlvbiBzZXRfaGVyb19zY3JlZW5faGVpZ2h0KCkge1xuICBpZiAod2luZG93LmlubmVyV2lkdGggPiA3NTApIHtcbiAgICAvLyBjb25zb2xlLmxvZyhcImhlcm8gc2V0IHNpemVcIik7XG4gIHZhciBpbm5lcl9oZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XG4gIHZhciBoZXJvX2Jsb2NrID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmhlcm8tcm93LS0xJyk7XG4gIGlmKHR5cGVvZihoZXJvX2Jsb2NrKSAhPSAndW5kZWZpbmVkJyAmJiBoZXJvX2Jsb2NrICE9IG51bGwpe1xuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaGVyby1yb3ctLTEnKS5zdHlsZS5oZWlnaHQgPSAgaW5uZXJfaGVpZ2h0ICsgXCJweFwiO1xuICAgIH1cbiAgfVxuICBlbHNlIHtcbiAgICB2YXIgaGVyb19ibG9jayA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oZXJvLXJvdy0tMScpO1xuICAgIGlmKHR5cGVvZihoZXJvX2Jsb2NrKSAhPSAndW5kZWZpbmVkJyAmJiBoZXJvX2Jsb2NrICE9IG51bGwpe1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5oZXJvLXJvdy0tMScpLnJlbW92ZUF0dHJpYnV0ZShcInN0eWxlXCIpO1xuICAgICAgfVxuICAgICAgLy8gY29uc29sZS5sb2coXCJoZXJvIGNsZWFyIHNpemVcIik7XG4gIH1cbn1cblxuXG5cbmlmKHR5cGVvZihuZXh0X2ZpbG1fbGluaykgIT0gJ3VuZGVmaW5lZCcgJiYgbmV4dF9maWxtX2xpbmsgIT0gbnVsbCl7XG4gIG5leHRfZmlsbV90cmlnZ2VyKCk7XG59IGVsc2V7XG4gIC8vIGNvbnNvbGUubG9nKFwibm8gbmV4dCBmaWxtXCIpO1xufVxuXG5cblxuXG5cblxud2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoc3RhcnQnLCBmdW5jdGlvbiBvbkZpcnN0VG91Y2goKSB7XG4gIC8vIHdlIGNvdWxkIHVzZSBhIGNsYXNzXG4gIHZhciByb290ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJIVE1MXCIpWzBdO1xuICByb290LmNsYXNzTGlzdC5hZGQoJ3RvdWNoLWRldmljZScpO1xuXG5cblxuXG5cbiAgLy8gd2Ugb25seSBuZWVkIHRvIGtub3cgb25jZSB0aGF0IGEgaHVtYW4gdG91Y2hlZCB0aGUgc2NyZWVuLCBzbyB3ZSBjYW4gc3RvcCBsaXN0ZW5pbmcgbm93XG4gIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0Jywgb25GaXJzdFRvdWNoLCBmYWxzZSk7XG59LCBmYWxzZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvYXNzZXRzL2pzL2FwcC5qcyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG5cdHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxuLy8gbW9kdWxlc1xuXG5cbnZhciBfZGVsZWdhdGUgPSByZXF1aXJlKCdkZWxlZ2F0ZScpO1xuXG52YXIgX2RlbGVnYXRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlbGVnYXRlKTtcblxudmFyIF9DYWNoZSA9IHJlcXVpcmUoJy4vbW9kdWxlcy9DYWNoZScpO1xuXG52YXIgX0NhY2hlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0NhY2hlKTtcblxudmFyIF9sb2FkUGFnZSA9IHJlcXVpcmUoJy4vbW9kdWxlcy9sb2FkUGFnZScpO1xuXG52YXIgX2xvYWRQYWdlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2xvYWRQYWdlKTtcblxudmFyIF9yZW5kZXJQYWdlID0gcmVxdWlyZSgnLi9tb2R1bGVzL3JlbmRlclBhZ2UnKTtcblxudmFyIF9yZW5kZXJQYWdlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlbmRlclBhZ2UpO1xuXG52YXIgX3RyaWdnZXJFdmVudCA9IHJlcXVpcmUoJy4vbW9kdWxlcy90cmlnZ2VyRXZlbnQnKTtcblxudmFyIF90cmlnZ2VyRXZlbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfdHJpZ2dlckV2ZW50KTtcblxudmFyIF9vbiA9IHJlcXVpcmUoJy4vbW9kdWxlcy9vbicpO1xuXG52YXIgX29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29uKTtcblxudmFyIF9vZmYgPSByZXF1aXJlKCcuL21vZHVsZXMvb2ZmJyk7XG5cbnZhciBfb2ZmMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29mZik7XG5cbnZhciBfdXBkYXRlVHJhbnNpdGlvbiA9IHJlcXVpcmUoJy4vbW9kdWxlcy91cGRhdGVUcmFuc2l0aW9uJyk7XG5cbnZhciBfdXBkYXRlVHJhbnNpdGlvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF91cGRhdGVUcmFuc2l0aW9uKTtcblxudmFyIF9nZXRBbmltYXRpb25Qcm9taXNlcyA9IHJlcXVpcmUoJy4vbW9kdWxlcy9nZXRBbmltYXRpb25Qcm9taXNlcycpO1xuXG52YXIgX2dldEFuaW1hdGlvblByb21pc2VzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldEFuaW1hdGlvblByb21pc2VzKTtcblxudmFyIF9nZXRQYWdlRGF0YSA9IHJlcXVpcmUoJy4vbW9kdWxlcy9nZXRQYWdlRGF0YScpO1xuXG52YXIgX2dldFBhZ2VEYXRhMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFBhZ2VEYXRhKTtcblxudmFyIF9wbHVnaW5zID0gcmVxdWlyZSgnLi9tb2R1bGVzL3BsdWdpbnMnKTtcblxudmFyIF91dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbnZhciBTd3VwID0gZnVuY3Rpb24gKCkge1xuXHRmdW5jdGlvbiBTd3VwKHNldE9wdGlvbnMpIHtcblx0XHRfY2xhc3NDYWxsQ2hlY2sodGhpcywgU3d1cCk7XG5cblx0XHQvLyBkZWZhdWx0IG9wdGlvbnNcblx0XHR2YXIgZGVmYXVsdHMgPSB7XG5cdFx0XHRhbmltYXRlSGlzdG9yeUJyb3dzaW5nOiBmYWxzZSxcblx0XHRcdGFuaW1hdGlvblNlbGVjdG9yOiAnW2NsYXNzKj1cInRyYW5zaXRpb24tXCJdJyxcblx0XHRcdGxpbmtTZWxlY3RvcjogJ2FbaHJlZl49XCInICsgd2luZG93LmxvY2F0aW9uLm9yaWdpbiArICdcIl06bm90KFtkYXRhLW5vLXN3dXBdKSwgYVtocmVmXj1cIi9cIl06bm90KFtkYXRhLW5vLXN3dXBdKSwgYVtocmVmXj1cIiNcIl06bm90KFtkYXRhLW5vLXN3dXBdKScsXG5cdFx0XHRjYWNoZTogdHJ1ZSxcblx0XHRcdGNvbnRhaW5lcnM6IFsnI3N3dXAnXSxcblx0XHRcdHJlcXVlc3RIZWFkZXJzOiB7XG5cdFx0XHRcdCdYLVJlcXVlc3RlZC1XaXRoJzogJ3N3dXAnLFxuXHRcdFx0XHRBY2NlcHQ6ICd0ZXh0L2h0bWwsIGFwcGxpY2F0aW9uL3hodG1sK3htbCdcblx0XHRcdH0sXG5cdFx0XHRwbHVnaW5zOiBbXSxcblx0XHRcdHNraXBQb3BTdGF0ZUhhbmRsaW5nOiBmdW5jdGlvbiBza2lwUG9wU3RhdGVIYW5kbGluZyhldmVudCkge1xuXHRcdFx0XHRyZXR1cm4gIShldmVudC5zdGF0ZSAmJiBldmVudC5zdGF0ZS5zb3VyY2UgPT09ICdzd3VwJyk7XG5cdFx0XHR9XG5cdFx0fTtcblxuXHRcdC8vIG1lcmdlIG9wdGlvbnNcblx0XHR2YXIgb3B0aW9ucyA9IF9leHRlbmRzKHt9LCBkZWZhdWx0cywgc2V0T3B0aW9ucyk7XG5cblx0XHQvLyBoYW5kbGVyIGFycmF5c1xuXHRcdHRoaXMuX2hhbmRsZXJzID0ge1xuXHRcdFx0YW5pbWF0aW9uSW5Eb25lOiBbXSxcblx0XHRcdGFuaW1hdGlvbkluU3RhcnQ6IFtdLFxuXHRcdFx0YW5pbWF0aW9uT3V0RG9uZTogW10sXG5cdFx0XHRhbmltYXRpb25PdXRTdGFydDogW10sXG5cdFx0XHRhbmltYXRpb25Ta2lwcGVkOiBbXSxcblx0XHRcdGNsaWNrTGluazogW10sXG5cdFx0XHRjb250ZW50UmVwbGFjZWQ6IFtdLFxuXHRcdFx0ZGlzYWJsZWQ6IFtdLFxuXHRcdFx0ZW5hYmxlZDogW10sXG5cdFx0XHRvcGVuUGFnZUluTmV3VGFiOiBbXSxcblx0XHRcdHBhZ2VMb2FkZWQ6IFtdLFxuXHRcdFx0cGFnZVJldHJpZXZlZEZyb21DYWNoZTogW10sXG5cdFx0XHRwYWdlVmlldzogW10sXG5cdFx0XHRwb3BTdGF0ZTogW10sXG5cdFx0XHRzYW1lUGFnZTogW10sXG5cdFx0XHRzYW1lUGFnZVdpdGhIYXNoOiBbXSxcblx0XHRcdHNlcnZlckVycm9yOiBbXSxcblx0XHRcdHRyYW5zaXRpb25TdGFydDogW10sXG5cdFx0XHR0cmFuc2l0aW9uRW5kOiBbXSxcblx0XHRcdHdpbGxSZXBsYWNlQ29udGVudDogW11cblx0XHR9O1xuXG5cdFx0Ly8gdmFyaWFibGUgZm9yIGlkIG9mIGVsZW1lbnQgdG8gc2Nyb2xsIHRvIGFmdGVyIHJlbmRlclxuXHRcdHRoaXMuc2Nyb2xsVG9FbGVtZW50ID0gbnVsbDtcblx0XHQvLyB2YXJpYWJsZSBmb3IgcHJvbWlzZSB1c2VkIGZvciBwcmVsb2FkLCBzbyBubyBuZXcgbG9hZGluZyBvZiB0aGUgc2FtZSBwYWdlIHN0YXJ0cyB3aGlsZSBwYWdlIGlzIGxvYWRpbmdcblx0XHR0aGlzLnByZWxvYWRQcm9taXNlID0gbnVsbDtcblx0XHQvLyB2YXJpYWJsZSBmb3Igc2F2ZSBvcHRpb25zXG5cdFx0dGhpcy5vcHRpb25zID0gb3B0aW9ucztcblx0XHQvLyB2YXJpYWJsZSBmb3IgcGx1Z2lucyBhcnJheVxuXHRcdHRoaXMucGx1Z2lucyA9IFtdO1xuXHRcdC8vIHZhcmlhYmxlIGZvciBjdXJyZW50IHRyYW5zaXRpb24gb2JqZWN0XG5cdFx0dGhpcy50cmFuc2l0aW9uID0ge307XG5cdFx0Ly8gdmFyaWFibGUgZm9yIGtlZXBpbmcgZXZlbnQgbGlzdGVuZXJzIGZyb20gXCJkZWxlZ2F0ZVwiXG5cdFx0dGhpcy5kZWxlZ2F0ZWRMaXN0ZW5lcnMgPSB7fTtcblxuXHRcdC8vIG1ha2UgbW9kdWxlcyBhY2Nlc3NpYmxlIGluIGluc3RhbmNlXG5cdFx0dGhpcy5jYWNoZSA9IG5ldyBfQ2FjaGUyLmRlZmF1bHQoKTtcblx0XHR0aGlzLmNhY2hlLnN3dXAgPSB0aGlzO1xuXHRcdHRoaXMubG9hZFBhZ2UgPSBfbG9hZFBhZ2UyLmRlZmF1bHQ7XG5cdFx0dGhpcy5yZW5kZXJQYWdlID0gX3JlbmRlclBhZ2UyLmRlZmF1bHQ7XG5cdFx0dGhpcy50cmlnZ2VyRXZlbnQgPSBfdHJpZ2dlckV2ZW50Mi5kZWZhdWx0O1xuXHRcdHRoaXMub24gPSBfb24yLmRlZmF1bHQ7XG5cdFx0dGhpcy5vZmYgPSBfb2ZmMi5kZWZhdWx0O1xuXHRcdHRoaXMudXBkYXRlVHJhbnNpdGlvbiA9IF91cGRhdGVUcmFuc2l0aW9uMi5kZWZhdWx0O1xuXHRcdHRoaXMuZ2V0QW5pbWF0aW9uUHJvbWlzZXMgPSBfZ2V0QW5pbWF0aW9uUHJvbWlzZXMyLmRlZmF1bHQ7XG5cdFx0dGhpcy5nZXRQYWdlRGF0YSA9IF9nZXRQYWdlRGF0YTIuZGVmYXVsdDtcblx0XHR0aGlzLmxvZyA9IGZ1bmN0aW9uICgpIHt9OyAvLyBoZXJlIHNvIGl0IGNhbiBiZSB1c2VkIGJ5IHBsdWdpbnNcblx0XHR0aGlzLnVzZSA9IF9wbHVnaW5zLnVzZTtcblx0XHR0aGlzLnVudXNlID0gX3BsdWdpbnMudW51c2U7XG5cdFx0dGhpcy5maW5kUGx1Z2luID0gX3BsdWdpbnMuZmluZFBsdWdpbjtcblxuXHRcdC8vIGVuYWJsZSBzd3VwXG5cdFx0dGhpcy5lbmFibGUoKTtcblx0fVxuXG5cdF9jcmVhdGVDbGFzcyhTd3VwLCBbe1xuXHRcdGtleTogJ2VuYWJsZScsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGVuYWJsZSgpIHtcblx0XHRcdHZhciBfdGhpcyA9IHRoaXM7XG5cblx0XHRcdC8vIGNoZWNrIGZvciBQcm9taXNlIHN1cHBvcnRcblx0XHRcdGlmICh0eXBlb2YgUHJvbWlzZSA9PT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdFx0Y29uc29sZS53YXJuKCdQcm9taXNlIGlzIG5vdCBzdXBwb3J0ZWQnKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXG5cdFx0XHQvLyBhZGQgZXZlbnQgbGlzdGVuZXJzXG5cdFx0XHR0aGlzLmRlbGVnYXRlZExpc3RlbmVycy5jbGljayA9ICgwLCBfZGVsZWdhdGUyLmRlZmF1bHQpKGRvY3VtZW50LCB0aGlzLm9wdGlvbnMubGlua1NlbGVjdG9yLCAnY2xpY2snLCB0aGlzLmxpbmtDbGlja0hhbmRsZXIuYmluZCh0aGlzKSk7XG5cdFx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncG9wc3RhdGUnLCB0aGlzLnBvcFN0YXRlSGFuZGxlci5iaW5kKHRoaXMpKTtcblxuXHRcdFx0Ly8gaW5pdGlhbCBzYXZlIHRvIGNhY2hlXG5cdFx0XHR2YXIgcGFnZSA9ICgwLCBfaGVscGVycy5nZXREYXRhRnJvbUh0bWwpKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5vdXRlckhUTUwsIHRoaXMub3B0aW9ucy5jb250YWluZXJzKTtcblx0XHRcdHBhZ2UudXJsID0gcGFnZS5yZXNwb25zZVVSTCA9ICgwLCBfaGVscGVycy5nZXRDdXJyZW50VXJsKSgpO1xuXHRcdFx0aWYgKHRoaXMub3B0aW9ucy5jYWNoZSkge1xuXHRcdFx0XHR0aGlzLmNhY2hlLmNhY2hlVXJsKHBhZ2UpO1xuXHRcdFx0fVxuXG5cdFx0XHQvLyBtYXJrIHN3dXAgYmxvY2tzIGluIGh0bWxcblx0XHRcdCgwLCBfaGVscGVycy5tYXJrU3d1cEVsZW1lbnRzKShkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQsIHRoaXMub3B0aW9ucy5jb250YWluZXJzKTtcblxuXHRcdFx0Ly8gbW91bnQgcGx1Z2luc1xuXHRcdFx0dGhpcy5vcHRpb25zLnBsdWdpbnMuZm9yRWFjaChmdW5jdGlvbiAocGx1Z2luKSB7XG5cdFx0XHRcdF90aGlzLnVzZShwbHVnaW4pO1xuXHRcdFx0fSk7XG5cblx0XHRcdC8vIG1vZGlmeSBpbml0aWFsIGhpc3RvcnkgcmVjb3JkXG5cdFx0XHR3aW5kb3cuaGlzdG9yeS5yZXBsYWNlU3RhdGUoT2JqZWN0LmFzc2lnbih7fSwgd2luZG93Lmhpc3Rvcnkuc3RhdGUsIHtcblx0XHRcdFx0dXJsOiB3aW5kb3cubG9jYXRpb24uaHJlZixcblx0XHRcdFx0cmFuZG9tOiBNYXRoLnJhbmRvbSgpLFxuXHRcdFx0XHRzb3VyY2U6ICdzd3VwJ1xuXHRcdFx0fSksIGRvY3VtZW50LnRpdGxlLCB3aW5kb3cubG9jYXRpb24uaHJlZik7XG5cblx0XHRcdC8vIHRyaWdnZXIgZW5hYmxlZCBldmVudFxuXHRcdFx0dGhpcy50cmlnZ2VyRXZlbnQoJ2VuYWJsZWQnKTtcblxuXHRcdFx0Ly8gYWRkIHN3dXAtZW5hYmxlZCBjbGFzcyB0byBodG1sIHRhZ1xuXHRcdFx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3N3dXAtZW5hYmxlZCcpO1xuXG5cdFx0XHQvLyB0cmlnZ2VyIHBhZ2UgdmlldyBldmVudFxuXHRcdFx0dGhpcy50cmlnZ2VyRXZlbnQoJ3BhZ2VWaWV3Jyk7XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnZGVzdHJveScsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGRlc3Ryb3koKSB7XG5cdFx0XHR2YXIgX3RoaXMyID0gdGhpcztcblxuXHRcdFx0Ly8gcmVtb3ZlIGRlbGVnYXRlZCBsaXN0ZW5lcnNcblx0XHRcdHRoaXMuZGVsZWdhdGVkTGlzdGVuZXJzLmNsaWNrLmRlc3Ryb3koKTtcblx0XHRcdHRoaXMuZGVsZWdhdGVkTGlzdGVuZXJzLm1vdXNlb3Zlci5kZXN0cm95KCk7XG5cblx0XHRcdC8vIHJlbW92ZSBwb3BzdGF0ZSBsaXN0ZW5lclxuXHRcdFx0d2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3BvcHN0YXRlJywgdGhpcy5wb3BTdGF0ZUhhbmRsZXIuYmluZCh0aGlzKSk7XG5cblx0XHRcdC8vIGVtcHR5IGNhY2hlXG5cdFx0XHR0aGlzLmNhY2hlLmVtcHR5KCk7XG5cblx0XHRcdC8vIHVubW91bnQgcGx1Z2luc1xuXHRcdFx0dGhpcy5vcHRpb25zLnBsdWdpbnMuZm9yRWFjaChmdW5jdGlvbiAocGx1Z2luKSB7XG5cdFx0XHRcdF90aGlzMi51bnVzZShwbHVnaW4pO1xuXHRcdFx0fSk7XG5cblx0XHRcdC8vIHJlbW92ZSBzd3VwIGRhdGEgYXRyaWJ1dGVzIGZyb20gYmxvY2tzXG5cdFx0XHQoMCwgX3V0aWxzLnF1ZXJ5QWxsKSgnW2RhdGEtc3d1cF0nKS5mb3JFYWNoKGZ1bmN0aW9uIChlbGVtZW50KSB7XG5cdFx0XHRcdGVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCdkYXRhLXN3dXAnKTtcblx0XHRcdH0pO1xuXG5cdFx0XHQvLyByZW1vdmUgaGFuZGxlcnNcblx0XHRcdHRoaXMub2ZmKCk7XG5cblx0XHRcdC8vIHRyaWdnZXIgZGlzYWJsZSBldmVudFxuXHRcdFx0dGhpcy50cmlnZ2VyRXZlbnQoJ2Rpc2FibGVkJyk7XG5cblx0XHRcdC8vIHJlbW92ZSBzd3VwLWVuYWJsZWQgY2xhc3MgZnJvbSBodG1sIHRhZ1xuXHRcdFx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ3N3dXAtZW5hYmxlZCcpO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2xpbmtDbGlja0hhbmRsZXInLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBsaW5rQ2xpY2tIYW5kbGVyKGV2ZW50KSB7XG5cdFx0XHQvLyBubyBjb250cm9sIGtleSBwcmVzc2VkXG5cdFx0XHRpZiAoIWV2ZW50Lm1ldGFLZXkgJiYgIWV2ZW50LmN0cmxLZXkgJiYgIWV2ZW50LnNoaWZ0S2V5ICYmICFldmVudC5hbHRLZXkpIHtcblx0XHRcdFx0Ly8gaW5kZXggb2YgcHJlc3NlZCBidXR0b24gbmVlZHMgdG8gYmUgY2hlY2tlZCBiZWNhdXNlIEZpcmVmb3ggdHJpZ2dlcnMgY2xpY2sgb24gYWxsIG1vdXNlIGJ1dHRvbnNcblx0XHRcdFx0aWYgKGV2ZW50LmJ1dHRvbiA9PT0gMCkge1xuXHRcdFx0XHRcdHRoaXMudHJpZ2dlckV2ZW50KCdjbGlja0xpbmsnLCBldmVudCk7XG5cdFx0XHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHR2YXIgbGluayA9IG5ldyBfaGVscGVycy5MaW5rKGV2ZW50LmRlbGVnYXRlVGFyZ2V0KTtcblx0XHRcdFx0XHRpZiAobGluay5nZXRBZGRyZXNzKCkgPT0gKDAsIF9oZWxwZXJzLmdldEN1cnJlbnRVcmwpKCkgfHwgbGluay5nZXRBZGRyZXNzKCkgPT0gJycpIHtcblx0XHRcdFx0XHRcdC8vIGxpbmsgdG8gdGhlIHNhbWUgVVJMXG5cdFx0XHRcdFx0XHRpZiAobGluay5nZXRIYXNoKCkgIT0gJycpIHtcblx0XHRcdFx0XHRcdFx0Ly8gbGluayB0byB0aGUgc2FtZSBVUkwgd2l0aCBoYXNoXG5cdFx0XHRcdFx0XHRcdHRoaXMudHJpZ2dlckV2ZW50KCdzYW1lUGFnZVdpdGhIYXNoJywgZXZlbnQpO1xuXHRcdFx0XHRcdFx0XHR2YXIgZWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IobGluay5nZXRIYXNoKCkpO1xuXHRcdFx0XHRcdFx0XHRpZiAoZWxlbWVudCAhPSBudWxsKSB7XG5cdFx0XHRcdFx0XHRcdFx0aGlzdG9yeS5yZXBsYWNlU3RhdGUoe1xuXHRcdFx0XHRcdFx0XHRcdFx0dXJsOiBsaW5rLmdldEFkZHJlc3MoKSArIGxpbmsuZ2V0SGFzaCgpLFxuXHRcdFx0XHRcdFx0XHRcdFx0cmFuZG9tOiBNYXRoLnJhbmRvbSgpLFxuXHRcdFx0XHRcdFx0XHRcdFx0c291cmNlOiAnc3d1cCdcblx0XHRcdFx0XHRcdFx0XHR9LCBkb2N1bWVudC50aXRsZSwgbGluay5nZXRBZGRyZXNzKCkgKyBsaW5rLmdldEhhc2goKSk7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0Ly8gcmVmZXJlbmNlZCBlbGVtZW50IG5vdCBmb3VuZFxuXHRcdFx0XHRcdFx0XHRcdGNvbnNvbGUud2FybignRWxlbWVudCBmb3Igb2Zmc2V0IG5vdCBmb3VuZCAoJyArIGxpbmsuZ2V0SGFzaCgpICsgJyknKTtcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0Ly8gbGluayB0byB0aGUgc2FtZSBVUkwgd2l0aG91dCBoYXNoXG5cdFx0XHRcdFx0XHRcdHRoaXMudHJpZ2dlckV2ZW50KCdzYW1lUGFnZScsIGV2ZW50KTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0Ly8gbGluayB0byBkaWZmZXJlbnQgdXJsXG5cdFx0XHRcdFx0XHRpZiAobGluay5nZXRIYXNoKCkgIT0gJycpIHtcblx0XHRcdFx0XHRcdFx0dGhpcy5zY3JvbGxUb0VsZW1lbnQgPSBsaW5rLmdldEhhc2goKTtcblx0XHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFx0Ly8gZ2V0IGN1c3RvbSB0cmFuc2l0aW9uIGZyb20gZGF0YVxuXHRcdFx0XHRcdFx0dmFyIGN1c3RvbVRyYW5zaXRpb24gPSBldmVudC5kZWxlZ2F0ZVRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3d1cC10cmFuc2l0aW9uJyk7XG5cblx0XHRcdFx0XHRcdC8vIGxvYWQgcGFnZVxuXHRcdFx0XHRcdFx0dGhpcy5sb2FkUGFnZSh7IHVybDogbGluay5nZXRBZGRyZXNzKCksIGN1c3RvbVRyYW5zaXRpb246IGN1c3RvbVRyYW5zaXRpb24gfSwgZmFsc2UpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Ly8gb3BlbiBpbiBuZXcgdGFiIChkbyBub3RoaW5nKVxuXHRcdFx0XHR0aGlzLnRyaWdnZXJFdmVudCgnb3BlblBhZ2VJbk5ld1RhYicsIGV2ZW50KTtcblx0XHRcdH1cblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdwb3BTdGF0ZUhhbmRsZXInLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBwb3BTdGF0ZUhhbmRsZXIoZXZlbnQpIHtcblx0XHRcdGlmICh0aGlzLm9wdGlvbnMuc2tpcFBvcFN0YXRlSGFuZGxpbmcoZXZlbnQpKSByZXR1cm47XG5cdFx0XHR2YXIgbGluayA9IG5ldyBfaGVscGVycy5MaW5rKGV2ZW50LnN0YXRlID8gZXZlbnQuc3RhdGUudXJsIDogd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lKTtcblx0XHRcdGlmIChsaW5rLmdldEhhc2goKSAhPT0gJycpIHtcblx0XHRcdFx0dGhpcy5zY3JvbGxUb0VsZW1lbnQgPSBsaW5rLmdldEhhc2goKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHR9XG5cdFx0XHR0aGlzLnRyaWdnZXJFdmVudCgncG9wU3RhdGUnLCBldmVudCk7XG5cdFx0XHR0aGlzLmxvYWRQYWdlKHsgdXJsOiBsaW5rLmdldEFkZHJlc3MoKSB9LCBldmVudCk7XG5cdFx0fVxuXHR9XSk7XG5cblx0cmV0dXJuIFN3dXA7XG59KCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFN3dXA7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDE0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBjbG9zZXN0ID0gcmVxdWlyZSgnLi9jbG9zZXN0Jyk7XG5cbi8qKlxuICogRGVsZWdhdGVzIGV2ZW50IHRvIGEgc2VsZWN0b3IuXG4gKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHBhcmFtIHtCb29sZWFufSB1c2VDYXB0dXJlXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIGRlbGVnYXRlKGVsZW1lbnQsIHNlbGVjdG9yLCB0eXBlLCBjYWxsYmFjaywgdXNlQ2FwdHVyZSkge1xuICAgIHZhciBsaXN0ZW5lckZuID0gbGlzdGVuZXIuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblxuICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lckZuLCB1c2VDYXB0dXJlKTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKHR5cGUsIGxpc3RlbmVyRm4sIHVzZUNhcHR1cmUpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4vKipcbiAqIEZpbmRzIGNsb3Nlc3QgbWF0Y2ggYW5kIGludm9rZXMgY2FsbGJhY2suXG4gKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHJldHVybiB7RnVuY3Rpb259XG4gKi9cbmZ1bmN0aW9uIGxpc3RlbmVyKGVsZW1lbnQsIHNlbGVjdG9yLCB0eXBlLCBjYWxsYmFjaykge1xuICAgIHJldHVybiBmdW5jdGlvbihlKSB7XG4gICAgICAgIGUuZGVsZWdhdGVUYXJnZXQgPSBjbG9zZXN0KGUudGFyZ2V0LCBzZWxlY3Rvcik7XG5cbiAgICAgICAgaWYgKGUuZGVsZWdhdGVUYXJnZXQpIHtcbiAgICAgICAgICAgIGNhbGxiYWNrLmNhbGwoZWxlbWVudCwgZSk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZGVsZWdhdGU7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kZWxlZ2F0ZS9zcmMvZGVsZWdhdGUuanNcbi8vIG1vZHVsZSBpZCA9IDE1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBET0NVTUVOVF9OT0RFX1RZUEUgPSA5O1xuXG4vKipcbiAqIEEgcG9seWZpbGwgZm9yIEVsZW1lbnQubWF0Y2hlcygpXG4gKi9cbmlmICh0eXBlb2YgRWxlbWVudCAhPT0gJ3VuZGVmaW5lZCcgJiYgIUVsZW1lbnQucHJvdG90eXBlLm1hdGNoZXMpIHtcbiAgICB2YXIgcHJvdG8gPSBFbGVtZW50LnByb3RvdHlwZTtcblxuICAgIHByb3RvLm1hdGNoZXMgPSBwcm90by5tYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgICAgICAgICAgICAgcHJvdG8ubW96TWF0Y2hlc1NlbGVjdG9yIHx8XG4gICAgICAgICAgICAgICAgICAgIHByb3RvLm1zTWF0Y2hlc1NlbGVjdG9yIHx8XG4gICAgICAgICAgICAgICAgICAgIHByb3RvLm9NYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgICAgICAgICAgICAgcHJvdG8ud2Via2l0TWF0Y2hlc1NlbGVjdG9yO1xufVxuXG4vKipcbiAqIEZpbmRzIHRoZSBjbG9zZXN0IHBhcmVudCB0aGF0IG1hdGNoZXMgYSBzZWxlY3Rvci5cbiAqXG4gKiBAcGFyYW0ge0VsZW1lbnR9IGVsZW1lbnRcbiAqIEBwYXJhbSB7U3RyaW5nfSBzZWxlY3RvclxuICogQHJldHVybiB7RnVuY3Rpb259XG4gKi9cbmZ1bmN0aW9uIGNsb3Nlc3QgKGVsZW1lbnQsIHNlbGVjdG9yKSB7XG4gICAgd2hpbGUgKGVsZW1lbnQgJiYgZWxlbWVudC5ub2RlVHlwZSAhPT0gRE9DVU1FTlRfTk9ERV9UWVBFKSB7XG4gICAgICAgIGlmICh0eXBlb2YgZWxlbWVudC5tYXRjaGVzID09PSAnZnVuY3Rpb24nICYmXG4gICAgICAgICAgICBlbGVtZW50Lm1hdGNoZXMoc2VsZWN0b3IpKSB7XG4gICAgICAgICAgcmV0dXJuIGVsZW1lbnQ7XG4gICAgICAgIH1cbiAgICAgICAgZWxlbWVudCA9IGVsZW1lbnQucGFyZW50Tm9kZTtcbiAgICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY2xvc2VzdDtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RlbGVnYXRlL3NyYy9jbG9zZXN0LmpzXG4vLyBtb2R1bGUgaWQgPSAxNlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbnZhciBDYWNoZSA9IGV4cG9ydHMuQ2FjaGUgPSBmdW5jdGlvbiAoKSB7XG5cdGZ1bmN0aW9uIENhY2hlKCkge1xuXHRcdF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDYWNoZSk7XG5cblx0XHR0aGlzLnBhZ2VzID0ge307XG5cdFx0dGhpcy5sYXN0ID0gbnVsbDtcblx0fVxuXG5cdF9jcmVhdGVDbGFzcyhDYWNoZSwgW3tcblx0XHRrZXk6ICdjYWNoZVVybCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGNhY2hlVXJsKHBhZ2UpIHtcblx0XHRcdGlmIChwYWdlLnVybCBpbiB0aGlzLnBhZ2VzID09PSBmYWxzZSkge1xuXHRcdFx0XHR0aGlzLnBhZ2VzW3BhZ2UudXJsXSA9IHBhZ2U7XG5cdFx0XHR9XG5cdFx0XHR0aGlzLmxhc3QgPSB0aGlzLnBhZ2VzW3BhZ2UudXJsXTtcblx0XHRcdHRoaXMuc3d1cC5sb2coJ0NhY2hlICgnICsgT2JqZWN0LmtleXModGhpcy5wYWdlcykubGVuZ3RoICsgJyknLCB0aGlzLnBhZ2VzKTtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdnZXRQYWdlJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZ2V0UGFnZSh1cmwpIHtcblx0XHRcdHJldHVybiB0aGlzLnBhZ2VzW3VybF07XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnZ2V0Q3VycmVudFBhZ2UnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBnZXRDdXJyZW50UGFnZSgpIHtcblx0XHRcdHJldHVybiB0aGlzLmdldFBhZ2Uod2luZG93LmxvY2F0aW9uLnBhdGhuYW1lICsgd2luZG93LmxvY2F0aW9uLnNlYXJjaCk7XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnZXhpc3RzJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZXhpc3RzKHVybCkge1xuXHRcdFx0cmV0dXJuIHVybCBpbiB0aGlzLnBhZ2VzO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2VtcHR5Jyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZW1wdHkoKSB7XG5cdFx0XHR0aGlzLnBhZ2VzID0ge307XG5cdFx0XHR0aGlzLmxhc3QgPSBudWxsO1xuXHRcdFx0dGhpcy5zd3VwLmxvZygnQ2FjaGUgY2xlYXJlZCcpO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbW92ZScsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbW92ZSh1cmwpIHtcblx0XHRcdGRlbGV0ZSB0aGlzLnBhZ2VzW3VybF07XG5cdFx0fVxuXHR9XSk7XG5cblx0cmV0dXJuIENhY2hlO1xufSgpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBDYWNoZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9tb2R1bGVzL0NhY2hlLmpzXG4vLyBtb2R1bGUgaWQgPSAxN1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL2hlbHBlcnMnKTtcblxudmFyIGxvYWRQYWdlID0gZnVuY3Rpb24gbG9hZFBhZ2UoZGF0YSwgcG9wc3RhdGUpIHtcblx0dmFyIF90aGlzID0gdGhpcztcblxuXHQvLyBjcmVhdGUgYXJyYXkgZm9yIHN0b3JpbmcgYW5pbWF0aW9uIHByb21pc2VzXG5cdHZhciBhbmltYXRpb25Qcm9taXNlcyA9IFtdLFxuXHQgICAgeGhyUHJvbWlzZSA9IHZvaWQgMDtcblx0dmFyIGFuaW1hdGVPdXQgPSBmdW5jdGlvbiBhbmltYXRlT3V0KCkge1xuXHRcdF90aGlzLnRyaWdnZXJFdmVudCgnYW5pbWF0aW9uT3V0U3RhcnQnKTtcblxuXHRcdC8vIGhhbmRsZSBjbGFzc2VzXG5cdFx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2lzLWNoYW5naW5nJyk7XG5cdFx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2lzLWxlYXZpbmcnKTtcblx0XHRkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaXMtYW5pbWF0aW5nJyk7XG5cdFx0aWYgKHBvcHN0YXRlKSB7XG5cdFx0XHRkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaXMtcG9wc3RhdGUnKTtcblx0XHR9XG5cdFx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3RvLScgKyAoMCwgX2hlbHBlcnMuY2xhc3NpZnkpKGRhdGEudXJsKSk7XG5cblx0XHQvLyBhbmltYXRpb24gcHJvbWlzZSBzdHVmZlxuXHRcdGFuaW1hdGlvblByb21pc2VzID0gX3RoaXMuZ2V0QW5pbWF0aW9uUHJvbWlzZXMoJ291dCcpO1xuXHRcdFByb21pc2UuYWxsKGFuaW1hdGlvblByb21pc2VzKS50aGVuKGZ1bmN0aW9uICgpIHtcblx0XHRcdF90aGlzLnRyaWdnZXJFdmVudCgnYW5pbWF0aW9uT3V0RG9uZScpO1xuXHRcdH0pO1xuXG5cdFx0Ly8gY3JlYXRlIGhpc3RvcnkgcmVjb3JkIGlmIHRoaXMgaXMgbm90IGEgcG9wc3RhdGUgY2FsbFxuXHRcdGlmICghcG9wc3RhdGUpIHtcblx0XHRcdC8vIGNyZWF0ZSBwb3AgZWxlbWVudCB3aXRoIG9yIHdpdGhvdXQgYW5jaG9yXG5cdFx0XHR2YXIgc3RhdGUgPSB2b2lkIDA7XG5cdFx0XHRpZiAoX3RoaXMuc2Nyb2xsVG9FbGVtZW50ICE9IG51bGwpIHtcblx0XHRcdFx0c3RhdGUgPSBkYXRhLnVybCArIF90aGlzLnNjcm9sbFRvRWxlbWVudDtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHN0YXRlID0gZGF0YS51cmw7XG5cdFx0XHR9XG5cblx0XHRcdCgwLCBfaGVscGVycy5jcmVhdGVIaXN0b3J5UmVjb3JkKShzdGF0ZSk7XG5cdFx0fVxuXHR9O1xuXG5cdHRoaXMudHJpZ2dlckV2ZW50KCd0cmFuc2l0aW9uU3RhcnQnLCBwb3BzdGF0ZSk7XG5cblx0Ly8gc2V0IHRyYW5zaXRpb24gb2JqZWN0XG5cdGlmIChkYXRhLmN1c3RvbVRyYW5zaXRpb24gIT0gbnVsbCkge1xuXHRcdHRoaXMudXBkYXRlVHJhbnNpdGlvbih3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUsIGRhdGEudXJsLCBkYXRhLmN1c3RvbVRyYW5zaXRpb24pO1xuXHRcdGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QuYWRkKCd0by0nICsgKDAsIF9oZWxwZXJzLmNsYXNzaWZ5KShkYXRhLmN1c3RvbVRyYW5zaXRpb24pKTtcblx0fSBlbHNlIHtcblx0XHR0aGlzLnVwZGF0ZVRyYW5zaXRpb24od2luZG93LmxvY2F0aW9uLnBhdGhuYW1lLCBkYXRhLnVybCk7XG5cdH1cblxuXHQvLyBzdGFydC9za2lwIGFuaW1hdGlvblxuXHRpZiAoIXBvcHN0YXRlIHx8IHRoaXMub3B0aW9ucy5hbmltYXRlSGlzdG9yeUJyb3dzaW5nKSB7XG5cdFx0YW5pbWF0ZU91dCgpO1xuXHR9IGVsc2Uge1xuXHRcdHRoaXMudHJpZ2dlckV2ZW50KCdhbmltYXRpb25Ta2lwcGVkJyk7XG5cdH1cblxuXHQvLyBzdGFydC9za2lwIGxvYWRpbmcgb2YgcGFnZVxuXHRpZiAodGhpcy5jYWNoZS5leGlzdHMoZGF0YS51cmwpKSB7XG5cdFx0eGhyUHJvbWlzZSA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG5cdFx0XHRyZXNvbHZlKCk7XG5cdFx0fSk7XG5cdFx0dGhpcy50cmlnZ2VyRXZlbnQoJ3BhZ2VSZXRyaWV2ZWRGcm9tQ2FjaGUnKTtcblx0fSBlbHNlIHtcblx0XHRpZiAoIXRoaXMucHJlbG9hZFByb21pc2UgfHwgdGhpcy5wcmVsb2FkUHJvbWlzZS5yb3V0ZSAhPSBkYXRhLnVybCkge1xuXHRcdFx0eGhyUHJvbWlzZSA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcblx0XHRcdFx0KDAsIF9oZWxwZXJzLmZldGNoKShfZXh0ZW5kcyh7fSwgZGF0YSwgeyBoZWFkZXJzOiBfdGhpcy5vcHRpb25zLnJlcXVlc3RIZWFkZXJzIH0pLCBmdW5jdGlvbiAocmVzcG9uc2UpIHtcblx0XHRcdFx0XHRpZiAocmVzcG9uc2Uuc3RhdHVzID09PSA1MDApIHtcblx0XHRcdFx0XHRcdF90aGlzLnRyaWdnZXJFdmVudCgnc2VydmVyRXJyb3InKTtcblx0XHRcdFx0XHRcdHJlamVjdChkYXRhLnVybCk7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdC8vIGdldCBqc29uIGRhdGFcblx0XHRcdFx0XHRcdHZhciBwYWdlID0gX3RoaXMuZ2V0UGFnZURhdGEocmVzcG9uc2UpO1xuXHRcdFx0XHRcdFx0aWYgKHBhZ2UgIT0gbnVsbCkge1xuXHRcdFx0XHRcdFx0XHRwYWdlLnVybCA9IGRhdGEudXJsO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0cmVqZWN0KGRhdGEudXJsKTtcblx0XHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0Ly8gcmVuZGVyIHBhZ2Vcblx0XHRcdFx0XHRcdF90aGlzLmNhY2hlLmNhY2hlVXJsKHBhZ2UpO1xuXHRcdFx0XHRcdFx0X3RoaXMudHJpZ2dlckV2ZW50KCdwYWdlTG9hZGVkJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHJlc29sdmUoKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0eGhyUHJvbWlzZSA9IHRoaXMucHJlbG9hZFByb21pc2U7XG5cdFx0fVxuXHR9XG5cblx0Ly8gd2hlbiBldmVyeXRoaW5nIGlzIHJlYWR5LCBoYW5kbGUgdGhlIG91dGNvbWVcblx0UHJvbWlzZS5hbGwoYW5pbWF0aW9uUHJvbWlzZXMuY29uY2F0KFt4aHJQcm9taXNlXSkpLnRoZW4oZnVuY3Rpb24gKCkge1xuXHRcdC8vIHJlbmRlciBwYWdlXG5cdFx0X3RoaXMucmVuZGVyUGFnZShfdGhpcy5jYWNoZS5nZXRQYWdlKGRhdGEudXJsKSwgcG9wc3RhdGUpO1xuXHRcdF90aGlzLnByZWxvYWRQcm9taXNlID0gbnVsbDtcblx0fSkuY2F0Y2goZnVuY3Rpb24gKGVycm9yVXJsKSB7XG5cdFx0Ly8gcmV3cml0ZSB0aGUgc2tpcFBvcFN0YXRlSGFuZGxpbmcgZnVuY3Rpb24gdG8gcmVkaXJlY3QgbWFudWFsbHkgd2hlbiB0aGUgaGlzdG9yeS5nbyBpcyBwcm9jZXNzZWRcblx0XHRfdGhpcy5vcHRpb25zLnNraXBQb3BTdGF0ZUhhbmRsaW5nID0gZnVuY3Rpb24gKCkge1xuXHRcdFx0d2luZG93LmxvY2F0aW9uID0gZXJyb3JVcmw7XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9O1xuXG5cdFx0Ly8gZ28gYmFjayB0byB0aGUgYWN0dWFsIHBhZ2Ugd2VyZSBzdGlsbCBhdFxuXHRcdHdpbmRvdy5oaXN0b3J5LmdvKC0xKTtcblx0fSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBsb2FkUGFnZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9tb2R1bGVzL2xvYWRQYWdlLmpzXG4vLyBtb2R1bGUgaWQgPSAxOFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG52YXIgY2xhc3NpZnkgPSBmdW5jdGlvbiBjbGFzc2lmeSh0ZXh0KSB7XG5cdHZhciBvdXRwdXQgPSB0ZXh0LnRvU3RyaW5nKCkudG9Mb3dlckNhc2UoKS5yZXBsYWNlKC9cXHMrL2csICctJykgLy8gUmVwbGFjZSBzcGFjZXMgd2l0aCAtXG5cdC5yZXBsYWNlKC9cXC8vZywgJy0nKSAvLyBSZXBsYWNlIC8gd2l0aCAtXG5cdC5yZXBsYWNlKC9bXlxcd1xcLV0rL2csICcnKSAvLyBSZW1vdmUgYWxsIG5vbi13b3JkIGNoYXJzXG5cdC5yZXBsYWNlKC9cXC1cXC0rL2csICctJykgLy8gUmVwbGFjZSBtdWx0aXBsZSAtIHdpdGggc2luZ2xlIC1cblx0LnJlcGxhY2UoL14tKy8sICcnKSAvLyBUcmltIC0gZnJvbSBzdGFydCBvZiB0ZXh0XG5cdC5yZXBsYWNlKC8tKyQvLCAnJyk7IC8vIFRyaW0gLSBmcm9tIGVuZCBvZiB0ZXh0XG5cdGlmIChvdXRwdXRbMF0gPT09ICcvJykgb3V0cHV0ID0gb3V0cHV0LnNwbGljZSgxKTtcblx0aWYgKG91dHB1dCA9PT0gJycpIG91dHB1dCA9ICdob21lcGFnZSc7XG5cdHJldHVybiBvdXRwdXQ7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjbGFzc2lmeTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL2NsYXNzaWZ5LmpzXG4vLyBtb2R1bGUgaWQgPSAxOVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG52YXIgY3JlYXRlSGlzdG9yeVJlY29yZCA9IGZ1bmN0aW9uIGNyZWF0ZUhpc3RvcnlSZWNvcmQodXJsKSB7XG5cdHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZSh7XG5cdFx0dXJsOiB1cmwgfHwgd2luZG93LmxvY2F0aW9uLmhyZWYuc3BsaXQod2luZG93LmxvY2F0aW9uLmhvc3RuYW1lKVsxXSxcblx0XHRyYW5kb206IE1hdGgucmFuZG9tKCksXG5cdFx0c291cmNlOiAnc3d1cCdcblx0fSwgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ3RpdGxlJylbMF0uaW5uZXJUZXh0LCB1cmwgfHwgd2luZG93LmxvY2F0aW9uLmhyZWYuc3BsaXQod2luZG93LmxvY2F0aW9uLmhvc3RuYW1lKVsxXSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVIaXN0b3J5UmVjb3JkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL2hlbHBlcnMvY3JlYXRlSGlzdG9yeVJlY29yZC5qc1xuLy8gbW9kdWxlIGlkID0gMjBcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07XG5cbnZhciBfdXRpbHMgPSByZXF1aXJlKCcuLi91dGlscycpO1xuXG52YXIgZ2V0RGF0YUZyb21IdG1sID0gZnVuY3Rpb24gZ2V0RGF0YUZyb21IdG1sKGh0bWwsIGNvbnRhaW5lcnMpIHtcblx0dmFyIGNvbnRlbnQgPSBodG1sLnJlcGxhY2UoJzxib2R5JywgJzxkaXYgaWQ9XCJzd3VwQm9keVwiJykucmVwbGFjZSgnPC9ib2R5PicsICc8L2Rpdj4nKTtcblx0dmFyIGZha2VEb20gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcblx0ZmFrZURvbS5pbm5lckhUTUwgPSBjb250ZW50O1xuXHR2YXIgYmxvY2tzID0gW107XG5cblx0dmFyIF9sb29wID0gZnVuY3Rpb24gX2xvb3AoaSkge1xuXHRcdGlmIChmYWtlRG9tLnF1ZXJ5U2VsZWN0b3IoY29udGFpbmVyc1tpXSkgPT0gbnVsbCkge1xuXHRcdFx0Ly8gcGFnZSBpbiBpbnZhbGlkXG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHR2OiBudWxsXG5cdFx0XHR9O1xuXHRcdH0gZWxzZSB7XG5cdFx0XHQoMCwgX3V0aWxzLnF1ZXJ5QWxsKShjb250YWluZXJzW2ldKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xuXHRcdFx0XHQoMCwgX3V0aWxzLnF1ZXJ5QWxsKShjb250YWluZXJzW2ldLCBmYWtlRG9tKVtpbmRleF0uc2V0QXR0cmlidXRlKCdkYXRhLXN3dXAnLCBibG9ja3MubGVuZ3RoKTsgLy8gbWFya3MgZWxlbWVudCB3aXRoIGRhdGEtc3d1cFxuXHRcdFx0XHRibG9ja3MucHVzaCgoMCwgX3V0aWxzLnF1ZXJ5QWxsKShjb250YWluZXJzW2ldLCBmYWtlRG9tKVtpbmRleF0ub3V0ZXJIVE1MKTtcblx0XHRcdH0pO1xuXHRcdH1cblx0fTtcblxuXHRmb3IgKHZhciBpID0gMDsgaSA8IGNvbnRhaW5lcnMubGVuZ3RoOyBpKyspIHtcblx0XHR2YXIgX3JldCA9IF9sb29wKGkpO1xuXG5cdFx0aWYgKCh0eXBlb2YgX3JldCA9PT0gJ3VuZGVmaW5lZCcgPyAndW5kZWZpbmVkJyA6IF90eXBlb2YoX3JldCkpID09PSBcIm9iamVjdFwiKSByZXR1cm4gX3JldC52O1xuXHR9XG5cblx0dmFyIGpzb24gPSB7XG5cdFx0dGl0bGU6IGZha2VEb20ucXVlcnlTZWxlY3RvcigndGl0bGUnKS5pbm5lclRleHQsXG5cdFx0cGFnZUNsYXNzOiBmYWtlRG9tLnF1ZXJ5U2VsZWN0b3IoJyNzd3VwQm9keScpLmNsYXNzTmFtZSxcblx0XHRvcmlnaW5hbENvbnRlbnQ6IGh0bWwsXG5cdFx0YmxvY2tzOiBibG9ja3Ncblx0fTtcblxuXHQvLyB0byBwcmV2ZW50IG1lbW9yeSBsZWFrc1xuXHRmYWtlRG9tLmlubmVySFRNTCA9ICcnO1xuXHRmYWtlRG9tID0gbnVsbDtcblxuXHRyZXR1cm4ganNvbjtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGdldERhdGFGcm9tSHRtbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL2dldERhdGFGcm9tSHRtbC5qc1xuLy8gbW9kdWxlIGlkID0gMjFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgZmV0Y2ggPSBmdW5jdGlvbiBmZXRjaChzZXRPcHRpb25zKSB7XG5cdHZhciBjYWxsYmFjayA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogZmFsc2U7XG5cblx0dmFyIGRlZmF1bHRzID0ge1xuXHRcdHVybDogd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lICsgd2luZG93LmxvY2F0aW9uLnNlYXJjaCxcblx0XHRtZXRob2Q6ICdHRVQnLFxuXHRcdGRhdGE6IG51bGwsXG5cdFx0aGVhZGVyczoge31cblx0fTtcblxuXHR2YXIgb3B0aW9ucyA9IF9leHRlbmRzKHt9LCBkZWZhdWx0cywgc2V0T3B0aW9ucyk7XG5cblx0dmFyIHJlcXVlc3QgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuXHRyZXF1ZXN0Lm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uICgpIHtcblx0XHRpZiAocmVxdWVzdC5yZWFkeVN0YXRlID09PSA0KSB7XG5cdFx0XHRpZiAocmVxdWVzdC5zdGF0dXMgIT09IDUwMCkge1xuXHRcdFx0XHRjYWxsYmFjayhyZXF1ZXN0KTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdGNhbGxiYWNrKHJlcXVlc3QpO1xuXHRcdFx0fVxuXHRcdH1cblx0fTtcblxuXHRyZXF1ZXN0Lm9wZW4ob3B0aW9ucy5tZXRob2QsIG9wdGlvbnMudXJsLCB0cnVlKTtcblx0T2JqZWN0LmtleXMob3B0aW9ucy5oZWFkZXJzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcblx0XHRyZXF1ZXN0LnNldFJlcXVlc3RIZWFkZXIoa2V5LCBvcHRpb25zLmhlYWRlcnNba2V5XSk7XG5cdH0pO1xuXHRyZXF1ZXN0LnNlbmQob3B0aW9ucy5kYXRhKTtcblx0cmV0dXJuIHJlcXVlc3Q7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBmZXRjaDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL2ZldGNoLmpzXG4vLyBtb2R1bGUgaWQgPSAyMlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG52YXIgdHJhbnNpdGlvbkVuZCA9IGZ1bmN0aW9uIHRyYW5zaXRpb25FbmQoKSB7XG5cdHZhciBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuXG5cdHZhciB0cmFuc0VuZEV2ZW50TmFtZXMgPSB7XG5cdFx0V2Via2l0VHJhbnNpdGlvbjogJ3dlYmtpdFRyYW5zaXRpb25FbmQnLFxuXHRcdE1velRyYW5zaXRpb246ICd0cmFuc2l0aW9uZW5kJyxcblx0XHRPVHJhbnNpdGlvbjogJ29UcmFuc2l0aW9uRW5kIG90cmFuc2l0aW9uZW5kJyxcblx0XHR0cmFuc2l0aW9uOiAndHJhbnNpdGlvbmVuZCdcblx0fTtcblxuXHRmb3IgKHZhciBuYW1lIGluIHRyYW5zRW5kRXZlbnROYW1lcykge1xuXHRcdGlmIChlbC5zdHlsZVtuYW1lXSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRyZXR1cm4gdHJhbnNFbmRFdmVudE5hbWVzW25hbWVdO1xuXHRcdH1cblx0fVxuXG5cdHJldHVybiBmYWxzZTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHRyYW5zaXRpb25FbmQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvaGVscGVycy90cmFuc2l0aW9uRW5kLmpzXG4vLyBtb2R1bGUgaWQgPSAyM1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG5cdHZhbHVlOiB0cnVlXG59KTtcbnZhciBnZXRDdXJyZW50VXJsID0gZnVuY3Rpb24gZ2V0Q3VycmVudFVybCgpIHtcblx0cmV0dXJuIHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSArIHdpbmRvdy5sb2NhdGlvbi5zZWFyY2g7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBnZXRDdXJyZW50VXJsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL2hlbHBlcnMvZ2V0Q3VycmVudFVybC5qc1xuLy8gbW9kdWxlIGlkID0gMjRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3V0aWxzID0gcmVxdWlyZSgnLi4vdXRpbHMnKTtcblxudmFyIG1hcmtTd3VwRWxlbWVudHMgPSBmdW5jdGlvbiBtYXJrU3d1cEVsZW1lbnRzKGVsZW1lbnQsIGNvbnRhaW5lcnMpIHtcblx0dmFyIGJsb2NrcyA9IDA7XG5cblx0dmFyIF9sb29wID0gZnVuY3Rpb24gX2xvb3AoaSkge1xuXHRcdGlmIChlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoY29udGFpbmVyc1tpXSkgPT0gbnVsbCkge1xuXHRcdFx0Y29uc29sZS53YXJuKCdFbGVtZW50ICcgKyBjb250YWluZXJzW2ldICsgJyBpcyBub3QgaW4gY3VycmVudCBwYWdlLicpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHQoMCwgX3V0aWxzLnF1ZXJ5QWxsKShjb250YWluZXJzW2ldKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xuXHRcdFx0XHQoMCwgX3V0aWxzLnF1ZXJ5QWxsKShjb250YWluZXJzW2ldLCBlbGVtZW50KVtpbmRleF0uc2V0QXR0cmlidXRlKCdkYXRhLXN3dXAnLCBibG9ja3MpO1xuXHRcdFx0XHRibG9ja3MrKztcblx0XHRcdH0pO1xuXHRcdH1cblx0fTtcblxuXHRmb3IgKHZhciBpID0gMDsgaSA8IGNvbnRhaW5lcnMubGVuZ3RoOyBpKyspIHtcblx0XHRfbG9vcChpKTtcblx0fVxufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gbWFya1N3dXBFbGVtZW50cztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL21hcmtTd3VwRWxlbWVudHMuanNcbi8vIG1vZHVsZSBpZCA9IDI1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG5cdHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxudmFyIExpbmsgPSBmdW5jdGlvbiAoKSB7XG5cdGZ1bmN0aW9uIExpbmsoZWxlbWVudE9yVXJsKSB7XG5cdFx0X2NsYXNzQ2FsbENoZWNrKHRoaXMsIExpbmspO1xuXG5cdFx0aWYgKGVsZW1lbnRPclVybCBpbnN0YW5jZW9mIEVsZW1lbnQgfHwgZWxlbWVudE9yVXJsIGluc3RhbmNlb2YgU1ZHRWxlbWVudCkge1xuXHRcdFx0dGhpcy5saW5rID0gZWxlbWVudE9yVXJsO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLmxpbmsgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XG5cdFx0XHR0aGlzLmxpbmsuaHJlZiA9IGVsZW1lbnRPclVybDtcblx0XHR9XG5cdH1cblxuXHRfY3JlYXRlQ2xhc3MoTGluaywgW3tcblx0XHRrZXk6ICdnZXRQYXRoJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZ2V0UGF0aCgpIHtcblx0XHRcdHZhciBwYXRoID0gdGhpcy5saW5rLnBhdGhuYW1lO1xuXHRcdFx0aWYgKHBhdGhbMF0gIT09ICcvJykge1xuXHRcdFx0XHRwYXRoID0gJy8nICsgcGF0aDtcblx0XHRcdH1cblx0XHRcdHJldHVybiBwYXRoO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2dldEFkZHJlc3MnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBnZXRBZGRyZXNzKCkge1xuXHRcdFx0dmFyIHBhdGggPSB0aGlzLmxpbmsucGF0aG5hbWUgKyB0aGlzLmxpbmsuc2VhcmNoO1xuXG5cdFx0XHRpZiAodGhpcy5saW5rLmdldEF0dHJpYnV0ZSgneGxpbms6aHJlZicpKSB7XG5cdFx0XHRcdHBhdGggPSB0aGlzLmxpbmsuZ2V0QXR0cmlidXRlKCd4bGluazpocmVmJyk7XG5cdFx0XHR9XG5cblx0XHRcdGlmIChwYXRoWzBdICE9PSAnLycpIHtcblx0XHRcdFx0cGF0aCA9ICcvJyArIHBhdGg7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gcGF0aDtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdnZXRIYXNoJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZ2V0SGFzaCgpIHtcblx0XHRcdHJldHVybiB0aGlzLmxpbmsuaGFzaDtcblx0XHR9XG5cdH1dKTtcblxuXHRyZXR1cm4gTGluaztcbn0oKTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gTGluaztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9oZWxwZXJzL0xpbmsuanNcbi8vIG1vZHVsZSBpZCA9IDI2XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG5cdHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF91dGlscyA9IHJlcXVpcmUoJy4uL3V0aWxzJyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL2hlbHBlcnMnKTtcblxudmFyIHJlbmRlclBhZ2UgPSBmdW5jdGlvbiByZW5kZXJQYWdlKHBhZ2UsIHBvcHN0YXRlKSB7XG5cdHZhciBfdGhpcyA9IHRoaXM7XG5cblx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2lzLWxlYXZpbmcnKTtcblxuXHQvLyByZXBsYWNlIHN0YXRlIGluIGNhc2UgdGhlIHVybCB3YXMgcmVkaXJlY3RlZFxuXHR2YXIgbGluayA9IG5ldyBfaGVscGVycy5MaW5rKHBhZ2UucmVzcG9uc2VVUkwpO1xuXHRpZiAod2luZG93LmxvY2F0aW9uLnBhdGhuYW1lICE9PSBsaW5rLmdldFBhdGgoKSkge1xuXHRcdHdpbmRvdy5oaXN0b3J5LnJlcGxhY2VTdGF0ZSh7XG5cdFx0XHR1cmw6IGxpbmsuZ2V0UGF0aCgpLFxuXHRcdFx0cmFuZG9tOiBNYXRoLnJhbmRvbSgpLFxuXHRcdFx0c291cmNlOiAnc3d1cCdcblx0XHR9LCBkb2N1bWVudC50aXRsZSwgbGluay5nZXRQYXRoKCkpO1xuXG5cdFx0Ly8gc2F2ZSBuZXcgcmVjb3JkIGZvciByZWRpcmVjdGVkIHVybFxuXHRcdHRoaXMuY2FjaGUuY2FjaGVVcmwoX2V4dGVuZHMoe30sIHBhZ2UsIHsgdXJsOiBsaW5rLmdldFBhdGgoKSB9KSk7XG5cdH1cblxuXHQvLyBvbmx5IGFkZCBmb3Igbm9uLXBvcHN0YXRlIHRyYW5zaXRpb25zXG5cdGlmICghcG9wc3RhdGUgfHwgdGhpcy5vcHRpb25zLmFuaW1hdGVIaXN0b3J5QnJvd3NpbmcpIHtcblx0XHRkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnaXMtcmVuZGVyaW5nJyk7XG5cdH1cblxuXHR0aGlzLnRyaWdnZXJFdmVudCgnd2lsbFJlcGxhY2VDb250ZW50JywgcG9wc3RhdGUpO1xuXG5cdC8vIHJlcGxhY2UgYmxvY2tzXG5cdGZvciAodmFyIGkgPSAwOyBpIDwgcGFnZS5ibG9ja3MubGVuZ3RoOyBpKyspIHtcblx0XHRkb2N1bWVudC5ib2R5LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLXN3dXA9XCInICsgaSArICdcIl0nKS5vdXRlckhUTUwgPSBwYWdlLmJsb2Nrc1tpXTtcblx0fVxuXG5cdC8vIHNldCB0aXRsZVxuXHRkb2N1bWVudC50aXRsZSA9IHBhZ2UudGl0bGU7XG5cblx0dGhpcy50cmlnZ2VyRXZlbnQoJ2NvbnRlbnRSZXBsYWNlZCcsIHBvcHN0YXRlKTtcblx0dGhpcy50cmlnZ2VyRXZlbnQoJ3BhZ2VWaWV3JywgcG9wc3RhdGUpO1xuXG5cdC8vIGVtcHR5IGNhY2hlIGlmIGl0J3MgZGlzYWJsZWQgKGJlY2F1c2UgcGFnZXMgY291bGQgYmUgcHJlbG9hZGVkIGFuZCBzdHVmZilcblx0aWYgKCF0aGlzLm9wdGlvbnMuY2FjaGUpIHtcblx0XHR0aGlzLmNhY2hlLmVtcHR5KCk7XG5cdH1cblxuXHQvLyBzdGFydCBhbmltYXRpb24gSU5cblx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG5cdFx0aWYgKCFwb3BzdGF0ZSB8fCBfdGhpcy5vcHRpb25zLmFuaW1hdGVIaXN0b3J5QnJvd3NpbmcpIHtcblx0XHRcdF90aGlzLnRyaWdnZXJFdmVudCgnYW5pbWF0aW9uSW5TdGFydCcpO1xuXHRcdFx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2lzLWFuaW1hdGluZycpO1xuXHRcdH1cblx0fSwgMTApO1xuXG5cdC8vIGhhbmRsZSBlbmQgb2YgYW5pbWF0aW9uXG5cdHZhciBhbmltYXRpb25Qcm9taXNlcyA9IHRoaXMuZ2V0QW5pbWF0aW9uUHJvbWlzZXMoJ2luJyk7XG5cdGlmICghcG9wc3RhdGUgfHwgdGhpcy5vcHRpb25zLmFuaW1hdGVIaXN0b3J5QnJvd3NpbmcpIHtcblx0XHRQcm9taXNlLmFsbChhbmltYXRpb25Qcm9taXNlcykudGhlbihmdW5jdGlvbiAoKSB7XG5cdFx0XHRfdGhpcy50cmlnZ2VyRXZlbnQoJ2FuaW1hdGlvbkluRG9uZScpO1xuXHRcdFx0X3RoaXMudHJpZ2dlckV2ZW50KCd0cmFuc2l0aW9uRW5kJywgcG9wc3RhdGUpO1xuXHRcdFx0Ly8gcmVtb3ZlIFwidG8te3BhZ2V9XCIgY2xhc3Nlc1xuXHRcdFx0ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTmFtZS5zcGxpdCgnICcpLmZvckVhY2goZnVuY3Rpb24gKGNsYXNzSXRlbSkge1xuXHRcdFx0XHRpZiAobmV3IFJlZ0V4cCgnXnRvLScpLnRlc3QoY2xhc3NJdGVtKSB8fCBjbGFzc0l0ZW0gPT09ICdpcy1jaGFuZ2luZycgfHwgY2xhc3NJdGVtID09PSAnaXMtcmVuZGVyaW5nJyB8fCBjbGFzc0l0ZW0gPT09ICdpcy1wb3BzdGF0ZScpIHtcblx0XHRcdFx0XHRkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShjbGFzc0l0ZW0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0fSBlbHNlIHtcblx0XHR0aGlzLnRyaWdnZXJFdmVudCgndHJhbnNpdGlvbkVuZCcsIHBvcHN0YXRlKTtcblx0fVxuXG5cdC8vIHJlc2V0IHNjcm9sbC10byBlbGVtZW50XG5cdHRoaXMuc2Nyb2xsVG9FbGVtZW50ID0gbnVsbDtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHJlbmRlclBhZ2U7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvbW9kdWxlcy9yZW5kZXJQYWdlLmpzXG4vLyBtb2R1bGUgaWQgPSAyN1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG52YXIgdHJpZ2dlckV2ZW50ID0gZnVuY3Rpb24gdHJpZ2dlckV2ZW50KGV2ZW50TmFtZSwgb3JpZ2luYWxFdmVudCkge1xuXHQvLyBjYWxsIHNhdmVkIGhhbmRsZXJzIHdpdGggXCJvblwiIG1ldGhvZCBhbmQgcGFzcyBvcmlnaW5hbEV2ZW50IG9iamVjdCBpZiBhdmFpbGFibGVcblx0dGhpcy5faGFuZGxlcnNbZXZlbnROYW1lXS5mb3JFYWNoKGZ1bmN0aW9uIChoYW5kbGVyKSB7XG5cdFx0dHJ5IHtcblx0XHRcdGhhbmRsZXIob3JpZ2luYWxFdmVudCk7XG5cdFx0fSBjYXRjaCAoZXJyb3IpIHtcblx0XHRcdGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXHRcdH1cblx0fSk7XG5cblx0Ly8gdHJpZ2dlciBldmVudCBvbiBkb2N1bWVudCB3aXRoIHByZWZpeCBcInN3dXA6XCJcblx0dmFyIGV2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KCdzd3VwOicgKyBldmVudE5hbWUsIHsgZGV0YWlsOiBldmVudE5hbWUgfSk7XG5cdGRvY3VtZW50LmRpc3BhdGNoRXZlbnQoZXZlbnQpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gdHJpZ2dlckV2ZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvdHJpZ2dlckV2ZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAyOFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG5cdHZhbHVlOiB0cnVlXG59KTtcbnZhciBvbiA9IGZ1bmN0aW9uIG9uKGV2ZW50LCBoYW5kbGVyKSB7XG5cdGlmICh0aGlzLl9oYW5kbGVyc1tldmVudF0pIHtcblx0XHR0aGlzLl9oYW5kbGVyc1tldmVudF0ucHVzaChoYW5kbGVyKTtcblx0fSBlbHNlIHtcblx0XHRjb25zb2xlLndhcm4oXCJVbnN1cHBvcnRlZCBldmVudCBcIiArIGV2ZW50ICsgXCIuXCIpO1xuXHR9XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBvbjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zd3VwL2xpYi9tb2R1bGVzL29uLmpzXG4vLyBtb2R1bGUgaWQgPSAyOVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG5cdHZhbHVlOiB0cnVlXG59KTtcbnZhciBvZmYgPSBmdW5jdGlvbiBvZmYoZXZlbnQsIGhhbmRsZXIpIHtcblx0dmFyIF90aGlzID0gdGhpcztcblxuXHRpZiAoZXZlbnQgIT0gbnVsbCkge1xuXHRcdGlmIChoYW5kbGVyICE9IG51bGwpIHtcblx0XHRcdGlmICh0aGlzLl9oYW5kbGVyc1tldmVudF0gJiYgdGhpcy5faGFuZGxlcnNbZXZlbnRdLmZpbHRlcihmdW5jdGlvbiAoc2F2ZWRIYW5kbGVyKSB7XG5cdFx0XHRcdHJldHVybiBzYXZlZEhhbmRsZXIgPT09IGhhbmRsZXI7XG5cdFx0XHR9KS5sZW5ndGgpIHtcblx0XHRcdFx0dmFyIHRvUmVtb3ZlID0gdGhpcy5faGFuZGxlcnNbZXZlbnRdLmZpbHRlcihmdW5jdGlvbiAoc2F2ZWRIYW5kbGVyKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHNhdmVkSGFuZGxlciA9PT0gaGFuZGxlcjtcblx0XHRcdFx0fSlbMF07XG5cdFx0XHRcdHZhciBpbmRleCA9IHRoaXMuX2hhbmRsZXJzW2V2ZW50XS5pbmRleE9mKHRvUmVtb3ZlKTtcblx0XHRcdFx0aWYgKGluZGV4ID4gLTEpIHtcblx0XHRcdFx0XHR0aGlzLl9oYW5kbGVyc1tldmVudF0uc3BsaWNlKGluZGV4LCAxKTtcblx0XHRcdFx0fVxuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Y29uc29sZS53YXJuKFwiSGFuZGxlciBmb3IgZXZlbnQgJ1wiICsgZXZlbnQgKyBcIicgbm8gZm91bmQuXCIpO1xuXHRcdFx0fVxuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLl9oYW5kbGVyc1tldmVudF0gPSBbXTtcblx0XHR9XG5cdH0gZWxzZSB7XG5cdFx0T2JqZWN0LmtleXModGhpcy5faGFuZGxlcnMpLmZvckVhY2goZnVuY3Rpb24gKGtleXMpIHtcblx0XHRcdF90aGlzLl9oYW5kbGVyc1trZXlzXSA9IFtdO1xuXHRcdH0pO1xuXHR9XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBvZmY7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvbW9kdWxlcy9vZmYuanNcbi8vIG1vZHVsZSBpZCA9IDMwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIlwidXNlIHN0cmljdFwiO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xudmFyIHVwZGF0ZVRyYW5zaXRpb24gPSBmdW5jdGlvbiB1cGRhdGVUcmFuc2l0aW9uKGZyb20sIHRvLCBjdXN0b20pIHtcblx0Ly8gdHJhbnNpdGlvbiByb3V0ZXNcblx0dGhpcy50cmFuc2l0aW9uID0ge1xuXHRcdGZyb206IGZyb20sXG5cdFx0dG86IHRvLFxuXHRcdGN1c3RvbTogY3VzdG9tXG5cdH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSB1cGRhdGVUcmFuc2l0aW9uO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvdXBkYXRlVHJhbnNpdGlvbi5qc1xuLy8gbW9kdWxlIGlkID0gMzFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3V0aWxzID0gcmVxdWlyZSgnLi4vdXRpbHMnKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vaGVscGVycycpO1xuXG52YXIgZ2V0QW5pbWF0aW9uUHJvbWlzZXMgPSBmdW5jdGlvbiBnZXRBbmltYXRpb25Qcm9taXNlcygpIHtcblx0dmFyIHByb21pc2VzID0gW107XG5cdHZhciBhbmltYXRlZEVsZW1lbnRzID0gKDAsIF91dGlscy5xdWVyeUFsbCkodGhpcy5vcHRpb25zLmFuaW1hdGlvblNlbGVjdG9yKTtcblx0YW5pbWF0ZWRFbGVtZW50cy5mb3JFYWNoKGZ1bmN0aW9uIChlbGVtZW50KSB7XG5cdFx0dmFyIHByb21pc2UgPSBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSkge1xuXHRcdFx0ZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCgwLCBfaGVscGVycy50cmFuc2l0aW9uRW5kKSgpLCBmdW5jdGlvbiAoZXZlbnQpIHtcblx0XHRcdFx0aWYgKGVsZW1lbnQgPT0gZXZlbnQudGFyZ2V0KSB7XG5cdFx0XHRcdFx0cmVzb2x2ZSgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9KTtcblx0XHRwcm9taXNlcy5wdXNoKHByb21pc2UpO1xuXHR9KTtcblx0cmV0dXJuIHByb21pc2VzO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0QW5pbWF0aW9uUHJvbWlzZXM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvbW9kdWxlcy9nZXRBbmltYXRpb25Qcm9taXNlcy5qc1xuLy8gbW9kdWxlIGlkID0gMzJcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi9oZWxwZXJzJyk7XG5cbnZhciBnZXRQYWdlRGF0YSA9IGZ1bmN0aW9uIGdldFBhZ2VEYXRhKHJlcXVlc3QpIHtcblx0Ly8gdGhpcyBtZXRob2QgY2FuIGJlIHJlcGxhY2VkIGluIGNhc2Ugb3RoZXIgY29udGVudCB0aGFuIGh0bWwgaXMgZXhwZWN0ZWQgdG8gYmUgcmVjZWl2ZWQgZnJvbSBzZXJ2ZXJcblx0Ly8gdGhpcyBmdW5jdGlvbiBzaG91bGQgYWx3YXlzIHJldHVybiB7dGl0bGUsIHBhZ2VDbGFzcywgb3JpZ2luYWxDb250ZW50LCBibG9ja3MsIHJlc3BvbnNlVVJMfVxuXHQvLyBpbiBjYXNlIHBhZ2UgaGFzIGludmFsaWQgc3RydWN0dXJlIC0gcmV0dXJuIG51bGxcblx0dmFyIGh0bWwgPSByZXF1ZXN0LnJlc3BvbnNlVGV4dDtcblx0dmFyIHBhZ2VPYmplY3QgPSAoMCwgX2hlbHBlcnMuZ2V0RGF0YUZyb21IdG1sKShodG1sLCB0aGlzLm9wdGlvbnMuY29udGFpbmVycyk7XG5cblx0aWYgKHBhZ2VPYmplY3QpIHtcblx0XHRwYWdlT2JqZWN0LnJlc3BvbnNlVVJMID0gcmVxdWVzdC5yZXNwb25zZVVSTCA/IHJlcXVlc3QucmVzcG9uc2VVUkwgOiB3aW5kb3cubG9jYXRpb24uaHJlZjtcblx0fSBlbHNlIHtcblx0XHRjb25zb2xlLndhcm4oJ1JlY2VpdmVkIHBhZ2UgaXMgaW52YWxpZC4nKTtcblx0XHRyZXR1cm4gbnVsbDtcblx0fVxuXG5cdHJldHVybiBwYWdlT2JqZWN0O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0UGFnZURhdGE7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc3d1cC9saWIvbW9kdWxlcy9nZXRQYWdlRGF0YS5qc1xuLy8gbW9kdWxlIGlkID0gMzNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xudmFyIHVzZSA9IGV4cG9ydHMudXNlID0gZnVuY3Rpb24gdXNlKHBsdWdpbikge1xuXHRpZiAoIXBsdWdpbi5pc1N3dXBQbHVnaW4pIHtcblx0XHRjb25zb2xlLndhcm4oJ05vdCBzd3VwIHBsdWdpbiBpbnN0YW5jZSAnICsgcGx1Z2luICsgJy4nKTtcblx0XHRyZXR1cm47XG5cdH1cblxuXHR0aGlzLnBsdWdpbnMucHVzaChwbHVnaW4pO1xuXHRwbHVnaW4uc3d1cCA9IHRoaXM7XG5cdGlmICh0eXBlb2YgcGx1Z2luLl9iZWZvcmVNb3VudCA9PT0gJ2Z1bmN0aW9uJykge1xuXHRcdHBsdWdpbi5fYmVmb3JlTW91bnQoKTtcblx0fVxuXHRwbHVnaW4ubW91bnQoKTtcblxuXHRyZXR1cm4gdGhpcy5wbHVnaW5zO1xufTtcblxudmFyIHVudXNlID0gZXhwb3J0cy51bnVzZSA9IGZ1bmN0aW9uIHVudXNlKHBsdWdpbikge1xuXHR2YXIgcGx1Z2luUmVmZXJlbmNlID0gdm9pZCAwO1xuXG5cdGlmICh0eXBlb2YgcGx1Z2luID09PSAnc3RyaW5nJykge1xuXHRcdHBsdWdpblJlZmVyZW5jZSA9IHRoaXMucGx1Z2lucy5maW5kKGZ1bmN0aW9uIChwKSB7XG5cdFx0XHRyZXR1cm4gcGx1Z2luID09PSBwLm5hbWU7XG5cdFx0fSk7XG5cdH0gZWxzZSB7XG5cdFx0cGx1Z2luUmVmZXJlbmNlID0gcGx1Z2luO1xuXHR9XG5cblx0aWYgKCFwbHVnaW5SZWZlcmVuY2UpIHtcblx0XHRjb25zb2xlLndhcm4oJ05vIHN1Y2ggcGx1Z2luLicpO1xuXHRcdHJldHVybjtcblx0fVxuXG5cdHBsdWdpblJlZmVyZW5jZS51bm1vdW50KCk7XG5cblx0aWYgKHR5cGVvZiBwbHVnaW5SZWZlcmVuY2UuX2FmdGVyVW5tb3VudCA9PT0gJ2Z1bmN0aW9uJykge1xuXHRcdHBsdWdpblJlZmVyZW5jZS5fYWZ0ZXJVbm1vdW50KCk7XG5cdH1cblxuXHR2YXIgaW5kZXggPSB0aGlzLnBsdWdpbnMuaW5kZXhPZihwbHVnaW5SZWZlcmVuY2UpO1xuXHR0aGlzLnBsdWdpbnMuc3BsaWNlKGluZGV4LCAxKTtcblxuXHRyZXR1cm4gdGhpcy5wbHVnaW5zO1xufTtcblxudmFyIGZpbmRQbHVnaW4gPSBleHBvcnRzLmZpbmRQbHVnaW4gPSBmdW5jdGlvbiBmaW5kUGx1Z2luKHBsdWdpbk5hbWUpIHtcblx0cmV0dXJuIHRoaXMucGx1Z2lucy5maW5kKGZ1bmN0aW9uIChwKSB7XG5cdFx0cmV0dXJuIHBsdWdpbk5hbWUgPT09IHAubmFtZTtcblx0fSk7XG59O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3N3dXAvbGliL21vZHVsZXMvcGx1Z2lucy5qc1xuLy8gbW9kdWxlIGlkID0gMzRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX3BsdWdpbiA9IHJlcXVpcmUoJ0Bzd3VwL3BsdWdpbicpO1xuXG52YXIgX3BsdWdpbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wbHVnaW4pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBCb2R5Q2xhc3NQbHVnaW4gPSBmdW5jdGlvbiAoX1BsdWdpbikge1xuXHRfaW5oZXJpdHMoQm9keUNsYXNzUGx1Z2luLCBfUGx1Z2luKTtcblxuXHRmdW5jdGlvbiBCb2R5Q2xhc3NQbHVnaW4ob3B0aW9ucykge1xuXHRcdF9jbGFzc0NhbGxDaGVjayh0aGlzLCBCb2R5Q2xhc3NQbHVnaW4pO1xuXG5cdFx0dmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKEJvZHlDbGFzc1BsdWdpbi5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKEJvZHlDbGFzc1BsdWdpbikpLmNhbGwodGhpcykpO1xuXG5cdFx0X3RoaXMubmFtZSA9ICdCb2R5Q2xhc3NQbHVnaW4nO1xuXG5cblx0XHR2YXIgZGVmYXVsdE9wdGlvbnMgPSB7XG5cdFx0XHRwcmVmaXg6ICcnXG5cdFx0fTtcblxuXHRcdF90aGlzLm9wdGlvbnMgPSBfZXh0ZW5kcyh7fSwgZGVmYXVsdE9wdGlvbnMsIG9wdGlvbnMpO1xuXHRcdHJldHVybiBfdGhpcztcblx0fVxuXG5cdF9jcmVhdGVDbGFzcyhCb2R5Q2xhc3NQbHVnaW4sIFt7XG5cdFx0a2V5OiAnbW91bnQnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBtb3VudCgpIHtcblx0XHRcdHZhciBfdGhpczIgPSB0aGlzO1xuXG5cdFx0XHR0aGlzLnN3dXAub24oJ2NvbnRlbnRSZXBsYWNlZCcsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0dmFyIHBhZ2UgPSBfdGhpczIuc3d1cC5jYWNoZS5nZXRDdXJyZW50UGFnZSgpO1xuXG5cdFx0XHRcdC8vIHJlbW92ZSBvbGQgY2xhc3Nlc1xuXHRcdFx0XHRkb2N1bWVudC5ib2R5LmNsYXNzTmFtZS5zcGxpdCgnICcpLmZvckVhY2goZnVuY3Rpb24gKGNsYXNzTmFtZSkge1xuXHRcdFx0XHRcdGlmIChfdGhpczIuaXNWYWxpZENsYXNzTmFtZShjbGFzc05hbWUpKSB7XG5cdFx0XHRcdFx0XHRkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5yZW1vdmUoY2xhc3NOYW1lKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdC8vIGFkZCBuZXcgY2xhc3Nlc1xuXHRcdFx0XHRpZiAocGFnZS5wYWdlQ2xhc3MgIT09ICcnKSB7XG5cdFx0XHRcdFx0cGFnZS5wYWdlQ2xhc3Muc3BsaXQoJyAnKS5mb3JFYWNoKGZ1bmN0aW9uIChjbGFzc05hbWUpIHtcblx0XHRcdFx0XHRcdGlmIChfdGhpczIuaXNWYWxpZENsYXNzTmFtZShjbGFzc05hbWUpKSB7XG5cdFx0XHRcdFx0XHRcdGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmFkZChjbGFzc05hbWUpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdpc1ZhbGlkQ2xhc3NOYW1lJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gaXNWYWxpZENsYXNzTmFtZShjbGFzc05hbWUpIHtcblx0XHRcdHJldHVybiBjbGFzc05hbWUgIT09ICcnICYmIGNsYXNzTmFtZS5pbmNsdWRlcyh0aGlzLm9wdGlvbnMucHJlZml4KTtcblx0XHR9XG5cdH1dKTtcblxuXHRyZXR1cm4gQm9keUNsYXNzUGx1Z2luO1xufShfcGx1Z2luMi5kZWZhdWx0KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gQm9keUNsYXNzUGx1Z2luO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL0Bzd3VwL2JvZHktY2xhc3MtcGx1Z2luL2xpYi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMzVcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfcGx1Z2luID0gcmVxdWlyZSgnQHN3dXAvcGx1Z2luJyk7XG5cbnZhciBfcGx1Z2luMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3BsdWdpbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIERlYnVnUGx1Z2luID0gZnVuY3Rpb24gKF9QbHVnaW4pIHtcbiAgICBfaW5oZXJpdHMoRGVidWdQbHVnaW4sIF9QbHVnaW4pO1xuXG4gICAgZnVuY3Rpb24gRGVidWdQbHVnaW4oKSB7XG4gICAgICAgIHZhciBfcmVmO1xuXG4gICAgICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIERlYnVnUGx1Z2luKTtcblxuICAgICAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKF9yZWYgPSBEZWJ1Z1BsdWdpbi5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKERlYnVnUGx1Z2luKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMubmFtZSA9IFwiRGVidWdQbHVnaW5cIiwgX3RoaXMudHJpZ2dlckV2ZW50ID0gZnVuY3Rpb24gKGV2ZW50TmFtZSwgb3JpZ2luYWxFdmVudCkge1xuICAgICAgICAgICAgaWYgKG9yaWdpbmFsRXZlbnQpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKCclY3N3dXA6JyArICclYycgKyBldmVudE5hbWUsICdjb2xvcjogIzM0MzQzNCcsICdjb2xvcjogIzAwOUFDRCcpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKG9yaWdpbmFsRXZlbnQpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJyVjc3d1cDonICsgJyVjJyArIGV2ZW50TmFtZSwgJ2NvbG9yOiAjMzQzNDM0JywgJ2NvbG9yOiAjMDA5QUNEJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIF90aGlzLnN3dXAuX3RyaWdnZXJFdmVudChldmVudE5hbWUsIG9yaWdpbmFsRXZlbnQpO1xuICAgICAgICB9LCBfdGhpcy5sb2cgPSBmdW5jdGlvbiAoc3RyLCBvYmplY3QpIHtcbiAgICAgICAgICAgIGlmIChvYmplY3QpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHN0cik7XG4gICAgICAgICAgICAgICAgZm9yICh2YXIga2V5IGluIG9iamVjdCkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhvYmplY3Rba2V5XSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coc3RyICsgJyVjJywgJ2NvbG9yOiAjMDA5QUNEJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIF90aGlzLmRlYnVnTG9nID0gZnVuY3Rpb24gKGxvZywgdHlwZSkge1xuICAgICAgICAgICAgaWYgKHR5cGUgPT09ICdlcnJvcicpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdERUJVRyBQTFVHSU46ICcgKyBsb2cpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ0RFQlVHIFBMVUdJTjogJyArIGxvZyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICAgIH1cblxuICAgIF9jcmVhdGVDbGFzcyhEZWJ1Z1BsdWdpbiwgW3tcbiAgICAgICAga2V5OiAnbW91bnQnLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gbW91bnQoKSB7XG4gICAgICAgICAgICB2YXIgc3d1cCA9IHRoaXMuc3d1cDtcblxuICAgICAgICAgICAgLy8gc2V0IG5vbi1lbXB0eSBsb2cgbWV0aG9kIG9mIHN3dXBcbiAgICAgICAgICAgIHN3dXAubG9nID0gdGhpcy5sb2c7XG5cbiAgICAgICAgICAgIC8vIHNldCBzd3VwIGluc3RhbmNlIGFzIGEgZ2xvYmFsIHZhcmlhYmxlIHN3dXBcbiAgICAgICAgICAgIHdpbmRvdy5zd3VwID0gc3d1cDtcblxuICAgICAgICAgICAgLy8gbWFrZSBldmVudHMgYXBwZWFyIGluIGNvbnNvbGVcbiAgICAgICAgICAgIHN3dXAuX3RyaWdnZXJFdmVudCA9IHN3dXAudHJpZ2dlckV2ZW50O1xuICAgICAgICAgICAgc3d1cC50cmlnZ2VyRXZlbnQgPSB0aGlzLnRyaWdnZXJFdmVudDtcblxuICAgICAgICAgICAgLy8gZGV0ZWN0IHJlbGF0aXZlIGxpbmtzIG5vdCBzdGFydGluZyB3aXRoIC8gb3IgI1xuICAgICAgICAgICAgdmFyIHBvdGVudGlhbGx5V3JvbmdMaW5rc1NlbGVjdG9yID0gJ2FbaHJlZl06bm90KFtocmVmXj1cIicgKyB3aW5kb3cubG9jYXRpb24ub3JpZ2luICsgJ1wiXSk6bm90KFtocmVmXj1cIi9cIl0pOm5vdChbaHJlZl49XCJodHRwXCJdKTpub3QoW2hyZWZePVwiL1wiXSk6bm90KFtocmVmXj1cIj9cIl0pOm5vdChbaHJlZl49XCIjXCJdKSc7XG5cbiAgICAgICAgICAgIHN3dXAub24oJ3BhZ2VWaWV3JywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHBvdGVudGlhbGx5V3JvbmdMaW5rc1NlbGVjdG9yKS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9yID0gJ0l0IHNlZW1zIHRoZXJlIGFyZSBzb21lIGxpbmtzIHdpdGggYSBocmVmIGF0dHJpYnV0ZSBub3Qgc3RhcnRpbmcgd2l0aCBcIiNcIiwgXCIvXCIgb3IgY3VycmVudCBkb21haW4sIHdoaWNoIGlzIHBvdGVudGlhbGx5IGEgcHJvYmxlbS4nO1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ0RFQlVHIFBMVUdJTjogJyArIGVycm9yLCBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHBvdGVudGlhbGx5V3JvbmdMaW5rc1NlbGVjdG9yKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHBvdGVudGlhbGx5V3JvbmdMaW5rc1NlbGVjdG9yKS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIF9lcnJvciA9ICdJdCBzZWVtcyB0aGVyZSBhcmUgc29tZSBsaW5rcyB3aXRoIGEgaHJlZiBhdHRyaWJ1dGUgbm90IHN0YXJ0aW5nIHdpdGggXCIjXCIsIFwiL1wiIG9yIGN1cnJlbnQgZG9tYWluLCB3aGljaCBpcyBwb3RlbnRpYWxseSBhIHByb2JsZW0uJztcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdERUJVRyBQTFVHSU46ICcgKyBfZXJyb3IsIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwocG90ZW50aWFsbHlXcm9uZ0xpbmtzU2VsZWN0b3IpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH0sIHtcbiAgICAgICAga2V5OiAndW5tb3VudCcsXG4gICAgICAgIHZhbHVlOiBmdW5jdGlvbiB1bm1vdW50KCkge1xuICAgICAgICAgICAgdGhpcy5zd3VwLmxvZyA9IGZ1bmN0aW9uICgpIHt9O1xuICAgICAgICAgICAgdGhpcy5zd3VwLnRyaWdnZXJFdmVudCA9IHRoaXMuc3d1cC5fdHJpZ2dlckV2ZW50O1xuICAgICAgICB9XG4gICAgfV0pO1xuXG4gICAgcmV0dXJuIERlYnVnUGx1Z2luO1xufShfcGx1Z2luMi5kZWZhdWx0KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gRGVidWdQbHVnaW47XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvQHN3dXAvZGVidWctcGx1Z2luL2xpYi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMzZcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX3BsdWdpbiA9IHJlcXVpcmUoJ0Bzd3VwL3BsdWdpbicpO1xuXG52YXIgX3BsdWdpbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wbHVnaW4pO1xuXG52YXIgX3BhdGhUb1JlZ2V4cCA9IHJlcXVpcmUoJ3BhdGgtdG8tcmVnZXhwJyk7XG5cbnZhciBfcGF0aFRvUmVnZXhwMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3BhdGhUb1JlZ2V4cCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIEpzUGx1Z2luID0gZnVuY3Rpb24gKF9QbHVnaW4pIHtcblx0X2luaGVyaXRzKEpzUGx1Z2luLCBfUGx1Z2luKTtcblxuXHRmdW5jdGlvbiBKc1BsdWdpbigpIHtcblx0XHR2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge307XG5cblx0XHRfY2xhc3NDYWxsQ2hlY2sodGhpcywgSnNQbHVnaW4pO1xuXG5cdFx0dmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKEpzUGx1Z2luLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoSnNQbHVnaW4pKS5jYWxsKHRoaXMpKTtcblxuXHRcdF90aGlzLm5hbWUgPSAnSnNQbHVnaW4nO1xuXHRcdF90aGlzLmN1cnJlbnRBbmltYXRpb24gPSBudWxsO1xuXG5cdFx0X3RoaXMuZ2V0QW5pbWF0aW9uUHJvbWlzZXMgPSBmdW5jdGlvbiAodHlwZSkge1xuXHRcdFx0dmFyIGFuaW1hdGlvbkluZGV4ID0gX3RoaXMuZ2V0QW5pbWF0aW9uSW5kZXgodHlwZSk7XG5cdFx0XHRyZXR1cm4gW190aGlzLmNyZWF0ZUFuaW1hdGlvblByb21pc2UoYW5pbWF0aW9uSW5kZXgsIHR5cGUpXTtcblx0XHR9O1xuXG5cdFx0X3RoaXMuY3JlYXRlQW5pbWF0aW9uUHJvbWlzZSA9IGZ1bmN0aW9uIChpbmRleCwgdHlwZSkge1xuXHRcdFx0dmFyIGN1cnJlbnRUcmFuc2l0aW9uUm91dGVzID0gX3RoaXMuc3d1cC50cmFuc2l0aW9uO1xuXHRcdFx0dmFyIGFuaW1hdGlvbiA9IF90aGlzLm9wdGlvbnNbaW5kZXhdO1xuXG5cdFx0XHRyZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUpIHtcblx0XHRcdFx0YW5pbWF0aW9uW3R5cGVdKHJlc29sdmUsIHtcblx0XHRcdFx0XHRwYXJhbXNGcm9tOiBhbmltYXRpb24ucmVnRnJvbS5leGVjKGN1cnJlbnRUcmFuc2l0aW9uUm91dGVzLmZyb20pLFxuXHRcdFx0XHRcdHBhcmFtc1RvOiBhbmltYXRpb24ucmVnVG8uZXhlYyhjdXJyZW50VHJhbnNpdGlvblJvdXRlcy50byksXG5cdFx0XHRcdFx0dHJhbnNpdGlvbjogY3VycmVudFRyYW5zaXRpb25Sb3V0ZXMsXG5cdFx0XHRcdFx0ZnJvbTogYW5pbWF0aW9uLmZyb20sXG5cdFx0XHRcdFx0dG86IGFuaW1hdGlvbi50b1xuXHRcdFx0XHR9KTtcblx0XHRcdH0pO1xuXHRcdH07XG5cblx0XHRfdGhpcy5nZXRBbmltYXRpb25JbmRleCA9IGZ1bmN0aW9uICh0eXBlKSB7XG5cdFx0XHQvLyBhbHJlYWR5IHNhdmVkIGZyb20gb3V0IGFuaW1hdGlvblxuXHRcdFx0aWYgKHR5cGUgPT09ICdpbicpIHtcblx0XHRcdFx0cmV0dXJuIF90aGlzLmN1cnJlbnRBbmltYXRpb247XG5cdFx0XHR9XG5cblx0XHRcdHZhciBhbmltYXRpb25zID0gX3RoaXMub3B0aW9ucztcblx0XHRcdHZhciBhbmltYXRpb25JbmRleCA9IDA7XG5cdFx0XHR2YXIgdG9wUmF0aW5nID0gMDtcblxuXHRcdFx0T2JqZWN0LmtleXMoYW5pbWF0aW9ucykuZm9yRWFjaChmdW5jdGlvbiAoa2V5LCBpbmRleCkge1xuXHRcdFx0XHR2YXIgYW5pbWF0aW9uID0gYW5pbWF0aW9uc1trZXldO1xuXHRcdFx0XHR2YXIgcmF0aW5nID0gX3RoaXMucmF0ZUFuaW1hdGlvbihhbmltYXRpb24pO1xuXG5cdFx0XHRcdGlmIChyYXRpbmcgPj0gdG9wUmF0aW5nKSB7XG5cdFx0XHRcdFx0YW5pbWF0aW9uSW5kZXggPSBpbmRleDtcblx0XHRcdFx0XHR0b3BSYXRpbmcgPSByYXRpbmc7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXG5cdFx0XHRfdGhpcy5jdXJyZW50QW5pbWF0aW9uID0gYW5pbWF0aW9uSW5kZXg7XG5cdFx0XHRyZXR1cm4gX3RoaXMuY3VycmVudEFuaW1hdGlvbjtcblx0XHR9O1xuXG5cdFx0X3RoaXMucmF0ZUFuaW1hdGlvbiA9IGZ1bmN0aW9uIChhbmltYXRpb24pIHtcblx0XHRcdHZhciBjdXJyZW50VHJhbnNpdGlvblJvdXRlcyA9IF90aGlzLnN3dXAudHJhbnNpdGlvbjtcblx0XHRcdHZhciByYXRpbmcgPSAwO1xuXG5cdFx0XHQvLyBydW4gcmVnZXhcblx0XHRcdHZhciBmcm9tTWF0Y2hlZCA9IGFuaW1hdGlvbi5yZWdGcm9tLnRlc3QoY3VycmVudFRyYW5zaXRpb25Sb3V0ZXMuZnJvbSk7XG5cdFx0XHR2YXIgdG9NYXRjaGVkID0gYW5pbWF0aW9uLnJlZ1RvLnRlc3QoY3VycmVudFRyYW5zaXRpb25Sb3V0ZXMudG8pO1xuXG5cdFx0XHQvLyBjaGVjayBpZiByZWdleCBwYXNzZXNcblx0XHRcdHJhdGluZyArPSBmcm9tTWF0Y2hlZCA/IDEgOiAwO1xuXHRcdFx0cmF0aW5nICs9IHRvTWF0Y2hlZCA/IDEgOiAwO1xuXG5cdFx0XHQvLyBiZWF0IGFsbCBvdGhlciBpZiBjdXN0b20gcGFyYW1ldGVyIGZpdHNcblx0XHRcdHJhdGluZyArPSBmcm9tTWF0Y2hlZCAmJiBhbmltYXRpb24udG8gPT09IGN1cnJlbnRUcmFuc2l0aW9uUm91dGVzLmN1c3RvbSA/IDIgOiAwO1xuXG5cdFx0XHRyZXR1cm4gcmF0aW5nO1xuXHRcdH07XG5cblx0XHR2YXIgZGVmYXVsdE9wdGlvbnMgPSBbe1xuXHRcdFx0ZnJvbTogJyguKiknLFxuXHRcdFx0dG86ICcoLiopJyxcblx0XHRcdG91dDogZnVuY3Rpb24gb3V0KG5leHQpIHtcblx0XHRcdFx0cmV0dXJuIG5leHQoKTtcblx0XHRcdH0sXG5cdFx0XHRpbjogZnVuY3Rpb24gX2luKG5leHQpIHtcblx0XHRcdFx0cmV0dXJuIG5leHQoKTtcblx0XHRcdH1cblx0XHR9XTtcblxuXHRcdF90aGlzLm9wdGlvbnMgPSBfZXh0ZW5kcyh7fSwgZGVmYXVsdE9wdGlvbnMsIG9wdGlvbnMpO1xuXG5cdFx0X3RoaXMuZ2VuZXJhdGVSZWdleCgpO1xuXHRcdHJldHVybiBfdGhpcztcblx0fVxuXG5cdF9jcmVhdGVDbGFzcyhKc1BsdWdpbiwgW3tcblx0XHRrZXk6ICdtb3VudCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIG1vdW50KCkge1xuXHRcdFx0dmFyIHN3dXAgPSB0aGlzLnN3dXA7XG5cblx0XHRcdHN3dXAuX2dldEFuaW1hdGlvblByb21pc2VzID0gc3d1cC5nZXRBbmltYXRpb25Qcm9taXNlcztcblx0XHRcdHN3dXAuZ2V0QW5pbWF0aW9uUHJvbWlzZXMgPSB0aGlzLmdldEFuaW1hdGlvblByb21pc2VzO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3VubW91bnQnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiB1bm1vdW50KCkge1xuXHRcdFx0c3d1cC5nZXRBbmltYXRpb25Qcm9taXNlcyA9IHN3dXAuX2dldEFuaW1hdGlvblByb21pc2VzO1xuXHRcdFx0c3d1cC5fZ2V0QW5pbWF0aW9uUHJvbWlzZXMgPSBudWxsO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2dlbmVyYXRlUmVnZXgnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBnZW5lcmF0ZVJlZ2V4KCkge1xuXHRcdFx0dmFyIF90aGlzMiA9IHRoaXM7XG5cblx0XHRcdHZhciBpc1JlZ2V4ID0gZnVuY3Rpb24gaXNSZWdleChzdHIpIHtcblx0XHRcdFx0cmV0dXJuIHN0ciBpbnN0YW5jZW9mIFJlZ0V4cDtcblx0XHRcdH07XG5cblx0XHRcdHRoaXMub3B0aW9ucyA9IE9iamVjdC5rZXlzKHRoaXMub3B0aW9ucykubWFwKGZ1bmN0aW9uIChrZXkpIHtcblx0XHRcdFx0cmV0dXJuIF9leHRlbmRzKHt9LCBfdGhpczIub3B0aW9uc1trZXldLCB7XG5cdFx0XHRcdFx0cmVnRnJvbTogaXNSZWdleChfdGhpczIub3B0aW9uc1trZXldLmZyb20pID8gX3RoaXMyLm9wdGlvbnNba2V5XS5mcm9tIDogKDAsIF9wYXRoVG9SZWdleHAyLmRlZmF1bHQpKF90aGlzMi5vcHRpb25zW2tleV0uZnJvbSksXG5cdFx0XHRcdFx0cmVnVG86IGlzUmVnZXgoX3RoaXMyLm9wdGlvbnNba2V5XS50bykgPyBfdGhpczIub3B0aW9uc1trZXldLnRvIDogKDAsIF9wYXRoVG9SZWdleHAyLmRlZmF1bHQpKF90aGlzMi5vcHRpb25zW2tleV0udG8pXG5cdFx0XHRcdH0pO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHR9XSk7XG5cblx0cmV0dXJuIEpzUGx1Z2luO1xufShfcGx1Z2luMi5kZWZhdWx0KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gSnNQbHVnaW47XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvQHN3dXAvanMtcGx1Z2luL2xpYi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gMzdcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLyoqXG4gKiBFeHBvc2UgYHBhdGhUb1JlZ2V4cGAuXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gcGF0aFRvUmVnZXhwXG5tb2R1bGUuZXhwb3J0cy5wYXJzZSA9IHBhcnNlXG5tb2R1bGUuZXhwb3J0cy5jb21waWxlID0gY29tcGlsZVxubW9kdWxlLmV4cG9ydHMudG9rZW5zVG9GdW5jdGlvbiA9IHRva2Vuc1RvRnVuY3Rpb25cbm1vZHVsZS5leHBvcnRzLnRva2Vuc1RvUmVnRXhwID0gdG9rZW5zVG9SZWdFeHBcblxuLyoqXG4gKiBEZWZhdWx0IGNvbmZpZ3MuXG4gKi9cbnZhciBERUZBVUxUX0RFTElNSVRFUiA9ICcvJ1xuXG4vKipcbiAqIFRoZSBtYWluIHBhdGggbWF0Y2hpbmcgcmVnZXhwIHV0aWxpdHkuXG4gKlxuICogQHR5cGUge1JlZ0V4cH1cbiAqL1xudmFyIFBBVEhfUkVHRVhQID0gbmV3IFJlZ0V4cChbXG4gIC8vIE1hdGNoIGVzY2FwZWQgY2hhcmFjdGVycyB0aGF0IHdvdWxkIG90aGVyd2lzZSBhcHBlYXIgaW4gZnV0dXJlIG1hdGNoZXMuXG4gIC8vIFRoaXMgYWxsb3dzIHRoZSB1c2VyIHRvIGVzY2FwZSBzcGVjaWFsIGNoYXJhY3RlcnMgdGhhdCB3b24ndCB0cmFuc2Zvcm0uXG4gICcoXFxcXFxcXFwuKScsXG4gIC8vIE1hdGNoIEV4cHJlc3Mtc3R5bGUgcGFyYW1ldGVycyBhbmQgdW4tbmFtZWQgcGFyYW1ldGVycyB3aXRoIGEgcHJlZml4XG4gIC8vIGFuZCBvcHRpb25hbCBzdWZmaXhlcy4gTWF0Y2hlcyBhcHBlYXIgYXM6XG4gIC8vXG4gIC8vIFwiOnRlc3QoXFxcXGQrKT9cIiA9PiBbXCJ0ZXN0XCIsIFwiXFxkK1wiLCB1bmRlZmluZWQsIFwiP1wiXVxuICAvLyBcIihcXFxcZCspXCIgID0+IFt1bmRlZmluZWQsIHVuZGVmaW5lZCwgXCJcXGQrXCIsIHVuZGVmaW5lZF1cbiAgJyg/OlxcXFw6KFxcXFx3KykoPzpcXFxcKCgoPzpcXFxcXFxcXC58W15cXFxcXFxcXCgpXSkrKVxcXFwpKT98XFxcXCgoKD86XFxcXFxcXFwufFteXFxcXFxcXFwoKV0pKylcXFxcKSkoWysqP10pPydcbl0uam9pbignfCcpLCAnZycpXG5cbi8qKlxuICogUGFyc2UgYSBzdHJpbmcgZm9yIHRoZSByYXcgdG9rZW5zLlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gIHN0clxuICogQHBhcmFtICB7T2JqZWN0PX0gb3B0aW9uc1xuICogQHJldHVybiB7IUFycmF5fVxuICovXG5mdW5jdGlvbiBwYXJzZSAoc3RyLCBvcHRpb25zKSB7XG4gIHZhciB0b2tlbnMgPSBbXVxuICB2YXIga2V5ID0gMFxuICB2YXIgaW5kZXggPSAwXG4gIHZhciBwYXRoID0gJydcbiAgdmFyIGRlZmF1bHREZWxpbWl0ZXIgPSAob3B0aW9ucyAmJiBvcHRpb25zLmRlbGltaXRlcikgfHwgREVGQVVMVF9ERUxJTUlURVJcbiAgdmFyIHdoaXRlbGlzdCA9IChvcHRpb25zICYmIG9wdGlvbnMud2hpdGVsaXN0KSB8fCB1bmRlZmluZWRcbiAgdmFyIHBhdGhFc2NhcGVkID0gZmFsc2VcbiAgdmFyIHJlc1xuXG4gIHdoaWxlICgocmVzID0gUEFUSF9SRUdFWFAuZXhlYyhzdHIpKSAhPT0gbnVsbCkge1xuICAgIHZhciBtID0gcmVzWzBdXG4gICAgdmFyIGVzY2FwZWQgPSByZXNbMV1cbiAgICB2YXIgb2Zmc2V0ID0gcmVzLmluZGV4XG4gICAgcGF0aCArPSBzdHIuc2xpY2UoaW5kZXgsIG9mZnNldClcbiAgICBpbmRleCA9IG9mZnNldCArIG0ubGVuZ3RoXG5cbiAgICAvLyBJZ25vcmUgYWxyZWFkeSBlc2NhcGVkIHNlcXVlbmNlcy5cbiAgICBpZiAoZXNjYXBlZCkge1xuICAgICAgcGF0aCArPSBlc2NhcGVkWzFdXG4gICAgICBwYXRoRXNjYXBlZCA9IHRydWVcbiAgICAgIGNvbnRpbnVlXG4gICAgfVxuXG4gICAgdmFyIHByZXYgPSAnJ1xuICAgIHZhciBuYW1lID0gcmVzWzJdXG4gICAgdmFyIGNhcHR1cmUgPSByZXNbM11cbiAgICB2YXIgZ3JvdXAgPSByZXNbNF1cbiAgICB2YXIgbW9kaWZpZXIgPSByZXNbNV1cblxuICAgIGlmICghcGF0aEVzY2FwZWQgJiYgcGF0aC5sZW5ndGgpIHtcbiAgICAgIHZhciBrID0gcGF0aC5sZW5ndGggLSAxXG4gICAgICB2YXIgYyA9IHBhdGhba11cbiAgICAgIHZhciBtYXRjaGVzID0gd2hpdGVsaXN0ID8gd2hpdGVsaXN0LmluZGV4T2YoYykgPiAtMSA6IHRydWVcblxuICAgICAgaWYgKG1hdGNoZXMpIHtcbiAgICAgICAgcHJldiA9IGNcbiAgICAgICAgcGF0aCA9IHBhdGguc2xpY2UoMCwgaylcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBQdXNoIHRoZSBjdXJyZW50IHBhdGggb250byB0aGUgdG9rZW5zLlxuICAgIGlmIChwYXRoKSB7XG4gICAgICB0b2tlbnMucHVzaChwYXRoKVxuICAgICAgcGF0aCA9ICcnXG4gICAgICBwYXRoRXNjYXBlZCA9IGZhbHNlXG4gICAgfVxuXG4gICAgdmFyIHJlcGVhdCA9IG1vZGlmaWVyID09PSAnKycgfHwgbW9kaWZpZXIgPT09ICcqJ1xuICAgIHZhciBvcHRpb25hbCA9IG1vZGlmaWVyID09PSAnPycgfHwgbW9kaWZpZXIgPT09ICcqJ1xuICAgIHZhciBwYXR0ZXJuID0gY2FwdHVyZSB8fCBncm91cFxuICAgIHZhciBkZWxpbWl0ZXIgPSBwcmV2IHx8IGRlZmF1bHREZWxpbWl0ZXJcblxuICAgIHRva2Vucy5wdXNoKHtcbiAgICAgIG5hbWU6IG5hbWUgfHwga2V5KyssXG4gICAgICBwcmVmaXg6IHByZXYsXG4gICAgICBkZWxpbWl0ZXI6IGRlbGltaXRlcixcbiAgICAgIG9wdGlvbmFsOiBvcHRpb25hbCxcbiAgICAgIHJlcGVhdDogcmVwZWF0LFxuICAgICAgcGF0dGVybjogcGF0dGVyblxuICAgICAgICA/IGVzY2FwZUdyb3VwKHBhdHRlcm4pXG4gICAgICAgIDogJ1teJyArIGVzY2FwZVN0cmluZyhkZWxpbWl0ZXIgPT09IGRlZmF1bHREZWxpbWl0ZXIgPyBkZWxpbWl0ZXIgOiAoZGVsaW1pdGVyICsgZGVmYXVsdERlbGltaXRlcikpICsgJ10rPydcbiAgICB9KVxuICB9XG5cbiAgLy8gUHVzaCBhbnkgcmVtYWluaW5nIGNoYXJhY3RlcnMuXG4gIGlmIChwYXRoIHx8IGluZGV4IDwgc3RyLmxlbmd0aCkge1xuICAgIHRva2Vucy5wdXNoKHBhdGggKyBzdHIuc3Vic3RyKGluZGV4KSlcbiAgfVxuXG4gIHJldHVybiB0b2tlbnNcbn1cblxuLyoqXG4gKiBDb21waWxlIGEgc3RyaW5nIHRvIGEgdGVtcGxhdGUgZnVuY3Rpb24gZm9yIHRoZSBwYXRoLlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gICAgICAgICAgICAgc3RyXG4gKiBAcGFyYW0gIHtPYmplY3Q9fSAgICAgICAgICAgIG9wdGlvbnNcbiAqIEByZXR1cm4geyFmdW5jdGlvbihPYmplY3Q9LCBPYmplY3Q9KX1cbiAqL1xuZnVuY3Rpb24gY29tcGlsZSAoc3RyLCBvcHRpb25zKSB7XG4gIHJldHVybiB0b2tlbnNUb0Z1bmN0aW9uKHBhcnNlKHN0ciwgb3B0aW9ucykpXG59XG5cbi8qKlxuICogRXhwb3NlIGEgbWV0aG9kIGZvciB0cmFuc2Zvcm1pbmcgdG9rZW5zIGludG8gdGhlIHBhdGggZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIHRva2Vuc1RvRnVuY3Rpb24gKHRva2Vucykge1xuICAvLyBDb21waWxlIGFsbCB0aGUgdG9rZW5zIGludG8gcmVnZXhwcy5cbiAgdmFyIG1hdGNoZXMgPSBuZXcgQXJyYXkodG9rZW5zLmxlbmd0aClcblxuICAvLyBDb21waWxlIGFsbCB0aGUgcGF0dGVybnMgYmVmb3JlIGNvbXBpbGF0aW9uLlxuICBmb3IgKHZhciBpID0gMDsgaSA8IHRva2Vucy5sZW5ndGg7IGkrKykge1xuICAgIGlmICh0eXBlb2YgdG9rZW5zW2ldID09PSAnb2JqZWN0Jykge1xuICAgICAgbWF0Y2hlc1tpXSA9IG5ldyBSZWdFeHAoJ14oPzonICsgdG9rZW5zW2ldLnBhdHRlcm4gKyAnKSQnKVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAoZGF0YSwgb3B0aW9ucykge1xuICAgIHZhciBwYXRoID0gJydcbiAgICB2YXIgZW5jb2RlID0gKG9wdGlvbnMgJiYgb3B0aW9ucy5lbmNvZGUpIHx8IGVuY29kZVVSSUNvbXBvbmVudFxuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0b2tlbnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciB0b2tlbiA9IHRva2Vuc1tpXVxuXG4gICAgICBpZiAodHlwZW9mIHRva2VuID09PSAnc3RyaW5nJykge1xuICAgICAgICBwYXRoICs9IHRva2VuXG4gICAgICAgIGNvbnRpbnVlXG4gICAgICB9XG5cbiAgICAgIHZhciB2YWx1ZSA9IGRhdGEgPyBkYXRhW3Rva2VuLm5hbWVdIDogdW5kZWZpbmVkXG4gICAgICB2YXIgc2VnbWVudFxuXG4gICAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcbiAgICAgICAgaWYgKCF0b2tlbi5yZXBlYXQpIHtcbiAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdFeHBlY3RlZCBcIicgKyB0b2tlbi5uYW1lICsgJ1wiIHRvIG5vdCByZXBlYXQsIGJ1dCBnb3QgYXJyYXknKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHZhbHVlLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgIGlmICh0b2tlbi5vcHRpb25hbCkgY29udGludWVcblxuICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIFwiJyArIHRva2VuLm5hbWUgKyAnXCIgdG8gbm90IGJlIGVtcHR5JylcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgdmFsdWUubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBzZWdtZW50ID0gZW5jb2RlKHZhbHVlW2pdLCB0b2tlbilcblxuICAgICAgICAgIGlmICghbWF0Y2hlc1tpXS50ZXN0KHNlZ21lbnQpKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdFeHBlY3RlZCBhbGwgXCInICsgdG9rZW4ubmFtZSArICdcIiB0byBtYXRjaCBcIicgKyB0b2tlbi5wYXR0ZXJuICsgJ1wiJylcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBwYXRoICs9IChqID09PSAwID8gdG9rZW4ucHJlZml4IDogdG9rZW4uZGVsaW1pdGVyKSArIHNlZ21lbnRcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnRpbnVlXG4gICAgICB9XG5cbiAgICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnIHx8IHR5cGVvZiB2YWx1ZSA9PT0gJ251bWJlcicgfHwgdHlwZW9mIHZhbHVlID09PSAnYm9vbGVhbicpIHtcbiAgICAgICAgc2VnbWVudCA9IGVuY29kZShTdHJpbmcodmFsdWUpLCB0b2tlbilcblxuICAgICAgICBpZiAoIW1hdGNoZXNbaV0udGVzdChzZWdtZW50KSkge1xuICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIFwiJyArIHRva2VuLm5hbWUgKyAnXCIgdG8gbWF0Y2ggXCInICsgdG9rZW4ucGF0dGVybiArICdcIiwgYnV0IGdvdCBcIicgKyBzZWdtZW50ICsgJ1wiJylcbiAgICAgICAgfVxuXG4gICAgICAgIHBhdGggKz0gdG9rZW4ucHJlZml4ICsgc2VnbWVudFxuICAgICAgICBjb250aW51ZVxuICAgICAgfVxuXG4gICAgICBpZiAodG9rZW4ub3B0aW9uYWwpIGNvbnRpbnVlXG5cbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIFwiJyArIHRva2VuLm5hbWUgKyAnXCIgdG8gYmUgJyArICh0b2tlbi5yZXBlYXQgPyAnYW4gYXJyYXknIDogJ2Egc3RyaW5nJykpXG4gICAgfVxuXG4gICAgcmV0dXJuIHBhdGhcbiAgfVxufVxuXG4vKipcbiAqIEVzY2FwZSBhIHJlZ3VsYXIgZXhwcmVzc2lvbiBzdHJpbmcuXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfSBzdHJcbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqL1xuZnVuY3Rpb24gZXNjYXBlU3RyaW5nIChzdHIpIHtcbiAgcmV0dXJuIHN0ci5yZXBsYWNlKC8oWy4rKj89XiE6JHt9KClbXFxdfC9cXFxcXSkvZywgJ1xcXFwkMScpXG59XG5cbi8qKlxuICogRXNjYXBlIHRoZSBjYXB0dXJpbmcgZ3JvdXAgYnkgZXNjYXBpbmcgc3BlY2lhbCBjaGFyYWN0ZXJzIGFuZCBtZWFuaW5nLlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gZ3JvdXBcbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqL1xuZnVuY3Rpb24gZXNjYXBlR3JvdXAgKGdyb3VwKSB7XG4gIHJldHVybiBncm91cC5yZXBsYWNlKC8oWz0hOiQvKCldKS9nLCAnXFxcXCQxJylcbn1cblxuLyoqXG4gKiBHZXQgdGhlIGZsYWdzIGZvciBhIHJlZ2V4cCBmcm9tIHRoZSBvcHRpb25zLlxuICpcbiAqIEBwYXJhbSAge09iamVjdH0gb3B0aW9uc1xuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5mdW5jdGlvbiBmbGFncyAob3B0aW9ucykge1xuICByZXR1cm4gb3B0aW9ucyAmJiBvcHRpb25zLnNlbnNpdGl2ZSA/ICcnIDogJ2knXG59XG5cbi8qKlxuICogUHVsbCBvdXQga2V5cyBmcm9tIGEgcmVnZXhwLlxuICpcbiAqIEBwYXJhbSAgeyFSZWdFeHB9IHBhdGhcbiAqIEBwYXJhbSAge0FycmF5PX0gIGtleXNcbiAqIEByZXR1cm4geyFSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIHJlZ2V4cFRvUmVnZXhwIChwYXRoLCBrZXlzKSB7XG4gIGlmICgha2V5cykgcmV0dXJuIHBhdGhcblxuICAvLyBVc2UgYSBuZWdhdGl2ZSBsb29rYWhlYWQgdG8gbWF0Y2ggb25seSBjYXB0dXJpbmcgZ3JvdXBzLlxuICB2YXIgZ3JvdXBzID0gcGF0aC5zb3VyY2UubWF0Y2goL1xcKCg/IVxcPykvZylcblxuICBpZiAoZ3JvdXBzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBncm91cHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGtleXMucHVzaCh7XG4gICAgICAgIG5hbWU6IGksXG4gICAgICAgIHByZWZpeDogbnVsbCxcbiAgICAgICAgZGVsaW1pdGVyOiBudWxsLFxuICAgICAgICBvcHRpb25hbDogZmFsc2UsXG4gICAgICAgIHJlcGVhdDogZmFsc2UsXG4gICAgICAgIHBhdHRlcm46IG51bGxcbiAgICAgIH0pXG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHBhdGhcbn1cblxuLyoqXG4gKiBUcmFuc2Zvcm0gYW4gYXJyYXkgaW50byBhIHJlZ2V4cC5cbiAqXG4gKiBAcGFyYW0gIHshQXJyYXl9ICBwYXRoXG4gKiBAcGFyYW0gIHtBcnJheT19ICBrZXlzXG4gKiBAcGFyYW0gIHtPYmplY3Q9fSBvcHRpb25zXG4gKiBAcmV0dXJuIHshUmVnRXhwfVxuICovXG5mdW5jdGlvbiBhcnJheVRvUmVnZXhwIChwYXRoLCBrZXlzLCBvcHRpb25zKSB7XG4gIHZhciBwYXJ0cyA9IFtdXG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXRoLmxlbmd0aDsgaSsrKSB7XG4gICAgcGFydHMucHVzaChwYXRoVG9SZWdleHAocGF0aFtpXSwga2V5cywgb3B0aW9ucykuc291cmNlKVxuICB9XG5cbiAgcmV0dXJuIG5ldyBSZWdFeHAoJyg/OicgKyBwYXJ0cy5qb2luKCd8JykgKyAnKScsIGZsYWdzKG9wdGlvbnMpKVxufVxuXG4vKipcbiAqIENyZWF0ZSBhIHBhdGggcmVnZXhwIGZyb20gc3RyaW5nIGlucHV0LlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gIHBhdGhcbiAqIEBwYXJhbSAge0FycmF5PX0gIGtleXNcbiAqIEBwYXJhbSAge09iamVjdD19IG9wdGlvbnNcbiAqIEByZXR1cm4geyFSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIHN0cmluZ1RvUmVnZXhwIChwYXRoLCBrZXlzLCBvcHRpb25zKSB7XG4gIHJldHVybiB0b2tlbnNUb1JlZ0V4cChwYXJzZShwYXRoLCBvcHRpb25zKSwga2V5cywgb3B0aW9ucylcbn1cblxuLyoqXG4gKiBFeHBvc2UgYSBmdW5jdGlvbiBmb3IgdGFraW5nIHRva2VucyBhbmQgcmV0dXJuaW5nIGEgUmVnRXhwLlxuICpcbiAqIEBwYXJhbSAgeyFBcnJheX0gIHRva2Vuc1xuICogQHBhcmFtICB7QXJyYXk9fSAga2V5c1xuICogQHBhcmFtICB7T2JqZWN0PX0gb3B0aW9uc1xuICogQHJldHVybiB7IVJlZ0V4cH1cbiAqL1xuZnVuY3Rpb24gdG9rZW5zVG9SZWdFeHAgKHRva2Vucywga2V5cywgb3B0aW9ucykge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fVxuXG4gIHZhciBzdHJpY3QgPSBvcHRpb25zLnN0cmljdFxuICB2YXIgc3RhcnQgPSBvcHRpb25zLnN0YXJ0ICE9PSBmYWxzZVxuICB2YXIgZW5kID0gb3B0aW9ucy5lbmQgIT09IGZhbHNlXG4gIHZhciBkZWxpbWl0ZXIgPSBvcHRpb25zLmRlbGltaXRlciB8fCBERUZBVUxUX0RFTElNSVRFUlxuICB2YXIgZW5kc1dpdGggPSBbXS5jb25jYXQob3B0aW9ucy5lbmRzV2l0aCB8fCBbXSkubWFwKGVzY2FwZVN0cmluZykuY29uY2F0KCckJykuam9pbignfCcpXG4gIHZhciByb3V0ZSA9IHN0YXJ0ID8gJ14nIDogJydcblxuICAvLyBJdGVyYXRlIG92ZXIgdGhlIHRva2VucyBhbmQgY3JlYXRlIG91ciByZWdleHAgc3RyaW5nLlxuICBmb3IgKHZhciBpID0gMDsgaSA8IHRva2Vucy5sZW5ndGg7IGkrKykge1xuICAgIHZhciB0b2tlbiA9IHRva2Vuc1tpXVxuXG4gICAgaWYgKHR5cGVvZiB0b2tlbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHJvdXRlICs9IGVzY2FwZVN0cmluZyh0b2tlbilcbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIGNhcHR1cmUgPSB0b2tlbi5yZXBlYXRcbiAgICAgICAgPyAnKD86JyArIHRva2VuLnBhdHRlcm4gKyAnKSg/OicgKyBlc2NhcGVTdHJpbmcodG9rZW4uZGVsaW1pdGVyKSArICcoPzonICsgdG9rZW4ucGF0dGVybiArICcpKSonXG4gICAgICAgIDogdG9rZW4ucGF0dGVyblxuXG4gICAgICBpZiAoa2V5cykga2V5cy5wdXNoKHRva2VuKVxuXG4gICAgICBpZiAodG9rZW4ub3B0aW9uYWwpIHtcbiAgICAgICAgaWYgKCF0b2tlbi5wcmVmaXgpIHtcbiAgICAgICAgICByb3V0ZSArPSAnKCcgKyBjYXB0dXJlICsgJyk/J1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJvdXRlICs9ICcoPzonICsgZXNjYXBlU3RyaW5nKHRva2VuLnByZWZpeCkgKyAnKCcgKyBjYXB0dXJlICsgJykpPydcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcm91dGUgKz0gZXNjYXBlU3RyaW5nKHRva2VuLnByZWZpeCkgKyAnKCcgKyBjYXB0dXJlICsgJyknXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKGVuZCkge1xuICAgIGlmICghc3RyaWN0KSByb3V0ZSArPSAnKD86JyArIGVzY2FwZVN0cmluZyhkZWxpbWl0ZXIpICsgJyk/J1xuXG4gICAgcm91dGUgKz0gZW5kc1dpdGggPT09ICckJyA/ICckJyA6ICcoPz0nICsgZW5kc1dpdGggKyAnKSdcbiAgfSBlbHNlIHtcbiAgICB2YXIgZW5kVG9rZW4gPSB0b2tlbnNbdG9rZW5zLmxlbmd0aCAtIDFdXG4gICAgdmFyIGlzRW5kRGVsaW1pdGVkID0gdHlwZW9mIGVuZFRva2VuID09PSAnc3RyaW5nJ1xuICAgICAgPyBlbmRUb2tlbltlbmRUb2tlbi5sZW5ndGggLSAxXSA9PT0gZGVsaW1pdGVyXG4gICAgICA6IGVuZFRva2VuID09PSB1bmRlZmluZWRcblxuICAgIGlmICghc3RyaWN0KSByb3V0ZSArPSAnKD86JyArIGVzY2FwZVN0cmluZyhkZWxpbWl0ZXIpICsgJyg/PScgKyBlbmRzV2l0aCArICcpKT8nXG4gICAgaWYgKCFpc0VuZERlbGltaXRlZCkgcm91dGUgKz0gJyg/PScgKyBlc2NhcGVTdHJpbmcoZGVsaW1pdGVyKSArICd8JyArIGVuZHNXaXRoICsgJyknXG4gIH1cblxuICByZXR1cm4gbmV3IFJlZ0V4cChyb3V0ZSwgZmxhZ3Mob3B0aW9ucykpXG59XG5cbi8qKlxuICogTm9ybWFsaXplIHRoZSBnaXZlbiBwYXRoIHN0cmluZywgcmV0dXJuaW5nIGEgcmVndWxhciBleHByZXNzaW9uLlxuICpcbiAqIEFuIGVtcHR5IGFycmF5IGNhbiBiZSBwYXNzZWQgaW4gZm9yIHRoZSBrZXlzLCB3aGljaCB3aWxsIGhvbGQgdGhlXG4gKiBwbGFjZWhvbGRlciBrZXkgZGVzY3JpcHRpb25zLiBGb3IgZXhhbXBsZSwgdXNpbmcgYC91c2VyLzppZGAsIGBrZXlzYCB3aWxsXG4gKiBjb250YWluIGBbeyBuYW1lOiAnaWQnLCBkZWxpbWl0ZXI6ICcvJywgb3B0aW9uYWw6IGZhbHNlLCByZXBlYXQ6IGZhbHNlIH1dYC5cbiAqXG4gKiBAcGFyYW0gIHsoc3RyaW5nfFJlZ0V4cHxBcnJheSl9IHBhdGhcbiAqIEBwYXJhbSAge0FycmF5PX0gICAgICAgICAgICAgICAga2V5c1xuICogQHBhcmFtICB7T2JqZWN0PX0gICAgICAgICAgICAgICBvcHRpb25zXG4gKiBAcmV0dXJuIHshUmVnRXhwfVxuICovXG5mdW5jdGlvbiBwYXRoVG9SZWdleHAgKHBhdGgsIGtleXMsIG9wdGlvbnMpIHtcbiAgaWYgKHBhdGggaW5zdGFuY2VvZiBSZWdFeHApIHtcbiAgICByZXR1cm4gcmVnZXhwVG9SZWdleHAocGF0aCwga2V5cylcbiAgfVxuXG4gIGlmIChBcnJheS5pc0FycmF5KHBhdGgpKSB7XG4gICAgcmV0dXJuIGFycmF5VG9SZWdleHAoLyoqIEB0eXBlIHshQXJyYXl9ICovIChwYXRoKSwga2V5cywgb3B0aW9ucylcbiAgfVxuXG4gIHJldHVybiBzdHJpbmdUb1JlZ2V4cCgvKiogQHR5cGUge3N0cmluZ30gKi8gKHBhdGgpLCBrZXlzLCBvcHRpb25zKVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcGF0aC10by1yZWdleHAvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDM4XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gICAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX3BsdWdpbiA9IHJlcXVpcmUoJ0Bzd3VwL3BsdWdpbicpO1xuXG52YXIgX3BsdWdpbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wbHVnaW4pO1xuXG52YXIgX2RlbGVnYXRlID0gcmVxdWlyZSgnZGVsZWdhdGUnKTtcblxudmFyIF9kZWxlZ2F0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWxlZ2F0ZSk7XG5cbnZhciBfdXRpbHMgPSByZXF1aXJlKCdzd3VwL2xpYi91dGlscycpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCdzd3VwL2xpYi9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIFByZWxvYWRQbHVnaW4gPSBmdW5jdGlvbiAoX1BsdWdpbikge1xuICAgIF9pbmhlcml0cyhQcmVsb2FkUGx1Z2luLCBfUGx1Z2luKTtcblxuICAgIGZ1bmN0aW9uIFByZWxvYWRQbHVnaW4oKSB7XG4gICAgICAgIHZhciBfcmVmO1xuXG4gICAgICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFByZWxvYWRQbHVnaW4pO1xuXG4gICAgICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICAgICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoX3JlZiA9IFByZWxvYWRQbHVnaW4uX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihQcmVsb2FkUGx1Z2luKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMubmFtZSA9IFwiUHJlbG9hZFBsdWdpblwiLCBfdGhpcy5vbkNvbnRlbnRSZXBsYWNlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIF90aGlzLnN3dXAucHJlbG9hZFBhZ2VzKCk7XG4gICAgICAgIH0sIF90aGlzLm9uTW91c2VvdmVyID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICB2YXIgc3d1cCA9IF90aGlzLnN3dXA7XG5cbiAgICAgICAgICAgIHN3dXAudHJpZ2dlckV2ZW50KCdob3ZlckxpbmsnLCBldmVudCk7XG5cbiAgICAgICAgICAgIHZhciBsaW5rID0gbmV3IF9oZWxwZXJzLkxpbmsoZXZlbnQuZGVsZWdhdGVUYXJnZXQpO1xuICAgICAgICAgICAgaWYgKGxpbmsuZ2V0QWRkcmVzcygpICE9PSAoMCwgX2hlbHBlcnMuZ2V0Q3VycmVudFVybCkoKSAmJiAhc3d1cC5jYWNoZS5leGlzdHMobGluay5nZXRBZGRyZXNzKCkpICYmIHN3dXAucHJlbG9hZFByb21pc2UgPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIHN3dXAucHJlbG9hZFByb21pc2UgPSBzd3VwLnByZWxvYWRQYWdlKGxpbmsuZ2V0QWRkcmVzcygpKTtcbiAgICAgICAgICAgICAgICBzd3VwLnByZWxvYWRQcm9taXNlLnJvdXRlID0gbGluay5nZXRBZGRyZXNzKCk7XG4gICAgICAgICAgICAgICAgc3d1cC5wcmVsb2FkUHJvbWlzZS5maW5hbGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgc3d1cC5wcmVsb2FkUHJvbWlzZSA9IG51bGw7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIF90aGlzLnByZWxvYWRQYWdlID0gZnVuY3Rpb24gKHBhdGhuYW1lKSB7XG4gICAgICAgICAgICB2YXIgc3d1cCA9IF90aGlzLnN3dXA7XG5cbiAgICAgICAgICAgIHZhciBsaW5rID0gbmV3IF9oZWxwZXJzLkxpbmsocGF0aG5hbWUpO1xuICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgICAgICAgICBpZiAobGluay5nZXRBZGRyZXNzKCkgIT0gKDAsIF9oZWxwZXJzLmdldEN1cnJlbnRVcmwpKCkgJiYgIXN3dXAuY2FjaGUuZXhpc3RzKGxpbmsuZ2V0QWRkcmVzcygpKSkge1xuICAgICAgICAgICAgICAgICAgICAoMCwgX2hlbHBlcnMuZmV0Y2gpKHsgdXJsOiBsaW5rLmdldEFkZHJlc3MoKSwgaGVhZGVyczogc3d1cC5vcHRpb25zLnJlcXVlc3RIZWFkZXJzIH0sIGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gNTAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3d1cC50cmlnZ2VyRXZlbnQoJ3NlcnZlckVycm9yJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGdldCBqc29uIGRhdGFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcGFnZSA9IHN3dXAuZ2V0UGFnZURhdGEocmVzcG9uc2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwYWdlICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnZS51cmwgPSBsaW5rLmdldEFkZHJlc3MoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3d1cC5jYWNoZS5jYWNoZVVybChwYWdlLCBzd3VwLm9wdGlvbnMuZGVidWdNb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3d1cC50cmlnZ2VyRXZlbnQoJ3BhZ2VQcmVsb2FkZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWplY3QobGluay5nZXRBZGRyZXNzKCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoc3d1cC5jYWNoZS5nZXRQYWdlKGxpbmsuZ2V0QWRkcmVzcygpKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoc3d1cC5jYWNoZS5nZXRQYWdlKGxpbmsuZ2V0QWRkcmVzcygpKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sIF90aGlzLnByZWxvYWRQYWdlcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICgwLCBfdXRpbHMucXVlcnlBbGwpKCdbZGF0YS1zd3VwLXByZWxvYWRdJykuZm9yRWFjaChmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIF90aGlzLnN3dXAucHJlbG9hZFBhZ2UoZWxlbWVudC5ocmVmKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9LCBfdGVtcCksIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgICB9XG5cbiAgICBfY3JlYXRlQ2xhc3MoUHJlbG9hZFBsdWdpbiwgW3tcbiAgICAgICAga2V5OiAnbW91bnQnLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gbW91bnQoKSB7XG4gICAgICAgICAgICB2YXIgc3d1cCA9IHRoaXMuc3d1cDtcblxuICAgICAgICAgICAgc3d1cC5faGFuZGxlcnMucGFnZVByZWxvYWRlZCA9IFtdO1xuICAgICAgICAgICAgc3d1cC5faGFuZGxlcnMuaG92ZXJMaW5rID0gW107XG5cbiAgICAgICAgICAgIHN3dXAucHJlbG9hZFBhZ2UgPSB0aGlzLnByZWxvYWRQYWdlO1xuICAgICAgICAgICAgc3d1cC5wcmVsb2FkUGFnZXMgPSB0aGlzLnByZWxvYWRQYWdlcztcblxuICAgICAgICAgICAgLy8gcmVnaXN0ZXIgbW91c2VvdmVyIGhhbmRsZXJcbiAgICAgICAgICAgIHN3dXAuZGVsZWdhdGVkTGlzdGVuZXJzLm1vdXNlb3ZlciA9ICgwLCBfZGVsZWdhdGUyLmRlZmF1bHQpKGRvY3VtZW50LmJvZHksIHN3dXAub3B0aW9ucy5saW5rU2VsZWN0b3IsICdtb3VzZW92ZXInLCB0aGlzLm9uTW91c2VvdmVyLmJpbmQodGhpcykpO1xuXG4gICAgICAgICAgICAvLyBpbml0aWFsIHByZWxvYWQgb2YgcGFnZSBmb3JtIGxpbmtzIHdpdGggW2RhdGEtc3d1cC1wcmVsb2FkXVxuICAgICAgICAgICAgc3d1cC5wcmVsb2FkUGFnZXMoKTtcblxuICAgICAgICAgICAgLy8gZG8gdGhlIHNhbWUgb24gZXZlcnkgY29udGVudCByZXBsYWNlXG4gICAgICAgICAgICBzd3VwLm9uKCdjb250ZW50UmVwbGFjZWQnLCB0aGlzLm9uQ29udGVudFJlcGxhY2VkKTtcbiAgICAgICAgfVxuICAgIH0sIHtcbiAgICAgICAga2V5OiAndW5tb3VudCcsXG4gICAgICAgIHZhbHVlOiBmdW5jdGlvbiB1bm1vdW50KCkge1xuICAgICAgICAgICAgdmFyIHN3dXAgPSB0aGlzLnN3dXA7XG5cbiAgICAgICAgICAgIHN3dXAuX2hhbmRsZXJzLnBhZ2VQcmVsb2FkZWQgPSBudWxsO1xuICAgICAgICAgICAgc3d1cC5faGFuZGxlcnMuaG92ZXJMaW5rID0gbnVsbDtcblxuICAgICAgICAgICAgc3d1cC5wcmVsb2FkUGFnZSA9IG51bGw7XG4gICAgICAgICAgICBzd3VwLnByZWxvYWRQYWdlcyA9IG51bGw7XG5cbiAgICAgICAgICAgIHN3dXAuZGVsZWdhdGVkTGlzdGVuZXJzLm1vdXNlb3Zlci5kZXN0cm95KCk7XG5cbiAgICAgICAgICAgIHN3dXAub2ZmKCdjb250ZW50UmVwbGFjZWQnLCB0aGlzLm9uQ29udGVudFJlcGxhY2VkKTtcbiAgICAgICAgfVxuICAgIH1dKTtcblxuICAgIHJldHVybiBQcmVsb2FkUGx1Z2luO1xufShfcGx1Z2luMi5kZWZhdWx0KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gUHJlbG9hZFBsdWdpbjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9Ac3d1cC9wcmVsb2FkLXBsdWdpbi9saWIvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDM5XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBjbG9zZXN0ID0gcmVxdWlyZSgnLi9jbG9zZXN0Jyk7XG5cbi8qKlxuICogRGVsZWdhdGVzIGV2ZW50IHRvIGEgc2VsZWN0b3IuXG4gKlxuICogQHBhcmFtIHtFbGVtZW50fSBlbGVtZW50XG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHBhcmFtIHtCb29sZWFufSB1c2VDYXB0dXJlXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIF9kZWxlZ2F0ZShlbGVtZW50LCBzZWxlY3RvciwgdHlwZSwgY2FsbGJhY2ssIHVzZUNhcHR1cmUpIHtcbiAgICB2YXIgbGlzdGVuZXJGbiA9IGxpc3RlbmVyLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG5cbiAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIodHlwZSwgbGlzdGVuZXJGbiwgdXNlQ2FwdHVyZSk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lckZuLCB1c2VDYXB0dXJlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLyoqXG4gKiBEZWxlZ2F0ZXMgZXZlbnQgdG8gYSBzZWxlY3Rvci5cbiAqXG4gKiBAcGFyYW0ge0VsZW1lbnR8U3RyaW5nfEFycmF5fSBbZWxlbWVudHNdXG4gKiBAcGFyYW0ge1N0cmluZ30gc2VsZWN0b3JcbiAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHBhcmFtIHtCb29sZWFufSB1c2VDYXB0dXJlXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmZ1bmN0aW9uIGRlbGVnYXRlKGVsZW1lbnRzLCBzZWxlY3RvciwgdHlwZSwgY2FsbGJhY2ssIHVzZUNhcHR1cmUpIHtcbiAgICAvLyBIYW5kbGUgdGhlIHJlZ3VsYXIgRWxlbWVudCB1c2FnZVxuICAgIGlmICh0eXBlb2YgZWxlbWVudHMuYWRkRXZlbnRMaXN0ZW5lciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICByZXR1cm4gX2RlbGVnYXRlLmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XG4gICAgfVxuXG4gICAgLy8gSGFuZGxlIEVsZW1lbnQtbGVzcyB1c2FnZSwgaXQgZGVmYXVsdHMgdG8gZ2xvYmFsIGRlbGVnYXRpb25cbiAgICBpZiAodHlwZW9mIHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgLy8gVXNlIGBkb2N1bWVudGAgYXMgdGhlIGZpcnN0IHBhcmFtZXRlciwgdGhlbiBhcHBseSBhcmd1bWVudHNcbiAgICAgICAgLy8gVGhpcyBpcyBhIHNob3J0IHdheSB0byAudW5zaGlmdCBgYXJndW1lbnRzYCB3aXRob3V0IHJ1bm5pbmcgaW50byBkZW9wdGltaXphdGlvbnNcbiAgICAgICAgcmV0dXJuIF9kZWxlZ2F0ZS5iaW5kKG51bGwsIGRvY3VtZW50KS5hcHBseShudWxsLCBhcmd1bWVudHMpO1xuICAgIH1cblxuICAgIC8vIEhhbmRsZSBTZWxlY3Rvci1iYXNlZCB1c2FnZVxuICAgIGlmICh0eXBlb2YgZWxlbWVudHMgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIGVsZW1lbnRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChlbGVtZW50cyk7XG4gICAgfVxuXG4gICAgLy8gSGFuZGxlIEFycmF5LWxpa2UgYmFzZWQgdXNhZ2VcbiAgICByZXR1cm4gQXJyYXkucHJvdG90eXBlLm1hcC5jYWxsKGVsZW1lbnRzLCBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICByZXR1cm4gX2RlbGVnYXRlKGVsZW1lbnQsIHNlbGVjdG9yLCB0eXBlLCBjYWxsYmFjaywgdXNlQ2FwdHVyZSk7XG4gICAgfSk7XG59XG5cbi8qKlxuICogRmluZHMgY2xvc2VzdCBtYXRjaCBhbmQgaW52b2tlcyBjYWxsYmFjay5cbiAqXG4gKiBAcGFyYW0ge0VsZW1lbnR9IGVsZW1lbnRcbiAqIEBwYXJhbSB7U3RyaW5nfSBzZWxlY3RvclxuICogQHBhcmFtIHtTdHJpbmd9IHR5cGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbGxiYWNrXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn1cbiAqL1xuZnVuY3Rpb24gbGlzdGVuZXIoZWxlbWVudCwgc2VsZWN0b3IsIHR5cGUsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgZS5kZWxlZ2F0ZVRhcmdldCA9IGNsb3Nlc3QoZS50YXJnZXQsIHNlbGVjdG9yKTtcblxuICAgICAgICBpZiAoZS5kZWxlZ2F0ZVRhcmdldCkge1xuICAgICAgICAgICAgY2FsbGJhY2suY2FsbChlbGVtZW50LCBlKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBkZWxlZ2F0ZTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL0Bzd3VwL3ByZWxvYWQtcGx1Z2luL25vZGVfbW9kdWxlcy9kZWxlZ2F0ZS9zcmMvZGVsZWdhdGUuanNcbi8vIG1vZHVsZSBpZCA9IDQwXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBET0NVTUVOVF9OT0RFX1RZUEUgPSA5O1xuXG4vKipcbiAqIEEgcG9seWZpbGwgZm9yIEVsZW1lbnQubWF0Y2hlcygpXG4gKi9cbmlmICh0eXBlb2YgRWxlbWVudCAhPT0gJ3VuZGVmaW5lZCcgJiYgIUVsZW1lbnQucHJvdG90eXBlLm1hdGNoZXMpIHtcbiAgICB2YXIgcHJvdG8gPSBFbGVtZW50LnByb3RvdHlwZTtcblxuICAgIHByb3RvLm1hdGNoZXMgPSBwcm90by5tYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgICAgICAgICAgICAgcHJvdG8ubW96TWF0Y2hlc1NlbGVjdG9yIHx8XG4gICAgICAgICAgICAgICAgICAgIHByb3RvLm1zTWF0Y2hlc1NlbGVjdG9yIHx8XG4gICAgICAgICAgICAgICAgICAgIHByb3RvLm9NYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgICAgICAgICAgICAgcHJvdG8ud2Via2l0TWF0Y2hlc1NlbGVjdG9yO1xufVxuXG4vKipcbiAqIEZpbmRzIHRoZSBjbG9zZXN0IHBhcmVudCB0aGF0IG1hdGNoZXMgYSBzZWxlY3Rvci5cbiAqXG4gKiBAcGFyYW0ge0VsZW1lbnR9IGVsZW1lbnRcbiAqIEBwYXJhbSB7U3RyaW5nfSBzZWxlY3RvclxuICogQHJldHVybiB7RnVuY3Rpb259XG4gKi9cbmZ1bmN0aW9uIGNsb3Nlc3QgKGVsZW1lbnQsIHNlbGVjdG9yKSB7XG4gICAgd2hpbGUgKGVsZW1lbnQgJiYgZWxlbWVudC5ub2RlVHlwZSAhPT0gRE9DVU1FTlRfTk9ERV9UWVBFKSB7XG4gICAgICAgIGlmICh0eXBlb2YgZWxlbWVudC5tYXRjaGVzID09PSAnZnVuY3Rpb24nICYmXG4gICAgICAgICAgICBlbGVtZW50Lm1hdGNoZXMoc2VsZWN0b3IpKSB7XG4gICAgICAgICAgcmV0dXJuIGVsZW1lbnQ7XG4gICAgICAgIH1cbiAgICAgICAgZWxlbWVudCA9IGVsZW1lbnQucGFyZW50Tm9kZTtcbiAgICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY2xvc2VzdDtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL0Bzd3VwL3ByZWxvYWQtcGx1Z2luL25vZGVfbW9kdWxlcy9kZWxlZ2F0ZS9zcmMvY2xvc2VzdC5qc1xuLy8gbW9kdWxlIGlkID0gNDFcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfcGx1Z2luID0gcmVxdWlyZSgnQHN3dXAvcGx1Z2luJyk7XG5cbnZhciBfcGx1Z2luMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3BsdWdpbik7XG5cbnZhciBfc2NybCA9IHJlcXVpcmUoJ3NjcmwnKTtcblxudmFyIF9zY3JsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NjcmwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBTY3JvbGxQbHVnaW4gPSBmdW5jdGlvbiAoX1BsdWdpbikge1xuICAgIF9pbmhlcml0cyhTY3JvbGxQbHVnaW4sIF9QbHVnaW4pO1xuXG4gICAgZnVuY3Rpb24gU2Nyb2xsUGx1Z2luKG9wdGlvbnMpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNjcm9sbFBsdWdpbik7XG5cbiAgICAgICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKFNjcm9sbFBsdWdpbi5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKFNjcm9sbFBsdWdpbikpLmNhbGwodGhpcykpO1xuXG4gICAgICAgIF90aGlzLm5hbWUgPSBcIlNjcm9sbFBsdWdpblwiO1xuXG4gICAgICAgIF90aGlzLm9uU2FtZVBhZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBfdGhpcy5zd3VwLnNjcm9sbFRvKDApO1xuICAgICAgICB9O1xuXG4gICAgICAgIF90aGlzLm9uU2FtZVBhZ2VXaXRoSGFzaCA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgdmFyIGxpbmsgPSBldmVudC5kZWxlZ2F0ZVRhcmdldDtcbiAgICAgICAgICAgIHZhciBlbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihsaW5rLmhhc2gpO1xuICAgICAgICAgICAgdmFyIHRvcCA9IGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wICsgd2luZG93LnBhZ2VZT2Zmc2V0O1xuICAgICAgICAgICAgX3RoaXMuc3d1cC5zY3JvbGxUbyh0b3ApO1xuICAgICAgICB9O1xuXG4gICAgICAgIF90aGlzLm9uVHJhbnNpdGlvblN0YXJ0ID0gZnVuY3Rpb24gKHBvcHN0YXRlKSB7XG4gICAgICAgICAgICBpZiAoX3RoaXMub3B0aW9ucy5kb1Njcm9sbGluZ1JpZ2h0QXdheSAmJiAhX3RoaXMuc3d1cC5zY3JvbGxUb0VsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICBfdGhpcy5kb1Njcm9sbGluZyhwb3BzdGF0ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgX3RoaXMub25Db250ZW50UmVwbGFjZWQgPSBmdW5jdGlvbiAocG9wc3RhdGUpIHtcbiAgICAgICAgICAgIGlmICghX3RoaXMub3B0aW9ucy5kb1Njcm9sbGluZ1JpZ2h0QXdheSB8fCBfdGhpcy5zd3VwLnNjcm9sbFRvRWxlbWVudCkge1xuICAgICAgICAgICAgICAgIF90aGlzLmRvU2Nyb2xsaW5nKHBvcHN0YXRlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICBfdGhpcy5kb1Njcm9sbGluZyA9IGZ1bmN0aW9uIChwb3BzdGF0ZSkge1xuICAgICAgICAgICAgdmFyIHN3dXAgPSBfdGhpcy5zd3VwO1xuXG4gICAgICAgICAgICBpZiAoIXBvcHN0YXRlIHx8IHN3dXAub3B0aW9ucy5hbmltYXRlSGlzdG9yeUJyb3dzaW5nKSB7XG4gICAgICAgICAgICAgICAgaWYgKHN3dXAuc2Nyb2xsVG9FbGVtZW50ICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHN3dXAuc2Nyb2xsVG9FbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRvcCA9IGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wICsgd2luZG93LnBhZ2VZT2Zmc2V0O1xuICAgICAgICAgICAgICAgICAgICAgICAgc3d1cC5zY3JvbGxUbyh0b3ApO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdFbGVtZW50ICcgKyBzd3VwLnNjcm9sbFRvRWxlbWVudCArICcgbm90IGZvdW5kJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgc3d1cC5zY3JvbGxUb0VsZW1lbnQgPSBudWxsO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHN3dXAuc2Nyb2xsVG8oMCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIHZhciBkZWZhdWx0T3B0aW9ucyA9IHtcbiAgICAgICAgICAgIGRvU2Nyb2xsaW5nUmlnaHRBd2F5OiBmYWxzZSxcbiAgICAgICAgICAgIGFuaW1hdGVTY3JvbGw6IHRydWUsXG4gICAgICAgICAgICBzY3JvbGxGcmljdGlvbjogMC4zLFxuICAgICAgICAgICAgc2Nyb2xsQWNjZWxlcmF0aW9uOiAwLjA0XG4gICAgICAgIH07XG5cbiAgICAgICAgX3RoaXMub3B0aW9ucyA9IF9leHRlbmRzKHt9LCBkZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XG4gICAgICAgIHJldHVybiBfdGhpcztcbiAgICB9XG5cbiAgICBfY3JlYXRlQ2xhc3MoU2Nyb2xsUGx1Z2luLCBbe1xuICAgICAgICBrZXk6ICdtb3VudCcsXG4gICAgICAgIHZhbHVlOiBmdW5jdGlvbiBtb3VudCgpIHtcbiAgICAgICAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICAgICAgICB2YXIgc3d1cCA9IHRoaXMuc3d1cDtcblxuICAgICAgICAgICAgLy8gYWRkIGVtcHR5IGhhbmRsZXJzIGFycmF5IGZvciBzdWJtaXRGb3JtIGV2ZW50XG4gICAgICAgICAgICBzd3VwLl9oYW5kbGVycy5zY3JvbGxEb25lID0gW107XG4gICAgICAgICAgICBzd3VwLl9oYW5kbGVycy5zY3JvbGxTdGFydCA9IFtdO1xuXG4gICAgICAgICAgICB0aGlzLnNjcmwgPSBuZXcgX3NjcmwyLmRlZmF1bHQoe1xuICAgICAgICAgICAgICAgIG9uU3RhcnQ6IGZ1bmN0aW9uIG9uU3RhcnQoKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzd3VwLnRyaWdnZXJFdmVudCgnc2Nyb2xsU3RhcnQnKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uRW5kOiBmdW5jdGlvbiBvbkVuZCgpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN3dXAudHJpZ2dlckV2ZW50KCdzY3JvbGxEb25lJyk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvbkNhbmNlbDogZnVuY3Rpb24gb25DYW5jZWwoKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzd3VwLnRyaWdnZXJFdmVudCgnc2Nyb2xsRG9uZScpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZnJpY3Rpb246IHRoaXMub3B0aW9ucy5zY3JvbGxGcmljdGlvbixcbiAgICAgICAgICAgICAgICBhY2NlbGVyYXRpb246IHRoaXMub3B0aW9ucy5zY3JvbGxBY2NlbGVyYXRpb25cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAvLyBzZXQgc2Nyb2xsVG8gbWV0aG9kIG9mIHN3dXAgYW5kIGFuaW1hdGUgYmFzZWQgb24gY3VycmVudCBhbmltYXRlU2Nyb2xsIG9wdGlvblxuICAgICAgICAgICAgc3d1cC5zY3JvbGxUbyA9IGZ1bmN0aW9uIChvZmZzZXQpIHtcbiAgICAgICAgICAgICAgICBpZiAoX3RoaXMyLm9wdGlvbnMuYW5pbWF0ZVNjcm9sbCkge1xuICAgICAgICAgICAgICAgICAgICBfdGhpczIuc2NybC5zY3JvbGxUbyhvZmZzZXQpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHN3dXAudHJpZ2dlckV2ZW50KCdzY3JvbGxTdGFydCcpO1xuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgb2Zmc2V0KTtcbiAgICAgICAgICAgICAgICAgICAgc3d1cC50cmlnZ2VyRXZlbnQoJ3Njcm9sbERvbmUnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAvLyBkaXNhYmxlIGJyb3dzZXIgc2Nyb2xsIGNvbnRyb2wgb24gcG9wc3RhdGVzIHdoZW5cbiAgICAgICAgICAgIC8vIGFuaW1hdGVIaXN0b3J5QnJvd3Npbmcgb3B0aW9uIGlzIGVuYWJsZWQgaW4gc3d1cFxuICAgICAgICAgICAgaWYgKHN3dXAub3B0aW9ucy5hbmltYXRlSGlzdG9yeUJyb3dzaW5nKSB7XG4gICAgICAgICAgICAgICAgd2luZG93Lmhpc3Rvcnkuc2Nyb2xsUmVzdG9yYXRpb24gPSAnbWFudWFsJztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gc2Nyb2xsIHRvIHRoZSB0b3Agb2YgdGhlIHBhZ2VcbiAgICAgICAgICAgIHN3dXAub24oJ3NhbWVQYWdlJywgdGhpcy5vblNhbWVQYWdlKTtcblxuICAgICAgICAgICAgLy8gc2Nyb2xsIHRvIHJlZmVyZW5jZWQgZWxlbWVudCBvbiB0aGUgc2FtZSBwYWdlXG4gICAgICAgICAgICBzd3VwLm9uKCdzYW1lUGFnZVdpdGhIYXNoJywgdGhpcy5vblNhbWVQYWdlV2l0aEhhc2gpO1xuXG4gICAgICAgICAgICAvLyBzY3JvbGwgdG8gdGhlIHJlZmVyZW5jZWQgZWxlbWVudFxuICAgICAgICAgICAgc3d1cC5vbigndHJhbnNpdGlvblN0YXJ0JywgdGhpcy5vblRyYW5zaXRpb25TdGFydCk7XG5cbiAgICAgICAgICAgIC8vIHNjcm9sbCB0byB0aGUgcmVmZXJlbmNlZCBlbGVtZW50IHdoZW4gaXQncyBpbiB0aGUgcGFnZSAoYWZ0ZXIgcmVuZGVyKVxuICAgICAgICAgICAgc3d1cC5vbignY29udGVudFJlcGxhY2VkJywgdGhpcy5vbkNvbnRlbnRSZXBsYWNlZCk7XG4gICAgICAgIH1cbiAgICB9LCB7XG4gICAgICAgIGtleTogJ3VubW91bnQnLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gdW5tb3VudCgpIHtcbiAgICAgICAgICAgIHRoaXMuc3d1cC5zY3JvbGxUbyA9IG51bGw7XG5cbiAgICAgICAgICAgIGRlbGV0ZSB0aGlzLnNjcmw7XG4gICAgICAgICAgICB0aGlzLnNjcmwgPSBudWxsO1xuXG4gICAgICAgICAgICB0aGlzLnN3dXAub2ZmKCdzYW1lUGFnZScsIHRoaXMub25TYW1lUGFnZSk7XG4gICAgICAgICAgICB0aGlzLnN3dXAub2ZmKCdzYW1lUGFnZVdpdGhIYXNoJywgdGhpcy5vblNhbWVQYWdlV2l0aEhhc2gpO1xuICAgICAgICAgICAgdGhpcy5zd3VwLm9mZigndHJhbnNpdGlvblN0YXJ0JywgdGhpcy5vblRyYW5zaXRpb25TdGFydCk7XG4gICAgICAgICAgICB0aGlzLnN3dXAub2ZmKCdjb250ZW50UmVwbGFjZWQnLCB0aGlzLm9uQ29udGVudFJlcGxhY2VkKTtcblxuICAgICAgICAgICAgdGhpcy5zd3VwLl9oYW5kbGVycy5zY3JvbGxEb25lID0gbnVsbDtcbiAgICAgICAgICAgIHRoaXMuc3d1cC5faGFuZGxlcnMuc2Nyb2xsU3RhcnQgPSBudWxsO1xuXG4gICAgICAgICAgICB3aW5kb3cuaGlzdG9yeS5zY3JvbGxSZXN0b3JhdGlvbiA9ICdhdXRvJztcbiAgICAgICAgfVxuICAgIH1dKTtcblxuICAgIHJldHVybiBTY3JvbGxQbHVnaW47XG59KF9wbHVnaW4yLmRlZmF1bHQpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBTY3JvbGxQbHVnaW47XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvQHN3dXAvc2Nyb2xsLXBsdWdpbi9saWIvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IDQyXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gICAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG52YXIgU2NybCA9IGZ1bmN0aW9uIFNjcmwob3B0aW9ucykge1xuICAgIHZhciBfdGhpcyA9IHRoaXM7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU2NybCk7XG5cbiAgICB0aGlzLl9yYWYgPSBudWxsO1xuICAgIHRoaXMuX3Bvc2l0aW9uWSA9IDA7XG4gICAgdGhpcy5fdmVsb2NpdHlZID0gMDtcbiAgICB0aGlzLl90YXJnZXRQb3NpdGlvblkgPSAwO1xuICAgIHRoaXMuX3RhcmdldFBvc2l0aW9uWVdpdGhPZmZzZXQgPSAwO1xuICAgIHRoaXMuX2RpcmVjdGlvbiA9IDA7XG5cbiAgICB0aGlzLnNjcm9sbFRvID0gZnVuY3Rpb24gKG9mZnNldCkge1xuICAgICAgICBpZiAob2Zmc2V0ICYmIG9mZnNldC5ub2RlVHlwZSkge1xuICAgICAgICAgICAgLy8gdGhlIG9mZnNldCBpcyBlbGVtZW50XG4gICAgICAgICAgICBfdGhpcy5fdGFyZ2V0UG9zaXRpb25ZID0gTWF0aC5yb3VuZChvZmZzZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wICsgd2luZG93LnBhZ2VZT2Zmc2V0KTtcbiAgICAgICAgfSBlbHNlIGlmIChwYXJzZUludChfdGhpcy5fdGFyZ2V0UG9zaXRpb25ZKSA9PT0gX3RoaXMuX3RhcmdldFBvc2l0aW9uWSkge1xuICAgICAgICAgICAgLy8gdGhlIG9mZnNldCBpcyBhIG51bWJlclxuICAgICAgICAgICAgX3RoaXMuX3RhcmdldFBvc2l0aW9uWSA9IE1hdGgucm91bmQob2Zmc2V0KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0FyZ3VtZW50IG11c3QgYmUgYSBudW1iZXIgb3IgYW4gZWxlbWVudC4nKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIGRvbid0IGFuaW1hdGUgYmV5b25kIHRoZSBkb2N1bWVudCBoZWlnaHRcbiAgICAgICAgaWYgKF90aGlzLl90YXJnZXRQb3NpdGlvblkgPiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsSGVpZ2h0IC0gd2luZG93LmlubmVySGVpZ2h0KSB7XG4gICAgICAgICAgICBfdGhpcy5fdGFyZ2V0UG9zaXRpb25ZID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbEhlaWdodCAtIHdpbmRvdy5pbm5lckhlaWdodDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIGNhbGN1bGF0ZWQgcmVxdWlyZWQgdmFsdWVzXG4gICAgICAgIF90aGlzLl9wb3NpdGlvblkgPSBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xuICAgICAgICBfdGhpcy5fZGlyZWN0aW9uID0gX3RoaXMuX3Bvc2l0aW9uWSA+IF90aGlzLl90YXJnZXRQb3NpdGlvblkgPyAtMSA6IDE7XG4gICAgICAgIF90aGlzLl90YXJnZXRQb3NpdGlvbllXaXRoT2Zmc2V0ID0gX3RoaXMuX3RhcmdldFBvc2l0aW9uWSArIF90aGlzLl9kaXJlY3Rpb247XG4gICAgICAgIF90aGlzLl92ZWxvY2l0eVkgPSAwO1xuXG4gICAgICAgIGlmIChfdGhpcy5fcG9zaXRpb25ZICE9PSBfdGhpcy5fdGFyZ2V0UG9zaXRpb25ZKSB7XG4gICAgICAgICAgICAvLyBzdGFydCBhbmltYXRpb25cbiAgICAgICAgICAgIF90aGlzLm9wdGlvbnMub25TdGFydCgpO1xuICAgICAgICAgICAgX3RoaXMuX2FuaW1hdGUoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIHBhZ2UgaXMgYWxyZWFkeSBhdCB0aGUgcG9zaXRpb25cbiAgICAgICAgICAgIF90aGlzLm9wdGlvbnMub25BbHJlYWR5QXRQb3NpdGlvbnMoKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICB0aGlzLl9hbmltYXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZGlzdGFuY2UgPSBfdGhpcy5fdXBkYXRlKCk7XG4gICAgICAgIF90aGlzLl9yZW5kZXIoKTtcblxuICAgICAgICBpZiAoX3RoaXMuX2RpcmVjdGlvbiA9PT0gMSAmJiBfdGhpcy5fdGFyZ2V0UG9zaXRpb25ZID4gX3RoaXMuX3Bvc2l0aW9uWSB8fCBfdGhpcy5fZGlyZWN0aW9uID09PSAtMSAmJiBfdGhpcy5fdGFyZ2V0UG9zaXRpb25ZIDwgX3RoaXMuX3Bvc2l0aW9uWSkge1xuICAgICAgICAgICAgLy8gY2FsY3VsYXRlIG5leHQgcG9zaXRpb25cbiAgICAgICAgICAgIF90aGlzLl9yYWYgPSByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoX3RoaXMuX2FuaW1hdGUpO1xuICAgICAgICAgICAgX3RoaXMub3B0aW9ucy5vblRpY2soKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIGZpbmlzaCBhbmQgc2V0IHBvc2l0aW9uIHRvIHRoZSBmaW5hbCBwb3NpdGlvblxuICAgICAgICAgICAgX3RoaXMuX3Bvc2l0aW9uWSA9IF90aGlzLl90YXJnZXRQb3NpdGlvblk7XG4gICAgICAgICAgICBfdGhpcy5fcmVuZGVyKCk7XG4gICAgICAgICAgICBfdGhpcy5fcmFmID0gbnVsbDtcbiAgICAgICAgICAgIF90aGlzLm9wdGlvbnMub25UaWNrKCk7XG4gICAgICAgICAgICBfdGhpcy5vcHRpb25zLm9uRW5kKCk7XG4gICAgICAgICAgICAvLyB0aGlzLnRyaWdnZXJFdmVudCgnc2Nyb2xsRG9uZScpXG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgdGhpcy5fdXBkYXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZGlzdGFuY2UgPSBfdGhpcy5fdGFyZ2V0UG9zaXRpb25ZV2l0aE9mZnNldCAtIF90aGlzLl9wb3NpdGlvblk7XG4gICAgICAgIHZhciBhdHRyYWN0aW9uID0gZGlzdGFuY2UgKiBfdGhpcy5vcHRpb25zLmFjY2VsZXJhdGlvbjtcblxuICAgICAgICBfdGhpcy5fdmVsb2NpdHlZICs9IGF0dHJhY3Rpb247XG5cbiAgICAgICAgX3RoaXMuX3ZlbG9jaXR5WSAqPSBfdGhpcy5vcHRpb25zLmZyaWN0aW9uO1xuICAgICAgICBfdGhpcy5fcG9zaXRpb25ZICs9IF90aGlzLl92ZWxvY2l0eVk7XG5cbiAgICAgICAgcmV0dXJuIE1hdGguYWJzKGRpc3RhbmNlKTtcbiAgICB9O1xuXG4gICAgdGhpcy5fcmVuZGVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgX3RoaXMuX3Bvc2l0aW9uWSk7XG4gICAgfTtcblxuICAgIC8vIGRlZmF1bHQgb3B0aW9uc1xuICAgIHZhciBkZWZhdWx0cyA9IHtcbiAgICAgICAgb25BbHJlYWR5QXRQb3NpdGlvbnM6IGZ1bmN0aW9uIG9uQWxyZWFkeUF0UG9zaXRpb25zKCkge30sXG4gICAgICAgIG9uQ2FuY2VsOiBmdW5jdGlvbiBvbkNhbmNlbCgpIHt9LFxuICAgICAgICBvbkVuZDogZnVuY3Rpb24gb25FbmQoKSB7fSxcbiAgICAgICAgb25TdGFydDogZnVuY3Rpb24gb25TdGFydCgpIHt9LFxuICAgICAgICBvblRpY2s6IGZ1bmN0aW9uIG9uVGljaygpIHt9LFxuICAgICAgICBmcmljdGlvbjogLjcsIC8vIDEgLSAuM1xuICAgICAgICBhY2NlbGVyYXRpb246IC4wNFxuXG4gICAgICAgIC8vIG1lcmdlIG9wdGlvbnNcbiAgICB9O3RoaXMub3B0aW9ucyA9IF9leHRlbmRzKHt9LCBkZWZhdWx0cywgb3B0aW9ucyk7XG5cbiAgICAvLyBzZXQgcmV2ZXJzZSBmcmljdGlvblxuICAgIGlmIChvcHRpb25zICYmIG9wdGlvbnMuZnJpY3Rpb24pIHtcbiAgICAgICAgdGhpcy5vcHRpb25zLmZyaWN0aW9uID0gMSAtIG9wdGlvbnMuZnJpY3Rpb247XG4gICAgfVxuXG4gICAgLy8gcmVnaXN0ZXIgbGlzdGVuZXIgZm9yIGNhbmNlbCBvbiB3aGVlbCBldmVudFxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdtb3VzZXdoZWVsJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIGlmIChfdGhpcy5fcmFmKSB7XG4gICAgICAgICAgICBfdGhpcy5vcHRpb25zLm9uQ2FuY2VsKCk7XG4gICAgICAgICAgICBjYW5jZWxBbmltYXRpb25GcmFtZShfdGhpcy5fcmFmKTtcbiAgICAgICAgICAgIF90aGlzLl9yYWYgPSBudWxsO1xuICAgICAgICB9XG4gICAgfSwge1xuICAgICAgICBwYXNzaXZlOiB0cnVlXG4gICAgfSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBTY3JsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3NjcmwvbGliL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSA0M1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIvLyBjb25zb2xlLmxvZyhcIlNjcm9sbG1vZHVsZS1sb2FkZWRcIik7XG5cblxuXG5mdW5jdGlvbiBzY3JvbGxJdChkZXN0aW5hdGlvbiwgZHVyYXRpb24gPSAyMDAsIGVhc2luZyA9ICdsaW5lYXInLCBjYWxsYmFjaykge1xuXG4gIGNvbnN0IGVhc2luZ3MgPSB7XG4gICAgbGluZWFyKHQpIHtcbiAgICAgIHJldHVybiB0O1xuICAgIH0sXG4gICAgZWFzZUluUXVhZCh0KSB7XG4gICAgICByZXR1cm4gdCAqIHQ7XG4gICAgfSxcbiAgICBlYXNlT3V0UXVhZCh0KSB7XG4gICAgICByZXR1cm4gdCAqICgyIC0gdCk7XG4gICAgfSxcbiAgICBlYXNlSW5PdXRRdWFkKHQpIHtcbiAgICAgIHJldHVybiB0IDwgMC41ID8gMiAqIHQgKiB0IDogLTEgKyAoNCAtIDIgKiB0KSAqIHQ7XG4gICAgfSxcbiAgICBlYXNlSW5DdWJpYyh0KSB7XG4gICAgICByZXR1cm4gdCAqIHQgKiB0O1xuICAgIH0sXG4gICAgZWFzZU91dEN1YmljKHQpIHtcbiAgICAgIHJldHVybiAoLS10KSAqIHQgKiB0ICsgMTtcbiAgICB9LFxuICAgIGVhc2VJbk91dEN1YmljKHQpIHtcbiAgICAgIHJldHVybiB0IDwgMC41ID8gNCAqIHQgKiB0ICogdCA6ICh0IC0gMSkgKiAoMiAqIHQgLSAyKSAqICgyICogdCAtIDIpICsgMTtcbiAgICB9LFxuICAgIGVhc2VJblF1YXJ0KHQpIHtcbiAgICAgIHJldHVybiB0ICogdCAqIHQgKiB0O1xuICAgIH0sXG4gICAgZWFzZU91dFF1YXJ0KHQpIHtcbiAgICAgIHJldHVybiAxIC0gKC0tdCkgKiB0ICogdCAqIHQ7XG4gICAgfSxcbiAgICBlYXNlSW5PdXRRdWFydCh0KSB7XG4gICAgICByZXR1cm4gdCA8IDAuNSA/IDggKiB0ICogdCAqIHQgKiB0IDogMSAtIDggKiAoLS10KSAqIHQgKiB0ICogdDtcbiAgICB9LFxuICAgIGVhc2VJblF1aW50KHQpIHtcbiAgICAgIHJldHVybiB0ICogdCAqIHQgKiB0ICogdDtcbiAgICB9LFxuICAgIGVhc2VPdXRRdWludCh0KSB7XG4gICAgICByZXR1cm4gMSArICgtLXQpICogdCAqIHQgKiB0ICogdDtcbiAgICB9LFxuICAgIGVhc2VJbk91dFF1aW50KHQpIHtcbiAgICAgIHJldHVybiB0IDwgMC41ID8gMTYgKiB0ICogdCAqIHQgKiB0ICogdCA6IDEgKyAxNiAqICgtLXQpICogdCAqIHQgKiB0ICogdDtcbiAgICB9XG4gIH07XG5cbiAgY29uc3Qgc3RhcnQgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XG4gIGNvbnN0IHN0YXJ0VGltZSA9ICdub3cnIGluIHdpbmRvdy5wZXJmb3JtYW5jZSA/IHBlcmZvcm1hbmNlLm5vdygpIDogbmV3IERhdGUoKS5nZXRUaW1lKCk7XG5cbiAgY29uc3QgZG9jdW1lbnRIZWlnaHQgPSBNYXRoLm1heChkb2N1bWVudC5ib2R5LnNjcm9sbEhlaWdodCwgZG9jdW1lbnQuYm9keS5vZmZzZXRIZWlnaHQsIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQsIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxIZWlnaHQsIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5vZmZzZXRIZWlnaHQpO1xuICBjb25zdCB3aW5kb3dIZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCB8fCBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnYm9keScpWzBdLmNsaWVudEhlaWdodDtcbiAgY29uc3QgZGVzdGluYXRpb25PZmZzZXQgPSB0eXBlb2YgZGVzdGluYXRpb24gPT09ICdudW1iZXInID8gZGVzdGluYXRpb24gOiBkZXN0aW5hdGlvbi5vZmZzZXRUb3A7XG4gIGNvbnN0IGRlc3RpbmF0aW9uT2Zmc2V0VG9TY3JvbGwgPSBNYXRoLnJvdW5kKGRvY3VtZW50SGVpZ2h0IC0gZGVzdGluYXRpb25PZmZzZXQgPCB3aW5kb3dIZWlnaHQgPyBkb2N1bWVudEhlaWdodCAtIHdpbmRvd0hlaWdodCA6IGRlc3RpbmF0aW9uT2Zmc2V0KTtcblxuICBpZiAoJ3JlcXVlc3RBbmltYXRpb25GcmFtZScgaW4gd2luZG93ID09PSBmYWxzZSkge1xuICAgIHdpbmRvdy5zY3JvbGwoMCwgZGVzdGluYXRpb25PZmZzZXRUb1Njcm9sbCk7XG4gICAgaWYgKGNhbGxiYWNrKSB7XG4gICAgICBjYWxsYmFjaygpO1xuICAgIH1cbiAgICByZXR1cm47XG4gIH1cblxuICBmdW5jdGlvbiBzY3JvbGwoKSB7XG4gICAgY29uc3Qgbm93ID0gJ25vdycgaW4gd2luZG93LnBlcmZvcm1hbmNlID8gcGVyZm9ybWFuY2Uubm93KCkgOiBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICBjb25zdCB0aW1lID0gTWF0aC5taW4oMSwgKChub3cgLSBzdGFydFRpbWUpIC8gZHVyYXRpb24pKTtcbiAgICBjb25zdCB0aW1lRnVuY3Rpb24gPSBlYXNpbmdzW2Vhc2luZ10odGltZSk7XG4gICAgd2luZG93LnNjcm9sbCgwLCBNYXRoLmNlaWwoKHRpbWVGdW5jdGlvbiAqIChkZXN0aW5hdGlvbk9mZnNldFRvU2Nyb2xsIC0gc3RhcnQpKSArIHN0YXJ0KSk7XG5cbiAgICBpZiAod2luZG93LnBhZ2VZT2Zmc2V0ID09PSBkZXN0aW5hdGlvbk9mZnNldFRvU2Nyb2xsKSB7XG4gICAgICBpZiAoY2FsbGJhY2spIHtcbiAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgIH1cbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoc2Nyb2xsKTtcbiAgfVxuXG4gIHNjcm9sbCgpO1xufVxuXG5cbndpbmRvdy5zY3JvbGxJdCA9IHNjcm9sbEl0O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2Fzc2V0cy9qcy9tb2R1bGVzL3Njcm9sbC1tb2R1bGUuanMiLCIoZnVuY3Rpb24od2luZG93LCBmYWN0b3J5KSB7XG5cdHZhciBsYXp5U2l6ZXMgPSBmYWN0b3J5KHdpbmRvdywgd2luZG93LmRvY3VtZW50KTtcblx0d2luZG93LmxhenlTaXplcyA9IGxhenlTaXplcztcblx0aWYodHlwZW9mIG1vZHVsZSA9PSAnb2JqZWN0JyAmJiBtb2R1bGUuZXhwb3J0cyl7XG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBsYXp5U2l6ZXM7XG5cdH1cbn0od2luZG93LCBmdW5jdGlvbiBsKHdpbmRvdywgZG9jdW1lbnQpIHtcblx0J3VzZSBzdHJpY3QnO1xuXHQvKmpzaGludCBlcW51bGw6dHJ1ZSAqL1xuXHRpZighZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSl7cmV0dXJuO31cblxuXHR2YXIgbGF6eXNpemVzLCBsYXp5U2l6ZXNDZmc7XG5cblx0dmFyIGRvY0VsZW0gPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XG5cblx0dmFyIERhdGUgPSB3aW5kb3cuRGF0ZTtcblxuXHR2YXIgc3VwcG9ydFBpY3R1cmUgPSB3aW5kb3cuSFRNTFBpY3R1cmVFbGVtZW50O1xuXG5cdHZhciBfYWRkRXZlbnRMaXN0ZW5lciA9ICdhZGRFdmVudExpc3RlbmVyJztcblxuXHR2YXIgX2dldEF0dHJpYnV0ZSA9ICdnZXRBdHRyaWJ1dGUnO1xuXG5cdHZhciBhZGRFdmVudExpc3RlbmVyID0gd2luZG93W19hZGRFdmVudExpc3RlbmVyXTtcblxuXHR2YXIgc2V0VGltZW91dCA9IHdpbmRvdy5zZXRUaW1lb3V0O1xuXG5cdHZhciByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgPSB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8IHNldFRpbWVvdXQ7XG5cblx0dmFyIHJlcXVlc3RJZGxlQ2FsbGJhY2sgPSB3aW5kb3cucmVxdWVzdElkbGVDYWxsYmFjaztcblxuXHR2YXIgcmVnUGljdHVyZSA9IC9ecGljdHVyZSQvaTtcblxuXHR2YXIgbG9hZEV2ZW50cyA9IFsnbG9hZCcsICdlcnJvcicsICdsYXp5aW5jbHVkZWQnLCAnX2xhenlsb2FkZWQnXTtcblxuXHR2YXIgcmVnQ2xhc3NDYWNoZSA9IHt9O1xuXG5cdHZhciBmb3JFYWNoID0gQXJyYXkucHJvdG90eXBlLmZvckVhY2g7XG5cblx0dmFyIGhhc0NsYXNzID0gZnVuY3Rpb24oZWxlLCBjbHMpIHtcblx0XHRpZighcmVnQ2xhc3NDYWNoZVtjbHNdKXtcblx0XHRcdHJlZ0NsYXNzQ2FjaGVbY2xzXSA9IG5ldyBSZWdFeHAoJyhcXFxcc3xeKScrY2xzKycoXFxcXHN8JCknKTtcblx0XHR9XG5cdFx0cmV0dXJuIHJlZ0NsYXNzQ2FjaGVbY2xzXS50ZXN0KGVsZVtfZ2V0QXR0cmlidXRlXSgnY2xhc3MnKSB8fCAnJykgJiYgcmVnQ2xhc3NDYWNoZVtjbHNdO1xuXHR9O1xuXG5cdHZhciBhZGRDbGFzcyA9IGZ1bmN0aW9uKGVsZSwgY2xzKSB7XG5cdFx0aWYgKCFoYXNDbGFzcyhlbGUsIGNscykpe1xuXHRcdFx0ZWxlLnNldEF0dHJpYnV0ZSgnY2xhc3MnLCAoZWxlW19nZXRBdHRyaWJ1dGVdKCdjbGFzcycpIHx8ICcnKS50cmltKCkgKyAnICcgKyBjbHMpO1xuXHRcdH1cblx0fTtcblxuXHR2YXIgcmVtb3ZlQ2xhc3MgPSBmdW5jdGlvbihlbGUsIGNscykge1xuXHRcdHZhciByZWc7XG5cdFx0aWYgKChyZWcgPSBoYXNDbGFzcyhlbGUsY2xzKSkpIHtcblx0XHRcdGVsZS5zZXRBdHRyaWJ1dGUoJ2NsYXNzJywgKGVsZVtfZ2V0QXR0cmlidXRlXSgnY2xhc3MnKSB8fCAnJykucmVwbGFjZShyZWcsICcgJykpO1xuXHRcdH1cblx0fTtcblxuXHR2YXIgYWRkUmVtb3ZlTG9hZEV2ZW50cyA9IGZ1bmN0aW9uKGRvbSwgZm4sIGFkZCl7XG5cdFx0dmFyIGFjdGlvbiA9IGFkZCA/IF9hZGRFdmVudExpc3RlbmVyIDogJ3JlbW92ZUV2ZW50TGlzdGVuZXInO1xuXHRcdGlmKGFkZCl7XG5cdFx0XHRhZGRSZW1vdmVMb2FkRXZlbnRzKGRvbSwgZm4pO1xuXHRcdH1cblx0XHRsb2FkRXZlbnRzLmZvckVhY2goZnVuY3Rpb24oZXZ0KXtcblx0XHRcdGRvbVthY3Rpb25dKGV2dCwgZm4pO1xuXHRcdH0pO1xuXHR9O1xuXG5cdHZhciB0cmlnZ2VyRXZlbnQgPSBmdW5jdGlvbihlbGVtLCBuYW1lLCBkZXRhaWwsIG5vQnViYmxlcywgbm9DYW5jZWxhYmxlKXtcblx0XHR2YXIgZXZlbnQgPSBkb2N1bWVudC5jcmVhdGVFdmVudCgnRXZlbnQnKTtcblxuXHRcdGlmKCFkZXRhaWwpe1xuXHRcdFx0ZGV0YWlsID0ge307XG5cdFx0fVxuXG5cdFx0ZGV0YWlsLmluc3RhbmNlID0gbGF6eXNpemVzO1xuXG5cdFx0ZXZlbnQuaW5pdEV2ZW50KG5hbWUsICFub0J1YmJsZXMsICFub0NhbmNlbGFibGUpO1xuXG5cdFx0ZXZlbnQuZGV0YWlsID0gZGV0YWlsO1xuXG5cdFx0ZWxlbS5kaXNwYXRjaEV2ZW50KGV2ZW50KTtcblx0XHRyZXR1cm4gZXZlbnQ7XG5cdH07XG5cblx0dmFyIHVwZGF0ZVBvbHlmaWxsID0gZnVuY3Rpb24gKGVsLCBmdWxsKXtcblx0XHR2YXIgcG9seWZpbGw7XG5cdFx0aWYoICFzdXBwb3J0UGljdHVyZSAmJiAoIHBvbHlmaWxsID0gKHdpbmRvdy5waWN0dXJlZmlsbCB8fCBsYXp5U2l6ZXNDZmcucGYpICkgKXtcblx0XHRcdGlmKGZ1bGwgJiYgZnVsbC5zcmMgJiYgIWVsW19nZXRBdHRyaWJ1dGVdKCdzcmNzZXQnKSl7XG5cdFx0XHRcdGVsLnNldEF0dHJpYnV0ZSgnc3Jjc2V0JywgZnVsbC5zcmMpO1xuXHRcdFx0fVxuXHRcdFx0cG9seWZpbGwoe3JlZXZhbHVhdGU6IHRydWUsIGVsZW1lbnRzOiBbZWxdfSk7XG5cdFx0fSBlbHNlIGlmKGZ1bGwgJiYgZnVsbC5zcmMpe1xuXHRcdFx0ZWwuc3JjID0gZnVsbC5zcmM7XG5cdFx0fVxuXHR9O1xuXG5cdHZhciBnZXRDU1MgPSBmdW5jdGlvbiAoZWxlbSwgc3R5bGUpe1xuXHRcdHJldHVybiAoZ2V0Q29tcHV0ZWRTdHlsZShlbGVtLCBudWxsKSB8fCB7fSlbc3R5bGVdO1xuXHR9O1xuXG5cdHZhciBnZXRXaWR0aCA9IGZ1bmN0aW9uKGVsZW0sIHBhcmVudCwgd2lkdGgpe1xuXHRcdHdpZHRoID0gd2lkdGggfHwgZWxlbS5vZmZzZXRXaWR0aDtcblxuXHRcdHdoaWxlKHdpZHRoIDwgbGF6eVNpemVzQ2ZnLm1pblNpemUgJiYgcGFyZW50ICYmICFlbGVtLl9sYXp5c2l6ZXNXaWR0aCl7XG5cdFx0XHR3aWR0aCA9ICBwYXJlbnQub2Zmc2V0V2lkdGg7XG5cdFx0XHRwYXJlbnQgPSBwYXJlbnQucGFyZW50Tm9kZTtcblx0XHR9XG5cblx0XHRyZXR1cm4gd2lkdGg7XG5cdH07XG5cblx0dmFyIHJBRiA9IChmdW5jdGlvbigpe1xuXHRcdHZhciBydW5uaW5nLCB3YWl0aW5nO1xuXHRcdHZhciBmaXJzdEZucyA9IFtdO1xuXHRcdHZhciBzZWNvbmRGbnMgPSBbXTtcblx0XHR2YXIgZm5zID0gZmlyc3RGbnM7XG5cblx0XHR2YXIgcnVuID0gZnVuY3Rpb24oKXtcblx0XHRcdHZhciBydW5GbnMgPSBmbnM7XG5cblx0XHRcdGZucyA9IGZpcnN0Rm5zLmxlbmd0aCA/IHNlY29uZEZucyA6IGZpcnN0Rm5zO1xuXG5cdFx0XHRydW5uaW5nID0gdHJ1ZTtcblx0XHRcdHdhaXRpbmcgPSBmYWxzZTtcblxuXHRcdFx0d2hpbGUocnVuRm5zLmxlbmd0aCl7XG5cdFx0XHRcdHJ1bkZucy5zaGlmdCgpKCk7XG5cdFx0XHR9XG5cblx0XHRcdHJ1bm5pbmcgPSBmYWxzZTtcblx0XHR9O1xuXG5cdFx0dmFyIHJhZkJhdGNoID0gZnVuY3Rpb24oZm4sIHF1ZXVlKXtcblx0XHRcdGlmKHJ1bm5pbmcgJiYgIXF1ZXVlKXtcblx0XHRcdFx0Zm4uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdGZucy5wdXNoKGZuKTtcblxuXHRcdFx0XHRpZighd2FpdGluZyl7XG5cdFx0XHRcdFx0d2FpdGluZyA9IHRydWU7XG5cdFx0XHRcdFx0KGRvY3VtZW50LmhpZGRlbiA/IHNldFRpbWVvdXQgOiByZXF1ZXN0QW5pbWF0aW9uRnJhbWUpKHJ1bik7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9O1xuXG5cdFx0cmFmQmF0Y2guX2xzRmx1c2ggPSBydW47XG5cblx0XHRyZXR1cm4gcmFmQmF0Y2g7XG5cdH0pKCk7XG5cblx0dmFyIHJBRkl0ID0gZnVuY3Rpb24oZm4sIHNpbXBsZSl7XG5cdFx0cmV0dXJuIHNpbXBsZSA/XG5cdFx0XHRmdW5jdGlvbigpIHtcblx0XHRcdFx0ckFGKGZuKTtcblx0XHRcdH0gOlxuXHRcdFx0ZnVuY3Rpb24oKXtcblx0XHRcdFx0dmFyIHRoYXQgPSB0aGlzO1xuXHRcdFx0XHR2YXIgYXJncyA9IGFyZ3VtZW50cztcblx0XHRcdFx0ckFGKGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0Zm4uYXBwbHkodGhhdCwgYXJncyk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdDtcblx0fTtcblxuXHR2YXIgdGhyb3R0bGUgPSBmdW5jdGlvbihmbil7XG5cdFx0dmFyIHJ1bm5pbmc7XG5cdFx0dmFyIGxhc3RUaW1lID0gMDtcblx0XHR2YXIgZ0RlbGF5ID0gbGF6eVNpemVzQ2ZnLnRocm90dGxlRGVsYXk7XG5cdFx0dmFyIHJJQ1RpbWVvdXQgPSBsYXp5U2l6ZXNDZmcucmljVGltZW91dDtcblx0XHR2YXIgcnVuID0gZnVuY3Rpb24oKXtcblx0XHRcdHJ1bm5pbmcgPSBmYWxzZTtcblx0XHRcdGxhc3RUaW1lID0gRGF0ZS5ub3coKTtcblx0XHRcdGZuKCk7XG5cdFx0fTtcblx0XHR2YXIgaWRsZUNhbGxiYWNrID0gcmVxdWVzdElkbGVDYWxsYmFjayAmJiBySUNUaW1lb3V0ID4gNDkgP1xuXHRcdFx0ZnVuY3Rpb24oKXtcblx0XHRcdFx0cmVxdWVzdElkbGVDYWxsYmFjayhydW4sIHt0aW1lb3V0OiBySUNUaW1lb3V0fSk7XG5cblx0XHRcdFx0aWYocklDVGltZW91dCAhPT0gbGF6eVNpemVzQ2ZnLnJpY1RpbWVvdXQpe1xuXHRcdFx0XHRcdHJJQ1RpbWVvdXQgPSBsYXp5U2l6ZXNDZmcucmljVGltZW91dDtcblx0XHRcdFx0fVxuXHRcdFx0fSA6XG5cdFx0XHRyQUZJdChmdW5jdGlvbigpe1xuXHRcdFx0XHRzZXRUaW1lb3V0KHJ1bik7XG5cdFx0XHR9LCB0cnVlKVxuXHRcdDtcblxuXHRcdHJldHVybiBmdW5jdGlvbihpc1ByaW9yaXR5KXtcblx0XHRcdHZhciBkZWxheTtcblxuXHRcdFx0aWYoKGlzUHJpb3JpdHkgPSBpc1ByaW9yaXR5ID09PSB0cnVlKSl7XG5cdFx0XHRcdHJJQ1RpbWVvdXQgPSAzMztcblx0XHRcdH1cblxuXHRcdFx0aWYocnVubmluZyl7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0cnVubmluZyA9ICB0cnVlO1xuXG5cdFx0XHRkZWxheSA9IGdEZWxheSAtIChEYXRlLm5vdygpIC0gbGFzdFRpbWUpO1xuXG5cdFx0XHRpZihkZWxheSA8IDApe1xuXHRcdFx0XHRkZWxheSA9IDA7XG5cdFx0XHR9XG5cblx0XHRcdGlmKGlzUHJpb3JpdHkgfHwgZGVsYXkgPCA5KXtcblx0XHRcdFx0aWRsZUNhbGxiYWNrKCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRzZXRUaW1lb3V0KGlkbGVDYWxsYmFjaywgZGVsYXkpO1xuXHRcdFx0fVxuXHRcdH07XG5cdH07XG5cblx0Ly9iYXNlZCBvbiBodHRwOi8vbW9kZXJuamF2YXNjcmlwdC5ibG9nc3BvdC5kZS8yMDEzLzA4L2J1aWxkaW5nLWJldHRlci1kZWJvdW5jZS5odG1sXG5cdHZhciBkZWJvdW5jZSA9IGZ1bmN0aW9uKGZ1bmMpIHtcblx0XHR2YXIgdGltZW91dCwgdGltZXN0YW1wO1xuXHRcdHZhciB3YWl0ID0gOTk7XG5cdFx0dmFyIHJ1biA9IGZ1bmN0aW9uKCl7XG5cdFx0XHR0aW1lb3V0ID0gbnVsbDtcblx0XHRcdGZ1bmMoKTtcblx0XHR9O1xuXHRcdHZhciBsYXRlciA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIGxhc3QgPSBEYXRlLm5vdygpIC0gdGltZXN0YW1wO1xuXG5cdFx0XHRpZiAobGFzdCA8IHdhaXQpIHtcblx0XHRcdFx0c2V0VGltZW91dChsYXRlciwgd2FpdCAtIGxhc3QpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0KHJlcXVlc3RJZGxlQ2FsbGJhY2sgfHwgcnVuKShydW4pO1xuXHRcdFx0fVxuXHRcdH07XG5cblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XG5cdFx0XHR0aW1lc3RhbXAgPSBEYXRlLm5vdygpO1xuXG5cdFx0XHRpZiAoIXRpbWVvdXQpIHtcblx0XHRcdFx0dGltZW91dCA9IHNldFRpbWVvdXQobGF0ZXIsIHdhaXQpO1xuXHRcdFx0fVxuXHRcdH07XG5cdH07XG5cblx0KGZ1bmN0aW9uKCl7XG5cdFx0dmFyIHByb3A7XG5cblx0XHR2YXIgbGF6eVNpemVzRGVmYXVsdHMgPSB7XG5cdFx0XHRsYXp5Q2xhc3M6ICdsYXp5bG9hZCcsXG5cdFx0XHRsb2FkZWRDbGFzczogJ2xhenlsb2FkZWQnLFxuXHRcdFx0bG9hZGluZ0NsYXNzOiAnbGF6eWxvYWRpbmcnLFxuXHRcdFx0cHJlbG9hZENsYXNzOiAnbGF6eXByZWxvYWQnLFxuXHRcdFx0ZXJyb3JDbGFzczogJ2xhenllcnJvcicsXG5cdFx0XHQvL3N0cmljdENsYXNzOiAnbGF6eXN0cmljdCcsXG5cdFx0XHRhdXRvc2l6ZXNDbGFzczogJ2xhenlhdXRvc2l6ZXMnLFxuXHRcdFx0c3JjQXR0cjogJ2RhdGEtc3JjJyxcblx0XHRcdHNyY3NldEF0dHI6ICdkYXRhLXNyY3NldCcsXG5cdFx0XHRzaXplc0F0dHI6ICdkYXRhLXNpemVzJyxcblx0XHRcdC8vcHJlbG9hZEFmdGVyTG9hZDogZmFsc2UsXG5cdFx0XHRtaW5TaXplOiA0MCxcblx0XHRcdGN1c3RvbU1lZGlhOiB7fSxcblx0XHRcdGluaXQ6IHRydWUsXG5cdFx0XHRleHBGYWN0b3I6IDEuNSxcblx0XHRcdGhGYWM6IDAuOCxcblx0XHRcdGxvYWRNb2RlOiAyLFxuXHRcdFx0bG9hZEhpZGRlbjogdHJ1ZSxcblx0XHRcdHJpY1RpbWVvdXQ6IDAsXG5cdFx0XHR0aHJvdHRsZURlbGF5OiAxMjUsXG5cdFx0fTtcblxuXHRcdGxhenlTaXplc0NmZyA9IHdpbmRvdy5sYXp5U2l6ZXNDb25maWcgfHwgd2luZG93LmxhenlzaXplc0NvbmZpZyB8fCB7fTtcblxuXHRcdGZvcihwcm9wIGluIGxhenlTaXplc0RlZmF1bHRzKXtcblx0XHRcdGlmKCEocHJvcCBpbiBsYXp5U2l6ZXNDZmcpKXtcblx0XHRcdFx0bGF6eVNpemVzQ2ZnW3Byb3BdID0gbGF6eVNpemVzRGVmYXVsdHNbcHJvcF07XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0c2V0VGltZW91dChmdW5jdGlvbigpe1xuXHRcdFx0aWYobGF6eVNpemVzQ2ZnLmluaXQpe1xuXHRcdFx0XHRpbml0KCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdH0pKCk7XG5cblx0dmFyIGxvYWRlciA9IChmdW5jdGlvbigpe1xuXHRcdHZhciBwcmVsb2FkRWxlbXMsIGlzQ29tcGxldGVkLCByZXNldFByZWxvYWRpbmdUaW1lciwgbG9hZE1vZGUsIHN0YXJ0ZWQ7XG5cblx0XHR2YXIgZUx2VywgZWx2SCwgZUx0b3AsIGVMbGVmdCwgZUxyaWdodCwgZUxib3R0b20sIGlzQm9keUhpZGRlbjtcblxuXHRcdHZhciByZWdJbWcgPSAvXmltZyQvaTtcblx0XHR2YXIgcmVnSWZyYW1lID0gL15pZnJhbWUkL2k7XG5cblx0XHR2YXIgc3VwcG9ydFNjcm9sbCA9ICgnb25zY3JvbGwnIGluIHdpbmRvdykgJiYgISgvKGdsZXxpbmcpYm90Ly50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpKTtcblxuXHRcdHZhciBzaHJpbmtFeHBhbmQgPSAwO1xuXHRcdHZhciBjdXJyZW50RXhwYW5kID0gMDtcblxuXHRcdHZhciBpc0xvYWRpbmcgPSAwO1xuXHRcdHZhciBsb3dSdW5zID0gLTE7XG5cblx0XHR2YXIgcmVzZXRQcmVsb2FkaW5nID0gZnVuY3Rpb24oZSl7XG5cdFx0XHRpc0xvYWRpbmctLTtcblx0XHRcdGlmKCFlIHx8IGlzTG9hZGluZyA8IDAgfHwgIWUudGFyZ2V0KXtcblx0XHRcdFx0aXNMb2FkaW5nID0gMDtcblx0XHRcdH1cblx0XHR9O1xuXG5cdFx0dmFyIGlzVmlzaWJsZSA9IGZ1bmN0aW9uIChlbGVtKSB7XG5cdFx0XHRpZiAoaXNCb2R5SGlkZGVuID09IG51bGwpIHtcblx0XHRcdFx0aXNCb2R5SGlkZGVuID0gZ2V0Q1NTKGRvY3VtZW50LmJvZHksICd2aXNpYmlsaXR5JykgPT0gJ2hpZGRlbic7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiBpc0JvZHlIaWRkZW4gfHwgKGdldENTUyhlbGVtLnBhcmVudE5vZGUsICd2aXNpYmlsaXR5JykgIT0gJ2hpZGRlbicgJiYgZ2V0Q1NTKGVsZW0sICd2aXNpYmlsaXR5JykgIT0gJ2hpZGRlbicpO1xuXHRcdH07XG5cblx0XHR2YXIgaXNOZXN0ZWRWaXNpYmxlID0gZnVuY3Rpb24oZWxlbSwgZWxlbUV4cGFuZCl7XG5cdFx0XHR2YXIgb3V0ZXJSZWN0O1xuXHRcdFx0dmFyIHBhcmVudCA9IGVsZW07XG5cdFx0XHR2YXIgdmlzaWJsZSA9IGlzVmlzaWJsZShlbGVtKTtcblxuXHRcdFx0ZUx0b3AgLT0gZWxlbUV4cGFuZDtcblx0XHRcdGVMYm90dG9tICs9IGVsZW1FeHBhbmQ7XG5cdFx0XHRlTGxlZnQgLT0gZWxlbUV4cGFuZDtcblx0XHRcdGVMcmlnaHQgKz0gZWxlbUV4cGFuZDtcblxuXHRcdFx0d2hpbGUodmlzaWJsZSAmJiAocGFyZW50ID0gcGFyZW50Lm9mZnNldFBhcmVudCkgJiYgcGFyZW50ICE9IGRvY3VtZW50LmJvZHkgJiYgcGFyZW50ICE9IGRvY0VsZW0pe1xuXHRcdFx0XHR2aXNpYmxlID0gKChnZXRDU1MocGFyZW50LCAnb3BhY2l0eScpIHx8IDEpID4gMCk7XG5cblx0XHRcdFx0aWYodmlzaWJsZSAmJiBnZXRDU1MocGFyZW50LCAnb3ZlcmZsb3cnKSAhPSAndmlzaWJsZScpe1xuXHRcdFx0XHRcdG91dGVyUmVjdCA9IHBhcmVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblx0XHRcdFx0XHR2aXNpYmxlID0gZUxyaWdodCA+IG91dGVyUmVjdC5sZWZ0ICYmXG5cdFx0XHRcdFx0XHRlTGxlZnQgPCBvdXRlclJlY3QucmlnaHQgJiZcblx0XHRcdFx0XHRcdGVMYm90dG9tID4gb3V0ZXJSZWN0LnRvcCAtIDEgJiZcblx0XHRcdFx0XHRcdGVMdG9wIDwgb3V0ZXJSZWN0LmJvdHRvbSArIDFcblx0XHRcdFx0XHQ7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHZpc2libGU7XG5cdFx0fTtcblxuXHRcdHZhciBjaGVja0VsZW1lbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgZUxsZW4sIGksIHJlY3QsIGF1dG9Mb2FkRWxlbSwgbG9hZGVkU29tZXRoaW5nLCBlbGVtRXhwYW5kLCBlbGVtTmVnYXRpdmVFeHBhbmQsIGVsZW1FeHBhbmRWYWwsXG5cdFx0XHRcdGJlZm9yZUV4cGFuZFZhbCwgZGVmYXVsdEV4cGFuZCwgcHJlbG9hZEV4cGFuZCwgaEZhYztcblx0XHRcdHZhciBsYXp5bG9hZEVsZW1zID0gbGF6eXNpemVzLmVsZW1lbnRzO1xuXG5cdFx0XHRpZigobG9hZE1vZGUgPSBsYXp5U2l6ZXNDZmcubG9hZE1vZGUpICYmIGlzTG9hZGluZyA8IDggJiYgKGVMbGVuID0gbGF6eWxvYWRFbGVtcy5sZW5ndGgpKXtcblxuXHRcdFx0XHRpID0gMDtcblxuXHRcdFx0XHRsb3dSdW5zKys7XG5cblx0XHRcdFx0Zm9yKDsgaSA8IGVMbGVuOyBpKyspe1xuXG5cdFx0XHRcdFx0aWYoIWxhenlsb2FkRWxlbXNbaV0gfHwgbGF6eWxvYWRFbGVtc1tpXS5fbGF6eVJhY2Upe2NvbnRpbnVlO31cblxuXHRcdFx0XHRcdGlmKCFzdXBwb3J0U2Nyb2xsIHx8IChsYXp5c2l6ZXMucHJlbWF0dXJlVW52ZWlsICYmIGxhenlzaXplcy5wcmVtYXR1cmVVbnZlaWwobGF6eWxvYWRFbGVtc1tpXSkpKXt1bnZlaWxFbGVtZW50KGxhenlsb2FkRWxlbXNbaV0pO2NvbnRpbnVlO31cblxuXHRcdFx0XHRcdGlmKCEoZWxlbUV4cGFuZFZhbCA9IGxhenlsb2FkRWxlbXNbaV1bX2dldEF0dHJpYnV0ZV0oJ2RhdGEtZXhwYW5kJykpIHx8ICEoZWxlbUV4cGFuZCA9IGVsZW1FeHBhbmRWYWwgKiAxKSl7XG5cdFx0XHRcdFx0XHRlbGVtRXhwYW5kID0gY3VycmVudEV4cGFuZDtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRpZiAoIWRlZmF1bHRFeHBhbmQpIHtcblx0XHRcdFx0XHRcdGRlZmF1bHRFeHBhbmQgPSAoIWxhenlTaXplc0NmZy5leHBhbmQgfHwgbGF6eVNpemVzQ2ZnLmV4cGFuZCA8IDEpID9cblx0XHRcdFx0XHRcdFx0ZG9jRWxlbS5jbGllbnRIZWlnaHQgPiA1MDAgJiYgZG9jRWxlbS5jbGllbnRXaWR0aCA+IDUwMCA/IDUwMCA6IDM3MCA6XG5cdFx0XHRcdFx0XHRcdGxhenlTaXplc0NmZy5leHBhbmQ7XG5cblx0XHRcdFx0XHRcdGxhenlzaXplcy5fZGVmRXggPSBkZWZhdWx0RXhwYW5kO1xuXG5cdFx0XHRcdFx0XHRwcmVsb2FkRXhwYW5kID0gZGVmYXVsdEV4cGFuZCAqIGxhenlTaXplc0NmZy5leHBGYWN0b3I7XG5cdFx0XHRcdFx0XHRoRmFjID0gbGF6eVNpemVzQ2ZnLmhGYWM7XG5cdFx0XHRcdFx0XHRpc0JvZHlIaWRkZW4gPSBudWxsO1xuXG5cdFx0XHRcdFx0XHRpZihjdXJyZW50RXhwYW5kIDwgcHJlbG9hZEV4cGFuZCAmJiBpc0xvYWRpbmcgPCAxICYmIGxvd1J1bnMgPiAyICYmIGxvYWRNb2RlID4gMiAmJiAhZG9jdW1lbnQuaGlkZGVuKXtcblx0XHRcdFx0XHRcdFx0Y3VycmVudEV4cGFuZCA9IHByZWxvYWRFeHBhbmQ7XG5cdFx0XHRcdFx0XHRcdGxvd1J1bnMgPSAwO1xuXHRcdFx0XHRcdFx0fSBlbHNlIGlmKGxvYWRNb2RlID4gMSAmJiBsb3dSdW5zID4gMSAmJiBpc0xvYWRpbmcgPCA2KXtcblx0XHRcdFx0XHRcdFx0Y3VycmVudEV4cGFuZCA9IGRlZmF1bHRFeHBhbmQ7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRjdXJyZW50RXhwYW5kID0gc2hyaW5rRXhwYW5kO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmKGJlZm9yZUV4cGFuZFZhbCAhPT0gZWxlbUV4cGFuZCl7XG5cdFx0XHRcdFx0XHRlTHZXID0gaW5uZXJXaWR0aCArIChlbGVtRXhwYW5kICogaEZhYyk7XG5cdFx0XHRcdFx0XHRlbHZIID0gaW5uZXJIZWlnaHQgKyBlbGVtRXhwYW5kO1xuXHRcdFx0XHRcdFx0ZWxlbU5lZ2F0aXZlRXhwYW5kID0gZWxlbUV4cGFuZCAqIC0xO1xuXHRcdFx0XHRcdFx0YmVmb3JlRXhwYW5kVmFsID0gZWxlbUV4cGFuZDtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRyZWN0ID0gbGF6eWxvYWRFbGVtc1tpXS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblxuXHRcdFx0XHRcdGlmICgoZUxib3R0b20gPSByZWN0LmJvdHRvbSkgPj0gZWxlbU5lZ2F0aXZlRXhwYW5kICYmXG5cdFx0XHRcdFx0XHQoZUx0b3AgPSByZWN0LnRvcCkgPD0gZWx2SCAmJlxuXHRcdFx0XHRcdFx0KGVMcmlnaHQgPSByZWN0LnJpZ2h0KSA+PSBlbGVtTmVnYXRpdmVFeHBhbmQgKiBoRmFjICYmXG5cdFx0XHRcdFx0XHQoZUxsZWZ0ID0gcmVjdC5sZWZ0KSA8PSBlTHZXICYmXG5cdFx0XHRcdFx0XHQoZUxib3R0b20gfHwgZUxyaWdodCB8fCBlTGxlZnQgfHwgZUx0b3ApICYmXG5cdFx0XHRcdFx0XHQobGF6eVNpemVzQ2ZnLmxvYWRIaWRkZW4gfHwgaXNWaXNpYmxlKGxhenlsb2FkRWxlbXNbaV0pKSAmJlxuXHRcdFx0XHRcdFx0KChpc0NvbXBsZXRlZCAmJiBpc0xvYWRpbmcgPCAzICYmICFlbGVtRXhwYW5kVmFsICYmIChsb2FkTW9kZSA8IDMgfHwgbG93UnVucyA8IDQpKSB8fCBpc05lc3RlZFZpc2libGUobGF6eWxvYWRFbGVtc1tpXSwgZWxlbUV4cGFuZCkpKXtcblx0XHRcdFx0XHRcdHVudmVpbEVsZW1lbnQobGF6eWxvYWRFbGVtc1tpXSk7XG5cdFx0XHRcdFx0XHRsb2FkZWRTb21ldGhpbmcgPSB0cnVlO1xuXHRcdFx0XHRcdFx0aWYoaXNMb2FkaW5nID4gOSl7YnJlYWs7fVxuXHRcdFx0XHRcdH0gZWxzZSBpZighbG9hZGVkU29tZXRoaW5nICYmIGlzQ29tcGxldGVkICYmICFhdXRvTG9hZEVsZW0gJiZcblx0XHRcdFx0XHRcdGlzTG9hZGluZyA8IDQgJiYgbG93UnVucyA8IDQgJiYgbG9hZE1vZGUgPiAyICYmXG5cdFx0XHRcdFx0XHQocHJlbG9hZEVsZW1zWzBdIHx8IGxhenlTaXplc0NmZy5wcmVsb2FkQWZ0ZXJMb2FkKSAmJlxuXHRcdFx0XHRcdFx0KHByZWxvYWRFbGVtc1swXSB8fCAoIWVsZW1FeHBhbmRWYWwgJiYgKChlTGJvdHRvbSB8fCBlTHJpZ2h0IHx8IGVMbGVmdCB8fCBlTHRvcCkgfHwgbGF6eWxvYWRFbGVtc1tpXVtfZ2V0QXR0cmlidXRlXShsYXp5U2l6ZXNDZmcuc2l6ZXNBdHRyKSAhPSAnYXV0bycpKSkpe1xuXHRcdFx0XHRcdFx0YXV0b0xvYWRFbGVtID0gcHJlbG9hZEVsZW1zWzBdIHx8IGxhenlsb2FkRWxlbXNbaV07XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0aWYoYXV0b0xvYWRFbGVtICYmICFsb2FkZWRTb21ldGhpbmcpe1xuXHRcdFx0XHRcdHVudmVpbEVsZW1lbnQoYXV0b0xvYWRFbGVtKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH07XG5cblx0XHR2YXIgdGhyb3R0bGVkQ2hlY2tFbGVtZW50cyA9IHRocm90dGxlKGNoZWNrRWxlbWVudHMpO1xuXG5cdFx0dmFyIHN3aXRjaExvYWRpbmdDbGFzcyA9IGZ1bmN0aW9uKGUpe1xuXHRcdFx0dmFyIGVsZW0gPSBlLnRhcmdldDtcblxuXHRcdFx0aWYgKGVsZW0uX2xhenlDYWNoZSkge1xuXHRcdFx0XHRkZWxldGUgZWxlbS5fbGF6eUNhY2hlO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdHJlc2V0UHJlbG9hZGluZyhlKTtcblx0XHRcdGFkZENsYXNzKGVsZW0sIGxhenlTaXplc0NmZy5sb2FkZWRDbGFzcyk7XG5cdFx0XHRyZW1vdmVDbGFzcyhlbGVtLCBsYXp5U2l6ZXNDZmcubG9hZGluZ0NsYXNzKTtcblx0XHRcdGFkZFJlbW92ZUxvYWRFdmVudHMoZWxlbSwgcmFmU3dpdGNoTG9hZGluZ0NsYXNzKTtcblx0XHRcdHRyaWdnZXJFdmVudChlbGVtLCAnbGF6eWxvYWRlZCcpO1xuXHRcdH07XG5cdFx0dmFyIHJhZmVkU3dpdGNoTG9hZGluZ0NsYXNzID0gckFGSXQoc3dpdGNoTG9hZGluZ0NsYXNzKTtcblx0XHR2YXIgcmFmU3dpdGNoTG9hZGluZ0NsYXNzID0gZnVuY3Rpb24oZSl7XG5cdFx0XHRyYWZlZFN3aXRjaExvYWRpbmdDbGFzcyh7dGFyZ2V0OiBlLnRhcmdldH0pO1xuXHRcdH07XG5cblx0XHR2YXIgY2hhbmdlSWZyYW1lU3JjID0gZnVuY3Rpb24oZWxlbSwgc3JjKXtcblx0XHRcdHRyeSB7XG5cdFx0XHRcdGVsZW0uY29udGVudFdpbmRvdy5sb2NhdGlvbi5yZXBsYWNlKHNyYyk7XG5cdFx0XHR9IGNhdGNoKGUpe1xuXHRcdFx0XHRlbGVtLnNyYyA9IHNyYztcblx0XHRcdH1cblx0XHR9O1xuXG5cdFx0dmFyIGhhbmRsZVNvdXJjZXMgPSBmdW5jdGlvbihzb3VyY2Upe1xuXHRcdFx0dmFyIGN1c3RvbU1lZGlhO1xuXG5cdFx0XHR2YXIgc291cmNlU3Jjc2V0ID0gc291cmNlW19nZXRBdHRyaWJ1dGVdKGxhenlTaXplc0NmZy5zcmNzZXRBdHRyKTtcblxuXHRcdFx0aWYoIChjdXN0b21NZWRpYSA9IGxhenlTaXplc0NmZy5jdXN0b21NZWRpYVtzb3VyY2VbX2dldEF0dHJpYnV0ZV0oJ2RhdGEtbWVkaWEnKSB8fCBzb3VyY2VbX2dldEF0dHJpYnV0ZV0oJ21lZGlhJyldKSApe1xuXHRcdFx0XHRzb3VyY2Uuc2V0QXR0cmlidXRlKCdtZWRpYScsIGN1c3RvbU1lZGlhKTtcblx0XHRcdH1cblxuXHRcdFx0aWYoc291cmNlU3Jjc2V0KXtcblx0XHRcdFx0c291cmNlLnNldEF0dHJpYnV0ZSgnc3Jjc2V0Jywgc291cmNlU3Jjc2V0KTtcblx0XHRcdH1cblx0XHR9O1xuXG5cdFx0dmFyIGxhenlVbnZlaWwgPSByQUZJdChmdW5jdGlvbiAoZWxlbSwgZGV0YWlsLCBpc0F1dG8sIHNpemVzLCBpc0ltZyl7XG5cdFx0XHR2YXIgc3JjLCBzcmNzZXQsIHBhcmVudCwgaXNQaWN0dXJlLCBldmVudCwgZmlyZXNMb2FkO1xuXG5cdFx0XHRpZighKGV2ZW50ID0gdHJpZ2dlckV2ZW50KGVsZW0sICdsYXp5YmVmb3JldW52ZWlsJywgZGV0YWlsKSkuZGVmYXVsdFByZXZlbnRlZCl7XG5cblx0XHRcdFx0aWYoc2l6ZXMpe1xuXHRcdFx0XHRcdGlmKGlzQXV0byl7XG5cdFx0XHRcdFx0XHRhZGRDbGFzcyhlbGVtLCBsYXp5U2l6ZXNDZmcuYXV0b3NpemVzQ2xhc3MpO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRlbGVtLnNldEF0dHJpYnV0ZSgnc2l6ZXMnLCBzaXplcyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0c3Jjc2V0ID0gZWxlbVtfZ2V0QXR0cmlidXRlXShsYXp5U2l6ZXNDZmcuc3Jjc2V0QXR0cik7XG5cdFx0XHRcdHNyYyA9IGVsZW1bX2dldEF0dHJpYnV0ZV0obGF6eVNpemVzQ2ZnLnNyY0F0dHIpO1xuXG5cdFx0XHRcdGlmKGlzSW1nKSB7XG5cdFx0XHRcdFx0cGFyZW50ID0gZWxlbS5wYXJlbnROb2RlO1xuXHRcdFx0XHRcdGlzUGljdHVyZSA9IHBhcmVudCAmJiByZWdQaWN0dXJlLnRlc3QocGFyZW50Lm5vZGVOYW1lIHx8ICcnKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGZpcmVzTG9hZCA9IGRldGFpbC5maXJlc0xvYWQgfHwgKCgnc3JjJyBpbiBlbGVtKSAmJiAoc3Jjc2V0IHx8IHNyYyB8fCBpc1BpY3R1cmUpKTtcblxuXHRcdFx0XHRldmVudCA9IHt0YXJnZXQ6IGVsZW19O1xuXG5cdFx0XHRcdGFkZENsYXNzKGVsZW0sIGxhenlTaXplc0NmZy5sb2FkaW5nQ2xhc3MpO1xuXG5cdFx0XHRcdGlmKGZpcmVzTG9hZCl7XG5cdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHJlc2V0UHJlbG9hZGluZ1RpbWVyKTtcblx0XHRcdFx0XHRyZXNldFByZWxvYWRpbmdUaW1lciA9IHNldFRpbWVvdXQocmVzZXRQcmVsb2FkaW5nLCAyNTAwKTtcblx0XHRcdFx0XHRhZGRSZW1vdmVMb2FkRXZlbnRzKGVsZW0sIHJhZlN3aXRjaExvYWRpbmdDbGFzcywgdHJ1ZSk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZihpc1BpY3R1cmUpe1xuXHRcdFx0XHRcdGZvckVhY2guY2FsbChwYXJlbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ3NvdXJjZScpLCBoYW5kbGVTb3VyY2VzKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKHNyY3NldCl7XG5cdFx0XHRcdFx0ZWxlbS5zZXRBdHRyaWJ1dGUoJ3NyY3NldCcsIHNyY3NldCk7XG5cdFx0XHRcdH0gZWxzZSBpZihzcmMgJiYgIWlzUGljdHVyZSl7XG5cdFx0XHRcdFx0aWYocmVnSWZyYW1lLnRlc3QoZWxlbS5ub2RlTmFtZSkpe1xuXHRcdFx0XHRcdFx0Y2hhbmdlSWZyYW1lU3JjKGVsZW0sIHNyYyk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdGVsZW0uc3JjID0gc3JjO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cdFx0XHRcdGlmKGlzSW1nICYmIChzcmNzZXQgfHwgaXNQaWN0dXJlKSl7XG5cdFx0XHRcdFx0dXBkYXRlUG9seWZpbGwoZWxlbSwge3NyYzogc3JjfSk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0aWYoZWxlbS5fbGF6eVJhY2Upe1xuXHRcdFx0XHRkZWxldGUgZWxlbS5fbGF6eVJhY2U7XG5cdFx0XHR9XG5cdFx0XHRyZW1vdmVDbGFzcyhlbGVtLCBsYXp5U2l6ZXNDZmcubGF6eUNsYXNzKTtcblxuXHRcdFx0ckFGKGZ1bmN0aW9uKCl7XG5cdFx0XHRcdC8vIFBhcnQgb2YgdGhpcyBjYW4gYmUgcmVtb3ZlZCBhcyBzb29uIGFzIHRoaXMgZml4IGlzIG9sZGVyOiBodHRwczovL2J1Z3MuY2hyb21pdW0ub3JnL3AvY2hyb21pdW0vaXNzdWVzL2RldGFpbD9pZD03NzMxICgyMDE1KVxuXHRcdFx0XHR2YXIgaXNMb2FkZWQgPSBlbGVtLmNvbXBsZXRlICYmIGVsZW0ubmF0dXJhbFdpZHRoID4gMTtcblxuXHRcdFx0XHRpZiggIWZpcmVzTG9hZCB8fCBpc0xvYWRlZCl7XG5cdFx0XHRcdFx0aWYgKGlzTG9hZGVkKSB7XG5cdFx0XHRcdFx0XHRhZGRDbGFzcyhlbGVtLCAnbHMtaXMtY2FjaGVkJyk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHN3aXRjaExvYWRpbmdDbGFzcyhldmVudCk7XG5cdFx0XHRcdFx0ZWxlbS5fbGF6eUNhY2hlID0gdHJ1ZTtcblx0XHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0XHRpZiAoJ19sYXp5Q2FjaGUnIGluIGVsZW0pIHtcblx0XHRcdFx0XHRcdFx0ZGVsZXRlIGVsZW0uX2xhenlDYWNoZTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9LCA5KTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoZWxlbS5sb2FkaW5nID09ICdsYXp5Jykge1xuXHRcdFx0XHRcdGlzTG9hZGluZy0tO1xuXHRcdFx0XHR9XG5cdFx0XHR9LCB0cnVlKTtcblx0XHR9KTtcblxuXHRcdHZhciB1bnZlaWxFbGVtZW50ID0gZnVuY3Rpb24gKGVsZW0pe1xuXHRcdFx0aWYgKGVsZW0uX2xhenlSYWNlKSB7cmV0dXJuO31cblx0XHRcdHZhciBkZXRhaWw7XG5cblx0XHRcdHZhciBpc0ltZyA9IHJlZ0ltZy50ZXN0KGVsZW0ubm9kZU5hbWUpO1xuXG5cdFx0XHQvL2FsbG93IHVzaW5nIHNpemVzPVwiYXV0b1wiLCBidXQgZG9uJ3QgdXNlLiBpdCdzIGludmFsaWQuIFVzZSBkYXRhLXNpemVzPVwiYXV0b1wiIG9yIGEgdmFsaWQgdmFsdWUgZm9yIHNpemVzIGluc3RlYWQgKGkuZS46IHNpemVzPVwiODB2d1wiKVxuXHRcdFx0dmFyIHNpemVzID0gaXNJbWcgJiYgKGVsZW1bX2dldEF0dHJpYnV0ZV0obGF6eVNpemVzQ2ZnLnNpemVzQXR0cikgfHwgZWxlbVtfZ2V0QXR0cmlidXRlXSgnc2l6ZXMnKSk7XG5cdFx0XHR2YXIgaXNBdXRvID0gc2l6ZXMgPT0gJ2F1dG8nO1xuXG5cdFx0XHRpZiggKGlzQXV0byB8fCAhaXNDb21wbGV0ZWQpICYmIGlzSW1nICYmIChlbGVtW19nZXRBdHRyaWJ1dGVdKCdzcmMnKSB8fCBlbGVtLnNyY3NldCkgJiYgIWVsZW0uY29tcGxldGUgJiYgIWhhc0NsYXNzKGVsZW0sIGxhenlTaXplc0NmZy5lcnJvckNsYXNzKSAmJiBoYXNDbGFzcyhlbGVtLCBsYXp5U2l6ZXNDZmcubGF6eUNsYXNzKSl7cmV0dXJuO31cblxuXHRcdFx0ZGV0YWlsID0gdHJpZ2dlckV2ZW50KGVsZW0sICdsYXp5dW52ZWlscmVhZCcpLmRldGFpbDtcblxuXHRcdFx0aWYoaXNBdXRvKXtcblx0XHRcdFx0IGF1dG9TaXplci51cGRhdGVFbGVtKGVsZW0sIHRydWUsIGVsZW0ub2Zmc2V0V2lkdGgpO1xuXHRcdFx0fVxuXG5cdFx0XHRlbGVtLl9sYXp5UmFjZSA9IHRydWU7XG5cdFx0XHRpc0xvYWRpbmcrKztcblxuXHRcdFx0bGF6eVVudmVpbChlbGVtLCBkZXRhaWwsIGlzQXV0bywgc2l6ZXMsIGlzSW1nKTtcblx0XHR9O1xuXG5cdFx0dmFyIGFmdGVyU2Nyb2xsID0gZGVib3VuY2UoZnVuY3Rpb24oKXtcblx0XHRcdGxhenlTaXplc0NmZy5sb2FkTW9kZSA9IDM7XG5cdFx0XHR0aHJvdHRsZWRDaGVja0VsZW1lbnRzKCk7XG5cdFx0fSk7XG5cblx0XHR2YXIgYWx0TG9hZG1vZGVTY3JvbGxMaXN0bmVyID0gZnVuY3Rpb24oKXtcblx0XHRcdGlmKGxhenlTaXplc0NmZy5sb2FkTW9kZSA9PSAzKXtcblx0XHRcdFx0bGF6eVNpemVzQ2ZnLmxvYWRNb2RlID0gMjtcblx0XHRcdH1cblx0XHRcdGFmdGVyU2Nyb2xsKCk7XG5cdFx0fTtcblxuXHRcdHZhciBvbmxvYWQgPSBmdW5jdGlvbigpe1xuXHRcdFx0aWYoaXNDb21wbGV0ZWQpe3JldHVybjt9XG5cdFx0XHRpZihEYXRlLm5vdygpIC0gc3RhcnRlZCA8IDk5OSl7XG5cdFx0XHRcdHNldFRpbWVvdXQob25sb2FkLCA5OTkpO1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblxuXHRcdFx0aXNDb21wbGV0ZWQgPSB0cnVlO1xuXG5cdFx0XHRsYXp5U2l6ZXNDZmcubG9hZE1vZGUgPSAzO1xuXG5cdFx0XHR0aHJvdHRsZWRDaGVja0VsZW1lbnRzKCk7XG5cblx0XHRcdGFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIGFsdExvYWRtb2RlU2Nyb2xsTGlzdG5lciwgdHJ1ZSk7XG5cdFx0fTtcblxuXHRcdHJldHVybiB7XG5cdFx0XHRfOiBmdW5jdGlvbigpe1xuXHRcdFx0XHRzdGFydGVkID0gRGF0ZS5ub3coKTtcblxuXHRcdFx0XHRsYXp5c2l6ZXMuZWxlbWVudHMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKGxhenlTaXplc0NmZy5sYXp5Q2xhc3MpO1xuXHRcdFx0XHRwcmVsb2FkRWxlbXMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKGxhenlTaXplc0NmZy5sYXp5Q2xhc3MgKyAnICcgKyBsYXp5U2l6ZXNDZmcucHJlbG9hZENsYXNzKTtcblxuXHRcdFx0XHRhZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aHJvdHRsZWRDaGVja0VsZW1lbnRzLCB0cnVlKTtcblxuXHRcdFx0XHRhZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aHJvdHRsZWRDaGVja0VsZW1lbnRzLCB0cnVlKTtcblxuXHRcdFx0XHRpZih3aW5kb3cuTXV0YXRpb25PYnNlcnZlcil7XG5cdFx0XHRcdFx0bmV3IE11dGF0aW9uT2JzZXJ2ZXIoIHRocm90dGxlZENoZWNrRWxlbWVudHMgKS5vYnNlcnZlKCBkb2NFbGVtLCB7Y2hpbGRMaXN0OiB0cnVlLCBzdWJ0cmVlOiB0cnVlLCBhdHRyaWJ1dGVzOiB0cnVlfSApO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdGRvY0VsZW1bX2FkZEV2ZW50TGlzdGVuZXJdKCdET01Ob2RlSW5zZXJ0ZWQnLCB0aHJvdHRsZWRDaGVja0VsZW1lbnRzLCB0cnVlKTtcblx0XHRcdFx0XHRkb2NFbGVtW19hZGRFdmVudExpc3RlbmVyXSgnRE9NQXR0ck1vZGlmaWVkJywgdGhyb3R0bGVkQ2hlY2tFbGVtZW50cywgdHJ1ZSk7XG5cdFx0XHRcdFx0c2V0SW50ZXJ2YWwodGhyb3R0bGVkQ2hlY2tFbGVtZW50cywgOTk5KTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGFkZEV2ZW50TGlzdGVuZXIoJ2hhc2hjaGFuZ2UnLCB0aHJvdHRsZWRDaGVja0VsZW1lbnRzLCB0cnVlKTtcblxuXHRcdFx0XHQvLywgJ2Z1bGxzY3JlZW5jaGFuZ2UnXG5cdFx0XHRcdFsnZm9jdXMnLCAnbW91c2VvdmVyJywgJ2NsaWNrJywgJ2xvYWQnLCAndHJhbnNpdGlvbmVuZCcsICdhbmltYXRpb25lbmQnXS5mb3JFYWNoKGZ1bmN0aW9uKG5hbWUpe1xuXHRcdFx0XHRcdGRvY3VtZW50W19hZGRFdmVudExpc3RlbmVyXShuYW1lLCB0aHJvdHRsZWRDaGVja0VsZW1lbnRzLCB0cnVlKTtcblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0aWYoKC9kJHxeYy8udGVzdChkb2N1bWVudC5yZWFkeVN0YXRlKSkpe1xuXHRcdFx0XHRcdG9ubG9hZCgpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdGFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBvbmxvYWQpO1xuXHRcdFx0XHRcdGRvY3VtZW50W19hZGRFdmVudExpc3RlbmVyXSgnRE9NQ29udGVudExvYWRlZCcsIHRocm90dGxlZENoZWNrRWxlbWVudHMpO1xuXHRcdFx0XHRcdHNldFRpbWVvdXQob25sb2FkLCAyMDAwMCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRpZihsYXp5c2l6ZXMuZWxlbWVudHMubGVuZ3RoKXtcblx0XHRcdFx0XHRjaGVja0VsZW1lbnRzKCk7XG5cdFx0XHRcdFx0ckFGLl9sc0ZsdXNoKCk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0dGhyb3R0bGVkQ2hlY2tFbGVtZW50cygpO1xuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0Y2hlY2tFbGVtczogdGhyb3R0bGVkQ2hlY2tFbGVtZW50cyxcblx0XHRcdHVudmVpbDogdW52ZWlsRWxlbWVudCxcblx0XHRcdF9hTFNMOiBhbHRMb2FkbW9kZVNjcm9sbExpc3RuZXIsXG5cdFx0fTtcblx0fSkoKTtcblxuXG5cdHZhciBhdXRvU2l6ZXIgPSAoZnVuY3Rpb24oKXtcblx0XHR2YXIgYXV0b3NpemVzRWxlbXM7XG5cblx0XHR2YXIgc2l6ZUVsZW1lbnQgPSByQUZJdChmdW5jdGlvbihlbGVtLCBwYXJlbnQsIGV2ZW50LCB3aWR0aCl7XG5cdFx0XHR2YXIgc291cmNlcywgaSwgbGVuO1xuXHRcdFx0ZWxlbS5fbGF6eXNpemVzV2lkdGggPSB3aWR0aDtcblx0XHRcdHdpZHRoICs9ICdweCc7XG5cblx0XHRcdGVsZW0uc2V0QXR0cmlidXRlKCdzaXplcycsIHdpZHRoKTtcblxuXHRcdFx0aWYocmVnUGljdHVyZS50ZXN0KHBhcmVudC5ub2RlTmFtZSB8fCAnJykpe1xuXHRcdFx0XHRzb3VyY2VzID0gcGFyZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdzb3VyY2UnKTtcblx0XHRcdFx0Zm9yKGkgPSAwLCBsZW4gPSBzb3VyY2VzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKXtcblx0XHRcdFx0XHRzb3VyY2VzW2ldLnNldEF0dHJpYnV0ZSgnc2l6ZXMnLCB3aWR0aCk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0aWYoIWV2ZW50LmRldGFpbC5kYXRhQXR0cil7XG5cdFx0XHRcdHVwZGF0ZVBvbHlmaWxsKGVsZW0sIGV2ZW50LmRldGFpbCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0dmFyIGdldFNpemVFbGVtZW50ID0gZnVuY3Rpb24gKGVsZW0sIGRhdGFBdHRyLCB3aWR0aCl7XG5cdFx0XHR2YXIgZXZlbnQ7XG5cdFx0XHR2YXIgcGFyZW50ID0gZWxlbS5wYXJlbnROb2RlO1xuXG5cdFx0XHRpZihwYXJlbnQpe1xuXHRcdFx0XHR3aWR0aCA9IGdldFdpZHRoKGVsZW0sIHBhcmVudCwgd2lkdGgpO1xuXHRcdFx0XHRldmVudCA9IHRyaWdnZXJFdmVudChlbGVtLCAnbGF6eWJlZm9yZXNpemVzJywge3dpZHRoOiB3aWR0aCwgZGF0YUF0dHI6ICEhZGF0YUF0dHJ9KTtcblxuXHRcdFx0XHRpZighZXZlbnQuZGVmYXVsdFByZXZlbnRlZCl7XG5cdFx0XHRcdFx0d2lkdGggPSBldmVudC5kZXRhaWwud2lkdGg7XG5cblx0XHRcdFx0XHRpZih3aWR0aCAmJiB3aWR0aCAhPT0gZWxlbS5fbGF6eXNpemVzV2lkdGgpe1xuXHRcdFx0XHRcdFx0c2l6ZUVsZW1lbnQoZWxlbSwgcGFyZW50LCBldmVudCwgd2lkdGgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH07XG5cblx0XHR2YXIgdXBkYXRlRWxlbWVudHNTaXplcyA9IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgaTtcblx0XHRcdHZhciBsZW4gPSBhdXRvc2l6ZXNFbGVtcy5sZW5ndGg7XG5cdFx0XHRpZihsZW4pe1xuXHRcdFx0XHRpID0gMDtcblxuXHRcdFx0XHRmb3IoOyBpIDwgbGVuOyBpKyspe1xuXHRcdFx0XHRcdGdldFNpemVFbGVtZW50KGF1dG9zaXplc0VsZW1zW2ldKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH07XG5cblx0XHR2YXIgZGVib3VuY2VkVXBkYXRlRWxlbWVudHNTaXplcyA9IGRlYm91bmNlKHVwZGF0ZUVsZW1lbnRzU2l6ZXMpO1xuXG5cdFx0cmV0dXJuIHtcblx0XHRcdF86IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdGF1dG9zaXplc0VsZW1zID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShsYXp5U2l6ZXNDZmcuYXV0b3NpemVzQ2xhc3MpO1xuXHRcdFx0XHRhZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCBkZWJvdW5jZWRVcGRhdGVFbGVtZW50c1NpemVzKTtcblx0XHRcdH0sXG5cdFx0XHRjaGVja0VsZW1zOiBkZWJvdW5jZWRVcGRhdGVFbGVtZW50c1NpemVzLFxuXHRcdFx0dXBkYXRlRWxlbTogZ2V0U2l6ZUVsZW1lbnRcblx0XHR9O1xuXHR9KSgpO1xuXG5cdHZhciBpbml0ID0gZnVuY3Rpb24oKXtcblx0XHRpZighaW5pdC5pKXtcblx0XHRcdGluaXQuaSA9IHRydWU7XG5cdFx0XHRhdXRvU2l6ZXIuXygpO1xuXHRcdFx0bG9hZGVyLl8oKTtcblx0XHR9XG5cdH07XG5cblx0bGF6eXNpemVzID0ge1xuXHRcdGNmZzogbGF6eVNpemVzQ2ZnLFxuXHRcdGF1dG9TaXplcjogYXV0b1NpemVyLFxuXHRcdGxvYWRlcjogbG9hZGVyLFxuXHRcdGluaXQ6IGluaXQsXG5cdFx0dVA6IHVwZGF0ZVBvbHlmaWxsLFxuXHRcdGFDOiBhZGRDbGFzcyxcblx0XHRyQzogcmVtb3ZlQ2xhc3MsXG5cdFx0aEM6IGhhc0NsYXNzLFxuXHRcdGZpcmU6IHRyaWdnZXJFdmVudCxcblx0XHRnVzogZ2V0V2lkdGgsXG5cdFx0ckFGOiByQUYsXG5cdH07XG5cblx0cmV0dXJuIGxhenlzaXplcztcbn1cbikpO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbGF6eXNpemVzL2xhenlzaXplcy5qc1xuLy8gbW9kdWxlIGlkID0gNDVcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL2xpYi9heGlvcycpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2F4aW9zL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSA0NlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKTtcbnZhciBiaW5kID0gcmVxdWlyZSgnLi9oZWxwZXJzL2JpbmQnKTtcbnZhciBBeGlvcyA9IHJlcXVpcmUoJy4vY29yZS9BeGlvcycpO1xudmFyIG1lcmdlQ29uZmlnID0gcmVxdWlyZSgnLi9jb3JlL21lcmdlQ29uZmlnJyk7XG52YXIgZGVmYXVsdHMgPSByZXF1aXJlKCcuL2RlZmF1bHRzJyk7XG5cbi8qKlxuICogQ3JlYXRlIGFuIGluc3RhbmNlIG9mIEF4aW9zXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IGRlZmF1bHRDb25maWcgVGhlIGRlZmF1bHQgY29uZmlnIGZvciB0aGUgaW5zdGFuY2VcbiAqIEByZXR1cm4ge0F4aW9zfSBBIG5ldyBpbnN0YW5jZSBvZiBBeGlvc1xuICovXG5mdW5jdGlvbiBjcmVhdGVJbnN0YW5jZShkZWZhdWx0Q29uZmlnKSB7XG4gIHZhciBjb250ZXh0ID0gbmV3IEF4aW9zKGRlZmF1bHRDb25maWcpO1xuICB2YXIgaW5zdGFuY2UgPSBiaW5kKEF4aW9zLnByb3RvdHlwZS5yZXF1ZXN0LCBjb250ZXh0KTtcblxuICAvLyBDb3B5IGF4aW9zLnByb3RvdHlwZSB0byBpbnN0YW5jZVxuICB1dGlscy5leHRlbmQoaW5zdGFuY2UsIEF4aW9zLnByb3RvdHlwZSwgY29udGV4dCk7XG5cbiAgLy8gQ29weSBjb250ZXh0IHRvIGluc3RhbmNlXG4gIHV0aWxzLmV4dGVuZChpbnN0YW5jZSwgY29udGV4dCk7XG5cbiAgcmV0dXJuIGluc3RhbmNlO1xufVxuXG4vLyBDcmVhdGUgdGhlIGRlZmF1bHQgaW5zdGFuY2UgdG8gYmUgZXhwb3J0ZWRcbnZhciBheGlvcyA9IGNyZWF0ZUluc3RhbmNlKGRlZmF1bHRzKTtcblxuLy8gRXhwb3NlIEF4aW9zIGNsYXNzIHRvIGFsbG93IGNsYXNzIGluaGVyaXRhbmNlXG5heGlvcy5BeGlvcyA9IEF4aW9zO1xuXG4vLyBGYWN0b3J5IGZvciBjcmVhdGluZyBuZXcgaW5zdGFuY2VzXG5heGlvcy5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUoaW5zdGFuY2VDb25maWcpIHtcbiAgcmV0dXJuIGNyZWF0ZUluc3RhbmNlKG1lcmdlQ29uZmlnKGF4aW9zLmRlZmF1bHRzLCBpbnN0YW5jZUNvbmZpZykpO1xufTtcblxuLy8gRXhwb3NlIENhbmNlbCAmIENhbmNlbFRva2VuXG5heGlvcy5DYW5jZWwgPSByZXF1aXJlKCcuL2NhbmNlbC9DYW5jZWwnKTtcbmF4aW9zLkNhbmNlbFRva2VuID0gcmVxdWlyZSgnLi9jYW5jZWwvQ2FuY2VsVG9rZW4nKTtcbmF4aW9zLmlzQ2FuY2VsID0gcmVxdWlyZSgnLi9jYW5jZWwvaXNDYW5jZWwnKTtcblxuLy8gRXhwb3NlIGFsbC9zcHJlYWRcbmF4aW9zLmFsbCA9IGZ1bmN0aW9uIGFsbChwcm9taXNlcykge1xuICByZXR1cm4gUHJvbWlzZS5hbGwocHJvbWlzZXMpO1xufTtcbmF4aW9zLnNwcmVhZCA9IHJlcXVpcmUoJy4vaGVscGVycy9zcHJlYWQnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBheGlvcztcblxuLy8gQWxsb3cgdXNlIG9mIGRlZmF1bHQgaW1wb3J0IHN5bnRheCBpbiBUeXBlU2NyaXB0XG5tb2R1bGUuZXhwb3J0cy5kZWZhdWx0ID0gYXhpb3M7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvYXhpb3MuanNcbi8vIG1vZHVsZSBpZCA9IDQ3XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi8uLi91dGlscycpO1xudmFyIGJ1aWxkVVJMID0gcmVxdWlyZSgnLi4vaGVscGVycy9idWlsZFVSTCcpO1xudmFyIEludGVyY2VwdG9yTWFuYWdlciA9IHJlcXVpcmUoJy4vSW50ZXJjZXB0b3JNYW5hZ2VyJyk7XG52YXIgZGlzcGF0Y2hSZXF1ZXN0ID0gcmVxdWlyZSgnLi9kaXNwYXRjaFJlcXVlc3QnKTtcbnZhciBtZXJnZUNvbmZpZyA9IHJlcXVpcmUoJy4vbWVyZ2VDb25maWcnKTtcblxuLyoqXG4gKiBDcmVhdGUgYSBuZXcgaW5zdGFuY2Ugb2YgQXhpb3NcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gaW5zdGFuY2VDb25maWcgVGhlIGRlZmF1bHQgY29uZmlnIGZvciB0aGUgaW5zdGFuY2VcbiAqL1xuZnVuY3Rpb24gQXhpb3MoaW5zdGFuY2VDb25maWcpIHtcbiAgdGhpcy5kZWZhdWx0cyA9IGluc3RhbmNlQ29uZmlnO1xuICB0aGlzLmludGVyY2VwdG9ycyA9IHtcbiAgICByZXF1ZXN0OiBuZXcgSW50ZXJjZXB0b3JNYW5hZ2VyKCksXG4gICAgcmVzcG9uc2U6IG5ldyBJbnRlcmNlcHRvck1hbmFnZXIoKVxuICB9O1xufVxuXG4vKipcbiAqIERpc3BhdGNoIGEgcmVxdWVzdFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBjb25maWcgVGhlIGNvbmZpZyBzcGVjaWZpYyBmb3IgdGhpcyByZXF1ZXN0IChtZXJnZWQgd2l0aCB0aGlzLmRlZmF1bHRzKVxuICovXG5BeGlvcy5wcm90b3R5cGUucmVxdWVzdCA9IGZ1bmN0aW9uIHJlcXVlc3QoY29uZmlnKSB7XG4gIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICAvLyBBbGxvdyBmb3IgYXhpb3MoJ2V4YW1wbGUvdXJsJ1ssIGNvbmZpZ10pIGEgbGEgZmV0Y2ggQVBJXG4gIGlmICh0eXBlb2YgY29uZmlnID09PSAnc3RyaW5nJykge1xuICAgIGNvbmZpZyA9IGFyZ3VtZW50c1sxXSB8fCB7fTtcbiAgICBjb25maWcudXJsID0gYXJndW1lbnRzWzBdO1xuICB9IGVsc2Uge1xuICAgIGNvbmZpZyA9IGNvbmZpZyB8fCB7fTtcbiAgfVxuXG4gIGNvbmZpZyA9IG1lcmdlQ29uZmlnKHRoaXMuZGVmYXVsdHMsIGNvbmZpZyk7XG5cbiAgLy8gU2V0IGNvbmZpZy5tZXRob2RcbiAgaWYgKGNvbmZpZy5tZXRob2QpIHtcbiAgICBjb25maWcubWV0aG9kID0gY29uZmlnLm1ldGhvZC50b0xvd2VyQ2FzZSgpO1xuICB9IGVsc2UgaWYgKHRoaXMuZGVmYXVsdHMubWV0aG9kKSB7XG4gICAgY29uZmlnLm1ldGhvZCA9IHRoaXMuZGVmYXVsdHMubWV0aG9kLnRvTG93ZXJDYXNlKCk7XG4gIH0gZWxzZSB7XG4gICAgY29uZmlnLm1ldGhvZCA9ICdnZXQnO1xuICB9XG5cbiAgLy8gSG9vayB1cCBpbnRlcmNlcHRvcnMgbWlkZGxld2FyZVxuICB2YXIgY2hhaW4gPSBbZGlzcGF0Y2hSZXF1ZXN0LCB1bmRlZmluZWRdO1xuICB2YXIgcHJvbWlzZSA9IFByb21pc2UucmVzb2x2ZShjb25maWcpO1xuXG4gIHRoaXMuaW50ZXJjZXB0b3JzLnJlcXVlc3QuZm9yRWFjaChmdW5jdGlvbiB1bnNoaWZ0UmVxdWVzdEludGVyY2VwdG9ycyhpbnRlcmNlcHRvcikge1xuICAgIGNoYWluLnVuc2hpZnQoaW50ZXJjZXB0b3IuZnVsZmlsbGVkLCBpbnRlcmNlcHRvci5yZWplY3RlZCk7XG4gIH0pO1xuXG4gIHRoaXMuaW50ZXJjZXB0b3JzLnJlc3BvbnNlLmZvckVhY2goZnVuY3Rpb24gcHVzaFJlc3BvbnNlSW50ZXJjZXB0b3JzKGludGVyY2VwdG9yKSB7XG4gICAgY2hhaW4ucHVzaChpbnRlcmNlcHRvci5mdWxmaWxsZWQsIGludGVyY2VwdG9yLnJlamVjdGVkKTtcbiAgfSk7XG5cbiAgd2hpbGUgKGNoYWluLmxlbmd0aCkge1xuICAgIHByb21pc2UgPSBwcm9taXNlLnRoZW4oY2hhaW4uc2hpZnQoKSwgY2hhaW4uc2hpZnQoKSk7XG4gIH1cblxuICByZXR1cm4gcHJvbWlzZTtcbn07XG5cbkF4aW9zLnByb3RvdHlwZS5nZXRVcmkgPSBmdW5jdGlvbiBnZXRVcmkoY29uZmlnKSB7XG4gIGNvbmZpZyA9IG1lcmdlQ29uZmlnKHRoaXMuZGVmYXVsdHMsIGNvbmZpZyk7XG4gIHJldHVybiBidWlsZFVSTChjb25maWcudXJsLCBjb25maWcucGFyYW1zLCBjb25maWcucGFyYW1zU2VyaWFsaXplcikucmVwbGFjZSgvXlxcPy8sICcnKTtcbn07XG5cbi8vIFByb3ZpZGUgYWxpYXNlcyBmb3Igc3VwcG9ydGVkIHJlcXVlc3QgbWV0aG9kc1xudXRpbHMuZm9yRWFjaChbJ2RlbGV0ZScsICdnZXQnLCAnaGVhZCcsICdvcHRpb25zJ10sIGZ1bmN0aW9uIGZvckVhY2hNZXRob2ROb0RhdGEobWV0aG9kKSB7XG4gIC8qZXNsaW50IGZ1bmMtbmFtZXM6MCovXG4gIEF4aW9zLnByb3RvdHlwZVttZXRob2RdID0gZnVuY3Rpb24odXJsLCBjb25maWcpIHtcbiAgICByZXR1cm4gdGhpcy5yZXF1ZXN0KHV0aWxzLm1lcmdlKGNvbmZpZyB8fCB7fSwge1xuICAgICAgbWV0aG9kOiBtZXRob2QsXG4gICAgICB1cmw6IHVybFxuICAgIH0pKTtcbiAgfTtcbn0pO1xuXG51dGlscy5mb3JFYWNoKFsncG9zdCcsICdwdXQnLCAncGF0Y2gnXSwgZnVuY3Rpb24gZm9yRWFjaE1ldGhvZFdpdGhEYXRhKG1ldGhvZCkge1xuICAvKmVzbGludCBmdW5jLW5hbWVzOjAqL1xuICBBeGlvcy5wcm90b3R5cGVbbWV0aG9kXSA9IGZ1bmN0aW9uKHVybCwgZGF0YSwgY29uZmlnKSB7XG4gICAgcmV0dXJuIHRoaXMucmVxdWVzdCh1dGlscy5tZXJnZShjb25maWcgfHwge30sIHtcbiAgICAgIG1ldGhvZDogbWV0aG9kLFxuICAgICAgdXJsOiB1cmwsXG4gICAgICBkYXRhOiBkYXRhXG4gICAgfSkpO1xuICB9O1xufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gQXhpb3M7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9BeGlvcy5qc1xuLy8gbW9kdWxlIGlkID0gNDhcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG5cbmZ1bmN0aW9uIEludGVyY2VwdG9yTWFuYWdlcigpIHtcbiAgdGhpcy5oYW5kbGVycyA9IFtdO1xufVxuXG4vKipcbiAqIEFkZCBhIG5ldyBpbnRlcmNlcHRvciB0byB0aGUgc3RhY2tcbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdWxmaWxsZWQgVGhlIGZ1bmN0aW9uIHRvIGhhbmRsZSBgdGhlbmAgZm9yIGEgYFByb21pc2VgXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSByZWplY3RlZCBUaGUgZnVuY3Rpb24gdG8gaGFuZGxlIGByZWplY3RgIGZvciBhIGBQcm9taXNlYFxuICpcbiAqIEByZXR1cm4ge051bWJlcn0gQW4gSUQgdXNlZCB0byByZW1vdmUgaW50ZXJjZXB0b3IgbGF0ZXJcbiAqL1xuSW50ZXJjZXB0b3JNYW5hZ2VyLnByb3RvdHlwZS51c2UgPSBmdW5jdGlvbiB1c2UoZnVsZmlsbGVkLCByZWplY3RlZCkge1xuICB0aGlzLmhhbmRsZXJzLnB1c2goe1xuICAgIGZ1bGZpbGxlZDogZnVsZmlsbGVkLFxuICAgIHJlamVjdGVkOiByZWplY3RlZFxuICB9KTtcbiAgcmV0dXJuIHRoaXMuaGFuZGxlcnMubGVuZ3RoIC0gMTtcbn07XG5cbi8qKlxuICogUmVtb3ZlIGFuIGludGVyY2VwdG9yIGZyb20gdGhlIHN0YWNrXG4gKlxuICogQHBhcmFtIHtOdW1iZXJ9IGlkIFRoZSBJRCB0aGF0IHdhcyByZXR1cm5lZCBieSBgdXNlYFxuICovXG5JbnRlcmNlcHRvck1hbmFnZXIucHJvdG90eXBlLmVqZWN0ID0gZnVuY3Rpb24gZWplY3QoaWQpIHtcbiAgaWYgKHRoaXMuaGFuZGxlcnNbaWRdKSB7XG4gICAgdGhpcy5oYW5kbGVyc1tpZF0gPSBudWxsO1xuICB9XG59O1xuXG4vKipcbiAqIEl0ZXJhdGUgb3ZlciBhbGwgdGhlIHJlZ2lzdGVyZWQgaW50ZXJjZXB0b3JzXG4gKlxuICogVGhpcyBtZXRob2QgaXMgcGFydGljdWxhcmx5IHVzZWZ1bCBmb3Igc2tpcHBpbmcgb3ZlciBhbnlcbiAqIGludGVyY2VwdG9ycyB0aGF0IG1heSBoYXZlIGJlY29tZSBgbnVsbGAgY2FsbGluZyBgZWplY3RgLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZuIFRoZSBmdW5jdGlvbiB0byBjYWxsIGZvciBlYWNoIGludGVyY2VwdG9yXG4gKi9cbkludGVyY2VwdG9yTWFuYWdlci5wcm90b3R5cGUuZm9yRWFjaCA9IGZ1bmN0aW9uIGZvckVhY2goZm4pIHtcbiAgdXRpbHMuZm9yRWFjaCh0aGlzLmhhbmRsZXJzLCBmdW5jdGlvbiBmb3JFYWNoSGFuZGxlcihoKSB7XG4gICAgaWYgKGggIT09IG51bGwpIHtcbiAgICAgIGZuKGgpO1xuICAgIH1cbiAgfSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEludGVyY2VwdG9yTWFuYWdlcjtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9jb3JlL0ludGVyY2VwdG9yTWFuYWdlci5qc1xuLy8gbW9kdWxlIGlkID0gNDlcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLy4uL3V0aWxzJyk7XG52YXIgdHJhbnNmb3JtRGF0YSA9IHJlcXVpcmUoJy4vdHJhbnNmb3JtRGF0YScpO1xudmFyIGlzQ2FuY2VsID0gcmVxdWlyZSgnLi4vY2FuY2VsL2lzQ2FuY2VsJyk7XG52YXIgZGVmYXVsdHMgPSByZXF1aXJlKCcuLi9kZWZhdWx0cycpO1xuXG4vKipcbiAqIFRocm93cyBhIGBDYW5jZWxgIGlmIGNhbmNlbGxhdGlvbiBoYXMgYmVlbiByZXF1ZXN0ZWQuXG4gKi9cbmZ1bmN0aW9uIHRocm93SWZDYW5jZWxsYXRpb25SZXF1ZXN0ZWQoY29uZmlnKSB7XG4gIGlmIChjb25maWcuY2FuY2VsVG9rZW4pIHtcbiAgICBjb25maWcuY2FuY2VsVG9rZW4udGhyb3dJZlJlcXVlc3RlZCgpO1xuICB9XG59XG5cbi8qKlxuICogRGlzcGF0Y2ggYSByZXF1ZXN0IHRvIHRoZSBzZXJ2ZXIgdXNpbmcgdGhlIGNvbmZpZ3VyZWQgYWRhcHRlci5cbiAqXG4gKiBAcGFyYW0ge29iamVjdH0gY29uZmlnIFRoZSBjb25maWcgdGhhdCBpcyB0byBiZSB1c2VkIGZvciB0aGUgcmVxdWVzdFxuICogQHJldHVybnMge1Byb21pc2V9IFRoZSBQcm9taXNlIHRvIGJlIGZ1bGZpbGxlZFxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGRpc3BhdGNoUmVxdWVzdChjb25maWcpIHtcbiAgdGhyb3dJZkNhbmNlbGxhdGlvblJlcXVlc3RlZChjb25maWcpO1xuXG4gIC8vIEVuc3VyZSBoZWFkZXJzIGV4aXN0XG4gIGNvbmZpZy5oZWFkZXJzID0gY29uZmlnLmhlYWRlcnMgfHwge307XG5cbiAgLy8gVHJhbnNmb3JtIHJlcXVlc3QgZGF0YVxuICBjb25maWcuZGF0YSA9IHRyYW5zZm9ybURhdGEoXG4gICAgY29uZmlnLmRhdGEsXG4gICAgY29uZmlnLmhlYWRlcnMsXG4gICAgY29uZmlnLnRyYW5zZm9ybVJlcXVlc3RcbiAgKTtcblxuICAvLyBGbGF0dGVuIGhlYWRlcnNcbiAgY29uZmlnLmhlYWRlcnMgPSB1dGlscy5tZXJnZShcbiAgICBjb25maWcuaGVhZGVycy5jb21tb24gfHwge30sXG4gICAgY29uZmlnLmhlYWRlcnNbY29uZmlnLm1ldGhvZF0gfHwge30sXG4gICAgY29uZmlnLmhlYWRlcnNcbiAgKTtcblxuICB1dGlscy5mb3JFYWNoKFxuICAgIFsnZGVsZXRlJywgJ2dldCcsICdoZWFkJywgJ3Bvc3QnLCAncHV0JywgJ3BhdGNoJywgJ2NvbW1vbiddLFxuICAgIGZ1bmN0aW9uIGNsZWFuSGVhZGVyQ29uZmlnKG1ldGhvZCkge1xuICAgICAgZGVsZXRlIGNvbmZpZy5oZWFkZXJzW21ldGhvZF07XG4gICAgfVxuICApO1xuXG4gIHZhciBhZGFwdGVyID0gY29uZmlnLmFkYXB0ZXIgfHwgZGVmYXVsdHMuYWRhcHRlcjtcblxuICByZXR1cm4gYWRhcHRlcihjb25maWcpLnRoZW4oZnVuY3Rpb24gb25BZGFwdGVyUmVzb2x1dGlvbihyZXNwb25zZSkge1xuICAgIHRocm93SWZDYW5jZWxsYXRpb25SZXF1ZXN0ZWQoY29uZmlnKTtcblxuICAgIC8vIFRyYW5zZm9ybSByZXNwb25zZSBkYXRhXG4gICAgcmVzcG9uc2UuZGF0YSA9IHRyYW5zZm9ybURhdGEoXG4gICAgICByZXNwb25zZS5kYXRhLFxuICAgICAgcmVzcG9uc2UuaGVhZGVycyxcbiAgICAgIGNvbmZpZy50cmFuc2Zvcm1SZXNwb25zZVxuICAgICk7XG5cbiAgICByZXR1cm4gcmVzcG9uc2U7XG4gIH0sIGZ1bmN0aW9uIG9uQWRhcHRlclJlamVjdGlvbihyZWFzb24pIHtcbiAgICBpZiAoIWlzQ2FuY2VsKHJlYXNvbikpIHtcbiAgICAgIHRocm93SWZDYW5jZWxsYXRpb25SZXF1ZXN0ZWQoY29uZmlnKTtcblxuICAgICAgLy8gVHJhbnNmb3JtIHJlc3BvbnNlIGRhdGFcbiAgICAgIGlmIChyZWFzb24gJiYgcmVhc29uLnJlc3BvbnNlKSB7XG4gICAgICAgIHJlYXNvbi5yZXNwb25zZS5kYXRhID0gdHJhbnNmb3JtRGF0YShcbiAgICAgICAgICByZWFzb24ucmVzcG9uc2UuZGF0YSxcbiAgICAgICAgICByZWFzb24ucmVzcG9uc2UuaGVhZGVycyxcbiAgICAgICAgICBjb25maWcudHJhbnNmb3JtUmVzcG9uc2VcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QocmVhc29uKTtcbiAgfSk7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2NvcmUvZGlzcGF0Y2hSZXF1ZXN0LmpzXG4vLyBtb2R1bGUgaWQgPSA1MFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxuLyoqXG4gKiBUcmFuc2Zvcm0gdGhlIGRhdGEgZm9yIGEgcmVxdWVzdCBvciBhIHJlc3BvbnNlXG4gKlxuICogQHBhcmFtIHtPYmplY3R8U3RyaW5nfSBkYXRhIFRoZSBkYXRhIHRvIGJlIHRyYW5zZm9ybWVkXG4gKiBAcGFyYW0ge0FycmF5fSBoZWFkZXJzIFRoZSBoZWFkZXJzIGZvciB0aGUgcmVxdWVzdCBvciByZXNwb25zZVxuICogQHBhcmFtIHtBcnJheXxGdW5jdGlvbn0gZm5zIEEgc2luZ2xlIGZ1bmN0aW9uIG9yIEFycmF5IG9mIGZ1bmN0aW9uc1xuICogQHJldHVybnMgeyp9IFRoZSByZXN1bHRpbmcgdHJhbnNmb3JtZWQgZGF0YVxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHRyYW5zZm9ybURhdGEoZGF0YSwgaGVhZGVycywgZm5zKSB7XG4gIC8qZXNsaW50IG5vLXBhcmFtLXJlYXNzaWduOjAqL1xuICB1dGlscy5mb3JFYWNoKGZucywgZnVuY3Rpb24gdHJhbnNmb3JtKGZuKSB7XG4gICAgZGF0YSA9IGZuKGRhdGEsIGhlYWRlcnMpO1xuICB9KTtcblxuICByZXR1cm4gZGF0YTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS90cmFuc2Zvcm1EYXRhLmpzXG4vLyBtb2R1bGUgaWQgPSA1MVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIvLyBzaGltIGZvciB1c2luZyBwcm9jZXNzIGluIGJyb3dzZXJcbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcblxuLy8gY2FjaGVkIGZyb20gd2hhdGV2ZXIgZ2xvYmFsIGlzIHByZXNlbnQgc28gdGhhdCB0ZXN0IHJ1bm5lcnMgdGhhdCBzdHViIGl0XG4vLyBkb24ndCBicmVhayB0aGluZ3MuICBCdXQgd2UgbmVlZCB0byB3cmFwIGl0IGluIGEgdHJ5IGNhdGNoIGluIGNhc2UgaXQgaXNcbi8vIHdyYXBwZWQgaW4gc3RyaWN0IG1vZGUgY29kZSB3aGljaCBkb2Vzbid0IGRlZmluZSBhbnkgZ2xvYmFscy4gIEl0J3MgaW5zaWRlIGFcbi8vIGZ1bmN0aW9uIGJlY2F1c2UgdHJ5L2NhdGNoZXMgZGVvcHRpbWl6ZSBpbiBjZXJ0YWluIGVuZ2luZXMuXG5cbnZhciBjYWNoZWRTZXRUaW1lb3V0O1xudmFyIGNhY2hlZENsZWFyVGltZW91dDtcblxuZnVuY3Rpb24gZGVmYXVsdFNldFRpbW91dCgpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3NldFRpbWVvdXQgaGFzIG5vdCBiZWVuIGRlZmluZWQnKTtcbn1cbmZ1bmN0aW9uIGRlZmF1bHRDbGVhclRpbWVvdXQgKCkge1xuICAgIHRocm93IG5ldyBFcnJvcignY2xlYXJUaW1lb3V0IGhhcyBub3QgYmVlbiBkZWZpbmVkJyk7XG59XG4oZnVuY3Rpb24gKCkge1xuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2Ygc2V0VGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gZGVmYXVsdFNldFRpbW91dDtcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2YgY2xlYXJUaW1lb3V0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBjbGVhclRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgIH1cbn0gKCkpXG5mdW5jdGlvbiBydW5UaW1lb3V0KGZ1bikge1xuICAgIGlmIChjYWNoZWRTZXRUaW1lb3V0ID09PSBzZXRUaW1lb3V0KSB7XG4gICAgICAgIC8vbm9ybWFsIGVudmlyb21lbnRzIGluIHNhbmUgc2l0dWF0aW9uc1xuICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xuICAgIH1cbiAgICAvLyBpZiBzZXRUaW1lb3V0IHdhc24ndCBhdmFpbGFibGUgYnV0IHdhcyBsYXR0ZXIgZGVmaW5lZFxuICAgIGlmICgoY2FjaGVkU2V0VGltZW91dCA9PT0gZGVmYXVsdFNldFRpbW91dCB8fCAhY2FjaGVkU2V0VGltZW91dCkgJiYgc2V0VGltZW91dCkge1xuICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gc2V0VGltZW91dDtcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gd2hlbiB3aGVuIHNvbWVib2R5IGhhcyBzY3Jld2VkIHdpdGggc2V0VGltZW91dCBidXQgbm8gSS5FLiBtYWRkbmVzc1xuICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dChmdW4sIDApO1xuICAgIH0gY2F0Y2goZSl7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0LmNhbGwobnVsbCwgZnVuLCAwKTtcbiAgICAgICAgfSBjYXRjaChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yXG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKHRoaXMsIGZ1biwgMCk7XG4gICAgICAgIH1cbiAgICB9XG5cblxufVxuZnVuY3Rpb24gcnVuQ2xlYXJUaW1lb3V0KG1hcmtlcikge1xuICAgIGlmIChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGNsZWFyVGltZW91dCkge1xuICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICAvLyBpZiBjbGVhclRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXG4gICAgaWYgKChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGRlZmF1bHRDbGVhclRpbWVvdXQgfHwgIWNhY2hlZENsZWFyVGltZW91dCkgJiYgY2xlYXJUaW1lb3V0KSB7XG4gICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGNsZWFyVGltZW91dDtcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXG4gICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIC8vIFdoZW4gd2UgYXJlIGluIEkuRS4gYnV0IHRoZSBzY3JpcHQgaGFzIGJlZW4gZXZhbGVkIHNvIEkuRS4gZG9lc24ndCAgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQuY2FsbChudWxsLCBtYXJrZXIpO1xuICAgICAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yLlxuICAgICAgICAgICAgLy8gU29tZSB2ZXJzaW9ucyBvZiBJLkUuIGhhdmUgZGlmZmVyZW50IHJ1bGVzIGZvciBjbGVhclRpbWVvdXQgdnMgc2V0VGltZW91dFxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKHRoaXMsIG1hcmtlcik7XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG59XG52YXIgcXVldWUgPSBbXTtcbnZhciBkcmFpbmluZyA9IGZhbHNlO1xudmFyIGN1cnJlbnRRdWV1ZTtcbnZhciBxdWV1ZUluZGV4ID0gLTE7XG5cbmZ1bmN0aW9uIGNsZWFuVXBOZXh0VGljaygpIHtcbiAgICBpZiAoIWRyYWluaW5nIHx8ICFjdXJyZW50UXVldWUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIGlmIChjdXJyZW50UXVldWUubGVuZ3RoKSB7XG4gICAgICAgIHF1ZXVlID0gY3VycmVudFF1ZXVlLmNvbmNhdChxdWV1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgIH1cbiAgICBpZiAocXVldWUubGVuZ3RoKSB7XG4gICAgICAgIGRyYWluUXVldWUoKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGRyYWluUXVldWUoKSB7XG4gICAgaWYgKGRyYWluaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIHRpbWVvdXQgPSBydW5UaW1lb3V0KGNsZWFuVXBOZXh0VGljayk7XG4gICAgZHJhaW5pbmcgPSB0cnVlO1xuXG4gICAgdmFyIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB3aGlsZShsZW4pIHtcbiAgICAgICAgY3VycmVudFF1ZXVlID0gcXVldWU7XG4gICAgICAgIHF1ZXVlID0gW107XG4gICAgICAgIHdoaWxlICgrK3F1ZXVlSW5kZXggPCBsZW4pIHtcbiAgICAgICAgICAgIGlmIChjdXJyZW50UXVldWUpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50UXVldWVbcXVldWVJbmRleF0ucnVuKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgICAgICBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgfVxuICAgIGN1cnJlbnRRdWV1ZSA9IG51bGw7XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBydW5DbGVhclRpbWVvdXQodGltZW91dCk7XG59XG5cbnByb2Nlc3MubmV4dFRpY2sgPSBmdW5jdGlvbiAoZnVuKSB7XG4gICAgdmFyIGFyZ3MgPSBuZXcgQXJyYXkoYXJndW1lbnRzLmxlbmd0aCAtIDEpO1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgYXJnc1tpIC0gMV0gPSBhcmd1bWVudHNbaV07XG4gICAgICAgIH1cbiAgICB9XG4gICAgcXVldWUucHVzaChuZXcgSXRlbShmdW4sIGFyZ3MpKTtcbiAgICBpZiAocXVldWUubGVuZ3RoID09PSAxICYmICFkcmFpbmluZykge1xuICAgICAgICBydW5UaW1lb3V0KGRyYWluUXVldWUpO1xuICAgIH1cbn07XG5cbi8vIHY4IGxpa2VzIHByZWRpY3RpYmxlIG9iamVjdHNcbmZ1bmN0aW9uIEl0ZW0oZnVuLCBhcnJheSkge1xuICAgIHRoaXMuZnVuID0gZnVuO1xuICAgIHRoaXMuYXJyYXkgPSBhcnJheTtcbn1cbkl0ZW0ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmZ1bi5hcHBseShudWxsLCB0aGlzLmFycmF5KTtcbn07XG5wcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xucHJvY2Vzcy5icm93c2VyID0gdHJ1ZTtcbnByb2Nlc3MuZW52ID0ge307XG5wcm9jZXNzLmFyZ3YgPSBbXTtcbnByb2Nlc3MudmVyc2lvbiA9ICcnOyAvLyBlbXB0eSBzdHJpbmcgdG8gYXZvaWQgcmVnZXhwIGlzc3Vlc1xucHJvY2Vzcy52ZXJzaW9ucyA9IHt9O1xuXG5mdW5jdGlvbiBub29wKCkge31cblxucHJvY2Vzcy5vbiA9IG5vb3A7XG5wcm9jZXNzLmFkZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3Mub25jZSA9IG5vb3A7XG5wcm9jZXNzLm9mZiA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUxpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlQWxsTGlzdGVuZXJzID0gbm9vcDtcbnByb2Nlc3MuZW1pdCA9IG5vb3A7XG5wcm9jZXNzLnByZXBlbmRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnByZXBlbmRPbmNlTGlzdGVuZXIgPSBub29wO1xuXG5wcm9jZXNzLmxpc3RlbmVycyA9IGZ1bmN0aW9uIChuYW1lKSB7IHJldHVybiBbXSB9XG5cbnByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmJpbmRpbmcgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcblxucHJvY2Vzcy5jd2QgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnLycgfTtcbnByb2Nlc3MuY2hkaXIgPSBmdW5jdGlvbiAoZGlyKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5wcm9jZXNzLnVtYXNrID0gZnVuY3Rpb24oKSB7IHJldHVybiAwOyB9O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcHJvY2Vzcy9icm93c2VyLmpzXG4vLyBtb2R1bGUgaWQgPSA1MlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4uL3V0aWxzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gbm9ybWFsaXplSGVhZGVyTmFtZShoZWFkZXJzLCBub3JtYWxpemVkTmFtZSkge1xuICB1dGlscy5mb3JFYWNoKGhlYWRlcnMsIGZ1bmN0aW9uIHByb2Nlc3NIZWFkZXIodmFsdWUsIG5hbWUpIHtcbiAgICBpZiAobmFtZSAhPT0gbm9ybWFsaXplZE5hbWUgJiYgbmFtZS50b1VwcGVyQ2FzZSgpID09PSBub3JtYWxpemVkTmFtZS50b1VwcGVyQ2FzZSgpKSB7XG4gICAgICBoZWFkZXJzW25vcm1hbGl6ZWROYW1lXSA9IHZhbHVlO1xuICAgICAgZGVsZXRlIGhlYWRlcnNbbmFtZV07XG4gICAgfVxuICB9KTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9ub3JtYWxpemVIZWFkZXJOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSA1M1xuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbnZhciBjcmVhdGVFcnJvciA9IHJlcXVpcmUoJy4vY3JlYXRlRXJyb3InKTtcblxuLyoqXG4gKiBSZXNvbHZlIG9yIHJlamVjdCBhIFByb21pc2UgYmFzZWQgb24gcmVzcG9uc2Ugc3RhdHVzLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHJlc29sdmUgQSBmdW5jdGlvbiB0aGF0IHJlc29sdmVzIHRoZSBwcm9taXNlLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gcmVqZWN0IEEgZnVuY3Rpb24gdGhhdCByZWplY3RzIHRoZSBwcm9taXNlLlxuICogQHBhcmFtIHtvYmplY3R9IHJlc3BvbnNlIFRoZSByZXNwb25zZS5cbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCByZXNwb25zZSkge1xuICB2YXIgdmFsaWRhdGVTdGF0dXMgPSByZXNwb25zZS5jb25maWcudmFsaWRhdGVTdGF0dXM7XG4gIGlmICghdmFsaWRhdGVTdGF0dXMgfHwgdmFsaWRhdGVTdGF0dXMocmVzcG9uc2Uuc3RhdHVzKSkge1xuICAgIHJlc29sdmUocmVzcG9uc2UpO1xuICB9IGVsc2Uge1xuICAgIHJlamVjdChjcmVhdGVFcnJvcihcbiAgICAgICdSZXF1ZXN0IGZhaWxlZCB3aXRoIHN0YXR1cyBjb2RlICcgKyByZXNwb25zZS5zdGF0dXMsXG4gICAgICByZXNwb25zZS5jb25maWcsXG4gICAgICBudWxsLFxuICAgICAgcmVzcG9uc2UucmVxdWVzdCxcbiAgICAgIHJlc3BvbnNlXG4gICAgKSk7XG4gIH1cbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9zZXR0bGUuanNcbi8vIG1vZHVsZSBpZCA9IDU0XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBVcGRhdGUgYW4gRXJyb3Igd2l0aCB0aGUgc3BlY2lmaWVkIGNvbmZpZywgZXJyb3IgY29kZSwgYW5kIHJlc3BvbnNlLlxuICpcbiAqIEBwYXJhbSB7RXJyb3J9IGVycm9yIFRoZSBlcnJvciB0byB1cGRhdGUuXG4gKiBAcGFyYW0ge09iamVjdH0gY29uZmlnIFRoZSBjb25maWcuXG4gKiBAcGFyYW0ge3N0cmluZ30gW2NvZGVdIFRoZSBlcnJvciBjb2RlIChmb3IgZXhhbXBsZSwgJ0VDT05OQUJPUlRFRCcpLlxuICogQHBhcmFtIHtPYmplY3R9IFtyZXF1ZXN0XSBUaGUgcmVxdWVzdC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbcmVzcG9uc2VdIFRoZSByZXNwb25zZS5cbiAqIEByZXR1cm5zIHtFcnJvcn0gVGhlIGVycm9yLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGVuaGFuY2VFcnJvcihlcnJvciwgY29uZmlnLCBjb2RlLCByZXF1ZXN0LCByZXNwb25zZSkge1xuICBlcnJvci5jb25maWcgPSBjb25maWc7XG4gIGlmIChjb2RlKSB7XG4gICAgZXJyb3IuY29kZSA9IGNvZGU7XG4gIH1cblxuICBlcnJvci5yZXF1ZXN0ID0gcmVxdWVzdDtcbiAgZXJyb3IucmVzcG9uc2UgPSByZXNwb25zZTtcbiAgZXJyb3IuaXNBeGlvc0Vycm9yID0gdHJ1ZTtcblxuICBlcnJvci50b0pTT04gPSBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgLy8gU3RhbmRhcmRcbiAgICAgIG1lc3NhZ2U6IHRoaXMubWVzc2FnZSxcbiAgICAgIG5hbWU6IHRoaXMubmFtZSxcbiAgICAgIC8vIE1pY3Jvc29mdFxuICAgICAgZGVzY3JpcHRpb246IHRoaXMuZGVzY3JpcHRpb24sXG4gICAgICBudW1iZXI6IHRoaXMubnVtYmVyLFxuICAgICAgLy8gTW96aWxsYVxuICAgICAgZmlsZU5hbWU6IHRoaXMuZmlsZU5hbWUsXG4gICAgICBsaW5lTnVtYmVyOiB0aGlzLmxpbmVOdW1iZXIsXG4gICAgICBjb2x1bW5OdW1iZXI6IHRoaXMuY29sdW1uTnVtYmVyLFxuICAgICAgc3RhY2s6IHRoaXMuc3RhY2ssXG4gICAgICAvLyBBeGlvc1xuICAgICAgY29uZmlnOiB0aGlzLmNvbmZpZyxcbiAgICAgIGNvZGU6IHRoaXMuY29kZVxuICAgIH07XG4gIH07XG4gIHJldHVybiBlcnJvcjtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9lbmhhbmNlRXJyb3IuanNcbi8vIG1vZHVsZSBpZCA9IDU1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxudmFyIGlzQWJzb2x1dGVVUkwgPSByZXF1aXJlKCcuLi9oZWxwZXJzL2lzQWJzb2x1dGVVUkwnKTtcbnZhciBjb21iaW5lVVJMcyA9IHJlcXVpcmUoJy4uL2hlbHBlcnMvY29tYmluZVVSTHMnKTtcblxuLyoqXG4gKiBDcmVhdGVzIGEgbmV3IFVSTCBieSBjb21iaW5pbmcgdGhlIGJhc2VVUkwgd2l0aCB0aGUgcmVxdWVzdGVkVVJMLFxuICogb25seSB3aGVuIHRoZSByZXF1ZXN0ZWRVUkwgaXMgbm90IGFscmVhZHkgYW4gYWJzb2x1dGUgVVJMLlxuICogSWYgdGhlIHJlcXVlc3RVUkwgaXMgYWJzb2x1dGUsIHRoaXMgZnVuY3Rpb24gcmV0dXJucyB0aGUgcmVxdWVzdGVkVVJMIHVudG91Y2hlZC5cbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gYmFzZVVSTCBUaGUgYmFzZSBVUkxcbiAqIEBwYXJhbSB7c3RyaW5nfSByZXF1ZXN0ZWRVUkwgQWJzb2x1dGUgb3IgcmVsYXRpdmUgVVJMIHRvIGNvbWJpbmVcbiAqIEByZXR1cm5zIHtzdHJpbmd9IFRoZSBjb21iaW5lZCBmdWxsIHBhdGhcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBidWlsZEZ1bGxQYXRoKGJhc2VVUkwsIHJlcXVlc3RlZFVSTCkge1xuICBpZiAoYmFzZVVSTCAmJiAhaXNBYnNvbHV0ZVVSTChyZXF1ZXN0ZWRVUkwpKSB7XG4gICAgcmV0dXJuIGNvbWJpbmVVUkxzKGJhc2VVUkwsIHJlcXVlc3RlZFVSTCk7XG4gIH1cbiAgcmV0dXJuIHJlcXVlc3RlZFVSTDtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY29yZS9idWlsZEZ1bGxQYXRoLmpzXG4vLyBtb2R1bGUgaWQgPSA1NlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogRGV0ZXJtaW5lcyB3aGV0aGVyIHRoZSBzcGVjaWZpZWQgVVJMIGlzIGFic29sdXRlXG4gKlxuICogQHBhcmFtIHtzdHJpbmd9IHVybCBUaGUgVVJMIHRvIHRlc3RcbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHRoZSBzcGVjaWZpZWQgVVJMIGlzIGFic29sdXRlLCBvdGhlcndpc2UgZmFsc2VcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc0Fic29sdXRlVVJMKHVybCkge1xuICAvLyBBIFVSTCBpcyBjb25zaWRlcmVkIGFic29sdXRlIGlmIGl0IGJlZ2lucyB3aXRoIFwiPHNjaGVtZT46Ly9cIiBvciBcIi8vXCIgKHByb3RvY29sLXJlbGF0aXZlIFVSTCkuXG4gIC8vIFJGQyAzOTg2IGRlZmluZXMgc2NoZW1lIG5hbWUgYXMgYSBzZXF1ZW5jZSBvZiBjaGFyYWN0ZXJzIGJlZ2lubmluZyB3aXRoIGEgbGV0dGVyIGFuZCBmb2xsb3dlZFxuICAvLyBieSBhbnkgY29tYmluYXRpb24gb2YgbGV0dGVycywgZGlnaXRzLCBwbHVzLCBwZXJpb2QsIG9yIGh5cGhlbi5cbiAgcmV0dXJuIC9eKFthLXpdW2EtelxcZFxcK1xcLVxcLl0qOik/XFwvXFwvL2kudGVzdCh1cmwpO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2lzQWJzb2x1dGVVUkwuanNcbi8vIG1vZHVsZSBpZCA9IDU3XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDcmVhdGVzIGEgbmV3IFVSTCBieSBjb21iaW5pbmcgdGhlIHNwZWNpZmllZCBVUkxzXG4gKlxuICogQHBhcmFtIHtzdHJpbmd9IGJhc2VVUkwgVGhlIGJhc2UgVVJMXG4gKiBAcGFyYW0ge3N0cmluZ30gcmVsYXRpdmVVUkwgVGhlIHJlbGF0aXZlIFVSTFxuICogQHJldHVybnMge3N0cmluZ30gVGhlIGNvbWJpbmVkIFVSTFxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGNvbWJpbmVVUkxzKGJhc2VVUkwsIHJlbGF0aXZlVVJMKSB7XG4gIHJldHVybiByZWxhdGl2ZVVSTFxuICAgID8gYmFzZVVSTC5yZXBsYWNlKC9cXC8rJC8sICcnKSArICcvJyArIHJlbGF0aXZlVVJMLnJlcGxhY2UoL15cXC8rLywgJycpXG4gICAgOiBiYXNlVVJMO1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2NvbWJpbmVVUkxzLmpzXG4vLyBtb2R1bGUgaWQgPSA1OFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxuLy8gSGVhZGVycyB3aG9zZSBkdXBsaWNhdGVzIGFyZSBpZ25vcmVkIGJ5IG5vZGVcbi8vIGMuZi4gaHR0cHM6Ly9ub2RlanMub3JnL2FwaS9odHRwLmh0bWwjaHR0cF9tZXNzYWdlX2hlYWRlcnNcbnZhciBpZ25vcmVEdXBsaWNhdGVPZiA9IFtcbiAgJ2FnZScsICdhdXRob3JpemF0aW9uJywgJ2NvbnRlbnQtbGVuZ3RoJywgJ2NvbnRlbnQtdHlwZScsICdldGFnJyxcbiAgJ2V4cGlyZXMnLCAnZnJvbScsICdob3N0JywgJ2lmLW1vZGlmaWVkLXNpbmNlJywgJ2lmLXVubW9kaWZpZWQtc2luY2UnLFxuICAnbGFzdC1tb2RpZmllZCcsICdsb2NhdGlvbicsICdtYXgtZm9yd2FyZHMnLCAncHJveHktYXV0aG9yaXphdGlvbicsXG4gICdyZWZlcmVyJywgJ3JldHJ5LWFmdGVyJywgJ3VzZXItYWdlbnQnXG5dO1xuXG4vKipcbiAqIFBhcnNlIGhlYWRlcnMgaW50byBhbiBvYmplY3RcbiAqXG4gKiBgYGBcbiAqIERhdGU6IFdlZCwgMjcgQXVnIDIwMTQgMDg6NTg6NDkgR01UXG4gKiBDb250ZW50LVR5cGU6IGFwcGxpY2F0aW9uL2pzb25cbiAqIENvbm5lY3Rpb246IGtlZXAtYWxpdmVcbiAqIFRyYW5zZmVyLUVuY29kaW5nOiBjaHVua2VkXG4gKiBgYGBcbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gaGVhZGVycyBIZWFkZXJzIG5lZWRpbmcgdG8gYmUgcGFyc2VkXG4gKiBAcmV0dXJucyB7T2JqZWN0fSBIZWFkZXJzIHBhcnNlZCBpbnRvIGFuIG9iamVjdFxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHBhcnNlSGVhZGVycyhoZWFkZXJzKSB7XG4gIHZhciBwYXJzZWQgPSB7fTtcbiAgdmFyIGtleTtcbiAgdmFyIHZhbDtcbiAgdmFyIGk7XG5cbiAgaWYgKCFoZWFkZXJzKSB7IHJldHVybiBwYXJzZWQ7IH1cblxuICB1dGlscy5mb3JFYWNoKGhlYWRlcnMuc3BsaXQoJ1xcbicpLCBmdW5jdGlvbiBwYXJzZXIobGluZSkge1xuICAgIGkgPSBsaW5lLmluZGV4T2YoJzonKTtcbiAgICBrZXkgPSB1dGlscy50cmltKGxpbmUuc3Vic3RyKDAsIGkpKS50b0xvd2VyQ2FzZSgpO1xuICAgIHZhbCA9IHV0aWxzLnRyaW0obGluZS5zdWJzdHIoaSArIDEpKTtcblxuICAgIGlmIChrZXkpIHtcbiAgICAgIGlmIChwYXJzZWRba2V5XSAmJiBpZ25vcmVEdXBsaWNhdGVPZi5pbmRleE9mKGtleSkgPj0gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBpZiAoa2V5ID09PSAnc2V0LWNvb2tpZScpIHtcbiAgICAgICAgcGFyc2VkW2tleV0gPSAocGFyc2VkW2tleV0gPyBwYXJzZWRba2V5XSA6IFtdKS5jb25jYXQoW3ZhbF0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcGFyc2VkW2tleV0gPSBwYXJzZWRba2V5XSA/IHBhcnNlZFtrZXldICsgJywgJyArIHZhbCA6IHZhbDtcbiAgICAgIH1cbiAgICB9XG4gIH0pO1xuXG4gIHJldHVybiBwYXJzZWQ7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvcGFyc2VIZWFkZXJzLmpzXG4vLyBtb2R1bGUgaWQgPSA1OVxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxubW9kdWxlLmV4cG9ydHMgPSAoXG4gIHV0aWxzLmlzU3RhbmRhcmRCcm93c2VyRW52KCkgP1xuXG4gIC8vIFN0YW5kYXJkIGJyb3dzZXIgZW52cyBoYXZlIGZ1bGwgc3VwcG9ydCBvZiB0aGUgQVBJcyBuZWVkZWQgdG8gdGVzdFxuICAvLyB3aGV0aGVyIHRoZSByZXF1ZXN0IFVSTCBpcyBvZiB0aGUgc2FtZSBvcmlnaW4gYXMgY3VycmVudCBsb2NhdGlvbi5cbiAgICAoZnVuY3Rpb24gc3RhbmRhcmRCcm93c2VyRW52KCkge1xuICAgICAgdmFyIG1zaWUgPSAvKG1zaWV8dHJpZGVudCkvaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpO1xuICAgICAgdmFyIHVybFBhcnNpbmdOb2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xuICAgICAgdmFyIG9yaWdpblVSTDtcblxuICAgICAgLyoqXG4gICAgKiBQYXJzZSBhIFVSTCB0byBkaXNjb3ZlciBpdCdzIGNvbXBvbmVudHNcbiAgICAqXG4gICAgKiBAcGFyYW0ge1N0cmluZ30gdXJsIFRoZSBVUkwgdG8gYmUgcGFyc2VkXG4gICAgKiBAcmV0dXJucyB7T2JqZWN0fVxuICAgICovXG4gICAgICBmdW5jdGlvbiByZXNvbHZlVVJMKHVybCkge1xuICAgICAgICB2YXIgaHJlZiA9IHVybDtcblxuICAgICAgICBpZiAobXNpZSkge1xuICAgICAgICAvLyBJRSBuZWVkcyBhdHRyaWJ1dGUgc2V0IHR3aWNlIHRvIG5vcm1hbGl6ZSBwcm9wZXJ0aWVzXG4gICAgICAgICAgdXJsUGFyc2luZ05vZGUuc2V0QXR0cmlidXRlKCdocmVmJywgaHJlZik7XG4gICAgICAgICAgaHJlZiA9IHVybFBhcnNpbmdOb2RlLmhyZWY7XG4gICAgICAgIH1cblxuICAgICAgICB1cmxQYXJzaW5nTm9kZS5zZXRBdHRyaWJ1dGUoJ2hyZWYnLCBocmVmKTtcblxuICAgICAgICAvLyB1cmxQYXJzaW5nTm9kZSBwcm92aWRlcyB0aGUgVXJsVXRpbHMgaW50ZXJmYWNlIC0gaHR0cDovL3VybC5zcGVjLndoYXR3Zy5vcmcvI3VybHV0aWxzXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgaHJlZjogdXJsUGFyc2luZ05vZGUuaHJlZixcbiAgICAgICAgICBwcm90b2NvbDogdXJsUGFyc2luZ05vZGUucHJvdG9jb2wgPyB1cmxQYXJzaW5nTm9kZS5wcm90b2NvbC5yZXBsYWNlKC86JC8sICcnKSA6ICcnLFxuICAgICAgICAgIGhvc3Q6IHVybFBhcnNpbmdOb2RlLmhvc3QsXG4gICAgICAgICAgc2VhcmNoOiB1cmxQYXJzaW5nTm9kZS5zZWFyY2ggPyB1cmxQYXJzaW5nTm9kZS5zZWFyY2gucmVwbGFjZSgvXlxcPy8sICcnKSA6ICcnLFxuICAgICAgICAgIGhhc2g6IHVybFBhcnNpbmdOb2RlLmhhc2ggPyB1cmxQYXJzaW5nTm9kZS5oYXNoLnJlcGxhY2UoL14jLywgJycpIDogJycsXG4gICAgICAgICAgaG9zdG5hbWU6IHVybFBhcnNpbmdOb2RlLmhvc3RuYW1lLFxuICAgICAgICAgIHBvcnQ6IHVybFBhcnNpbmdOb2RlLnBvcnQsXG4gICAgICAgICAgcGF0aG5hbWU6ICh1cmxQYXJzaW5nTm9kZS5wYXRobmFtZS5jaGFyQXQoMCkgPT09ICcvJykgP1xuICAgICAgICAgICAgdXJsUGFyc2luZ05vZGUucGF0aG5hbWUgOlxuICAgICAgICAgICAgJy8nICsgdXJsUGFyc2luZ05vZGUucGF0aG5hbWVcbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgb3JpZ2luVVJMID0gcmVzb2x2ZVVSTCh3aW5kb3cubG9jYXRpb24uaHJlZik7XG5cbiAgICAgIC8qKlxuICAgICogRGV0ZXJtaW5lIGlmIGEgVVJMIHNoYXJlcyB0aGUgc2FtZSBvcmlnaW4gYXMgdGhlIGN1cnJlbnQgbG9jYXRpb25cbiAgICAqXG4gICAgKiBAcGFyYW0ge1N0cmluZ30gcmVxdWVzdFVSTCBUaGUgVVJMIHRvIHRlc3RcbiAgICAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIFVSTCBzaGFyZXMgdGhlIHNhbWUgb3JpZ2luLCBvdGhlcndpc2UgZmFsc2VcbiAgICAqL1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIGlzVVJMU2FtZU9yaWdpbihyZXF1ZXN0VVJMKSB7XG4gICAgICAgIHZhciBwYXJzZWQgPSAodXRpbHMuaXNTdHJpbmcocmVxdWVzdFVSTCkpID8gcmVzb2x2ZVVSTChyZXF1ZXN0VVJMKSA6IHJlcXVlc3RVUkw7XG4gICAgICAgIHJldHVybiAocGFyc2VkLnByb3RvY29sID09PSBvcmlnaW5VUkwucHJvdG9jb2wgJiZcbiAgICAgICAgICAgIHBhcnNlZC5ob3N0ID09PSBvcmlnaW5VUkwuaG9zdCk7XG4gICAgICB9O1xuICAgIH0pKCkgOlxuXG4gIC8vIE5vbiBzdGFuZGFyZCBicm93c2VyIGVudnMgKHdlYiB3b3JrZXJzLCByZWFjdC1uYXRpdmUpIGxhY2sgbmVlZGVkIHN1cHBvcnQuXG4gICAgKGZ1bmN0aW9uIG5vblN0YW5kYXJkQnJvd3NlckVudigpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiBpc1VSTFNhbWVPcmlnaW4oKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfTtcbiAgICB9KSgpXG4pO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYXhpb3MvbGliL2hlbHBlcnMvaXNVUkxTYW1lT3JpZ2luLmpzXG4vLyBtb2R1bGUgaWQgPSA2MFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbnZhciB1dGlscyA9IHJlcXVpcmUoJy4vLi4vdXRpbHMnKTtcblxubW9kdWxlLmV4cG9ydHMgPSAoXG4gIHV0aWxzLmlzU3RhbmRhcmRCcm93c2VyRW52KCkgP1xuXG4gIC8vIFN0YW5kYXJkIGJyb3dzZXIgZW52cyBzdXBwb3J0IGRvY3VtZW50LmNvb2tpZVxuICAgIChmdW5jdGlvbiBzdGFuZGFyZEJyb3dzZXJFbnYoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICB3cml0ZTogZnVuY3Rpb24gd3JpdGUobmFtZSwgdmFsdWUsIGV4cGlyZXMsIHBhdGgsIGRvbWFpbiwgc2VjdXJlKSB7XG4gICAgICAgICAgdmFyIGNvb2tpZSA9IFtdO1xuICAgICAgICAgIGNvb2tpZS5wdXNoKG5hbWUgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQodmFsdWUpKTtcblxuICAgICAgICAgIGlmICh1dGlscy5pc051bWJlcihleHBpcmVzKSkge1xuICAgICAgICAgICAgY29va2llLnB1c2goJ2V4cGlyZXM9JyArIG5ldyBEYXRlKGV4cGlyZXMpLnRvR01UU3RyaW5nKCkpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmICh1dGlscy5pc1N0cmluZyhwYXRoKSkge1xuICAgICAgICAgICAgY29va2llLnB1c2goJ3BhdGg9JyArIHBhdGgpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmICh1dGlscy5pc1N0cmluZyhkb21haW4pKSB7XG4gICAgICAgICAgICBjb29raWUucHVzaCgnZG9tYWluPScgKyBkb21haW4pO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmIChzZWN1cmUgPT09IHRydWUpIHtcbiAgICAgICAgICAgIGNvb2tpZS5wdXNoKCdzZWN1cmUnKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBjb29raWUuam9pbignOyAnKTtcbiAgICAgICAgfSxcblxuICAgICAgICByZWFkOiBmdW5jdGlvbiByZWFkKG5hbWUpIHtcbiAgICAgICAgICB2YXIgbWF0Y2ggPSBkb2N1bWVudC5jb29raWUubWF0Y2gobmV3IFJlZ0V4cCgnKF58O1xcXFxzKikoJyArIG5hbWUgKyAnKT0oW147XSopJykpO1xuICAgICAgICAgIHJldHVybiAobWF0Y2ggPyBkZWNvZGVVUklDb21wb25lbnQobWF0Y2hbM10pIDogbnVsbCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlOiBmdW5jdGlvbiByZW1vdmUobmFtZSkge1xuICAgICAgICAgIHRoaXMud3JpdGUobmFtZSwgJycsIERhdGUubm93KCkgLSA4NjQwMDAwMCk7XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgfSkoKSA6XG5cbiAgLy8gTm9uIHN0YW5kYXJkIGJyb3dzZXIgZW52ICh3ZWIgd29ya2VycywgcmVhY3QtbmF0aXZlKSBsYWNrIG5lZWRlZCBzdXBwb3J0LlxuICAgIChmdW5jdGlvbiBub25TdGFuZGFyZEJyb3dzZXJFbnYoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICB3cml0ZTogZnVuY3Rpb24gd3JpdGUoKSB7fSxcbiAgICAgICAgcmVhZDogZnVuY3Rpb24gcmVhZCgpIHsgcmV0dXJuIG51bGw7IH0sXG4gICAgICAgIHJlbW92ZTogZnVuY3Rpb24gcmVtb3ZlKCkge31cbiAgICAgIH07XG4gICAgfSkoKVxuKTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2F4aW9zL2xpYi9oZWxwZXJzL2Nvb2tpZXMuanNcbi8vIG1vZHVsZSBpZCA9IDYxXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIid1c2Ugc3RyaWN0JztcblxudmFyIENhbmNlbCA9IHJlcXVpcmUoJy4vQ2FuY2VsJyk7XG5cbi8qKlxuICogQSBgQ2FuY2VsVG9rZW5gIGlzIGFuIG9iamVjdCB0aGF0IGNhbiBiZSB1c2VkIHRvIHJlcXVlc3QgY2FuY2VsbGF0aW9uIG9mIGFuIG9wZXJhdGlvbi5cbiAqXG4gKiBAY2xhc3NcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGV4ZWN1dG9yIFRoZSBleGVjdXRvciBmdW5jdGlvbi5cbiAqL1xuZnVuY3Rpb24gQ2FuY2VsVG9rZW4oZXhlY3V0b3IpIHtcbiAgaWYgKHR5cGVvZiBleGVjdXRvciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ2V4ZWN1dG9yIG11c3QgYmUgYSBmdW5jdGlvbi4nKTtcbiAgfVxuXG4gIHZhciByZXNvbHZlUHJvbWlzZTtcbiAgdGhpcy5wcm9taXNlID0gbmV3IFByb21pc2UoZnVuY3Rpb24gcHJvbWlzZUV4ZWN1dG9yKHJlc29sdmUpIHtcbiAgICByZXNvbHZlUHJvbWlzZSA9IHJlc29sdmU7XG4gIH0pO1xuXG4gIHZhciB0b2tlbiA9IHRoaXM7XG4gIGV4ZWN1dG9yKGZ1bmN0aW9uIGNhbmNlbChtZXNzYWdlKSB7XG4gICAgaWYgKHRva2VuLnJlYXNvbikge1xuICAgICAgLy8gQ2FuY2VsbGF0aW9uIGhhcyBhbHJlYWR5IGJlZW4gcmVxdWVzdGVkXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdG9rZW4ucmVhc29uID0gbmV3IENhbmNlbChtZXNzYWdlKTtcbiAgICByZXNvbHZlUHJvbWlzZSh0b2tlbi5yZWFzb24pO1xuICB9KTtcbn1cblxuLyoqXG4gKiBUaHJvd3MgYSBgQ2FuY2VsYCBpZiBjYW5jZWxsYXRpb24gaGFzIGJlZW4gcmVxdWVzdGVkLlxuICovXG5DYW5jZWxUb2tlbi5wcm90b3R5cGUudGhyb3dJZlJlcXVlc3RlZCA9IGZ1bmN0aW9uIHRocm93SWZSZXF1ZXN0ZWQoKSB7XG4gIGlmICh0aGlzLnJlYXNvbikge1xuICAgIHRocm93IHRoaXMucmVhc29uO1xuICB9XG59O1xuXG4vKipcbiAqIFJldHVybnMgYW4gb2JqZWN0IHRoYXQgY29udGFpbnMgYSBuZXcgYENhbmNlbFRva2VuYCBhbmQgYSBmdW5jdGlvbiB0aGF0LCB3aGVuIGNhbGxlZCxcbiAqIGNhbmNlbHMgdGhlIGBDYW5jZWxUb2tlbmAuXG4gKi9cbkNhbmNlbFRva2VuLnNvdXJjZSA9IGZ1bmN0aW9uIHNvdXJjZSgpIHtcbiAgdmFyIGNhbmNlbDtcbiAgdmFyIHRva2VuID0gbmV3IENhbmNlbFRva2VuKGZ1bmN0aW9uIGV4ZWN1dG9yKGMpIHtcbiAgICBjYW5jZWwgPSBjO1xuICB9KTtcbiAgcmV0dXJuIHtcbiAgICB0b2tlbjogdG9rZW4sXG4gICAgY2FuY2VsOiBjYW5jZWxcbiAgfTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQ2FuY2VsVG9rZW47XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvY2FuY2VsL0NhbmNlbFRva2VuLmpzXG4vLyBtb2R1bGUgaWQgPSA2MlxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogU3ludGFjdGljIHN1Z2FyIGZvciBpbnZva2luZyBhIGZ1bmN0aW9uIGFuZCBleHBhbmRpbmcgYW4gYXJyYXkgZm9yIGFyZ3VtZW50cy5cbiAqXG4gKiBDb21tb24gdXNlIGNhc2Ugd291bGQgYmUgdG8gdXNlIGBGdW5jdGlvbi5wcm90b3R5cGUuYXBwbHlgLlxuICpcbiAqICBgYGBqc1xuICogIGZ1bmN0aW9uIGYoeCwgeSwgeikge31cbiAqICB2YXIgYXJncyA9IFsxLCAyLCAzXTtcbiAqICBmLmFwcGx5KG51bGwsIGFyZ3MpO1xuICogIGBgYFxuICpcbiAqIFdpdGggYHNwcmVhZGAgdGhpcyBleGFtcGxlIGNhbiBiZSByZS13cml0dGVuLlxuICpcbiAqICBgYGBqc1xuICogIHNwcmVhZChmdW5jdGlvbih4LCB5LCB6KSB7fSkoWzEsIDIsIDNdKTtcbiAqICBgYGBcbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFja1xuICogQHJldHVybnMge0Z1bmN0aW9ufVxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHNwcmVhZChjYWxsYmFjaykge1xuICByZXR1cm4gZnVuY3Rpb24gd3JhcChhcnIpIHtcbiAgICByZXR1cm4gY2FsbGJhY2suYXBwbHkobnVsbCwgYXJyKTtcbiAgfTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9heGlvcy9saWIvaGVscGVycy9zcHJlYWQuanNcbi8vIG1vZHVsZSBpZCA9IDYzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCJdLCJzb3VyY2VSb290IjoiIn0=