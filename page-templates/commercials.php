<?php
/*
Template Name: COMMERCIALS PAGE
*/
get_header(); ?>

<div class="main-app" style="background-color: var(--main-bg-color);">

<div class="pjax-container--main">


	<div class="page-top-header container-eq">
<h1>Commercials</h1>
	</div>

	<section class="section--commercials-grid container-eq">
		<div class="commercials-flex-grid">


			<?php
				 $posts = get_posts(array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'commercials'
				 ));

				 if( $posts ): ?>
			<?php foreach( $posts as $post ):
				 setup_postdata( $post );
				 ?>


				 <?php $project_homepage_image = get_field( 'project_homepage_image' ); ?>

				 <div class="com-cell" style="">
					 <a class="com-cell-link-box js-film-link com-block-link" href="<?php echo get_permalink(); ?>"  data-clientname="<?php the_field( 'client_name' ); ?>" data-projectname="<?php the_field( 'project_name' ); ?>">
					 <div class="com-cell-inner a-ratio-16-9">


						 <div class="a-ratio-item">

							 <img class="lazyload lazy-anim" data-src="<?php echo $project_homepage_image['sizes']['project-preview']; ?>" alt="<?php echo $project_homepage_image['alt']; ?>">
						 </div>

						 <!-- <canvas class="noise-canvas noise-canvas_2"></canvas> -->
							 </div>
							 <div class="com-film-title">
								 <h2><?php the_field( 'client_name' ); ?> - <?php the_field( 'project_name' ); ?></h2>
							 </div>
						 </a>


				 </div>


			 <?php endforeach; ?>
			 <?php wp_reset_postdata(); ?>
			 <?php endif; ?>


			<!-- <div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link com-block-link" href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img class="lazyload lazy-anim" data-src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>


						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>
			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_2"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_2"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_2"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_1"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_1"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_1"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_1"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_1"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_1"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_1"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div>

			<div class="com-cell" style="">
				<a class="com-cell-link-box js-film-link " href="<?php echo home_url() ?>/commercials/test-commercial/"  data-clientname="OnePlus" data-projectname="projeeeect">
				<div class="com-cell-inner js-film-link a-ratio-16-9">


					<div class="a-ratio-item">
						<img src="<?php echo get_template_directory_uri(); ?>/src/assets/images/2.jpg" alt="">
					</div>

					<canvas class="noise-canvas noise-canvas_1"></canvas>
						</div>
						<div class="com-film-title">
							<h2>SMART - CARSINO</h2>
						</div>
					</a>


			</div> -->



		</div>

	</section>



	<div class="bottom-link">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42 42"><title>Untitled-8</title><path d="M1516.72,1486.22c1.85-1.06,2.8-.69,3.28.16s0.27,1.9-1.48,3c-4.44,2.7-9.52,3.38-9.15,5.23s5.29-1.06,10.52-1.74c2.11-.26,2.91.42,3,1.32s-0.48,1.8-2.54,2.11c-5.18.74-14.49-1-14.86,0.69s9.73,1.69,14.86,3.12c2.06,0.58,2.54,1.48,2.22,2.38s-1.11,1.48-3.12,1c-4.92-1.16-9.46-4.28-10.57-2.86s4.86,3.33,9,6.29c1.74,1.27,1.9,2.38,1.22,3.12-0.48.69-1.64,1-3.33-.26-4.12-3-11.31-11-12.37-10.15-1.38,1,7.14,8.35,9.78,12.79,1.06,1.85.69,2.8-.16,3.28s-1.9.26-3-1.48c-2.7-4.44-3.38-9.46-5.23-9.09s1.06,5.23,1.74,10.47c0.26,2.11-.42,2.91-1.32,3s-1.8-.47-2.11-2.54c-0.74-5.18,1-14.49-.69-14.86s-1.69,9.73-3.12,14.86c-0.58,2.06-1.48,2.54-2.38,2.22s-1.48-1.11-1-3.12c1.16-4.92,4.23-9.2,2.8-10.36-1.37-1-3.28,4.65-6.24,8.83-1.27,1.74-2.38,1.9-3.12,1.22-0.69-.47-1-1.64.26-3.33,3-4.12,10.89-11.58,10-12.64-1-1.32-8.25,7.4-12.69,10-1.85,1.06-2.8.69-3.28-.16s-0.27-1.9,1.48-3c4.44-2.7,9.62-3.49,9.25-5.34s-5.39,1.16-10.63,1.85c-2.11.26-2.91-.42-3-1.32s0.48-1.8,2.54-2.11c5.18-.74,14.49,1,14.86-0.69s-9.73-1.69-14.86-3.12c-2.06-.58-2.54-1.48-2.22-2.38s1.11-1.48,3.12-1c4.92,1.16,9.83,4.39,10.94,3s-5.23-3.44-9.41-6.4c-1.74-1.27-1.9-2.38-1.21-3.12,0.48-.69,1.64-1,3.33.27,4.12,3,11.15,10.47,12.26,9.57,1.32-.9-7-7.77-9.67-12.21-1.06-1.85-.69-2.8.16-3.28s1.9-.26,3,1.48c2.7,4.44,3.49,9.68,5.34,9.31s-1.16-5.45-1.85-10.68c-0.26-2.11.42-2.91,1.32-3s1.8,0.47,2.11,2.54c0.74,5.18-1,14.49.69,14.86s1.69-9.73,3.12-14.86c0.58-2.06,1.48-2.54,2.38-2.22s1.48,1.11,1,3.12c-1.16,4.92-4.23,9.36-2.8,10.47s3.28-4.76,6.24-8.94c1.27-1.74,2.38-1.9,3.12-1.21,0.69,0.47,1,1.64-.27,3.33-3,4.12-10.52,10.94-9.67,12,1,1.37,7.88-6.77,12.32-9.41" transform="translate(-1480.87 -1476.54)" style="fill:#2d2c2a"/></svg>
			 <h2><a class="nav-link pjax-link" href="<?php echo home_url() ?>/short-films/" data-swup-transition="basic-transition">Short Films</a></h2>
	</div>

<!-- pjax-container -->
</div>




<!-- MAIN_APP -->
</div>
<!-- CLIENT_BODY -->
</div>



<!-- FIXED_STUFF -->


	<?php get_template_part( 'template-parts/content', 'fixedstuff' ); ?>



<?php get_footer();
