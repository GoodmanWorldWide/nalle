<?php
/**
 * PAGE HERO
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
	<!-- <section class="page-main-section">
		<div class="grid-container">

		<div class="grid-x grid-padding-x align-justify align-middle split-row">
			<div class="cell split-image-cell sc-image-left small-order-2 large-order-1">
				<img src="http://maria01.co/wp-content/uploads/2017/03/maria_old_11.jpg" alt="">
			</div>
			<div class="cell split-text-cell sc-text-right small-order-1 large-order-2">
				<div class="">
					<h3>TESTI TESTI TESTI TESTI TESTI TESTI TESTI TESTI</h3>
					<p>With the expansion project launch, Helsinki and its startup community are looking forward to the next era of growth. The campus expansion wants to open room to more startups, growth companies, investors and corporates to continue building the future in Helsinki.</p>
				</div>

			</div>




		</div>
		</div>
		</section> -->



			<?php if ( have_rows( 'page_content' ) ): ?>
				<section class="page-main-section">
						<div class="grid-container">
			<?php while ( have_rows( 'page_content' ) ) : the_row(); ?>
				<?php if ( get_row_layout() == 'text_+_image_' ) : ?>


					<?php if ( get_sub_field( 'image_align' ) == 1 ) { ?>
						<div class="grid-x grid-padding-x align-middle split-row">
							<?php if ( have_rows( 'text_content' ) ) : ?>
								<div class="cell split-text-cell sc-text-left small-order-2 large-order-1">
								<?php while ( have_rows( 'text_content' ) ) : the_row(); ?>
										<div>
											<?php if( get_sub_field('block_header') ): ?>
										<h3><?php the_sub_field( 'block_header' ); ?></h3>
										<?php endif; ?>
									<?php the_sub_field( 'block_content' ); ?>
								<?php endwhile; ?>
								</div>
								</div>
							<?php endif; ?>
							<div class="cell split-image-cell align-self-top sc-image-right small-order-1 large-order-2">
								<div class="a-ratio-2-3">
									<div class="a-ratio-item">
								<?php $content_image = get_sub_field( 'content_image' ); ?>
								<?php if ( $content_image ) { ?>
									<picture>
										<source media="(min-width: 500px)" srcset="<?php echo $content_image['sizes']['m-split']; ?>">
											<source media="(min-width: 0px)" srcset="<?php echo $content_image['sizes']['m-split-small']; ?>">
										<img class="lazy-anim lazyload" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" src="" data-src="<?php echo $content_image['sizes']['m-split']; ?>" alt="<?php the_title(); ?>" style="width:100%;">
					</picture>
						</div>
							</div>
								</div>
								<?php } ?>





						</div>



					<?php	} else { ?>
	<div class="grid-x grid-padding-x align-middle split-row">

						<?php $content_image = get_sub_field( 'content_image' ); ?>

						<?php if ( $content_image ) { ?>
							<div class="cell split-image-cell sc-image-left align-self-top small-order-1 large-order-1">
							<!-- <img src="<?php echo $content_image['url']; ?>" alt="<?php echo $content_image['alt']; ?>" /> -->
							<div class="a-ratio-2-3">
								<div class="a-ratio-item">
							<picture>
				<source media="(min-width: 500px)" srcset="<?php echo $content_image['sizes']['m-split']; ?>">
					<source media="(min-width: 0px)" srcset="<?php echo $content_image['sizes']['m-split-small']; ?>">
				<img class="lazy-anim lazyload" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="<?php echo $content_image['sizes']['m-split']; ?>" alt="<?php the_title(); ?>" style="width:100%;">
			</picture>
								</div>
							</div>
						</div>
						<?php } ?>

						<?php if ( have_rows( 'text_content' ) ) : ?>
							<div class="cell split-text-cell sc-text-right small-order-2 large-order-2">
							<?php while ( have_rows( 'text_content' ) ) : the_row(); ?>
								<div>
									<?php if( get_sub_field('block_header') ): ?>
								<h3><?php the_sub_field( 'block_header' ); ?></h3>
								<?php endif; ?>
								<?php the_sub_field( 'block_content' ); ?>
								</div>
							<?php endwhile; ?>
							</div>
						<?php endif; ?>

					</div>

				<?php 	} ?>


			<?php elseif ( get_row_layout() == 'full_image' ) : ?>
		<?php $image = get_sub_field( 'image' ); ?>
	 <?php if ( $image ) { ?>
		 <?php $image = get_sub_field( 'image' ); ?>
		 <div class="content-full-image-wrapper">


			<div class="content-full-image">
				<div class="content-full-image-inner">
				<picture>
  <source media="(min-width: 1000px)" srcset="<?php echo $image['sizes']['m-full']; ?>">
  <source media="(min-width: 500px)" srcset="<?php echo $image['sizes']['m-full-medium']; ?>">
		<source media="(min-width: 0px)" srcset="<?php echo $image['sizes']['m-split-small']; ?>">
  <img class="lazy-anim lazyload" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" data-src="<?php echo $image['sizes']['m-full-medium']; ?>" alt="<?php the_title(); ?>" style="width:100%;">
</picture>

						</div>

						</div>
							</div>
		<?php } ?>

	<?php elseif ( get_row_layout() == 'text_blocks' ) : ?>
		<?php if ( have_rows( 'Text Blocks' ) ) : ?>
			<div class="grid-x grid-padding-x align-center text-block-row">
			<?php while ( have_rows( 'Text Blocks' ) ) : the_row(); ?>
				<div class="cell text-block-cell">

				<h3><?php the_sub_field( 'header' ); ?></h3>
				<div class="">
					<?php the_sub_field( 'content' ); ?>
				</div>
							</div>

			<?php endwhile; ?>
							</div>
		<?php else : ?>
			<?php // no rows found ?>

		<?php endif; ?>

	<?php elseif ( get_row_layout() == 'text_+_text' ) : ?>
		<div class="grid-x grid-padding-x split-row">
	<?php if ( have_rows( 'text_content' ) ) : ?>
		<div class="cell split-text-cell sc-text-left double-text-first">
		<?php while ( have_rows( 'text_content' ) ) : the_row(); ?>
			<div>
				<?php if( get_sub_field('block_header') ): ?>
			<h3><?php the_sub_field( 'block_header' ); ?></h3>
			<?php endif; ?>
			<?php the_sub_field( 'block_content' ); ?>
			</div>
		<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<?php if ( have_rows( 'text_content_Right' ) ) : ?>
			<div class="cell split-text-cell sc-text-right">
		<?php while ( have_rows( 'text_content_Right' ) ) : the_row(); ?>
			<div>
				<?php if( get_sub_field('block_header') ): ?>
			<h3><?php the_sub_field( 'block_header' ); ?></h3>
			<?php endif; ?>
			<?php the_sub_field( 'block_content' ); ?>
			</div>
		<?php endwhile; ?>

				</div>
	<?php endif; ?>

</div>
				<?php endif; ?>



			<?php endwhile; ?>
		</div>
		</section>
		<?php endif; ?>
