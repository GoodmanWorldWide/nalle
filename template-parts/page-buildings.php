<?php
/**
 * PAGE HERO
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
	<!-- <section class="page-main-section">
		<div class="grid-container">

		<div class="grid-x grid-padding-x align-justify align-middle split-row">
			<div class="cell split-image-cell sc-image-left small-order-2 large-order-1">
				<img src="http://maria01.co/wp-content/uploads/2017/03/maria_old_11.jpg" alt="">
			</div>
			<div class="cell split-text-cell sc-text-right small-order-1 large-order-2">
				<div class="">
					<h3>TESTI TESTI TESTI TESTI TESTI TESTI TESTI TESTI</h3>
					<p>With the expansion project launch, Helsinki and its startup community are looking forward to the next era of growth. The campus expansion wants to open room to more startups, growth companies, investors and corporates to continue building the future in Helsinki.</p>
				</div>

			</div>




		</div>
		</div>
		</section> -->

<section class="page-main-section">
		<div class="grid-container">
			<?php if (have_rows('buildings_content')): ?>
				<?php while (have_rows('buildings_content')) : the_row(); ?>
					<?php if (get_row_layout() == 'text_+_image_') : ?>

						<?php if (get_sub_field('image_align') == 1) {  ?>
								<div class="grid-x grid-padding-x align-middle split-row">
							<?php if (have_rows('text_content')) : ?>
										<div class="cell split-text-cell sc-text-left small-order-2 large-order-1">
								<?php while (have_rows('text_content')) : the_row(); ?>
													<div class="building--fixer">
									<h3><?php the_sub_field('block_header'); ?></h3>
									<?php the_sub_field('block_content'); ?>
									<?php if (have_rows('numbers_unit')) : ?>
										<div class="buildings-info-numbers grid-x align-center" data-aos="numbers-anim">

						<?php while (have_rows('numbers_unit')) : the_row(); ?>
						<div class="info-numbers cell">
								<div class="info-number-box-wrap">
						<h3><?php the_sub_field('number'); ?></h3>
							</div>
						<span><?php the_sub_field('name'); ?></span>
						</div>
						<?php endwhile; ?>
													</div>
					<?php endif; ?>
								<?php endwhile; ?>
							</div>
							</div>
							<?php endif; ?>
							<?php $content_image_images = get_sub_field('content_image'); ?>
							<?php if ($content_image_images) :  ?>
								<div class="cell split-image-cell align-self-top sc-image-right small-order-1 large-order-2">
									<div class="a-ratio-2-3">


								<div class="swiper-container b-swiper-container main-slider loading">
									<div class="swiper-wrapper">

								<?php foreach ($content_image_images as $content_image_image): ?>
									 <div class="swiper-slide b-swiper-slide">
										 <figure class="slide-bgimg" style="background-image:url(<?php echo $content_image_image['sizes']['medium']; ?>)">
											 <img src="<?php echo $content_image_image['sizes']['medium']; ?>" class="entity-img" />
										 </figure>


									 </div>
								<?php endforeach; ?>
								 </div>
								 <div class="swiper-button-prev swiper-button-white"></div>
								<div class="swiper-button-next swiper-button-white"></div>
								  </div>
										  </div>
													</div>
							<?php endif; ?>
							     </div>
					<?php	} else { ?>
						<div class="grid-x grid-padding-x align-middle split-row">

					<?php $content_image_images = get_sub_field('content_image'); ?>
					<?php if ($content_image_images) :  ?>
						<div class="cell split-image-cell sc-image-left align-self-top small-order-1 large-order-1">
									<div class="a-ratio-2-3">
						<div class="swiper-container b-swiper-container main-slider loading">
							<div class="swiper-wrapper">

						<?php foreach ($content_image_images as $content_image_image): ?>
							 <div class="swiper-slide b-swiper-slide">
								 <figure class="slide-bgimg" style="background-image:url(<?php echo $content_image_image['sizes']['medium']; ?>)">
									 <img src="<?php echo $content_image_image['sizes']['medium']; ?>" class="entity-img" />
								 </figure>

							 </div>
						<?php endforeach; ?>
						 </div>
						 <div class="swiper-button-prev swiper-button-white"></div>
						<div class="swiper-button-next swiper-button-white"></div>
							</div>
									</div>
												</div>
					<?php endif; ?>
					<?php if (have_rows('text_content')) : ?>
								<div class="cell split-text-cell sc-text-right small-order-2 large-order-2">
						<?php while (have_rows('text_content')) : the_row(); ?>
											<div class="building--fixer">
							<h3><?php the_sub_field('block_header'); ?></h3>
							<?php the_sub_field('block_content'); ?>
							<?php if (have_rows('numbers_unit')) : ?>
								<div class="buildings-info-numbers grid-x align-center" data-aos="numbers-anim">

				<?php while (have_rows('numbers_unit')) : the_row(); ?>
				<div class="info-numbers cell">
					<div class="info-number-box-wrap">

				<h3><?php the_sub_field('number'); ?></h3>
						</div>
				<span><?php the_sub_field('name'); ?></span>
				</div>
				<?php endwhile; ?>
											</div>
			<?php endif; ?>
						<?php endwhile; ?>
					</div>
					</div>
					<?php endif; ?>

							 </div>
				<?php		} ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else: ?>
				<?php // no layouts found?>
			<?php endif; ?>

		</div>
		</section>
