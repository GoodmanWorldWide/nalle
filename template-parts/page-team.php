<?php
/**
 * PAGE HERO
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>


<section class="page-team-section">
		<div class="grid-container">
			<div class="grid-x t-member-grid align-center small-up-2 medium-up-4 large-up-5 xslarge-up-6">
				<?php if ( have_rows( 'team_member' ) ) : ?>
	<?php while ( have_rows( 'team_member' ) ) : the_row(); ?>
		<div class="cell t-member">
		<?php $image = get_sub_field( 'image' ); ?>
		<?php if ( $image ) { ?>
			<img src="<?php echo $image['sizes']['m-team']; ?>" alt="<?php the_title(); ?>" style="width:auto;">

		<?php } ?>
		<p class="t-member-name"><?php the_sub_field( 'name_' ); ?></p>
		<p class="t-member-title"><?php the_sub_field( 'title' ); ?></p>
		<p class="t-member-email"><?php the_sub_field( 'email' ); ?></p>
		<div class="t-member-content">
			<?php the_sub_field( 'links_+_extra' ); ?>
		</div>
		</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>


</div>

		</div>
		</section>
