<?php
/**
 * PAGE HERO
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<?php if ( have_rows( 'page_hero' ) ): ?>
	<?php while ( have_rows( 'page_hero' ) ) : the_row(); ?>
	<section class="split-hero">
		<div class="grid-container">
		<div class="grid-x grid-padding-x align-center">
		<?php if ( get_row_layout() == 'header-lead' ) : ?>
			<div class="cell large-6 split-text-cell split-cell-left">
				<h2><?php the_sub_field( 'page_title' ); ?></h2>
			</div>
			<div class="cell large-6 split-text-cell split-cell-right">
				<p><?php the_sub_field( 'lead_text' ); ?>	</p>
			</div>
			<?php $header_image_large = get_sub_field( 'header_image_large' ); ?>
			<?php if ( $header_image_large ) { ?>
				<div class="cell large-12 full-cell-image">
				<img src="<?php echo $header_image_large['url']; ?>" alt="<?php echo $header_image_large['alt']; ?>" />
			</div>
			<?php } ?>
		<?php elseif ( get_row_layout() == 'Header-image' ) : ?>
			<div class="cell large-6 align-self-middle split-text-cell split-cell-left">
				<h2><?php the_sub_field( 'page_title' ); ?></h2>
				<p><?php the_sub_field( 'lead_text' ); ?>	</p>
			</div>
			<?php $header_image = get_sub_field( 'header_image' ); ?>

			<?php if ( $header_image ) { ?>
				<div class="cell large-6 align-self-middle split-cell-right split-image">
				<img src="<?php echo $header_image['url']; ?>" alt="<?php echo $header_image['alt']; ?>" />
					</div>
			<?php } ?>

		<?php endif; ?>
		</div>
		</div>
		</section>
	<?php endwhile; ?>

<?php endif; ?>
