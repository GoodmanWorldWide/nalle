<?php
/**
 * FOOTER
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<div class="project-grid-trigger-wrapper">
	<div class="project-view-trigger-wrapper">
		<span id="js-project-view-trigger">View other projects</span>
	</div>

</div>

<section class="project-grid-overlay home--project-grid-section">
		<div class="inner-mover">
	<div class="hide-project-overlay-wrapper">
		<span id="js-close-project-view">Back to current project</span>
	</div>
<?php get_template_part( 'template-parts/content', 'projects-for-overlay' ); ?>
	</div>
</section>
