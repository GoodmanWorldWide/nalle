<?php
/**
 * Projects grriiiiid
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div class="grid-x align-center">

	<?php if ( have_rows( 'project' ) ) : ?>
	<?php while ( have_rows( 'project' ) ) : the_row(); ?>
		<?php $post_object = get_sub_field( 'choose_project' ); ?>
		<?php if ( $post_object ): ?>
			<?php $post = $post_object; ?>
			<?php setup_postdata( $post ); ?>



		<!-- <?php the_sub_field( 'project_name' ); ?> -->
		<?php $project_featured_image = get_sub_field( 'project_featured_image' ); ?>
		<?php if ( get_sub_field( 'small_image' ) == 1 ) {  ?>
			<div class="cell medium-6 large-4 home--project-grid-cell project-image-not-loaded">
				<div class="project-grid-cell-inner">
			<a href="<?php echo get_permalink(); ?>" class="link-home-to-project" data-swup-transition="to-project-transition">
				<div class="aspect-gdmn-image light-aspect-bg">
					<picture>


				<source media="(min-width: 800px)" data-srcset="<?php echo $project_featured_image['sizes']['medium-image']; ?>">
					<source media="(min-width: 0px)" data-srcset="<?php echo $project_featured_image['sizes']['small-image']; ?>">
				<img class="image-scaler lazyload lazy-anim home--project-image" data-src="<?php echo $project_featured_image['sizes']['medium-image']; ?>" />
				</picture>

					</div>
			</a>
			<a href="<?php echo get_permalink(); ?>" class="link-home-to-project p-name" data-swup-transition="to-project-transition"><h3 class="project-grid-header"><?php the_field( 'client_name' ); ?></h3></a>
			</div>
			</div>
		<?php	} else {  ?>
			<div class="cell medium-6 large-6 home--project-grid-cell project-image-not-loaded">
				<div class="project-grid-cell-inner">
			<a href="<?php echo get_permalink(); ?>" class="link-home-to-project" data-swup-transition="to-project-transition">
				<div class="aspect-gdmn-image light-aspect-bg">
					<picture>


				<source media="(min-width: 800px)" data-srcset="<?php echo $project_featured_image['sizes']['medium-image']; ?>">
					<source media="(min-width: 0px)" data-srcset="<?php echo $project_featured_image['sizes']['small-image']; ?>">
				<img class="image-scaler lazyload lazy-anim home--project-image" data-src="<?php echo $project_featured_image['sizes']['medium-image']; ?>" />
				</picture>
					</div>
			</a>
			<a href="<?php echo get_permalink(); ?>" class="link-home-to-project p-name" data-swup-transition="to-project-transition"><h3 class="project-grid-header"><?php the_field( 'client_name' ); ?></h3></a>
			</div>
			</div>
		<?php	} ?>
					<?php wp_reset_postdata(); ?>
			<?php endif; ?>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>







</div>
