<?php
/**
 * PAGE HERO
 *
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<?php if ( have_rows( 'header_content' ) ): ?>
	<?php while ( have_rows( 'header_content' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'center_align' ) : ?>

			<section class="page-hero-section-center">
					<div class="grid-container hero-wrap-center">
				<h1><?php the_sub_field( 'p-header' ); ?></h1>
				<?php if( get_sub_field('sub_header') ): ?>
						<h4><?php the_sub_field( 'sub_header' ); ?>	</h4>
<?php endif; ?>

			</div>
			</section>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>
