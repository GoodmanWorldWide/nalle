 // import Masonry from 'masonry-layout';
import stickybits from 'stickybits'
// import gsap from "gsap";
import SmoothScroll from "smooth-scroll";
// import {
//   headroom
// } from "./modules/headroom";
//
// headroom();


//SWUP IPMORTS
import Swup from 'swup';
import SwupBodyClassPlugin from '@swup/body-class-plugin';
// import SwupDebugPlugin from '@swup/debug-plugin';
import SwupJsPlugin from '@swup/js-plugin';
import SwupPreloadPlugin from '@swup/preload-plugin';
import SwupScrollPlugin from '@swup/scroll-plugin';

  // import Marquee3k from 'marquee3000';

// lazySizes.cfg.expFactor = 4;
// import 'lazysizes';
// document.addEventListener('lazyloaded', function(e){
//
// if (e.target.classList.contains('home--film-image')) {
//   // setTimeout(function(){
//
// e.target.closest(".aspect-box-inner").classList.add("film-thumb-loaded");
// // }, 3000);
//
// }
//
// });

import Splitting from "splitting";
import AOS from 'aos';
/////IMPORTS ENDS//////

 // var elem = document.querySelector('.home-projects-grid');
 // var msnry = new Masonry( elem, {
 //   // options
 //   itemSelector: '.home-projects-grid-item',
 //   columnWidth: '.grid-sizer',
 //   transitionDuration: 0,
 //   percentPosition: true
 // });



 /////IMAGE LOADING /////



 ////LOGO ANIM ///////


 var showreel = document.querySelector('#jaymo-showreel');
 var get_showreel_url = showreel.getAttribute("data-src");
  showreel.src = get_showreel_url;

 function myTimer() {
   // console.log('one');
   var root = document.getElementsByTagName("HTML")[0];
   root.classList.add("show-jaymo");
   setTimeout(function() {
     root.classList.remove("show-jaymo");
     // console.log('two');
   },4000);
   }

   function stop_logo_anim() {
     // console.log("stop-logo");
     clearInterval(logoAnim);
     }


 // DEFER IMAGES FUNCTION
function deferImages(){
	// get all images with 'deferload' class
	var $images = document.querySelectorAll("img.lazyload");

	// if there are images on the page run through each and update src
	if($images.length > 0) {
		for (var i = 0, len = $images.length; i < len; i++) {

			// get url from each image
			var image_url = $images[i].getAttribute("data-src");
			// set image src

      $images[i].onload = function(e) {
    e.target.closest(".aspect-box-inner").classList.add("film-thumb-loaded");
    // console.log(e);
      };
      $images[i].onerror = function() {
        // console.log("noload");
      };
			$images[i].src = image_url;


			// debugging
			var $lognumber = i + 1;
			// console.log("Image No." + $lognumber + " loaded");
		}
	}
	// console.log("Images loaded")
}


// PAGE LOADED
window.addEventListener('load', function() {
	// run defer image function
   deferImages();
}, false);


Splitting();

///FILTERSS////

AOS.init({
  // Global settings:
  disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
  initClassName: 'aos-init', // class applied after initialization
  animatedClassName: 'aos-animate', // class applied on animation
  useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)


  // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  offset: 0, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 400, // values from 0 to 3000, with step 50ms
  easing: 'ease', // default easing for AOS animations
  once: true, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
  anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

});


document.addEventListener('click', function (event) {
var root = document.getElementsByTagName("HTML")[0];
  if ( event.target.classList.contains( 'js-mobile-filter-toggler' ) ) {
// console.log("toggler");
if (root.classList.contains('mobile-filters--active')) {
root.classList.remove('mobile-filters--active');
var filter_all_button = document.querySelectorAll(".filter-show-all")
for(var i = 0; i < filter_all_button.length; i++) {
  filter_all_button[i].classList.add("active-filter-button");
  }

const d_item_wrapper = document.querySelector(".home-projects-grid");
var items_disabled = document.querySelector(".disable-d-item");
if(typeof(items_disabled) != 'undefined' && items_disabled != null){
       d_item_wrapper.style.opacity = "0";
       // console.log("disbaled items");
   } else{
         // console.log("NONONO disbaled items");
   }

  let d_label = document.getElementsByClassName("filter-button");
for(var i = 0; i < d_label.length; i++) {
d_label[i].classList.remove("active-filter-button");
}
var d_item = document.getElementsByClassName("home-projects-grid-item");
setTimeout(function(){
for(var i = 0; i < d_item.length; i++) {
  // console.log(d_item[i].dataset.label);
    d_item[i].classList.remove("disable-d-item");
    d_item[i].classList.add("active-d-item");
    // msnry.layout();
  }
  setTimeout(function(){
  d_item_wrapper.style.opacity = "1";

}, 100);
}, 500);


}
else {
root.classList.add('mobile-filters--active');

}

  }







    if ( event.target.classList.contains( 'filter-button' ) ) {

        // console.log("filter");
        let e_target = event.target;
        // console.log(e_target);
        // e_target.classlist.add("is-active");
        let d_label = document.getElementsByClassName("filter-button");
        var data_d_label = event.target.getAttribute('data-label')
        // console.log(data_d_label);
        var d_item = document.getElementsByClassName("home-projects-grid-item");

        const d_item_wrapper = document.querySelector(".home-projects-grid");
        d_item_wrapper.style.opacity = "0";

          //BUTTONS
        for(var i = 0; i < d_label.length; i++) {
          d_label[i].classList.remove("active-filter-button");
          }
          var filter_all_button = document.querySelectorAll(".filter-show-all")
          for(var i = 0; i < filter_all_button.length; i++) {
            filter_all_button[i].classList.remove("active-filter-button");
            }


          event.target.classList.add("active-filter-button");



            setTimeout(function(){
              var root = document.getElementsByTagName("HTML")[0];
              root.classList.add("filters-active");
              //LOGOS
          for(var i = 0; i < d_item.length; i++) {
            // console.log(d_item[i].dataset.label);

            if ( d_item[i].classList.contains( data_d_label )) {
              d_item[i].classList.remove("disable-d-item");
              d_item[i].classList.add("active-d-item");
              // msnry.layout();
              }
              else {
                d_item[i].classList.remove("active-d-item");
                d_item[i].classList.add("disable-d-item");
                // msnry.layout();
              }
            }
            setTimeout(function(){
            d_item_wrapper.style.opacity = "1";
          }, 100);
          // var elmnt = document.querySelector(".projects-container");
          // elmnt.scrollIntoView();

        }, 500);

    }



      if ( event.target.classList.contains( 'filter-show-all' ) ) {

          event.target.classList.add("active-filter-button");
          var filter_all_button = document.querySelectorAll(".filter-show-all")
          for(var i = 0; i < filter_all_button.length; i++) {
            filter_all_button[i].classList.add("active-filter-button");
            }

        const d_item_wrapper = document.querySelector(".home-projects-grid");
        d_item_wrapper.style.opacity = "0";
            let d_label = document.getElementsByClassName("filter-button");
        for(var i = 0; i < d_label.length; i++) {
          d_label[i].classList.remove("active-filter-button");
          }
        var d_item = document.getElementsByClassName("home-projects-grid-item");
          setTimeout(function(){
            var root = document.getElementsByTagName("HTML")[0];
            root.classList.remove("filters-active");
          for(var i = 0; i < d_item.length; i++) {
            // console.log(d_item[i].dataset.label);
              d_item[i].classList.remove("disable-d-item");
              d_item[i].classList.add("active-d-item");
              // msnry.layout();
            }
            setTimeout(function(){
            d_item_wrapper.style.opacity = "1";

          }, 100);
        }, 500);
      }


}, false);



//////STICKY/////

var sticky_elem = document.querySelector(".project-filter-container")
stickybits(sticky_elem, {useStickyClasses: true

});











//VIEWS
 function initviews() {


 }


 var anchors = document.getElementsByClassName("home-2-project-link");
for(var z = 0; z < anchors.length; z++) {

  var elem = anchors[z];
  elem.onclick = function() {
    document.querySelector('.project-loading-wrapper').classList.remove("project-loading-wrapper--hide");
    // console.log(elem);
    	var Newheading = this.dataset.heading;
      var imgFullURL = this.dataset.imageurl;
      // console.log(Newheading);
      //   console.log(imgFullURL);
        var bg_image = document.querySelector(".project-loading-wrapper-bg");
        bg_image.removeAttribute('src');
        bg_image.src = imgFullURL;
      var gName = document.createTextNode(Newheading);
      document.querySelector(".project-loading-wrapper-name").innerHTML = "";
     document.querySelector('.project-loading-wrapper-name').appendChild(gName);
    return false;

};
}






 const js_options = [

 {
   from: '(.*)', to: 'jaymo-project-view',
   out: (next) => {
     // player.pause().then(function() {
     //     console.log("vimeopaused beofre load");
     // });
    window.gBodyScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    var root = document.getElementsByTagName("HTML")[0];
    if (typeof logoAnim === 'undefined' || logoAnim === null) {

  }
  else {
  stop_logo_anim();
  }
    setTimeout(function(){
      document.querySelector('.project-loading-wrapper').style.opacity = "1";
setTimeout(function(){
  document.querySelector('.header-bg-wrap').style.display = "none";
document.querySelector('.project-grid-overlay').classList.add("project-grid-overlay--transform-overlay");
 // document.querySelector('#jaymo-body').style.display = "none";
 setTimeout(function(){
   window.scrollTo(0,0);
   next();
 }, 100);
}, 700);


      }, 300);
    // setTimeout(function(){
//     document.querySelector('.project-grid-overlay').classList.add("project-grid-overlay--transform-overlay");
//
//
//    //  var show_overlay = gsap.to(document.querySelector('.project-grid-overlay'), {
//    //   opacity: 1,
//    //   duration: 0.80,
//    //   ease: "power2.inOut",
//    //   onComplete:function(){
//    //       show_overlay.kill();
//    //     root.classList.add('flow-magic--on');
//    //
//    //     window.scrollTo(0, 0);
//    //     console.log("anim-readyyy");
//    //     setTimeout(function(){
//    //     next();
//    //   }, 10);
//    //        }
//    // });
//
//    var i = 0;
//    var original = document.querySelector('.project-loading-wrapper');
//    setTimeout(function(){
//    function duplicate() {
//      var clone = original.cloneNode(true); // "deep" clone
//      clone.id = "the-clone";
//      // or clone.id = ""; if the divs don't need an ID
//      document.querySelector('.film-loader-duplicator').appendChild(clone);
//        }
//        duplicate();
//      }, 100);
//      setTimeout(function(){
//        document.querySelector('#jaymo-body').style.display = "none";
//      }, 700);
// setTimeout(function(){
//  root.classList.add("next-film-loading");
// setTimeout(function(){
//
//     next();
//   }, 0);
// }, 1100);
//   root.classList.remove("header--not-on-top");
// }, 300);

   },
   in: function(next) {
     document.querySelector('#jaymo-body').style.display = "none";
     window.scrollTo(0,0);


     var root = document.getElementsByTagName("HTML")[0];
     // root.classList.remove("next-film-loading");


     // var duplicator_content = document.querySelector('#the-clone');
     // if(duplicator_content){
     // duplicator_content.parentNode.removeChild(duplicator_content);
     //  }


     var fw_hero_film = document.querySelector('#hero-film-id iframe');
     var fw_hero_film_player = new Vimeo.Player(fw_hero_film);

     fw_hero_film_player.on('loaded', function() {

       console.log('video is loaded');
       document.querySelector('#hero-film-id').style.opacity = "1";
     });


     document.querySelector('.project-grid-overlay').classList.remove("project-grid-overlay--pusher");
     // document.querySelector('.project-grid-overlay').classList.remove("transform-overlay");
     let next_link = document.querySelector(".next-film-link");






     document.querySelector('.project-loading-wrapper').style.opacity ="0";
     document.querySelector('.hide-project-overlay-wrapper').style.display = "block";
       setTimeout(function(){

         var Newheading = document.querySelector(".next-film-link").dataset.heading;
         var imgFullURL = document.querySelector(".next-film-link").dataset.imageurl;
         // console.log(Newheading);
         //   console.log(imgFullURL);
           var bg_image = document.querySelector(".project-loading-wrapper-bg");
           bg_image.removeAttribute('src');
           bg_image.src = imgFullURL;
         var gName = document.createTextNode(Newheading);
         document.querySelector(".project-loading-wrapper-name").innerHTML = "";
        document.querySelector('.project-loading-wrapper-name').appendChild(gName);
         document.querySelector('.project-loading-wrapper').classList.add("project-loading-wrapper--hide");
             document.querySelector('.project-loading-wrapper').style.opacity = "1";
             document.querySelector('.project-loading-wrapper').classList.add("transform-for-nex-film");

             document.querySelector('.hide-project-overlay-wrapper').style.opacity = "1";
             document.querySelector('.project-view-styler').style.opacity = "1";
              next();
       }, 600);



     // gsap.to(document.querySelector('.project-loading-wrapper'), 0.5, {
     //   opacity: 0,
     //   delay: 0,
     //   ease: "linear.none",
     //   onComplete:function(){
     //     document.querySelector('.project-loading-wrapper').classList.add("project-loading-wrapper--hide");
     //     document.querySelector('.project-loading-wrapper').style.opacity = "1";
     //     document.querySelector('.hide-project-overlay-wrapper').style.display = "block";
     //     root.classList.add('flow-magic--on');
     //     window.scrollTo(0, 0);
     //     gsap.to(document.querySelector('.hide-project-overlay-wrapper'), 0.5, {
     //       opacity: 1,
     //
     //       ease: "power2.inOut",
     //       onComplete:function(){
     //            }
     //     });
     //     gsap.to(document.querySelector('.project-view-styler'), 0.5, {
     //       opacity: 1,
     //
     //       ease: "liner.none",
     //       onComplete:function(){
     //         next();
     //            }
     //     });
     //        }
     // });



   }

 },

 ///NEXT PROJECT/////

 {
   from: '(.*)', to: 'next-project-transition',
   out: (next) => {

     var root = document.getElementsByTagName("HTML")[0];
document.querySelector('#project-view').style.opacity = "0.5";
document.querySelector('.project-loading-wrapper').classList.remove("project-loading-wrapper--hide");
root.classList.add("next-film-transitions");

      setTimeout(function(){




       var screen_height = window.innerHeight
       || document.documentElement.clientHeight
       || document.body.clientHeight;



       var scroll_height = Math.max(
         // document.body.scrollHeight, document.documentElement.scrollHeight,
         // document.body.offsetHeight, document.documentElement.offsetHeight,
         document.body.clientHeight, document.documentElement.clientHeight
       );

       var scroll_amount = window.pageYOffset

       var distanceFromBottom =   scroll_height - screen_height - scroll_amount;
       // console.log(distanceFromBottom + "from bottom");
       window.tranform_var = -screen_height - distanceFromBottom
       let transform_css_style = 'translate3d(0, ' + tranform_var + 'px ,0)'
       let transform_css_style_fixed = 'translate3d(0, ' + -screen_height + 'px ,0)'



        document.querySelector('.project-grid-overlay').style.transform = transform_css_style

        document.querySelector('.project-loading-wrapper').style.transform = transform_css_style_fixed

        setTimeout(function(){
          root.classList.remove("next-film-transitions");
          document.querySelector('.project-grid-overlay').removeAttribute("style");
          window.scrollTo(0,0);
          // document.querySelector('.project-grid-overlay').style.transform = "translate(0,0)";
          // document.querySelector('.project-loading-wrapper').removeAttribute("style");

          next();

        }, 1300);

       //  gsap.fromTo(document.querySelector('.project-loading-wrapper'), {
       //      x: 0,
       //      y:0,
       //      z:0,
       //    },
       //        {
       //   x: 0,
       //   y:-screen_height,
       //   z:0,
       //   duration: 0.75,
       //   delay: 0,
       //   ease: "power2.inOut",
       //
       //   onComplete:function(){
       //
       //
       //
       //        }
       // });


        //  gsap.fromTo(document.querySelector('.project-grid-overlay'), {
        //      x: 0,
        //      y:0,
        //      z:0,
        //    },
        //        {
        //   x: 0,
        //   y:tranform_var,
        //   z:0,
        //   duration: 0.75,
        //   delay: 0,
        //   ease: "power2.inOut",
        //   clearProps: 'all',
        //   onComplete:function(){
        //     // root.classList.add("next-film-loading");
        //
        //     setTimeout(function(){
        //       window.scrollTo(0,0);
        //       // document.querySelector('.project-grid-overlay').style.transform = "translate(0,0)";
        //       // document.querySelector('.project-loading-wrapper').removeAttribute("style");
        //
        //       next();
        //
        //   }, 300);
        //     // window.scrollTo(0,0);
        //     // root.classList.add("next-film-loading");
        //     // document.querySelector('.project-grid-overlay').style.transform = "translate(0,0)";
        //     // document.querySelector('.project-loading-wrapper').removeAttribute("style");
        //
        //
        //        }
        // });

        // var i = 0;
        // var original = document.querySelector('.project-loading-wrapper');
        //
        // function duplicate() {
        //   var clone = original.cloneNode(true); // "deep" clone
        //   clone.id = "";
        //   // or clone.id = ""; if the divs don't need an ID
        //   document.querySelector('.film-loader-duplicator').appendChild(clone);
        //     }
        //     duplicate();
        //
        //   setTimeout(function(){
        //     root.classList.add("next-film-loading");
        //     document.querySelector('.project-grid-overlay').style.transform = "translate(0,0)";
        //     // window.scrollTo(0,0);
        //     // document.querySelector('.project-grid-overlay').scrollTop = 0;
        //       // document.querySelector('.project-grid-overlay').style.transform = "translate(0,0)";
        //
        //         // document.querySelector('.project-grid-overlay').removeAttribute("style");
        //         // document.querySelector('.project-grid-overlay').style.display = "none";
        //         document.querySelector('.project-loading-wrapper').removeAttribute("style");
        //
        //
        //         setTimeout(function(){
        //           // document.querySelector('.project-grid-overlay').style.display = "none";
        //           // document.querySelector('.project-grid-overlay').removeAttribute("style");
        //           // root.classList.remove("next-film-loading");
        //
        //
        //           // window.scrollTo(0,0);
        //           root.classList.remove("next-film-loading");
        //            var fixed_loader = document.querySelector('.film-loader-duplicator');
        //           while (fixed_loader.firstChild) fixed_loader.removeChild(fixed_loader.firstChild);
        //           next();
        //       }, 300);
        //   }, 1000);
     //  gsap.fromTo(document.querySelector('.project-grid-overlay'), {
     //      x: 0,
     //      y:0,
     //      z:0,
     //    },
     //        {
     //   x: 0,
     //   y:tranform_var,
     //   z:0,
     //   duration: 0.75,
     //   delay: 0,
     //   ease: "power2.inOut",
     //   onComplete:function(){
     //
     //     window.scrollTo(0,0);
     //     root.classList.add("next-film-loading");
     //     document.querySelector('.project-grid-overlay').style.transform = "translate(0,0)";
     //     document.querySelector('.project-loading-wrapper').removeAttribute("style");
     //     next();
     //
     //        }
     // });



  }, 300);
   },
   in: function(next) {
      var root = document.getElementsByTagName("HTML")[0];
       root.classList.remove("next-film-loading");




     let next_link = document.querySelector(".next-film-link");

     var fw_hero_film = document.querySelector('#hero-film-id iframe');
     var fw_hero_film_player = new Vimeo.Player(fw_hero_film);

     fw_hero_film_player.on('loaded', function() {

       // console.log('video is loaded');
       document.querySelector('#hero-film-id').style.opacity = "1";
     });

      // next_link.onclick = function() {
        // console.log("THIS LINK WORKS");
        // Marquee3k.pauseAll();
        // console.log(elem);

        // return false;

     // };
     // if (window.innerWidth > 600) {
     //   var root = document.getElementsByTagName("HTML")[0];
     //   var marq2 = document.querySelector('.marq-hide-for-desktop-2');
     //   marq2.parentNode.removeChild(marq2);
     //   var marq3 = document.querySelector('.marq-hide-for-desktop-3');
     //   marq3.parentNode.removeChild(marq3);
     // }
     // Marquee3k.init();


     var root = document.getElementsByTagName("HTML")[0];
     window.tranform_var = undefined;


     document.querySelector('.project-loading-wrapper').style.transition = "opacity 0.5s";
     document.querySelector('.project-loading-wrapper').style.opacity ="0";
     document.querySelector('.hide-project-overlay-wrapper').style.display = "block";
       setTimeout(function(){
         document.querySelector('.project-grid-overlay').removeAttribute("style");

         document.querySelector('.project-loading-wrapper').classList.add("project-loading-wrapper--hide");
          document.querySelector('.project-loading-wrapper').removeAttribute("style");
             document.querySelector('.project-loading-wrapper').style.opacity = "1";

             document.querySelector('.hide-project-overlay-wrapper').style.opacity = "1";
             document.querySelector('.project-view-styler').style.opacity = "1";
             var Newheading = document.querySelector(".next-film-link").dataset.heading;
             var imgFullURL = document.querySelector(".next-film-link").dataset.imageurl;
             // console.log(Newheading);
             //   console.log(imgFullURL);
               var bg_image = document.querySelector(".project-loading-wrapper-bg");
               bg_image.removeAttribute('src');
               bg_image.src = imgFullURL;
             var gName = document.createTextNode(Newheading);
             document.querySelector(".project-loading-wrapper-name").innerHTML = "";
            document.querySelector('.project-loading-wrapper-name').appendChild(gName);




              next();
       }, 600);

     // gsap.to(document.querySelector('.project-loading-wrapper'), 0.5, {
     //   opacity: 0,
     //   delay: 0.5,
     //   ease: "linear.none",
     //   onComplete:function(){
     //     document.querySelector('.project-loading-wrapper').classList.add("project-loading-wrapper--hide");
     //      document.querySelector('.project-loading-wrapper').style.opacity = "1";
     //     document.querySelector('.hide-project-overlay-wrapper').style.display = "block";
     //      root.classList.remove("next-film-loading");
     //
     //
     //     gsap.to(document.querySelector('.project-view-styler'), 0.5, {
     //       opacity: 1,
     //
     //       ease: "linear.none",
     //       onComplete:function(){
     //         next();
     //            }
     //     });
     //        }
     // });



   }

 },





 {
   from: '(.*)', to: 'hide-active-project',
   out: (next) => {
     document.querySelector('.hide-project-overlay-wrapper').style.display = "block";
      var root = document.getElementsByTagName("HTML")[0];
     window.pagesrol = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
     var minuspagesrol = - (pagesrol);
     // console.log(pagesrol);
     document.querySelector('.header-bg-wrap').removeAttribute("style");
     document.querySelector(".project-grid-overlay").classList.remove("project-grid-overlay--transform-overlay");
     document.querySelector(".inner-mover--project").style.transform = "translateY(" + minuspagesrol + "px)";

     document.querySelector('#jaymo-body').removeAttribute("style");
     document.querySelector('.hide-project-overlay-wrapper').style.opacity = "0";
      document.querySelector('.project-view-styler').style.opacity = "0";
      document.querySelector('.project-loading-wrapper').style.opacity = "0";
      document.querySelector('.project-loading-wrapper').classList.remove("transform-for-nex-film");
      root.classList.add("filter-sticky--super-active");
     setTimeout(function(){
       window.scrollTo(0, gBodyScroll);
       document.querySelector(".project-grid-overlay").style.opacity = "0";
       setTimeout(function(){
         document.querySelector(".project-grid-overlay").classList.add("project-grid-overlay--pusher");
         document.querySelector(".project-grid-overlay").removeAttribute("style");
         document.querySelector(".inner-mover--project").removeAttribute("style");
         document.querySelector('.hide-project-overlay-wrapper').removeAttribute("style");

         next();
       }, 600);
     }, 500);





     //   window.scrollTo(0, gBodyScroll);
     //   root.classList.add("filter-sticky--super-active");
     //   document.querySelector(".project-grid-overlay").style.opacity = "0";
     //   setTimeout(function(){
     //     document.querySelector(".project-grid-overlay").classList.add("project-grid-overlay--pusher");
     //     document.querySelector(".project-grid-overlay").classList.remove("project-grid-overlay--transform-overlay");
     //     document.querySelector(".project-grid-overlay").removeAttribute("style");
     //     document.querySelector(".inner-mover--project").removeAttribute("style");
     //        document.querySelector('.hide-project-overlay-wrapper').removeAttribute("style");
     //   setTimeout(function(){
     //       next();
     //
     //     }, 10);
     //
     //   }, 500);
     // }, 500);

     // gsap.to(document.querySelector('.hide-project-overlay-wrapper'), 0.3, {
     //   opacity: 0,
     //   ease: "linear.none",
     //      clearProps: "all",
     //   onComplete:function(){
     //     document.querySelector('.hide-project-overlay-wrapper').removeAttribute("style");
     //      document.querySelector('.hide-project-overlay-wrapper').style.display = "none";
     //      window.scrollTo(0, gBodyScroll);
     //
     //        setTimeout(function(){
     //      document.querySelector(".project-grid-overlay").style.opacity = "0";
     //    }, 100);
     //
     //      // document.querySelector(".project-grid-overlay").classList.add("project-grid-overlay--pusher");
     //      // document.querySelector(".project-grid-overlay").removeAttribute("style");
     //      // var close_overlay = gsap.fromTo(document.querySelector('.project-grid-overlay'), {
     //      //     x: 0,
     //      //     y:0,
     //      //     z:0,
     //      //   },
     //      //   {
     //      //   yPercent: 100,
     //      //   x: 0,
     //      //   z:0,
     //      //   duration: 1,
     //      //   force3D: true,
     //      //   ease: "power2.inOut",
     //      //   clearProps: "all",
     //      //   onComplete:function(){
     //      //     close_overlay.kill();
     //      //       console.log("anim-backkkk");
     //      //     //   root.classList.add('headroom--pinned');
     //      //     //   root.classList.remove('headroom--unpinned');
     //      //     // root.classList.remove('header-fix-to-top');
     //      //     document.querySelector(".inner-mover--project").removeAttribute("style");
     //      //     setTimeout(function(){
     //      //     next();
     //      //   }, 10);
     //      //        }
     //      //
     //      // });
     //        }
     // });











   },
   in: function(next) {
     var duplicator_content = document.querySelector('#the-clone');
     if(duplicator_content){
     duplicator_content.parentNode.removeChild(duplicator_content);
      }
       var root = document.getElementsByTagName("HTML")[0];

     root.classList.add("headroom--unpinned");
     setTimeout(function(){
          root.classList.add("filter-sticky--active");
        root.classList.remove("filter-sticky--super-active");
        root.classList.remove("keep-header-hidden");
        window.logoAnim = setInterval(myTimer, 8000);
      }, 500);
     next();

   }

 },


 {
   from: '(.*)', to: 'basic-transition',
   out: (next) => {
     var root = document.getElementsByTagName("HTML")[0];
      document.body.style.display = "none";
     if (document.body.classList.contains('single-projects')) {
     var home_url = "<?php echo home_url() ?>#films"; // "A string here"
     // console.log(home_url);
     window.location.replace(home_url);
       }
       if (document.body.classList.contains('home')) {
       var home_url = "<?php echo home_url() ?>"; // "A string here"
       // console.log(home_url);
       window.location.replace(home_url);
         }
   },

   in: function(next) {

   },

 },


 ];

 const options = {

   containers: ['.pjax-container'],
   animationSelector: '[class*="page-transition"]',
   animateHistoryBrowsing: true,
   plugins: [
     new SwupJsPlugin(js_options),
     // new SwupDebugPlugin(),

     // new SwupPreloadPlugin(),
     new SwupBodyClassPlugin(),
 //     new SwupScrollPlugin({
 //     doScrollingRightAway: false,
 //     animateScroll: false,
 //     scrollFriction: 0.3,
 //     cache: false,
 //     scrollAcceleration: 0.04,
 // }),
   ],
 };
 const swup = new Swup(options);
 // swup.findPlugin('ScrollPlugin');
 //
 // let scrollPositions = [];
 // let scrollToSavedPosition = null;


window.gBodyScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

 // var close_overlay_project_view = function() {
 //   var root = document.getElementsByTagName("HTML")[0];
 //   var pagesrol = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
 //   var minuspagesrol = - (pagesrol);
 //   // console.log(pagesrol);
 //
 //   setTimeout(function(){
 //     root.classList.add('headroom--pinned');
 //     root.classList.remove('headroom--unpinned');
 // }, 10);
 //
 //   document.querySelector(".inner-mover--project").style.transform = "translateY(" + minuspagesrol + "px)";
 //   root.classList.remove('flow-magic--on');
 //   setTimeout(function(){
 //   window.scrollTo(0, gBodyScroll);
 //
 //   var close_overlay = gsap.to(document.querySelector('.project-grid-overlay'), 0.75, {
 //     yPercent: 100,
 //     y:0,
 //     z:0,
 //
 //     ease: "power2.inOut",
 //     clearProps: "all",
 //     onComplete:function(){
 //       close_overlay.kill();
 //         console.log("anim-backkkk");
 //         root.classList.add('headroom--pinned');
 //         root.classList.remove('headroom--unpinned');
 //       root.classList.remove('header-fix-to-top');
 //       document.querySelector(".inner-mover--project").removeAttribute("style");
 //
 //          }
 //   });
 // }, 300);
 // }





 ////// BIO SCRIPTS//////



 var bio_links = document.getElementsByClassName("js-activate-bio");
for(var z = 0; z < bio_links.length; z++) {

  var elem = bio_links[z];
  elem.onclick = function() {

window.gBodyScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
// console.log(gBodyScroll + "bio");
// console.log("BIO LINK");
    var root = document.getElementsByTagName("HTML")[0];
document.querySelector('#jaymo-body').style.transition = "opacity 0.5s";
document.querySelector('#jaymo-body').style.opacity = "0.3";
document.querySelector('.jaymo-bio-section').style.display = "block";

  if (typeof logoAnim === 'undefined' || logoAnim === null) {

}
else {
stop_logo_anim();
}
  document.querySelector('.hide-bio-overlay-wrapper').style.display = "block";
setTimeout(function(){
document.querySelector('.jaymo-bio-section').classList.add("jaymo-bio-section--show-bio-section");
setTimeout(function(){
  document.querySelector('.header-bg-wrap').style.visibility = "hidden";
document.querySelector('#jaymo-body').style.display = "none";
document.querySelector('.jaymo-bio-section').style.position = "relative";
window.scrollTo(0, 0);
// window.scrollTo(0, 0);
document.querySelector('.hide-bio-overlay-wrapper').style.opacity = "1";
setTimeout(function(){
document.querySelector('.jaymo-bio-section').classList.add("show-shaker");
setTimeout(function(){
document.querySelector('.jaymo-bio-section').classList.add("run-shaker");
}, 300);

}, 100);
}, 1000);
}, 500);


    root.classList.remove("header--not-on-top");
// setTimeout(function(){
//
// var root = document.getElementsByTagName("HTML")[0];
// // document.querySelector('.inner-mover--bio').style.filter = "none";
// var root = document.getElementsByTagName("HTML")[0];
//     root.classList.add('bio-view--active');
//     window.scrollTo(0, 0);
// setTimeout(function(){
// document.querySelector('.hide-bio-overlay-wrapper').style.opacity = "1";
//
// }, 100);
// }, 600);

    return false;
};

}

document.querySelector('.bio-hider-button').onclick = function() {
  var root = document.getElementsByTagName("HTML")[0];
  // document.querySelector('.jaymo-bio-section').classList.remove("show-shaker");
  document.querySelector('.jaymo-bio-section').classList.remove("run-shaker");

  // setTimeout(function(){
  // document.querySelector('.jaymo-bio-section').classList.remove("run-shaker");
  // }, 300);

  window.pagesrol = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  var minuspagesrol = - (pagesrol);
  // console.log(pagesrol);
  if (root.classList.contains('header--not-on-top')) {
root.classList.add("bio-button--keep-position");
  }
  document.querySelector('.header-bg-wrap').removeAttribute("style");
  document.querySelector('.hide-bio-overlay-wrapper').style.display = "block";
    document.querySelector('.hide-bio-overlay-wrapper').style.opacity = "0";
    document.querySelector('.jaymo-bio-section').style.position = "fixed";
  document.querySelector(".inner-mover--bio").style.transform = "translateY(" + minuspagesrol + "px)";
  document.querySelector('#jaymo-body').removeAttribute("style");
  root.classList.add("filter-sticky--super-active");
  // window.scrollTo(0, 0);



    setTimeout(function(){

      window.scrollTo(0, gBodyScroll);
  setTimeout(function(){
      document.querySelector('.jaymo-bio-section').classList.remove("jaymo-bio-section--show-bio-section");
      setTimeout(function(){
        document.querySelector(".inner-mover--bio").removeAttribute("style");
        document.querySelector('.jaymo-bio-section').removeAttribute("style");
        root.classList.add("filter-sticky--active");
        root.classList.remove("filter-sticky--super-active");
        document.querySelector('.jaymo-bio-section').classList.remove("show-shaker");
        document.querySelector('.hide-bio-overlay-wrapper').style.display = "none";
        root.classList.remove("bio-button--keep-position");
      window.logoAnim = setInterval(myTimer, 8000);
      }, 900);
    }, 200);
  }, 300);


 //    setTimeout(function(){
 //      window.scrollTo(0, gBodyScroll);
 //      document.querySelector('.jaymo-bio-section').style.opacity = "0";
 //
 //     setTimeout(function(){
 //       document.querySelector(".inner-mover--bio").removeAttribute("style");
 //       document.querySelector('.jaymo-bio-section').classList.remove("jaymo-bio-section--show-bio-section");
 //       document.querySelector('.jaymo-bio-section').removeAttribute("style");
 //       document.querySelector('.hide-bio-overlay-wrapper').removeAttribute("style");
 //       document.querySelector('.bio-container').removeAttribute("style");
 //        document.querySelector('.hide-bio-overlay-wrapper').style.display = "none";
 //        root.classList.add("filter-sticky--active");
 //      root.classList.remove("filter-sticky--super-active");
 //   }, 700);
 // }, 500);




return false;

};





    var iframe = document.querySelector('#jaymo-showreel');
    var player = new Vimeo.Player(iframe);


    player.pause().then(function() {
        // console.log("vimeopaused before load");
    });
    player.on('loaded', function() {
      // console.log('video is loaded');

        // player.pause().then(function() {
        //     console.log("vimeopaused");
        // });


      player.play().then(function() {
        // console.log('vimeoplay');
        setTimeout(function(){
          // console.log('vimeoplay-anims');
        document.querySelector(".hero-video-container").removeAttribute("style");




        document.querySelector(".text-eq").classList.add("text-stroker");
        document.querySelector(".hero-video-container").removeAttribute("style");
          document.querySelector(".jm-reel-header").classList.add("text-blender");
          document.querySelector('.hero-film-loading').style.opacity = "0";
          var root = document.getElementsByTagName("HTML")[0];
          root.classList.add("show-jaymo");
          setTimeout(function() {
            root.classList.remove("show-jaymo");
            // console.log('two');
          },4000);
           window.logoAnim = setInterval(myTimer, 8000);
           setTimeout(function() {
           document.querySelector('.jm-reel-header').style.opacity = "0";
           var d1 = document.querySelector('.jm-reel-header');
           setTimeout(function() {
           d1.parentNode.removeChild(d1);
           var d2 = document.querySelector('.hero-film-loading');
           d2.parentNode.removeChild(d2);
         },1000);
         },3200);
        // gsap.to(document.querySelector('.hero-film-loading'), 0, {
        //   opacity: 0,
        //
        //
        //   ease: "power2.inOut",
        //   onComplete:function(){
        //     var root = document.getElementsByTagName("HTML")[0];
        //     root.classList.add("show-jaymo");
        //     setTimeout(function() {
        //       root.classList.remove("show-jaymo");
        //       console.log('two');
        //     },4000);
        //      window.logoAnim = setInterval(myTimer, 8000);
        //
        //
        //
        //
        //     gsap.to(document.querySelector('.jm-reel-header'), 1, {
        //       opacity: 0,
        //       delay:3.2,
        //
        //
        //       ease: "power2.inOut",
        //       onComplete:function(){
        //         var d1 = document.querySelector('.jm-reel-header');
        //         d1.parentNode.removeChild(d1);
        //         var d2 = document.querySelector('.hero-film-loading');
        //         d2.parentNode.removeChild(d2);
        //
        //            }
        //     });
        //        }
        // });
      }, 300);
}).catch(function(error) {
  switch (error.name) {
    case 'PasswordError':
        // The video is password-protected
        break;

    case 'PrivacyError':
        // The video is private
        break;

    default:
        // Some other error occurred
        break;
  }
});



    });

    // player.getVideoTitle().then(function(title) {
    //   console.log('title:', title);
    // });


    //////SCROLLL////





var scroll = new SmoothScroll('a[href*="#"]', {
speed: 1000,
easing: 'easeInOutCubic',
updateURL: false,
speedAsDuration: false,
durationMin: 500,

});

var refresh_aos = function (event) {
  setTimeout(function(){
    AOS.refresh();
    // console.log("aos refresh");
}, 1000);


}

document.addEventListener('scrollStop', refresh_aos, false);




///////NOISE/////


const noise = () => {
    let canvas, ctx;

    let wWidth, wHeight;

    let noiseData = [];
    let frame = 0;

    let loopTimeout;


    // Create Noise
    const createNoise = () => {
        const idata = ctx.createImageData(wWidth, wHeight);
        const buffer32 = new Uint32Array(idata.data.buffer);
        const len = buffer32.length;

        for (let i = 0; i < len; i++) {
            if (Math.random() < 0.5) {
                buffer32[i] = 0xff000000;
            }
        }

        noiseData.push(idata);
    };


    // Play Noise
    const paintNoise = () => {
        if (frame === 9) {
            frame = 0;
        } else {
            frame++;
        }

        ctx.putImageData(noiseData[frame], 0, 0);
    };


    // Loop
    const loop = () => {
        paintNoise(frame);

        loopTimeout = window.setTimeout(() => {
            window.requestAnimationFrame(loop);
        }, (1000 / 25));
    };


    // Setup
    const setup = () => {
        wWidth = window.innerWidth;
        wHeight = window.innerHeight;

        canvas.width = wWidth;
        canvas.height = wHeight;

        for (let i = 0; i < 10; i++) {
            createNoise();
        }

        loop();
    };


    // Reset
    let resizeThrottle;
    const reset = () => {
        window.addEventListener('resize', () => {
            window.clearTimeout(resizeThrottle);

            resizeThrottle = window.setTimeout(() => {
                window.clearTimeout(loopTimeout);
                setup();
            }, 200);
        }, false);
    };


    // Init
    const init = (() => {
        canvas = document.getElementById('noise');
        ctx = canvas.getContext('2d');

        setup();
    })();
};

// noise();



/////PARALLEX ////

function disable_sizing_amims() {
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.add('disable-animation-delay');
  setTimeout(function(){
    root.classList.remove('disable-animation-delay');
  }, 1000);
}

window.onresize = disable_sizing_amims;


/////HEADER SCROLL////


window.onscroll = function() {
var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop

var root = document.getElementsByTagName("HTML")[0];
if (sticky_elem.classList.contains( 'js-is-sticky' )) {
	// console.log("is sticky");

  root.classList.add('filter-sticky--active');
} else {
	root.classList.remove('filter-sticky--active');
}


if (scrollTop > 20) {
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.add("header--not-on-top");


}
else {
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.remove("header--not-on-top");

}

};




/////SET HERO SECTION HEIGHT ////


var screen_height_load  = window.innerHeight
|| document.documentElement.clientHeight
|| document.body.clientHeight;

if (window.innerWidth < 1200) {
    document.querySelector('.section--jm-reel').style.height = screen_height_load + "px"
}


// function reportWindowSize() {
//   if (window.innerHeight < screen_height_load - 200){
//     console.log(screen_height_load);
//     console.log(window.innerHeight);
//
//     console.log("CHANGIIN HEIGHT" + screen.orientation.angle);
  // }

  // if (window.innerHeight > screen_height_load + 200){
  //   console.log(screen_height_load);
  //   console.log(window.innerHeight);
  //
  //   console.log("CHANGIIN HEIGHT" + screen.orientation.angle);
  // }
// }

// window.addEventListener('resize', reportWindowSize);

// window.addEventListener("orientationchange", function() {
//   console.log("the orientation of the device is now " + screen.orientation.angle);
//
// setTimeout(function(){
//   var screen_height_orientation  = window.innerHeight
//   || document.documentElement.clientHeight
//   || document.body.clientHeight;
//   alert("Hello! I am an alert box!!");
//   document.querySelector('.section--jm-reel').style.height = screen_height_orientation + "px"
// }, 300);
// });
