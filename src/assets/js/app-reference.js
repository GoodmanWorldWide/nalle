

//////INCLUDE VANTA.JS FUCKERY //////////////////

!function(e,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports._vantaEffect=t():e._vantaEffect=t()}("undefined"!=typeof self?self:this,function(){return function(e){var t={};function n(i){if(t[i])return t[i].exports;var o=t[i]={i:i,l:!1,exports:{}};return e[i].call(o.exports,o,o.exports,n),o.l=!0,o.exports}return n.m=e,n.c=t,n.d=function(e,t,i){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:i})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var i=Object.create(null);if(n.r(i),Object.defineProperty(i,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var o in e)n.d(i,o,function(t){return e[t]}.bind(null,o));return i},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=9)}([function(e,t,n){"use strict";function i(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e}function o(){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)||window.innerWidth<600}n.d(t,"c",function(){return i}),n.d(t,"d",function(){return o}),n.d(t,"h",function(){return s}),n.d(t,"g",function(){return r}),n.d(t,"f",function(){return a}),n.d(t,"e",function(){return h}),n.d(t,"a",function(){return u}),n.d(t,"b",function(){return l}),Number.prototype.clamp=function(e,t){return Math.min(Math.max(this,e),t)};const s=e=>e[Math.floor(Math.random()*e.length)];function r(e,t){return null==e&&(e=0),null==t&&(t=1),e+Math.random()*(t-e)}function a(e,t){return null==e&&(e=0),null==t&&(t=1),Math.floor(e+Math.random()*(t-e+1))}var h=e=>document.querySelector(e);const u=e=>"number"==typeof e?"#"+("00000"+e.toString(16)).slice(-6):e,l=(e,t=1)=>{const n=u(e),i=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(n),o=i?{r:parseInt(i[1],16),g:parseInt(i[2],16),b:parseInt(i[3],16)}:null;return"rgba("+o.r+","+o.g+","+o.b+","+t+")"}},function(e,t,n){"use strict";n.d(t,"a",function(){return r});var i=n(0);const o="object"==typeof window,s=o&&window.THREE||{};o&&!window.VANTA&&(window.VANTA={});const r=o&&window.VANTA||{};r.register=(e,t)=>r[e]=e=>new t(e),r.version="0.5.1";var a=function(){return Array.prototype.unshift.call(arguments,"[VANTA]"),console.error.apply(this,arguments)};r.VantaBase=class{constructor(e={}){if(!o)return!1;var t,n,h,u;if(r.current=this,this.windowMouseMoveWrapper=this.windowMouseMoveWrapper.bind(this),this.windowTouchWrapper=this.windowTouchWrapper.bind(this),this.resize=this.resize.bind(this),this.animationLoop=this.animationLoop.bind(this),this.restart=this.restart.bind(this),this.options=Object(i.c)({},this.defaultOptions),e instanceof HTMLElement||"string"==typeof e?Object(i.c)(this.options,{el:e}):Object(i.c)(this.options,e),this.el=this.options.el,null==this.el)a('Instance needs "el" param!');else if(!(this.options.el instanceof HTMLElement||(u=this.el,this.el=Object(i.e)(u),this.el)))return void a("Cannot find element",u);for(h=0;h<this.el.children.length;h++)t=this.el.children[h],"static"===getComputedStyle(t).position&&(t.style.position="relative"),"auto"===getComputedStyle(t).zIndex&&(t.style.zIndex=1);"static"===getComputedStyle(this.el).position&&(this.el.style.position="relative"),this.initThree(),this.setSize(),this.uniforms={u_time:{type:"f",value:1},u_resolution:{type:"v2",value:new s.Vector2(1,1)},u_mouse:{type:"v2",value:new s.Vector2(0,0)}};try{this.init()}catch(e){return n=e,a("Init error"),a(n),this.el.removeChild(this.renderer.domElement),void(this.options.backgroundColor&&(console.log("[VANTA] Falling back to backgroundColor"),this.el.style.background=Object(i.a)(this.options.backgroundColor)))}window.addEventListener("resize",this.resize),this.resize(),this.animationLoop(),window.addEventListener("scroll",this.windowMouseMoveWrapper),window.addEventListener("mousemove",this.windowMouseMoveWrapper),window.addEventListener("touchstart",this.windowTouchWrapper),window.addEventListener("touchmove",this.windowTouchWrapper)}applyCanvasStyles(e,t={}){Object(i.c)(e.style,{position:"absolute",zIndex:0,top:0,left:0,background:""}),Object(i.c)(e.style,t),e.classList.add("vanta-canvas")}initThree(){s.WebGLRenderer?(this.renderer=new s.WebGLRenderer({alpha:!0,antialias:!0}),this.el.appendChild(this.renderer.domElement),this.applyCanvasStyles(this.renderer.domElement),isNaN(this.options.backgroundAlpha)&&(this.options.backgroundAlpha=1),this.scene=new s.Scene):console.warn("[VANTA] No THREE defined on window")}windowMouseMoveWrapper(e){const t=this.renderer.domElement.getBoundingClientRect(),n=e.clientX-t.left,i=e.clientY-t.top;n>=0&&i>=0&&n<=t.width&&i<=t.height&&(this.mouseX=n,this.mouseY=i,this.options.mouseEase||this.triggerMouseMove(n,i))}windowTouchWrapper(e){if(1===e.touches.length){const t=this.renderer.domElement.getBoundingClientRect(),n=e.touches[0].clientX-t.left,i=e.touches[0].clientY-t.top;n>=0&&i>=0&&n<=t.width&&i<=t.height&&(this.mouseX=n,this.mouseY=i,this.options.mouseEase||this.triggerMouseMove(n,i))}}triggerMouseMove(e,t){this.uniforms&&(this.uniforms.u_mouse.value.x=e/this.scale,this.uniforms.u_mouse.value.y=t/this.scale);const n=e/this.width,i=t/this.height;"function"==typeof this.onMouseMove&&this.onMouseMove(n,i)}setSize(){this.scale||(this.scale=1),Object(i.d)()&&this.options.scaleMobile?this.scale=this.options.scaleMobile:this.options.scale&&(this.scale=this.options.scale),this.width=this.el.offsetWidth||window.innerWidth,this.height=this.el.offsetHeight||window.innerHeight}resize(){var e,t;this.setSize(),null!=(e=this.camera)&&(e.aspect=this.width/this.height),null!=(t=this.camera)&&"function"==typeof t.updateProjectionMatrix&&t.updateProjectionMatrix(),this.renderer&&(this.renderer.setSize(this.width,this.height),this.renderer.setPixelRatio(window.devicePixelRatio/this.scale)),"function"==typeof this.onResize&&this.onResize()}animationLoop(){var e,t,n,i,o,s;return this.t||(this.t=0),this.t+=1,this.t2||(this.t2=0),this.t2+=this.options.speed||1,this.uniforms&&(this.uniforms.u_time.value=.016667*this.t2),e=this.el.offsetHeight,t=this.el.getBoundingClientRect(),s=window.pageYOffset||(document.documentElement||document.body.parentNode||document.body).scrollTop,i=(o=t.top+s)-window.innerHeight,n=o+e,this.options.mouseEase&&(this.mouseEaseX=this.mouseEaseX||this.mouseX||0,this.mouseEaseY=this.mouseEaseY||this.mouseY||0,Math.abs(this.mouseEaseX-this.mouseX)+Math.abs(this.mouseEaseY-this.mouseY)>.1&&(this.mouseEaseX=this.mouseEaseX+.05*(this.mouseX-this.mouseEaseX),this.mouseEaseY=this.mouseEaseY+.05*(this.mouseY-this.mouseEaseY),this.triggerMouseMove(this.mouseEaseX,this.mouseEaseY))),i<=s&&s<=n&&("function"==typeof this.onUpdate&&this.onUpdate(),this.scene&&this.camera&&(this.renderer.render(this.scene,this.camera),this.renderer.setClearColor(this.options.backgroundColor,this.options.backgroundAlpha)),this.fps&&this.fps.update&&this.fps.update()),this.req=window.requestAnimationFrame(this.animationLoop)}restart(){if(this.scene)for(;this.scene.children.length;)this.scene.remove(this.scene.children[0]);"function"==typeof this.onRestart&&this.onRestart(),this.init()}init(){"function"==typeof this.onInit&&this.onInit()}destroy(){"function"==typeof this.onDestroy&&this.onDestroy(),window.removeEventListener("touchstart",this.windowTouchWrapper),window.removeEventListener("touchmove",this.windowTouchWrapper),window.removeEventListener("scroll",this.windowMouseMoveWrapper),window.removeEventListener("mousemove",this.windowMouseMoveWrapper),window.removeEventListener("resize",this.resize),window.cancelAnimationFrame(this.req),this.renderer&&(this.el.removeChild(this.renderer.domElement),this.renderer=null,this.scene=null)}},t.b=r.VantaBase},function(e,t,n){"use strict";n.d(t,"b",function(){return s});var i=n(1),o=n(0);n.d(t,"a",function(){return i.a}),"object"==typeof THREE&&(THREE.Color.prototype.toVector=function(){return new THREE.Vector3(this.r,this.g,this.b)});class s extends i.b{constructor(e){super(e),this.mode="shader",this.updateUniforms=this.updateUniforms.bind(this)}initBasicShader(e=this.fragmentShader,t=this.vertexShader){var n,i,o;return t||(t="uniform float u_time;\nuniform vec2 u_resolution;\nvoid main() {\n  gl_Position = vec4( position, 1.0 );\n}"),this.updateUniforms(),"function"==typeof this.valuesChanger&&this.valuesChanger(),n=new THREE.ShaderMaterial({uniforms:this.uniforms,vertexShader:t,fragmentShader:e}),(o=this.options.texturePath)&&(this.uniforms.u_tex={type:"t",value:(new THREE.TextureLoader).load(o)}),i=new THREE.Mesh(new THREE.PlaneGeometry(2,2),n),this.scene.add(i),this.camera=new THREE.Camera,this.camera.position.z=1}updateUniforms(){var e,t,n,i;for(e in t={},n=this.options)i=n[e],-1!==e.toLowerCase().indexOf("color")?t[e]={type:"v3",value:new THREE.Color(i).toVector()}:"number"==typeof i&&(t[e]={type:"f",value:i});return Object(o.c)(this.uniforms,t)}init(){super.init(),this.fragmentShader&&this.initBasicShader()}resize(){super.resize(),this.uniforms.u_resolution.value.x=this.width/this.scale,this.uniforms.u_resolution.value.y=this.height/this.scale}}},,,,,,,function(e,t,n){"use strict";n.r(t);var i=n(2);class o extends i.b{}t.default=i.a.register("FOG",o),o.prototype.defaultOptions={highlightColor:16761600,midtoneColor:16719616,lowlightColor:2949375,baseColor:16772075,blurFactor:.6,speed:1,zoom:1,scale:2,scaleMobile:4},o.prototype.fragmentShader="uniform vec2 u_resolution;\nuniform vec2 u_mouse;\nuniform float u_time;\n\nuniform float blurFactor;\nuniform vec3 baseColor;\nuniform vec3 lowlightColor;\nuniform vec3 midtoneColor;\nuniform vec3 highlightColor;\nuniform float zoom;\n\nfloat random (in vec2 _st) {\n  return fract(sin(dot(_st.xy,\n                        vec2(12.9898,78.233)))*\n      43758.5453123);\n}\n\n// Based on Morgan McGuire @morgan3d\n// https://www.shadertoy.com/view/4dS3Wd\nfloat noise (in vec2 _st) {\n  vec2 i = floor(_st);\n  vec2 f = fract(_st);\n\n  // Four corners in 2D of a tile\n  float a = random(i);\n  float b = random(i + vec2(1.0, 0.0));\n  float c = random(i + vec2(0.0, 1.0));\n  float d = random(i + vec2(1.0, 1.0));\n\n  vec2 u = f * f * (3.0 - 2.0 * f);\n\n  return mix(a, b, u.x) +\n          (c - a)* u.y * (1.0 - u.x) +\n          (d - b) * u.x * u.y;\n}\n\n#define NUM_OCTAVES 6\n\nfloat fbm ( in vec2 _st) {\n  float v = 0.0;\n  float a = blurFactor;\n  vec2 shift = vec2(100.0);\n  // Rotate to reduce axial bias\n  mat2 rot = mat2(cos(0.5), sin(0.5),\n                  -sin(0.5), cos(0.50));\n  for (int i = 0; i < NUM_OCTAVES; ++i) {\n      v += a * noise(_st);\n      _st = rot * _st * 2.0 + shift;\n      a *= (1. - blurFactor);\n  }\n  return v;\n}\n\nvoid main() {\n  vec2 st = gl_FragCoord.xy / u_resolution.xy*3.;\n  st.x *= 0.7 * u_resolution.x / u_resolution.y ; // Still keep it more landscape than square\n  st *= zoom;\n\n  // st += st * abs(sin(u_time*0.1)*3.0);\n  vec3 color = vec3(0.0);\n\n  vec2 q = vec2(0.);\n  q.x = fbm( st + 0.00*u_time);\n  q.y = fbm( st + vec2(1.0));\n\n  vec2 dir = vec2(0.15,0.126);\n  vec2 r = vec2(0.);\n  r.x = fbm( st + 1.0*q + vec2(1.7,9.2)+ dir.x*u_time );\n  r.y = fbm( st + 1.0*q + vec2(8.3,2.8)+ dir.y*u_time);\n\n  float f = fbm(st+r);\n\n  color = mix(baseColor,\n              lowlightColor,\n              clamp((f*f)*4.0,0.0,1.0));\n\n  color = mix(color,\n              midtoneColor,\n              clamp(length(q),0.0,1.0));\n\n  color = mix(color,\n              highlightColor,\n              clamp(length(r.x),0.0,1.0));\n\n  vec3 finalColor = mix(baseColor, color, f*f*f+.6*f*f+.5*f);\n  gl_FragColor = vec4(finalColor,1.0);\n}\n"}])});

//////INCLUDE VANTA.JS FUCKERY //////////////////


function scrollIt(destination, duration = 200, easing = 'linear', callback) {

  const easings = {
    linear(t) {
      return t;
    },
    easeInQuad(t) {
      return t * t;
    },
    easeOutQuad(t) {
      return t * (2 - t);
    },
    easeInOutQuad(t) {
      return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
    },
    easeInCubic(t) {
      return t * t * t;
    },
    easeOutCubic(t) {
      return (--t) * t * t + 1;
    },
    easeInOutCubic(t) {
      return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
    },
    easeInQuart(t) {
      return t * t * t * t;
    },
    easeOutQuart(t) {
      return 1 - (--t) * t * t * t;
    },
    easeInOutQuart(t) {
      return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t;
    },
    easeInQuint(t) {
      return t * t * t * t * t;
    },
    easeOutQuint(t) {
      return 1 + (--t) * t * t * t * t;
    },
    easeInOutQuint(t) {
      return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
    }
  };

  const start = window.pageYOffset;
  const startTime = 'now' in window.performance ? performance.now() : new Date().getTime();

  const documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
  const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
  const destinationOffset = typeof destination === 'number' ? destination : destination.offsetTop;
  const destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset);

  if ('requestAnimationFrame' in window === false) {
    window.scroll(0, destinationOffsetToScroll);
    if (callback) {
      callback();
    }
    return;
  }

  function scroll() {
    const now = 'now' in window.performance ? performance.now() : new Date().getTime();
    const time = Math.min(1, ((now - startTime) / duration));
    const timeFunction = easings[easing](time);
    window.scroll(0, Math.ceil((timeFunction * (destinationOffsetToScroll - start)) + start));

    if (window.pageYOffset === destinationOffsetToScroll) {
      if (callback) {
        callback();
      }
      return;
    }

    requestAnimationFrame(scroll);
  }

  scroll();
}


// import Player from '@vimeo/player';
import navs from "./modules/navs";

import {
  headroom
} from "./modules/headroom";

// import AOS from 'aos';
import slider from './modules/slider';

import SmoothScroll from "smooth-scroll";
 // import Masonry from 'masonry-layout';
 import imagesLoaded from 'imagesLoaded';
  // import Granim from 'granim';
import 'lazysizes';


import gsap from "gsap";

// import * as THREE from 'three';

// import 'vanta/dist/vanta.fog.min.js';

///// RUN HEADROOM MODULE
headroom();

//SWUP IPMORTS
import Swup from 'swup';
import SwupBodyClassPlugin from '@swup/body-class-plugin';
import SwupDebugPlugin from '@swup/debug-plugin';
import SwupJsPlugin from '@swup/js-plugin';
import SwupPreloadPlugin from '@swup/preload-plugin';
import SwupScrollPlugin from '@swup/scroll-plugin';






// MAKE JAVASCRIPT RUN FOR MODERN BROWSERS ONLY
if (supportsES6) {



lazySizes.cfg.expFactor = 4;
document.addEventListener('lazyloaded', function(e){

if (e.target.classList.contains('home--project-image')) {
  // setTimeout(function(){

e.target.closest(".home--project-grid-cell").classList.remove("project-image-not-loaded");
// }, 3000);

}

});


// EBLEM TEXT ROTATION

var Emblem={init:function(e,t){var n=document.querySelector(e),a=t?t:n.innerHTML;n.innerHTML="";for(var r=0;r<a.length;r++){var d=a[r],l=document.createElement("span"),o=document.createTextNode(d),i=360/a.length*r,p=(Math.PI/a.length).toFixed(0)*r,h=(Math.PI/a.length).toFixed(0)*r;l.appendChild(o),l.style.webkitTransform="rotateZ("+i+"deg) translate3d("+p+"px,"+h+"px,0)",l.style.transform="rotateZ("+i+"deg) translate3d("+p+"px,"+h+"px,0)",n.appendChild(l)}}};
  Emblem.init('.emblem');

// EBLEM TEXT ROTATION

//  VANTA CANVAS

//  VANTA CANVAS


/*
This plugin extends lazySizes to lazyLoad:
background images, videos/posters and scripts
Background-Image:
For background images, use data-bg attribute:
<div class="lazyload" data-bg="bg-img.jpg"></div>
 Video:
 For video/audio use data-poster and preload="none":
 <video class="lazyload" data-poster="poster.jpg" preload="none">
 <!-- sources -->
 </video>
 Scripts:
 For scripts use data-script:
 <div class="lazyload" data-script="module-name.js"></div>
 Script modules using require:
 For modules using require use data-require:
 <div class="lazyload" data-require="module-name"></div>
*/

(function(window, factory) {
	var globalInstall = function(){
		factory(window.lazySizes);
		window.removeEventListener('lazyunveilread', globalInstall, true);
	};

	factory = factory.bind(null, window, window.document);

	if(typeof module == 'object' && module.exports){
		factory(require('lazysizes'));
	} else if(window.lazySizes) {
		globalInstall();
	} else {
		window.addEventListener('lazyunveilread', globalInstall, true);
	}
}(window, function(window, document, lazySizes) {
	/*jshint eqnull:true */
	'use strict';
	var bgLoad, regBgUrlEscape;
	var uniqueUrls = {};

	if(document.addEventListener){
		regBgUrlEscape = /\(|\)|\s|'/;

		bgLoad = function (url, cb){
			var img = document.createElement('img');
			img.onload = function(){
				img.onload = null;
				img.onerror = null;
				img = null;
				cb();
			};
			img.onerror = img.onload;

			img.src = url;

			if(img && img.complete && img.onload){
				img.onload();
			}
		};

		addEventListener('lazybeforeunveil', function(e){
			if(e.detail.instance != lazySizes){return;}

			var tmp, load, bg, poster;
			if(!e.defaultPrevented) {

				if(e.target.preload == 'none'){
					e.target.preload = 'auto';
				}

				tmp = e.target.getAttribute('data-link');
				if(tmp){
					addStyleScript(tmp, true);
				}

				// handle data-script
				tmp = e.target.getAttribute('data-script');
				if(tmp){
					addStyleScript(tmp);
				}

				// handle data-require
				tmp = e.target.getAttribute('data-require');
				if(tmp){
					if(lazySizes.cfg.requireJs){
						lazySizes.cfg.requireJs([tmp]);
					} else {
						addStyleScript(tmp);
					}
				}

				// handle data-bg
				bg = e.target.getAttribute('data-bg');
				if (bg) {
					e.detail.firesLoad = true;
					load = function(){
						e.target.style.backgroundImage = 'url(' + (regBgUrlEscape.test(bg) ? JSON.stringify(bg) : bg ) + ')';
						e.detail.firesLoad = false;
						lazySizes.fire(e.target, '_lazyloaded', {}, true, true);
					};

					bgLoad(bg, load);
				}

				// handle data-poster
				poster = e.target.getAttribute('data-poster');
				if(poster){
					e.detail.firesLoad = true;
					load = function(){
						e.target.poster = poster;
						e.detail.firesLoad = false;
						lazySizes.fire(e.target, '_lazyloaded', {}, true, true);
					};

					bgLoad(poster, load);

				}
			}
		}, false);

	}

	function addStyleScript(src, style){
		if(uniqueUrls[src]){
			return;
		}
		var elem = document.createElement(style ? 'link' : 'script');
		var insertElem = document.getElementsByTagName('script')[0];

		if(style){
			elem.rel = 'stylesheet';
			elem.href = src;
		} else {
			elem.src = src;
		}
		uniqueUrls[src] = true;
		uniqueUrls[elem.src || elem.href] = true;
		insertElem.parentNode.insertBefore(elem, insertElem);
	}
}));

// import lozad from 'lozad'



// // accrodion for Menu
// import {
//   accordion
// } from "./modules/accordion";
// // accordion();







// AOS.init({
//   // Global settings:
//   disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
//   startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
//   initClassName: 'aos-init', // class applied after initialization
//   animatedClassName: 'aos-animate', // class applied on animation
//   useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
//
//
//
//   // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
//   offset: 0, // offset (in px) from the original trigger point
//   delay: 0, // values from 0 to 3000, with step 50ms
//   duration: 400, // values from 0 to 3000, with step 50ms
//   easing: 'ease', // default easing for AOS animations
//   once: true, // whether animation should happen only once - while scrolling down
//   mirror: false, // whether elements should animate out while scrolling past them
//   anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
//
// });










  function vanta_destroy() {
    if (typeof vanta_effect_home !== 'undefined') {
         vanta_effect_home.destroy()
         console.log("home destroy effects");
    }
    else {
    console.log("no effecf home");
    }
    if (typeof vanta_effect_agency !== 'undefined') {
         vanta_effect_agency.destroy()
         console.log("agency destroy effects");
    }
    else {
    console.log("no effect agency");
    }
  }



// hover shit
function _hover_shit () {
function project_link_hover_active(event) {
  var target = event.target;
  var parent = target.parentNode;//parent of "target"
  parent.classList.add("project-hover-active");
}
function project_link_hover_disable(event) {
  var target = event.target;
  var parent = target.parentNode;//parent of "target"
  parent.classList.remove("project-hover-active");
}


var link_home2project = document.querySelectorAll(".link-home-to-project");
for (var i = 0; i < link_home2project.length; i++) {
  (function(i) {
    link_home2project[i].addEventListener("mouseenter", project_link_hover_active);
    link_home2project[i].addEventListener("mouseleave", project_link_hover_disable);
  })(i);
}
}



function active_nav_link(event) {
  var target = event.target;
  var nav_links = document.querySelectorAll(".nav-link");
  for (var i = 0; i < nav_links.length; i++) {
    (function(i) {
      nav_links[i].style.textDecoration = "none";
      nav_links[i].classList.remove("activate-nav-link");
    })(i);
    }
    target.classList.add("activate-nav-link");
}

var nav_links = document.querySelectorAll(".nav-link");
for (var i = 0; i < nav_links.length; i++) {
  (function(i) {
    nav_links[i].addEventListener("click", active_nav_link);
  })(i);
}



///ON LOAD FOR pages

if (document.body.classList.contains('page-template-lab')) {
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.add('canvas-transition-effector');
    }

//VIEWS
 function initviews() {

_hover_shit();
var nav_links = document.querySelectorAll(".nav-link");
for (var i = 0; i < nav_links.length; i++) {
  (function(i) {
    nav_links[i].removeAttribute("style");
    nav_links[i].classList.remove("activate-nav-link");
  })(i);
  }




    if (document.body.classList.contains('page-template-project-page')) {
      _hover_shit();
    }

    if (document.body.classList.contains('single-projects')) {
      document.getElementById("js-info-trigger").addEventListener("click", function(){
        // console.log("jjjeee");
          const prod_view = document.querySelector("#project-view");
        if (prod_view.classList.contains('prod-info--active')) {
          setTimeout(function(){
            document.querySelector('.project-grid-section').removeAttribute("style");
          }, 300);

          prod_view.classList.remove('prod-info--active');
        }
        else {
          prod_view.classList.add('prod-info--active');
          let info_container_height = document.querySelector('.project-info-container').clientHeight;
          // console.log(info_container_height);
          let bottom_fix = 40 + info_container_height + "px"
          let info_container_transform_height = info_container_height
          let transform_variable = 'translate3d(0, ' + info_container_transform_height + 'px ,0)'
          // console.log(transform_variable);


          document.querySelector('.project-grid-section').style.webkitTransform = transform_variable
          document.querySelector('.project-grid-section').style.marginBottom = bottom_fix
        }
      });
      document.getElementById("js-info-trigger--mobile").addEventListener("click", function(){
        // console.log("jjjeee");
          const prod_view = document.querySelector("#project-view");
        if (prod_view.classList.contains('prod-info--active')) {
          setTimeout(function(){
            document.querySelector('.project-grid-section').removeAttribute("style");
          }, 300);

          prod_view.classList.remove('prod-info--active');
        }
        else {
          prod_view.classList.add('prod-info--active');
          let info_container_height = document.querySelector('.project-info-container--mobile').clientHeight;
          // console.log(info_container_height);
          let bottom_fix = 40 + info_container_height + "px"
          let info_container_transform_height = info_container_height
          let transform_variable = 'translate3d(0, ' + info_container_transform_height + 'px ,0)'
          // console.log(transform_variable);


          document.querySelector('.project-grid-section').style.webkitTransform = transform_variable
          document.querySelector('.project-grid-section').style.marginBottom = bottom_fix
        }
      });


        // document.querySelector(".project--next-project").addEventListener("click", function(){
        //   document.querySelector(".project--next-project").removeAttribute("style");
        //   var root = document.getElementsByTagName("HTML")[0];
        //     root.classList.add('next-project--activated');
        //     setTimeout(function(){
        //       document.querySelector('.project-grid-section').style.display = "none"
        //       document.querySelector('.project-hero-section').style.display = "none"
        //       root.classList.remove('next-project--activated');
        //     }, 1000);
        //     });

      } else {

      }
        if (document.body.classList.contains('page-template-home')) {
              _hover_shit();
              m_carousel();



window.vanta_effect_home = VANTA.FOG({
	el: "#vanta",
	highlightColor: 0x11be8b,

	 // highlightColor: 0x52d4e6,

	  midtoneColor: 0xff00f0,

	  lowlightColor: 0xfff,


	  baseColor: 0xffd100,
    highlightColor: 0x11be8b,
    highlightColor: 0xb400ff,
midtoneColor: 0xffd1,
lowlightColor: 0x1eff00,
baseColor: 0x0,



 blurFactor: 0.7,
 speed: 2,
 zoom: 0.5,

})




// VANTA.FOG({
// 	el: "#loading-canvas",
// 	highlightColor: 0x11be8b,
//
// 	 // highlightColor: 0x52d4e6,
//
// 	  midtoneColor: 0xff00f0,
//
// 	  lowlightColor: 0xfff,
//
//
// 	  baseColor: 0xffd100,
//
// 	  // baseColor: 0xfffe00,
// 		 // baseColor: 		0x86ff00,
//
//
//
//  blurFactor: 0.7,
//  speed: 2,
//  zoom: 0.5
// })





          Emblem.init('.emblem--home');
          // var granimInstance = new Granim({
          //     element: '#canvas-image-blending',
          //     direction: 'diagonal',
          //     isPausedWhenNotInView: true,
          //     // image : {
          //     //     source: '//localhost:3000/goodman/wp-content/themes/gdmn/dist/assets/images/canvas-image.jpg',
          //     //     blendingMode: 'lighten',
          //     // },
          //     // position	:	['center', 'center'],
          //     states : {
          //         "default-state": {
          //             gradients: [
          //               ['#b251f2', '#00f9ff'],
          //                 ['#0575E6', '#00F260'],
          //                 ['#ff00f3', '#0575E6']
          //             ],
          //             transitionSpeed: 	2000
          //         }
          //     }
          // });
          // heroAnim();
          if (supportsES6) {

          } else {

          }

            }

            if (document.body.classList.contains('page-template-agency')) {
              window.vanta_effect_agency = VANTA.FOG({
              	el: "#vanta",
                highlightColor: 0xb400ff,
   midtoneColor: 0xffd1,
   lowlightColor: 0x1eff00,
    baseColor: 0x0,




               blurFactor: 0.7,
               speed: 2,
               zoom: 0.5
              })

                }


 }

// var imgLoad = imagesLoaded( document.querySelector(".main-app") );
//
//
//
// imgLoad.on( 'progress', function( instance, image ) {
//   var result = image.isLoaded ? 'loaded' : 'broken';
//   console.log( 'image is ' + result + ' for ' + image.img.src );
//     LoadingImage.img.classlist.add("heee");
// });

if (supportsES6) {
  window.addEventListener('DOMContentLoaded', (event) => {

    document.querySelector(".main-app").classList.remove("app-loader");
  });

} else {


}





const js_options = [



    {
    from: '(.*)', to: 'next-project-transition',
    out: (next) => {







        document.querySelector(".project--next-project").removeAttribute("style");
        var root = document.getElementsByTagName("HTML")[0];

        var project_view = document.getElementById("project-view")

        // scrollIt(50000);
        root.classList.add('next-project--activated');
        project_view.classList.add('next-project-activated--fix');

        if (root.classList.contains('headroom--unpinned')) {
          document.querySelector(".main-header").style.opacity = "0";
        }




        setTimeout(function(){


          var scroll_Height = Math.max(
            // document.body.scrollHeight, document.documentElement.scrollHeight,
            // document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
          );

          var scroll_amount = window.pageYOffset

          var screen_height = window.innerHeight
          || document.documentElement.clientHeight
          || document.body.clientHeight;


          var distanceFromBottom =   scroll_Height - screen_height - scroll_amount

          // console.log(distanceFromBottom);
    if (window.innerWidth <= 500) {
      // console.log("Less than 500");
      // var transform_height = screen_height - 388 + distanceFromBottom;
      // var transform_height = screen_height - 188 + distanceFromBottom;
      // var transform_height = screen_height - 168 + distanceFromBottom;
        var transform_height = screen_height - 184 + distanceFromBottom;
      }
      else if (window.innerWidth <= 1200) {
  var transform_height = screen_height - 568 + distanceFromBottom;
  }
    else {
      // console.log("More than 500");
      var transform_height = screen_height - 548 + distanceFromBottom;
        }
  let tranform_dimension_variable = transform_height
          // var transform_height = screen_height - 548 + distanceFromBottom
          var transform_height_fix = -tranform_dimension_variable
          var transform_variable = 'translate3d(0, ' + transform_height_fix + 'px ,0)'
console.log(transform_height_fix);



          root.classList.add('next-project--transforming');
          var mobile_opa_fix = gsap.to(document.querySelector('.js-mox-1'), 0.5, {
            opacity: 1,
            ease: "power2.inOut",
            onComplete:function(){
                 }
          });
          var mobile_opa_fix = gsap.to(document.querySelector('.js-mox-2'), 0.5, {
            opacity: 1,
            ease: "power2.inOut",
            onComplete:function(){
                 }
          });
          var move_project = gsap.to(document.querySelector('.project--next-project--inner'), 1, {
            y: transform_height_fix,
            ease: "power2.inOut",
            onComplete:function(){
                move_project.kill();
              root.classList.remove('next-project--activated');
                root.classList.remove('next-project--transforming');
                  setTimeout(function(){
                next();
              }, 10);
                 }
          });




        //   document.querySelector(".project--next-project--inner").style.webkitTransform = transform_variable
      }, 500);

          setTimeout(function(){
            gsap.to(document.querySelector('.main-header'), 0.5, {
              opacity: 1,
              ease: "power2.inOut",
              onComplete:function(){
                root.classList.add('header-fix-to-top');
                   }
            });
          }, 500);

          // setTimeout(next,1500);


    },
    in: function(next) {
          var root = document.getElementsByTagName("HTML")[0];


      // root.classList.remove('header-fix-to-top');
    //     setTimeout(function(){
    //     var root = document.getElementsByTagName("HTML")[0];
    //   root.classList.remove('next-project--activated');
    //     root.classList.remove('next-project--transforming');
    // }, 100);
            initviews();
            next();

              setTimeout(function(){
                  root.classList.remove('header-fix-to-top');
              }, 500);

              document.querySelector(".light-aspect-bg").style.background = "white";
      setTimeout(next,1000);

    },

  },
  {
  from: '(.*)', to: 'lab-transition',
  out: (next) => {
    document.querySelector(".header-bg-wrap").classList.add('mix-blend-mode-transition--fix');
    document.querySelector(".emblem-wrapper").style.opacity = "0";

    var root = document.getElementsByTagName("HTML")[0];

    root.classList.add('page-swap');

  //       setTimeout(function(){
  //   root.classList.add('header--transition-mod');
  //   // root.classList.add('dark-mode');
  // }, 500);
    // console.log("LAB LAB transition");

    if (root.classList.contains('headroom--unpinned')) {
      document.querySelector(".main-header").style.opacity = "0";
        root.classList.add('canvas--unpinnned');
    }

     gsap.to(document.querySelector('.main-app'), 0.5, {
       opacity: 0,
       ease: "power2.inOut",
       onComplete:function(){
         root.classList.add('header--transition-mod');
                vanta_destroy();
                setTimeout(function(){
                next();
              }, 10);
            }
     });

  },
  in: function(next) {
    document.querySelector(".lab-grid-wrapper").style.opacity = "0";
      initviews();
        document.querySelector(".emblem-wrapper").style.opacity = "1";
    var root = document.getElementsByTagName("HTML")[0];
    gsap.to(document.querySelector('.main-header'), 0, {
      opacity: 1,
      ease: "power2.inOut",
      onComplete:function(){
        root.classList.remove('canvas--unpinnned');
           }
    });

gsap.to(document.querySelector('.main-app'), 0.5, {
  opacity: 1,
  ease: "power2.inOut",
  onComplete:function(){
    gsap.to(document.querySelector('.lab-grid-wrapper'), 0.5, {
      opacity: 1,
      ease: "power2.inOut",
      onComplete:function(){
           }
    });
              root.classList.remove('page-swap');
              root.classList.remove('header--transition-mod');
              document.querySelector(".header-bg-wrap").classList.remove('mix-blend-mode-transition--fix');
                document.querySelector(".emblem-wrapper").removeAttribute("style");
                root.classList.add('canvas-transition-effector');
           setTimeout(function(){
           next();
         }, 10);
       }
});










  },

},



{
  from: '(.*)', to: 'to-project-transition',
  out: (next) => {
    var root = document.getElementsByTagName("HTML")[0];
root.classList.add('page-swap');
if (root.classList.contains('headroom--unpinned')) {
    root.classList.add('unpinned--delay');
  document.querySelector(".main-header").style.opacity = "0";
}
gsap.to(document.querySelector('.main-app'), 0.5, {
  opacity: 0,
  ease: "power2.inOut",
  onComplete:function(){
           root.classList.add('header--transition-mod');
           vanta_destroy();
           setTimeout(function(){
           next();
         }, 10);
       }
});

  },
  in: function(next) {

      // swup.use(new SwupPreloadPlugin());
    var root = document.getElementsByTagName("HTML")[0];
    initviews();


    imagesLoaded( document.querySelector('.project-hero-image'), function( instance ) {
      gsap.to(document.querySelector('.main-header'), 0.5, {
        opacity: 1,
        ease: "power2.inOut",
        onComplete:function(){
             }
      });
  console.log('all images are loaded');
   if (root.classList.contains('unpinned--delay') ){
  gsap.to(document.querySelector('.main-app'), 0.5, {
    opacity: 1,
    delay:0.5,
    ease: "power2.inOut",
    onComplete:function(){
                root.classList.remove('page-swap');
                root.classList.remove('unpinned--delay');
                root.classList.remove('header--transition-mod');

             setTimeout(function(){
             next();
           }, 10);
         }
  });
}
else {
  gsap.to(document.querySelector('.main-app'), 0.5, {
    opacity: 1,
    ease: "power2.inOut",
    onComplete:function(){
                root.classList.remove('page-swap');
                root.classList.remove('header--transition-mod');

             setTimeout(function(){
             next();
           }, 10);
         }
  });
}

});


  },

},
{
  from: '(.*)', to: 'agency-transition',
  out: (next) => {

    var root = document.getElementsByTagName("HTML")[0];
root.classList.add('page-swap');
if (root.classList.contains('headroom--unpinned')) {
    root.classList.add('unpinned--delay');
    if (root.classList.contains('canvas-transition-effector') ) {
}
else {
document.querySelector(".main-header").style.opacity = "0";

}
}
gsap.to(document.querySelector('.main-app'), 0.5, {
  opacity: 0,
  ease: "power2.inOut",
  onComplete:function(){
           root.classList.add('header--transition-mod');

           vanta_destroy();
           setTimeout(function(){
           next();
         }, 10);
       }
});


  },
  in: function(next) {
      // swup.use(new SwupPreloadPlugin());
    var root = document.getElementsByTagName("HTML")[0];
    // console.log("start-delay");
    initviews();

      gsap.to(document.querySelector('.main-header'), 0.5, {
        opacity: 1,
        ease: "power2.inOut",
        onComplete:function(){
             }
      });




  gsap.to(document.querySelector('.main-app'), 0.5, {
    opacity: 1,
    ease: "power2.inOut",
    onComplete:function(){
                root.classList.remove('page-swap');
                root.classList.remove('header--transition-mod');
                  root.classList.remove('canvas-transition-effector');
             setTimeout(function(){
             next();
           }, 10);
         }
  });




  },

},

{
  from: '(.*)', to: 'project-view',
  out: (next) => {
    setTimeout(next,100);
    console.log("project-view-starts");
      var root = document.getElementsByTagName("HTML")[0];
    var pagesrol = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    var minuspagesrol = - (pagesrol);
    root.classList.remove('hide-active-project');
    document.querySelector(".inner-mover").style.transform = "translateY(" + minuspagesrol + "px)";
  },
  in: function(next) {
      initviews();
    var root = document.getElementsByTagName("HTML")[0];
    imagesLoaded( document.querySelector('.project-hero-image'), function( instance ) {
      var close_overlay = gsap.to(document.querySelector('.project-grid-overlay'), 0.5, {
        xPercent: 100,
        y:0,
        z:0,

        ease: "power2.inOut",
        clearProps: "all",
        onComplete:function(){
          close_overlay.kill();
            console.log("project-loaded");
          root.classList.remove('header-fix-to-top');
          document.querySelector(".inner-mover").removeAttribute("style");
          setTimeout(function(){
          next();
        }, 10);
             }
      });
});
  },

},


{
  from: '(.*)', to: 'basic-transition',
  out: (next) => {
    var root = document.getElementsByTagName("HTML")[0];
    if (root.classList.contains('headroom--unpinned') ) {
      root.classList.add('unpinned--delay');
        if (root.classList.contains('canvas-transition-effector') ) {
    }
    else {
document.querySelector(".main-header").style.opacity = "0";

    }
    }
    var pagesrol = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    var minuspagesrol = - (pagesrol);


    root.classList.add('page-swap');

if (root.classList.contains('hide-active-project') ){
  root.classList.remove('hide-active-project');
  var close_overlay = gsap.to(document.querySelector('.project-grid-overlay'), 0.5, {
    xPercent: 100,
    y:0,
    z:0,

    ease: "power2.inOut",
    clearProps: "all",
    onComplete:function(){
      close_overlay.kill();
        console.log("project-loaded");
      root.classList.remove('header-fix-to-top');
      document.querySelector(".inner-mover").removeAttribute("style");
      setTimeout(function(){

    }, 10);
         }
  });
    }



     gsap.to(document.querySelector('.main-app'), 0.5, {
       opacity: 0,
       ease: "power2.inOut",
       onComplete:function(){
root.classList.add('header--transition-mod');
                vanta_destroy();
                setTimeout(function(){
                next();
              }, 10);
            }
     });

    // console.log("BASIC BASIC");



  },
  in: function(next) {
      initviews();
    var root = document.getElementsByTagName("HTML")[0];
    console.log("basicccccc");
    gsap.to(document.querySelector('.main-header'), 0.5, {
      opacity: 1,
      ease: "power2.inOut",
      onComplete:function(){
        root.classList.remove('canvas--unpinnned');
           }
    });

    if (root.classList.contains('canvas-transition-effector') ) {
      gsap.to(document.querySelector('.main-app'), 0.5, {
        delay: 0.5,
        opacity: 1,
        ease: "power2.inOut",
        onComplete:function(){

          root.classList.remove('project-view-active');
          root.classList.remove('canvas-transition-effector');
                root.classList.remove('header--transition-mod');

          root.classList.remove('header-fix-to-top');
          root.classList.remove('page-swap');
          document.querySelector(".inner-mover").removeAttribute("style");

                 setTimeout(function(){
                 next();
               }, 10);
             }
      });
}
else if (root.classList.contains('unpinned--delay') ){
  gsap.to(document.querySelector('.main-app'), 0.5, {
    delay: 0.5,
    opacity: 1,
    ease: "power2.inOut",
    onComplete:function(){
      root.classList.remove('unpinned--delay');
      root.classList.remove('project-view-active');
      root.classList.remove('canvas-transition-effector');
            root.classList.remove('header--transition-mod');

      root.classList.remove('header-fix-to-top');
      root.classList.remove('page-swap');
      document.querySelector(".inner-mover").removeAttribute("style");

             setTimeout(function(){
             next();
           }, 10);
         }
  });
}

else if (root.classList.contains('is-popstate') ){
  gsap.to(document.querySelector('.main-app'), 0.5, {
    delay: 0.5,
    opacity: 1,
    ease: "power2.inOut",
    onComplete:function(){
      root.classList.remove('unpinned--delay');
      root.classList.remove('project-view-active');
      root.classList.remove('canvas-transition-effector');
            root.classList.remove('header--transition-mod');
            console.log("popstate transition");
      root.classList.remove('header-fix-to-top');
      root.classList.remove('page-swap');
      document.querySelector(".inner-mover").removeAttribute("style");

             setTimeout(function(){
             next();
           }, 10);
         }
  });
}
else {
  gsap.to(document.querySelector('.main-app'), 0.5, {
    opacity: 1,
    ease: "power2.inOut",
    onComplete:function(){
      root.classList.remove('project-view-active');
      root.classList.remove('canvas-transition-effector');
            root.classList.remove('header--transition-mod');

      root.classList.remove('header-fix-to-top');
      root.classList.remove('page-swap');
      document.querySelector(".inner-mover").removeAttribute("style");

             setTimeout(function(){
             next();
           }, 10);
         }
  });
}




  },

},





];

const options = {

  containers: ['.pjax-container'],
  animationSelector: '[class*="page-transition"]',
  animateHistoryBrowsing: true,
  plugins: [
    new SwupJsPlugin(js_options),
    // new SwupDebugPlugin(),

    new SwupPreloadPlugin(),
    new SwupBodyClassPlugin(),
    new SwupScrollPlugin({
    doScrollingRightAway: false,
    animateScroll: false,
    scrollFriction: 0.3,
    cache: false,
    scrollAcceleration: 0.04,
}),
  ],
};
const swup = new Swup(options);
swup.findPlugin('ScrollPlugin');

let scrollPositions = [];
let scrollToSavedPosition = null;

// swup.on('clickLink', () => {
// 	scrollPositions[window.location.href] = window.scrollY;
// });
//
// swup.on('popState', () => {
// 	scrollToSavedPosition = true;
// });
//
// swup.on('animationInStart', () => {
// 	if (scrollToSavedPosition)
// 		window.scrollTo(0, scrollPositions[window.location.href]);
// 	scrollToSavedPosition = false;
// });




/////INIT VIEWS//////

initviews();


// document.querySelector('.services-link').addEventListener('click', (e) => {
// //   for (const s_link of document.querySelectorAll('.scroll-link')) {
// //   s_link.classList.remove('active');
// // }
// //   e.target.classList.add('active');
//   scrollIt(
//     document.querySelector('.services-wrapper'),
//     500,
//     'easeOutQuad',
//     () => console.log(`Just finished scrolling to ${window.pageYOffset}px`)
//   );
// });
// document.querySelector('.about-link').addEventListener('click', (e) => {
// //   for (const s_link of document.querySelectorAll('.scroll-link')) {
// //   s_link.classList.remove('active');
// // }
// //   e.target.classList.add('active');
//   scrollIt(
//     document.querySelector('.agency-manifest-wrapper'),
//
//     500,
//     'easeOutQuad',
//     () => console.log(`Just finished scrolling to ${window.pageYOffset}px`)
//   );
// });
//
// document.querySelector('.contact-link').addEventListener('click', (e) => {
// //   for (const s_link of document.querySelectorAll('.scroll-link')) {
// //   s_link.classList.remove('active');
// // }
// //   e.target.classList.add('active');
//   scrollIt(
//     document.querySelector('.contact-wrapper'),
//
//     500,
//     'easeOutQuad',
//     () => console.log(`Just finished scrolling to ${window.pageYOffset}px`)
//   );
// });
//
// window.onscroll = function() {
//
//
//   var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
//   if (scrollTop <= 320) {
//     for (const s_link of document.querySelectorAll('.scroll-link')) {
//     s_link.classList.remove('active');
//   }
//           document.querySelector('.about-link').classList.add('active');
//   }
//
//     if (scrollTop > 320) {
//       for (const s_link of document.querySelectorAll('.scroll-link')) {
//       s_link.classList.remove('active');
//     }
//             document.querySelector('.services-link').classList.add('active');
//     }
//     if (scrollTop > 1000) {
//       for (const s_link of document.querySelectorAll('.scroll-link')) {
//       s_link.classList.remove('active');
//     }
//             document.querySelector('.contact-link').classList.add('active');
//     }
// };







///OVERLAY module


window.gBodyScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;



document.querySelector("#js-project-view-trigger").addEventListener("click", function(){
  window.gBodyScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  // console.log(gBodyScroll);
  // console.log("project-view-active");
  var root = document.getElementsByTagName("HTML")[0];

  // root.classList.add('project-view-active');
   var show_overlay = gsap.to(document.querySelector('.project-grid-overlay'), {
    x: 0,
    y:0,
    z:0,
    duration: 0.5,

    ease: "power2.inOut",
    onComplete:function(){
        show_overlay.kill();
      root.classList.add('hide-active-project');
      root.classList.add('header-fix-to-top');
      window.scrollTo(0, 0);
      console.log("anim-readyyy");
         }
  });




});

var close_overlay_project_view = function() {
  var root = document.getElementsByTagName("HTML")[0];
  var pagesrol = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  console.log(pagesrol);
  var minuspagesrol = - (pagesrol);
  // console.log(pagesrol);
  root.classList.remove('hide-active-project');
  setTimeout(function(){
    root.classList.add('headroom--pinned');
    root.classList.remove('headroom--unpinned');
}, 10);

  document.querySelector(".inner-mover").style.transform = "translateY(" + minuspagesrol + "px)";
  window.scrollTo(0, gBodyScroll);
  var close_overlay = gsap.to(document.querySelector('.project-grid-overlay'), 0.5, {
    xPercent: 100,
    y:0,
    z:0,

    ease: "power2.inOut",
    clearProps: "all",
    onComplete:function(){
      close_overlay.kill();
        console.log("anim-backkkk");
        root.classList.add('headroom--pinned');
        root.classList.remove('headroom--unpinned');
      root.classList.remove('header-fix-to-top');
      document.querySelector(".inner-mover").removeAttribute("style");
         }
  });
}

document.querySelector("#js-close-project-view").addEventListener("click", function(){
close_overlay_project_view();
});



swup.off('samePage');
document.addEventListener('swup:samePage', event => {
  event.preventDefault();
close_overlay_project_view();

});




/*!
 * Run a callback function after scrolling has stopped
 * (c) 2017 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Function} callback The function to run after scrolling
//  */
// var scrollStop = function (callback) {
//
// 	// Make sure a valid callback was provided
// 	if (!callback || typeof callback !== 'function') return;
//
// 	// Setup scrolling variable
// 	var isScrolling;
//
// 	// Listen for scroll events
// 	window.addEventListener('scroll', function (event) {
//       var root = document.getElementsByTagName("HTML")[0];
//       root.classList.remove('scroll-stop');
//       root.classList.add('scrolling-active');
// 		// Clear our timeout throughout the scroll
// 		window.clearTimeout(isScrolling);
//
// 		// Set a timeout to run after scrolling ends
// 		isScrolling = setTimeout(function() {
//
// 			// Run the callback
// 			callback();
//
// 		}, 66);
//
// 	}, false);
//
// };


// scrollStop(function () {
//     console.log('Scrolling has stopped.');
//     var root = document.getElementsByTagName("HTML")[0];
//       root.classList.remove('scrolling-active');
//     root.classList.add('scroll-stop');
//
// });


// UTIL SHITTT

let window_height = window.innerHeight
document.querySelector(".project-grid-trigger-wrapper").style.height = window.innerHeight + "px"
// UTILS SHIIIT

var isIphone = navigator.userAgent.indexOf("iPhone") != -1 ;

if (isIphone) {
document.querySelector(".project-grid-trigger-wrapper").classList.add("ios-fix");
}
else {
  document.querySelector(".project-grid-trigger-wrapper").classList.add("no-ios-fix");
}





var hasTouch;
window.addEventListener('touchstart', function setHasTouch () {
    hasTouch = true;
    if (hasTouch) {
      var root = document.getElementsByTagName("HTML")[0];
      root.classList.add('touch-device');
    }
    else {

    }
    // Remove event listener once fired, otherwise it'll kill scrolling
    // performance
    window.removeEventListener('touchstart', setHasTouch);
}, false);







// MODER BROWSER IF STATEMENT ENDS

}


else {
     var root = document.getElementsByTagName("body")[0];
  root.classList.add('no-ie6-site');
  document.querySelector(".deprecated-alert").addEventListener("click", hide_alert_box);

  function hide_alert_box() {
      document.querySelector(".deprecated-alert").style.display = "none";
  }
}

// MODER BROWSER FUNCTION STOP

if (/MSIE 10/i.test(navigator.userAgent)) {

   var root = document.getElementsByTagName("body")[0];
   root.classList.add('edge-ie--common-detection');
    root.classList.add('ie-alert');
}

if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
        var root = document.getElementsByTagName("body")[0];
     root.classList.add('edge-ie--common-detection');
       root.classList.add('ie-alert');
}

if (/Edge\/\d./i.test(navigator.userAgent)){
    var root = document.getElementsByTagName("body")[0];
    var root_html = document.getElementsByTagName("HTML")[0];
   root.classList.add('edge-ie--common-detection');
   root_html.classList.add('edge-ie--common-detection');
}






  // document.querySelector(".loading-page-indicator").classList.add('show-loading');




  //// smooth scrollYYYYYY

  var scroll = new SmoothScroll('a[href*="#"]', {
	speed: 1000,
  easing: 'easeInOutCubic',
  updateURL: false,
  offset: 60,
  speedAsDuration: false
});







 // Log scroll events
var logScrollEvent = function (event) {
  // for (const s_link of document.querySelectorAll('.scroll-link')) {
  //    s_link.classList.remove('active');
  //  }

   var link_span = event.detail.toggle.parentElement

   setTimeout(function(){
      link_span.classList.add("active");
 }, 500);

   // console.log(link_span);

	// // The event type
	// console.log('type:', event.type);
  //
	// // The anchor element being scrolled to
	// console.log('anchor:', event.detail.anchor);
  //
	// // The anchor link that triggered the scroll
	// console.log('toggle:', event.detail.toggle);

};

// Listen for scroll events
// document.addEventListener('scrollStart', logScrollEvent, false);








// window.addEventListener('scroll', function (event) {
//   for (const s_link of document.querySelectorAll('.scroll-link')) {
//     setTimeout(function(){
//           s_link.classList.remove('active');
//   }, 0);
//
//   }
//
// }, false);







/////// REZISEEE SHIIIIIT FOR SIDE NAV ////////////


var
    timeout = false, // holder for timeout id
    delay = 250, // delay after event is "complete" to run callback
    calls = 0;

// window.resize callback function
function getDimensions() {
  var root = document.getElementsByTagName("HTML")[0];
  if (root.classList.contains('touch-device')) {
  // console.log("touch-now-rezise");
}
else {
  let window_height = window.innerHeight
  document.querySelector(".project-grid-trigger-wrapper").style.height = window.innerHeight + "px";
  // console.log("resize nav");
}
}

// window.resize event listener
window.addEventListener('resize', function() {
	// clear the timeout
  clearTimeout(timeout);
  // start timing for event "completion"
  timeout = setTimeout(getDimensions, delay);
});

// getDimensions();

window.addEventListener("orientationchange", function() {
  console.log("the orientation of the device is now " + screen.orientation.angle);
  setTimeout(function(){
     getDimensions();
}, 500);
});
