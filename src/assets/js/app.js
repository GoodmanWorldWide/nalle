




import Swup from 'swup';
const axios = require('axios').default;
import SwupBodyClassPlugin from '@swup/body-class-plugin';
import SwupDebugPlugin from '@swup/debug-plugin';
import SwupJsPlugin from '@swup/js-plugin';
import SwupPreloadPlugin from '@swup/preload-plugin';
import SwupScrollPlugin from '@swup/scroll-plugin';

import  "./modules/scroll-module";

// import {
//   headroom
// } from "./modules/headroom";

lazySizes.cfg.expFactor = 4;
import 'lazysizes';
document.addEventListener('lazyloaded', function(e){

if (e.target.classList.contains('hero-image')) {
  // setTimeout(function(){

e.target.closest(".js-film-link").classList.remove("image--loading");
// }, 3000);

}

});



////HERO MOUSE OVER ///

function hero_hover(){
var hero_hover_item = document.querySelectorAll(".hero-block-link");

if(hero_hover_item.length > 0) {
  for (var i = 0, len = hero_hover_item.length; i < len; i++) {
    hero_hover_item[i].addEventListener("mouseenter", function(e){

// e.target.appendChild(canvas_item);
  e.target.classList.add("hover--active");
});
hero_hover_item[i].addEventListener("mouseleave", function(e){


e.target.classList.remove("hover--active");
});

}

}

}

function com_hover(){
var hero_hover_item = document.querySelectorAll(".com-block-link");

if(hero_hover_item.length > 0) {
  for (var i = 0, len = hero_hover_item.length; i < len; i++) {
    hero_hover_item[i].addEventListener("mouseenter", function(e){
var canvas_item_2 = document.querySelector(".noise-canvas_2");
// e.target.querySelector('.com-cell-inner').appendChild(canvas_item_2);
  e.target.classList.add("hover--active");
});
hero_hover_item[i].addEventListener("mouseleave", function(e){


e.target.classList.remove("hover--active");
});

}

}

}







////HERO load more ///


function hero_load_more() {



const link = document.querySelector(".js-load-second-row");
const spinner = document.querySelector(".spin-shit");
link.addEventListener('click', event => {
  event.preventDefault();
  var the_url = event.currentTarget.href;
  // console.log(the_url);



  link.style.opacity = "0";
  spinner.style.opacity = "1";



  axios.get(the_url)
    .then(function (response) {
      // handle success
      document.querySelector(".hero-row--2").classList.remove("hide");
      // console.log(response);
      // console.log(response.data);
      var com_home_container = document.querySelector('.hero-row--2');
        var wrapper = document.createElement('div');
            wrapper.innerHTML = response.data;
            var newContent = wrapper.querySelector('.section--commercials-grid');
            com_home_container.appendChild(newContent);
            setTimeout(function(){
                link.style.display = "none";
            spinner.style.opacity = "0";
          }, 500);

          setTimeout(function () {
spinner.style.opacity = "0";

setTimeout(function(){
document.querySelector(".hero-row--2").classList.add("transform-com-block");
}, 500);
scrollIt(document.querySelector('.home-scroll-point'), 1000, 'easeInOutQuint', function () {
spinner.style.display = "none";
document.querySelector(".hero-row--2").classList.add("transform-com-block");
// init_second_noise();
com_hover();
window.pjax_target = document.querySelectorAll(".com-block-link");
init_pjax_links();
document.querySelector(".bottom-link").classList.remove("bottom-link-home");
});
}, 500);



    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // console.log("axios request");
    });






//  setTimeout(function(){
// document.querySelector(".hero-row--2").classList.remove("hero-cell-transform-trigger");
//
// }, 500);
//
// setTimeout(function(){
//   document.querySelector(".commercials-flex-grid").style.opacity = "1";
// // document.querySelector(".hero-row--1").classList.add("second-row-loaded");
// }, 1100);
return false;
  });

}










//////SWUP//////


const js_options = [


  {
    from: '(.*)', to: 'film-view-transition',
    out: (next) => {
      var root = document.getElementsByTagName("HTML")[0];
      document.querySelector('.project-overlay-view').classList.add("project-overlay-view--show");
      // next();
      // console.log("pjax");

    },

    in: function(next) {
      next();
    },

  },




{
  from: '(.*)', to: 'basic-transition',
  out: (next) => {
    var root = document.getElementsByTagName("HTML")[0];
    root.classList.add("pjax--fade");
    // root.classList.remove("activate-animation");
    // setTimeout(function(){
        next();
    // }, 500);


  },

  in: function(next) {
      window.scrollTo(0, 0);

    var root = document.getElementsByTagName("HTML")[0];
      if (root.classList.contains('activate-animation')) {
        // console.log("logo anim runnning");
      }
      else {
          root.classList.add("activate-animation");
          setTimeout(function(){
            root.classList.remove("activate-animation");
          }, 2600);
      }

      init_views();
      setTimeout(function(){
    root.classList.remove("pjax--fade");
    next();
  }, 100);
    // next();

  },

},


];



const options = {

  containers: ['.pjax-container--main'],
  linkSelector:
    '.pjax-link',
  animationSelector: '[class*="page-transition"]',
  animateHistoryBrowsing: true,
  plugins: [
    new SwupJsPlugin(js_options),
    // new SwupDebugPlugin(),

    new SwupPreloadPlugin(),
    new SwupBodyClassPlugin(),
//     new SwupScrollPlugin({
//     doScrollingRightAway: false,
//     animateScroll: false,
//     scrollFriction: 0.3,
//     cache: false,
//     scrollAcceleration: 0.04,
// }),
  ],
};


if (document.body.classList.contains('single')) {

}
else {
  const swup = new Swup(options);
  document.addEventListener('swup:samePage', (event) => {
  // console.log("same page");
  document.getElementsByTagName("body")[0].classList.remove("mobile-menu--active");
  });
}



// const link = document.querySelector(".hero-cell-inner");
// link.addEventListener('click', event => {
//   event.preventDefault();
//   var the_url = event.currentTarget.href;
//   console.log(the_url);
//
//   swup.preloadPage(the_url);
//
//
//
//
//
// return false;
// });




// LOAD FILM ///




function create_project_data(){
var client_name = event.currentTarget.dataset.clientname;
  var project_name = event.currentTarget.dataset.projectname;

var cName = document.createTextNode(client_name);
var pName = document.createTextNode(project_name);
// console.log(cName);
// console.log(pName);
document.querySelector(".film-loading-wrapper--client-name").innerHTML = "";
document.querySelector('.film-loading-wrapper--client-name').appendChild(cName);
document.querySelector(".film-loading-wrapper--project-name").innerHTML = "";
document.querySelector('.film-loading-wrapper--project-name').appendChild(pName);
}



///CUSTOM PJAX From GRID////







function init_pjax_links() {
var film_links = pjax_target;
// console.log(film_links);
for(var z = 0; z < film_links.length; z++) {
  var elem = film_links[z];

  elem.addEventListener('click', event => {
      event.preventDefault();
    event.currentTarget.classList.add("prevent-click");
event.currentTarget.classList.remove("hover--active");




create_project_data();

////TRANSITION Before load////





/////THE FECTH //////

  var the_url = event.currentTarget.href;
  // console.log(the_url);
  var stateObj;
  window.history.pushState(stateObj, "", the_url);
  var oldContent = document.querySelector('.pjax-container--film');
if (oldContent !== null) {
  oldContent.parentNode.removeChild(oldContent);
}

window.gBodyScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
document.querySelector('.close-film-button-wrapper').style.display = "block";
  var root = document.getElementsByTagName("HTML")[0];

  document.querySelector('.project-overlay-view').removeAttribute("style");

setTimeout(function(){
document.querySelector('.project-overlay-view').classList.add("project-overlay-view--show");
}, 300);


// Make a request for a user with a given ID
axios.get(the_url)
  .then(function (response) {
    // handle success

    // console.log(response);
    // console.log(response.data);
    var film_container = document.querySelector('.pjax-parent');
      var film_container = document.querySelector('.pjax-parent');

      var wrapper = document.createElement('div');
          wrapper.innerHTML = response.data;
          var newContent = wrapper.querySelector('.pjax-container--film');
          film_container.appendChild(newContent);
            setTimeout(function(){
              document.querySelector('.main-app').style.opacity = "0";
              document.querySelector('.main-navigation--wrapper').style.visibility = "hidden";
              root.classList.add("body-bg-override");
              setTimeout(function(){
            document.querySelector('#nalle-body').style.display = "none";
            document.querySelector('.project-overlay-view').style.position = "relative";
          }, 10);
            window.scrollTo(0, 0);
            document.querySelector('.prevent-click').classList.remove("prevent-click");
              var next_film_link = document.querySelector(".js-next-film-link");
              if(typeof(next_film_link) != 'undefined' && next_film_link != null){
                next_film_trigger();
              } else{
                // console.log("no next film");
              }

              document.querySelector('.film-loading-wrapper').style.opacity = "0";
              setTimeout(function(){
                document.querySelector('.film-loading-wrapper').style.display = "none";
                var root = document.getElementsByTagName("HTML")[0];
                document.querySelector('.close-fim-button').style.opacity = "1";
                document.querySelector('.pjax-container--main').removeAttribute("style");

           document.querySelector('.pjax-container--film').classList.remove("film--hidden");



         }, 500);
       }, 1000);


  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // console.log("axios request");
    root.classList.add("overlay-view--active");
  });








return false;
});
}

}



///////NNEXT FILM LINKS /////////

//Combine code if needed more edits////

function next_film_trigger() {


var next_film_link = document.querySelector(".js-next-film-link");


next_film_link.addEventListener('click', event => {
// console.log("next fiiiiilm");
    event.currentTarget.classList.add("prevent-click");
  event.preventDefault();



document.querySelector('.film-loading-wrapper').style.removeProperty('display');
create_project_data();

   setTimeout(function(){
     window.scrollTo(0, 0);
     document.querySelector('.film-loading-wrapper').style.opacity = "1";
         // window.scrollTo(0, 0);
       }, 500);

////TRANSITION Before load////


  var root = document.getElementsByTagName("HTML")[0];



/////THE FECTH //////

  var the_url = event.currentTarget.href;
  var stateObj;
  window.history.replaceState(stateObj, "", the_url);
  // console.log(the_url);
  var oldContent = document.querySelector('.pjax-container--film');
if (oldContent !== null) {
  oldContent.style.opacity = 0;
}



axios.get(the_url)
  .then(function (response) {
    // handle success

    // console.log(response);
    // console.log(response.data);
    var film_container = document.querySelector('.pjax-parent');


      var wrapper = document.createElement('div');
          wrapper.innerHTML = response.data;
          var newContent = wrapper.querySelector('.pjax-container--film');
          film_container.appendChild(newContent);
            setTimeout(function(){
              oldContent.parentNode.removeChild(oldContent);
              next_film_trigger();
              document.querySelector('.film-loading-wrapper').style.opacity = "0";
              setTimeout(function(){

                var root = document.getElementsByTagName("HTML")[0];
                document.querySelector('.close-film-button-wrapper').style.opacity = "1";
                document.querySelector('.film-loading-wrapper').style.display = "none";
              document.querySelector('.project-overlay-view').style.position = "relative";
              window.scrollTo(0, 0);
           document.querySelector('.pjax-container--film').classList.remove("film--hidden");


          }, 300);
        }, 1000);


  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // console.log("axios request");
  });





return false;
});

}



//////LOAD FILM ENDS ///////




//////CLOSE FILM ///////

function close_overlay() {

var root = document.getElementsByTagName("HTML")[0];
window.pagesrol = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
var minuspagesrol = - (pagesrol);
// console.log(pagesrol);
//   if (root.classList.contains('header--not-on-top')) {
// root.classList.add("bio-button--keep-position");
//   }
// document.querySelector('.header-bg-wrap').removeAttribute("style");
// document.querySelector('.hide-bio-overlay-wrapper').style.display = "block";
  document.querySelector('.close-fim-button').style.opacity = "0";
  document.querySelector('.project-overlay-view').style.position = "fixed";
document.querySelector(".inner-mover").style.transform = "translateY(" + minuspagesrol + "px)";
document.querySelector('#nalle-body').style.display = "";


  // setTimeout(function(){
      root.classList.remove("body-bg-override");


  // }, 100);
// window.scrollTo(0, 0);
  setTimeout(function(){
    window.scrollTo(0, gBodyScroll);
setTimeout(function(){
    document.querySelector('.main-app').style.opacity = "";
    document.querySelector('.main-navigation--wrapper').style.visibility = "visible";
    document.querySelector('.project-overlay-view').classList.remove("project-overlay-view--show");
    setTimeout(function(){
      document.querySelector(".inner-mover").removeAttribute("style");
      document.querySelector('.film-loading-wrapper').removeAttribute("style");
      document.querySelector('.project-overlay-view').style.display = "none";
      // document.querySelector('.jaymo-bio-section').removeAttribute("style");
      // root.classList.add("filter-sticky--active");
      // root.classList.remove("filter-sticky--super-active");
      // document.querySelector('.jaymo-bio-section').classList.remove("show-shaker");
      document.querySelector('.close-film-button-wrapper').style.display = "none";
      // root.classList.remove("bio-button--keep-position");
      var oldContent = document.querySelector('.pjax-container--film');



      if (document.body.classList.contains('page-template-home')) {
        var stateObj;
        window.history.replaceState(stateObj, "", home_url);

      }
      if (document.body.classList.contains('page-template-commercials')) {
        var stateObj;
        window.history.replaceState(stateObj, "", home_url + "/commercials/");
      }

      if (document.body.classList.contains('page-template-shortfilms')) {
        var stateObj;
        window.history.replaceState(stateObj, "", home_url + "/short-films/");
      }

      root.classList.remove("overlay-view--active");

    if (oldContent !== null) {
      oldContent.parentNode.removeChild(oldContent);
    }


    }, 600);
  }, 100);
}, 500);
}



document.querySelector('#js-close-film').onclick = function() {
close_overlay();
return false;
};


function create_canvas_1 () {
var new_canvas = document.createElement('canvas');
new_canvas.className = "noise-canvas noise-canvas_1";
console.log(new_canvas);
// console.log(canvas_item);
var canvas_parent = document.querySelector('.hero-block-link');
var positionInfo = canvas_parent.getBoundingClientRect();
var c_height = positionInfo.height;
var c_width = positionInfo.width;
console.log(c_height);
console.log(c_width);
document.querySelector(".hero-block-link").appendChild(new_canvas);
window.canvas_item = document.querySelector(".noise-canvas_1");
noise_1();
}


  // for (var i = 0, len = canvas_item.length; i < len; i++) {
    // noise_1();
  // }

function noise_1() {
var viewWidth,
    viewHeight,
    canvas = canvas_item,
    ctx;

// change these settings
var patternSize = 64,
    patternScaleX = 3,
    patternScaleY = 1,
    patternRefreshInterval = 4,
    patternAlpha = 25; // int between 0 and 255,

var patternPixelDataLength = patternSize * patternSize * 4,
    patternCanvas,
    patternCtx,
    patternData,
    frame = 0;

// window.onload = function() {
    initCanvas();
    initGrain();
    requestAnimationFrame(loop);
// };

// create a canvas which will render the grain
function initCanvas() {
    viewWidth = canvas.width = canvas.clientWidth;
    viewHeight = canvas.height = canvas.clientHeight;
    ctx = canvas.getContext('2d');
    console.log(ctx);
    ctx.scale(patternScaleX, patternScaleY);
}

// create a canvas which will be used as a pattern
function initGrain() {
    patternCanvas = document.createElement('canvas');
    console.log(patternCanvas);
    patternCanvas.width = patternSize;
    patternCanvas.height = patternSize;
    patternCtx = patternCanvas.getContext('2d');
    patternData = patternCtx.createImageData(patternSize, patternSize);
}

// put a random shade of gray into every pixel of the pattern
function update() {
    var value;

    for (var i = 0; i < patternPixelDataLength; i += 4) {
        value = (Math.random() * 205) | 0;

        patternData.data[i    ] = value;
        patternData.data[i + 1] = value;
        patternData.data[i + 2] = value;
        patternData.data[i + 3] = patternAlpha;
    }

    patternCtx.putImageData(patternData, 0, 0);
}

// fill the canvas using the pattern
function draw() {
    ctx.clearRect(0, 0, viewWidth, viewHeight);

    ctx.fillStyle = ctx.createPattern(patternCanvas, 'repeat');
    ctx.fillRect(0, 0, viewWidth, viewHeight);
}

function loop() {
    if (++frame % patternRefreshInterval === 0) {
        update();
        draw();
    }

    requestAnimationFrame(loop);
}

}






//////MOBILE NAV/////


function mobile_nav() {
const menu_trigger_open = document.querySelector(".js-mobile-nav-trigger");
// const menu_trigger_close = document.querySelector(".js-close-mobile-menu");
  menu_trigger_open.addEventListener("click", function(){
    // console.log("MOBILE MENU");
    if (document.body.classList.contains('mobile-menu--active')) {
      document.getElementsByTagName("body")[0].classList.remove("mobile-menu--active");
    }
    else {
      document.getElementsByTagName("body")[0].classList.add("mobile-menu--active");
    }





  });

}
mobile_nav();


  // setTimeout(function(){
// noise_1();
// noise_2();
// }, 2000);






//SECOND NOISE //

function init_second_noise() {
  var new_canvas = document.createElement('canvas');
  new_canvas.className = "noise-canvas noise-canvas_2";
  console.log(new_canvas);


  // console.log(canvas_item);
  // var canvas_parent = document.querySelector('.com-cell-inner');

  var canvas_parents = document.querySelectorAll('.com-cell-inner');
  var last_canvas = canvas_parents[canvas_parents.length- 1];

  var positionInfo = last_canvas.getBoundingClientRect();
  var c_height = positionInfo.height;
  var c_width = positionInfo.width;
  console.log(c_height);
  console.log(c_width);

  var append_block_parents = document.querySelectorAll('.com-block-link');
  var append_block = append_block_parents[append_block_parents.length- 1];

  append_block.appendChild(new_canvas);
  window.canvas_item_2 = document.querySelector(".noise-canvas_2");
  console.log(canvas_item_2);
noise_2();
  function noise_2() {
    console.log("noise 2 active");
  var viewWidth,
      viewHeight,
      canvas = canvas_item_2,
      ctx;

  // change these settings
  var patternSize = 64,
      patternScaleX = 3,
      patternScaleY = 1,
      patternRefreshInterval = 4,
      patternAlpha = 25; // int between 0 and 255,

  var patternPixelDataLength = patternSize * patternSize * 4,
      patternCanvas,
      patternCtx,
      patternData,
      frame = 0;

  // window.onload = function() {
      initCanvas();
      initGrain();
      requestAnimationFrame(loop);
  // };

  // create a canvas which will render the grain
  function initCanvas() {
      viewWidth = canvas.width = canvas.clientWidth;
      viewHeight = canvas.height = canvas.clientHeight;
      ctx = canvas.getContext('2d');

      ctx.scale(patternScaleX, patternScaleY);
  }

  // create a canvas which will be used as a pattern
  function initGrain() {
      patternCanvas = document.createElement('canvas');
      patternCanvas.width = patternSize;
      patternCanvas.height = patternSize;
      patternCtx = patternCanvas.getContext('2d');
      patternData = patternCtx.createImageData(patternSize, patternSize);
  }

  // put a random shade of gray into every pixel of the pattern
  function update() {
      var value;

      for (var i = 0; i < patternPixelDataLength; i += 4) {
          value = (Math.random() * 205) | 0;

          patternData.data[i    ] = value;
          patternData.data[i + 1] = value;
          patternData.data[i + 2] = value;
          patternData.data[i + 3] = patternAlpha;
      }

      patternCtx.putImageData(patternData, 0, 0);
  }

  // fill the canvas using the pattern
  function draw() {
      ctx.clearRect(0, 0, viewWidth, viewHeight);

      ctx.fillStyle = ctx.createPattern(patternCanvas, 'repeat');
      ctx.fillRect(0, 0, viewWidth, viewHeight);
  }

  function loop() {
      if (++frame % patternRefreshInterval === 0) {
          update();
          draw();
      }

      requestAnimationFrame(loop);
  }

  }
  }






  //////HEADER SSCROLLLL///////


  window.onscroll = function() {
  var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop

  var root = document.getElementsByTagName("HTML")[0];

  if (scrollTop > 20) {
    var root = document.getElementsByTagName("HTML")[0];
    root.classList.add("header--not-on-top");


  }
  else {
    var root = document.getElementsByTagName("HTML")[0];
    root.classList.remove("header--not-on-top");

  }

  };





function video_loader() {
  // window.addEventListener('load', function() {
      var video = document.querySelector('#contact-video-id');

      // var preloader = document.querySelector('.preloader');

      function checkLoad() {
          if (video.readyState === 4) {
              // preloader.parentNode.removeChild(preloader);
              console.log("video loaded");

              video.play();
          } else {
              setTimeout(checkLoad, 100);
          }
      }

      checkLoad();
  // }, false);
}





  //////////// View constractor ////////

  function init_views() {
// console.log("init app");
if (document.body.classList.contains('page-template-home')) {
  set_hero_screen_height();
  // console.log("home init");
  function init_pjax() {
  window.pjax_target = document.querySelectorAll(".hero-block-link");
  init_pjax_links();
  }
  window.addEventListener("resize", set_hero_screen_height);
  init_pjax();
  hero_load_more();
  // create_canvas_1();
  hero_hover();
}
if (document.body.classList.contains('page-template-commercials')) {
    // console.log("commercials init");
  function init_pjax() {
  window.pjax_target = document.querySelectorAll(".com-block-link");
  init_pjax_links();
  }

  init_pjax();
  // init_second_noise();
  com_hover();

}

if (document.body.classList.contains('page-template-shortfilms')) {
    // console.log("commercials init");
  function init_pjax() {
  window.pjax_target = document.querySelectorAll(".com-block-link");
  init_pjax_links();
  }

  init_pjax();
// init_second_noise();
  com_hover();

}

if (document.body.classList.contains('page-template-contact')) {
    // console.log("contact init");
    video_loader();
    setTimeout(function(){
        document.querySelector('.contact-video-cell').style.opacity = "1";
  }, 1000);


}




  }


  init_views();



  var isIphone = navigator.userAgent.indexOf("iPhone") != -1 ;

  if (isIphone) {
    var root = document.getElementsByTagName("HTML")[0];
      root.classList.add("ios-fix");
  }
  else {
    //   var root = document.getElementsByTagName("HTML")[0];
    // root.classList.add("no-ios-fix");
  }

  var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                 navigator.userAgent &&
                 navigator.userAgent.indexOf('CriOS') == -1 &&
                 navigator.userAgent.indexOf('FxiOS') == -1;


                 if (isSafari) {
                   var root = document.getElementsByTagName("HTML")[0];
                     root.classList.add("ios-fix");

                 }
                 else {
                 //   var root = document.getElementsByTagName("HTML")[0];
                 // root.classList.add("no-ios-fix");
                 }



  setTimeout(function(){
var root = document.getElementsByTagName("HTML")[0];
root.classList.remove("page-is-loading");
  setTimeout(function(){
    root.classList.add("activate-animation");
      setTimeout(function(){
            root.classList.remove("logo-opacity");
            root.classList.remove("activate-animation");
      }, 2700);
  }, 0);
  setTimeout(function(){
document.querySelector('.loading-smoke').style.display = "none";
// root.classList.remove("logo-opacity");
  }, 500);
}, 500);







window.onpopstate = function (event) {
  if (event.state) {
    // history changed because of pushState/replaceState
    // console.log("pushstaaate");
          var root = document.getElementsByTagName("HTML")[0];
    if (root.classList.contains('overlay-view--active')) {
close_overlay();
    }
    var newPageUrl = location.href;
    console.log(newPageUrl);
  } else {
    var root = document.getElementsByTagName("HTML")[0];
    // console.log("pushstaaate but something elseee");
    if (root.classList.contains('overlay-view--active')) {
close_overlay();
    }

    var newPageUrl = location.href;
    console.log(newPageUrl);
    if (/commercials/.test(window.location.href)) {
      console.log("COMMERICALLLLL");
      location.replace(newPageUrl);
    }
    if (/shortfilms/.test(window.location.href)) {
      console.log("COMMERICALLLLL");
      location.replace(newPageUrl);
    }

  }
}



function set_hero_screen_height() {
  if (window.innerWidth > 750) {
    // console.log("hero set size");
  var inner_height = window.innerHeight;
  var hero_block = document.querySelector('.hero-row--1');
  if(typeof(hero_block) != 'undefined' && hero_block != null){
  document.querySelector('.hero-row--1').style.height =  inner_height + "px";
    }
  }
  else {
    var hero_block = document.querySelector('.hero-row--1');
    if(typeof(hero_block) != 'undefined' && hero_block != null){
    document.querySelector('.hero-row--1').removeAttribute("style");
      }
      // console.log("hero clear size");
  }
}



if(typeof(next_film_link) != 'undefined' && next_film_link != null){
  next_film_trigger();
} else{
  // console.log("no next film");
}






window.addEventListener('touchstart', function onFirstTouch() {
  // we could use a class
  var root = document.getElementsByTagName("HTML")[0];
  root.classList.add('touch-device');





  // we only need to know once that a human touched the screen, so we can stop listening now
  window.removeEventListener('touchstart', onFirstTouch, false);
}, false);
