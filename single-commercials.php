<?php
/*
Template Name: Goodman Project
Template Post Type: post, page, film

*/
get_header(); ?>

<!-- <style media="screen">
	body {
		visibility: hidden;
	}
</style>

<script type="text/javascript">
var home_url = "<?php echo home_url() ?>"; // "A string here"
console.log(home_url);
window.location.replace(home_url);
</script> -->

<div class="main-app" style="">

<div id="project-view" class="pjax-container--film film--hidden" style="">


<section class="project-main-container container-eq">
	<h1 class="project-name-box"><?php the_field( 'client_name' ); ?></h1>
	<h2 class="project-name-box"><?php the_field( 'project_name' ); ?></h2>


	<?php if( get_field('project_story_') ): ?>

		<div class="project-story">
	<p><?php the_field( 'project_story_' ); ?></p>
	</div>
	<?php endif; ?>


	<?php if ( have_rows( 'films' ) ) : ?>
	<?php while ( have_rows( 'films' ) ) : the_row(); ?>
		<div id="" class="project-hero-film-wrapper <?php the_sub_field( 'video_aspect' ); ?>">
<?php the_sub_field( 'project_hero_film' ); ?>
		</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
	<div class="project-info-wrapper">

		<div class="grid-x">
			<div class="cell project-info-cell">
				<p><?php the_field( 'project_info' ); ?></p>
			</div>
			</div>
	</div>

	<?php if ( have_rows( 'project_content' ) ): ?>
		<div class="project-xtra-content-wrapper">

	<?php while ( have_rows( 'project_content' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'full_image' ) : ?>
			<?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ) { ?>
				<div class="xtra--full-image">
					<img src="<?php echo $image['sizes']['medium-image']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			<?php } ?>
		<?php elseif ( get_row_layout() == 'video_embed' ) : ?>
			<div class="xtra--embed-wrapper">
			<?php the_sub_field( 'video' ); ?>
						</div>

		<?php elseif ( get_row_layout() == 'double_image' ) : ?>
			<div class="xtra--double-image-wrapper">
			<?php $left_image = get_sub_field( 'left_image' ); ?>
			<?php if ( $left_image ) { ?>
				<div class="double-image--left">
					<img src="<?php echo $left_image['sizes']['medium-image']; ?>" alt="<?php echo $left_image['alt']; ?>" />
				</div>
			<?php } ?>
			<?php $right_image = get_sub_field( 'right_image' ); ?>
			<?php if ( $right_image ) { ?>
				<div class="double-image--right">
				<img src="<?php echo $right_image['sizes']['medium-image']; ?>" alt="<?php echo $right_image['alt']; ?>" />
						</div>
			<?php } ?>
				</div>
		<?php endif; ?>
	<?php endwhile; ?>
		</div>
<?php endif; ?>


</section>




<?php
$next_post = get_adjacent_post();

if (!empty( $next_post )): ?>


			  <?php $c_name = get_field('client_name' , $next_post->ID) ?>
				<?php $prod_name = get_field('project_name' , $next_post->ID) ?>
				<?php $project_homepage_image = get_field( 'project_homepage_image', $next_post->ID ); ?>

				<?php $next_post_tittle = get_the_title($next_post) ?>

				<div class="link-2-next-film-wrapper container-eq">
<a class="js-next-film-link" href="<?php echo get_permalink( $next_post->ID ); ?>" data-clientname="<?php echo $c_name ?>" data-projectname="<?php echo $prod_name ?>">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 116.92 50.67"><title></title><path d="M1495.91,1519.84c-36.93,0-53.94-6-53.94-18.73,0-12.51,16.71-19.35,49.66-20.33a10,10,0,0,0-1.48,4.91,0.2,0.2,0,0,0,.35.14,19.26,19.26,0,0,1,5.66-5.19,0.5,0.5,0,0,0,0-.88,18,18,0,0,1-6-4.74,0.19,0.19,0,0,0-.34.15,10.23,10.23,0,0,0,1.7,4.62c-34,1-50.47,8-50.47,21.32s17.33,19.73,54.94,19.73a0.5,0.5,0,0,0,0-1" transform="translate(-1440.97 -1474.95)" style="fill:#f4f2ed"/><path d="M1557.88,1501.11c0-14-17.85-21.06-54.94-21.44h0a0.5,0.5,0,0,0,0,1c36.43,0.38,54,7.06,54,20.44,0,12.07-15.33,17.83-49.65,18.65a10,10,0,0,0,1.51-4.89,0.19,0.19,0,0,0-.35-0.14,19.26,19.26,0,0,1-5.71,5.16,0.49,0.49,0,0,0,0,.87,18,18,0,0,1,6,4.79,0.19,0.19,0,0,0,.34-0.15,10.2,10.2,0,0,0-1.68-4.65c34.9-.84,50.49-6.91,50.49-19.64" transform="translate(-1440.97 -1474.95)" style="fill:#f4f2ed"/><polygon points="21.38 21.25 19.69 21.25 19.69 30.32 21.4 30.32 21.4 24.05 26.49 30.32 28.21 30.32 28.21 21.25 26.47 21.25 26.47 27.58 21.38 21.25" style="fill:#f4f2ed"/><polygon points="37.77 22.97 37.77 21.25 30.55 21.25 30.55 30.32 37.9 30.32 37.9 28.6 32.27 28.6 32.27 26.38 35.45 26.38 35.45 24.62 32.27 24.62 32.27 22.97 37.77 22.97" style="fill:#f4f2ed"/><polygon points="41.49 30.32 43.98 27.04 46.46 30.32 48.58 30.32 45.05 25.61 48.3 21.25 46.17 21.25 43.98 24.15 41.77 21.25 39.65 21.25 42.9 25.61 39.37 30.32 41.49 30.32" style="fill:#f4f2ed"/><polygon points="49.81 22.97 52.85 22.97 52.85 30.32 54.56 30.32 54.56 22.97 57.61 22.97 57.61 21.25 49.81 21.25 49.81 22.97" style="fill:#f4f2ed"/><polygon points="71.34 22.97 71.34 21.25 63.99 21.25 63.99 30.32 65.7 30.32 65.7 26.38 69.08 26.38 69.08 24.62 65.7 24.62 65.7 22.97 71.34 22.97" style="fill:#f4f2ed"/><rect x="73.7" y="21.25" width="1.72" height="9.07" style="fill:#f4f2ed"/><polygon points="77.79 21.25 77.79 30.32 85.14 30.32 85.14 28.6 79.51 28.6 79.51 21.25 77.79 21.25" style="fill:#f4f2ed"/><polygon points="97.27 30.32 97.27 21.25 95.64 21.25 92.37 26.96 89.05 21.25 87.47 21.25 87.47 30.32 89.16 30.32 89.16 24.95 92.37 30.32 95.55 24.95 95.55 30.32 97.27 30.32" style="fill:#f4f2ed"/></svg>
					</a>
			 </div>
<?php else : ?>
	<?php
		 $posts = get_posts(array(
			'posts_per_page'	=> 1,
			'post_type'			=> 'commercials'
		 ));

		 if( $posts ): ?>
	<?php foreach( $posts as $post ):
		 setup_postdata( $post );
		 ?>
		 <?php $project_hero_image_first = get_field( 'project_hero_image' ); ?>
		 <?php $c_name_first = get_field('client_name') ?>
			<?php $prod_name_first = get_field('project_name') ?>

			<?php $project_homepage_image = get_field( 'project_homepage_image' ); ?>

				 <div class="link-2-next-film-wrapper container-eq">

			 <a class="js-next-film-link" href="<?php echo get_permalink(); ?>" data-clientname="<?php the_field( 'client_name' ); ?>" data-projectname="<?php the_field( 'project_name' ); ?>">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 116.92 50.67"><title>Untitled-14</title><path d="M1495.91,1519.84c-36.93,0-53.94-6-53.94-18.73,0-12.51,16.71-19.35,49.66-20.33a10,10,0,0,0-1.48,4.91,0.2,0.2,0,0,0,.35.14,19.26,19.26,0,0,1,5.66-5.19,0.5,0.5,0,0,0,0-.88,18,18,0,0,1-6-4.74,0.19,0.19,0,0,0-.34.15,10.23,10.23,0,0,0,1.7,4.62c-34,1-50.47,8-50.47,21.32s17.33,19.73,54.94,19.73a0.5,0.5,0,0,0,0-1" transform="translate(-1440.97 -1474.95)" style="fill:#f4f2ed"/><path d="M1557.88,1501.11c0-14-17.85-21.06-54.94-21.44h0a0.5,0.5,0,0,0,0,1c36.43,0.38,54,7.06,54,20.44,0,12.07-15.33,17.83-49.65,18.65a10,10,0,0,0,1.51-4.89,0.19,0.19,0,0,0-.35-0.14,19.26,19.26,0,0,1-5.71,5.16,0.49,0.49,0,0,0,0,.87,18,18,0,0,1,6,4.79,0.19,0.19,0,0,0,.34-0.15,10.2,10.2,0,0,0-1.68-4.65c34.9-.84,50.49-6.91,50.49-19.64" transform="translate(-1440.97 -1474.95)" style="fill:#f4f2ed"/><polygon points="21.38 21.25 19.69 21.25 19.69 30.32 21.4 30.32 21.4 24.05 26.49 30.32 28.21 30.32 28.21 21.25 26.47 21.25 26.47 27.58 21.38 21.25" style="fill:#f4f2ed"/><polygon points="37.77 22.97 37.77 21.25 30.55 21.25 30.55 30.32 37.9 30.32 37.9 28.6 32.27 28.6 32.27 26.38 35.45 26.38 35.45 24.62 32.27 24.62 32.27 22.97 37.77 22.97" style="fill:#f4f2ed"/><polygon points="41.49 30.32 43.98 27.04 46.46 30.32 48.58 30.32 45.05 25.61 48.3 21.25 46.17 21.25 43.98 24.15 41.77 21.25 39.65 21.25 42.9 25.61 39.37 30.32 41.49 30.32" style="fill:#f4f2ed"/><polygon points="49.81 22.97 52.85 22.97 52.85 30.32 54.56 30.32 54.56 22.97 57.61 22.97 57.61 21.25 49.81 21.25 49.81 22.97" style="fill:#f4f2ed"/><polygon points="71.34 22.97 71.34 21.25 63.99 21.25 63.99 30.32 65.7 30.32 65.7 26.38 69.08 26.38 69.08 24.62 65.7 24.62 65.7 22.97 71.34 22.97" style="fill:#f4f2ed"/><rect x="73.7" y="21.25" width="1.72" height="9.07" style="fill:#f4f2ed"/><polygon points="77.79 21.25 77.79 30.32 85.14 30.32 85.14 28.6 79.51 28.6 79.51 21.25 77.79 21.25" style="fill:#f4f2ed"/><polygon points="97.27 30.32 97.27 21.25 95.64 21.25 92.37 26.96 89.05 21.25 87.47 21.25 87.47 30.32 89.16 30.32 89.16 24.95 92.37 30.32 95.55 24.95 95.55 30.32 97.27 30.32" style="fill:#f4f2ed"/></svg>
					 </a>
 				</div>



	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
<?php endif; ?>


</div>

<!-- FIXED_STUFF -->


	<?php get_template_part( 'template-parts/content', 'fixedstuff' ); ?>


<?php get_footer();
