<?php
/*
Template Name: Short Film
Template Post Type: post, page, film

*/
get_header(); ?>

<!-- <style media="screen">
	body {
		visibility: hidden;
	}
</style>

<script type="text/javascript">
var home_url = "<?php echo home_url() ?>"; // "A string here"
console.log(home_url);
window.location.replace(home_url);
</script> -->

<div class="main-app" style="">

<div id="project-view" class="pjax-container--film film--hidden" style="">


<section class="project-main-container container-eq">
	<h1 class="project-name-box"><?php the_field( 'client_name' ); ?></h1>
	<h2 class="project-name-box"><?php the_field( 'project_name' ); ?></h2>


	<?php if( get_field('project_story_') ): ?>
		<div class="project-story">
			<p>
	<?php the_field( 'project_story_' ); ?>
	</p>
	</div>
	<?php endif; ?>


	<?php if ( have_rows( 'films' ) ) : ?>
	<?php while ( have_rows( 'films' ) ) : the_row(); ?>
		<div id="" class="project-hero-film-wrapper <?php the_field( 'video_aspect' ); ?>">

				<?php the_field( 'project_hero_film' ); ?>
		</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
	<div class="project-info-wrapper">

		<div class="grid-x">
			<div class="cell project-info-cell">
				<p><?php the_field( 'project_info' ); ?></p>
			</div>
			</div>
	</div>

	<?php if ( have_rows( 'project_content' ) ): ?>
		<div class="project-xtra-content-wrapper">

	<?php while ( have_rows( 'project_content' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'full_image' ) : ?>
			<?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ) { ?>
				<div class="xtra--full-image">
					<img src="<?php echo $image['sizes']['medium-image']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			<?php } ?>
		<?php elseif ( get_row_layout() == 'video_embed' ) : ?>
			<div class="xtra--embed-wrapper">
			<?php the_sub_field( 'video' ); ?>
						</div>

		<?php elseif ( get_row_layout() == 'double_image' ) : ?>
			<div class="xtra--double-image-wrapper">
			<?php $left_image = get_sub_field( 'left_image' ); ?>
			<?php if ( $left_image ) { ?>
				<div class="double-image--left">
					<img src="<?php echo $left_image['sizes']['medium-image']; ?>" alt="<?php echo $left_image['alt']; ?>" />
				</div>
			<?php } ?>
			<?php $right_image = get_sub_field( 'right_image' ); ?>
			<?php if ( $right_image ) { ?>
				<div class="double-image--right">
				<img src="<?php echo $right_image['sizes']['medium-image']; ?>" alt="<?php echo $right_image['alt']; ?>" />
						</div>
			<?php } ?>
				</div>
		<?php endif; ?>
	<?php endwhile; ?>
		</div>
<?php endif; ?>


</section>




<!-- <?php
$next_post = get_adjacent_post();

if (!empty( $next_post )): ?>


			  <?php $c_name = get_field('client_name' , $next_post->ID) ?>
				<?php $prod_name = get_field('project_name' , $next_post->ID) ?>
				<?php $project_homepage_image = get_field( 'project_homepage_image', $next_post->ID ); ?>

				<?php $next_post_tittle = get_the_title($next_post) ?>

				<div class="link-2-next-film-wrapper container-eq">
<a class="js-next-film-link" href="<?php echo get_permalink( $next_post->ID ); ?>" data-clientname="<?php echo $c_name ?>" data-projectname="<?php echo $prod_name ?>">
			 <h2>Next Film</h2>
					</a>
			 </div>
<?php else : ?>
	<?php
		 $posts = get_posts(array(
			'posts_per_page'	=> 1,
			'post_type'			=> 'shortfilms'
		 ));



		 if (( $posts ) && ( $posts->found_posts > 0)): ?>
	<?php foreach( $posts as $post ):
		 setup_postdata( $post );
		 ?>

		 <?php echo $posts->found_posts(); ?>
		 <?php $project_hero_image_first = get_field( 'project_hero_image' ); ?>
		 <?php $c_name_first = get_field('client_name') ?>
			<?php $prod_name_first = get_field('project_name') ?>

			<?php $project_homepage_image = get_field( 'project_homepage_image' ); ?>

				 <div class="link-2-next-film-wrapper container-eq">

			 <a class="js-next-film-link" href="<?php echo get_permalink(); ?>" data-clientname="<?php the_field( 'client_name' ); ?>" data-projectname="<?php the_field( 'project_name' ); ?>">
 				<h2>Next Film</h2>
					 </a>
 				</div>



	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
<?php endif; ?> -->


</div>

<!-- FIXED_STUFF -->


	<?php get_template_part( 'template-parts/content', 'fixedstuff' ); ?>


<?php get_footer();
