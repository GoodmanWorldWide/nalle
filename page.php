<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <div class="main-app">


 <section class="basic-section main-top-pad transition-fade">
 <div class="wrap-small">
 <div class="page-haeding">
 <h1><?php the_field( 'case_section_header' ); ?></h1>
 <?php if( get_field('lead_paragraph') ): ?>
 <h3 class="lead-p"><?php the_field( 'lead_paragraph' ); ?></h3>
 <?php endif; ?>
 </div>


 	<?php if ( have_rows( '_case' ) ) : ?>
 		<div class="main-content-block">
 		<?php while ( have_rows( '_case' ) ) : the_row(); ?>
 			<?php the_sub_field( 'case_name' ); ?>
 			<?php if ( have_rows( 'case_content' ) ): ?>
 				<?php while ( have_rows( 'case_content' ) ) : the_row(); ?>
 					<?php if ( get_row_layout() == 'case_content_inner' ) : ?>
 						<div class="b-text-block">
 						<?php the_sub_field( 'content__' ); ?>
 										</div>
 					<?php elseif ( get_row_layout() == 'case_media_inner' ) : ?>
 						<?php if ( have_rows( 'product_media__copy' ) ): ?>
 							<?php while ( have_rows( 'product_media__copy' ) ) : the_row(); ?>
 								<div class="media-container">
 								<?php if ( get_row_layout() == 'product_image' ) : ?>
 									<?php $p_image = get_sub_field( 'p_image' ); ?>
 									<?php if ( $p_image ) { ?>
 										<img src="<?php echo $p_image['url']; ?>" alt="<?php echo $p_image['alt']; ?>" />
 									<?php } ?>
 								<?php elseif ( get_row_layout() == 'product_video' ) : ?>
 									  <div class="aspect-video-embed">
 									<?php the_sub_field( 'p_video' ); ?>
 										</div>
 								<?php endif; ?>
 										</div>
 							<?php endwhile; ?>
 						<?php else: ?>
 							<?php // no layouts found ?>
 						<?php endif; ?>
 					<?php endif; ?>
 				<?php endwhile; ?>
 			<?php else: ?>
 				<?php // no layouts found ?>
 			<?php endif; ?>
 		<?php endwhile; ?>
 				</div>
 	<?php else : ?>
 		<?php // no rows found ?>
 	<?php endif; ?>




 </div>
 </section>

 </div>

 <?php get_footer();
