<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** Gutenberg editor support */
require_once( 'library/gutenberg.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );


//ACF OPTIONS
// if( function_exists('acf_add_options_page') ) {
//
// 	acf_add_options_page('Navigation/Footer');
//
//
// }


function add_linebreak_shortcode() {
return '<br />';
}
add_shortcode('br', 'add_linebreak_shortcode' );


/**
 *  Remove the h1 tag from the WordPress editor.
 *
 *  @param   array  $settings  The array of editor settings
 *  @return  array             The modified edit settings
 */

function my_format_TinyMCE( $in ) {
        $in['block_formats'] = "Paragraph=p; Heading=h3;Preformatted=pre";
    return $in;
}
add_filter( 'tiny_mce_before_init', 'my_format_TinyMCE' );
add_image_size( 'large-image', 3000, 3000 );
add_image_size( 'x-medium-image', 2000, 2000 );
add_image_size( 'medium-image', 1700, 2000 );
add_image_size( 'small-image', 1200, 2000 );

add_image_size( 'project-preview', 800, 800 );

add_filter('jpeg_quality', function($arg){return 100;});


// add_image_size( 'm-split-small', 600, 600, false );
// add_image_size( 'm-split', 1100, 1100, false );
//
//
// add_image_size( 'm-full-medium', 1200, 675, true );
// add_image_size( 'm-full', 2048, 1152, true );
//
//
// add_image_size( 'm-team', 400, 600, true );




function my_acf_admin_head() {
?>
<style type="text/css">

.acf-postbox:nth-child(even) {
	background: #d4eee4;

	    border: solid 1px #e2e4e7;
		    border-radius: 10px 10px 0px 0px;
			margin: 2rem 0;
}

.acf-postbox:nth-child(odd) {
	background: #e8f0f8;


	    border: solid 1px #e2e4e7;
		    border-radius: 10px 10px 0px 0px;
}

.acf-postbox .inside  {
	background: white;
}

.edit-post-meta-boxes-area .postbox>.inside {
border:none;

}

	.acf-flexible-content .layout {

	    border: 1px solid #6cd4be;
	       border-radius: 10px 10px 0px 0px;
	}

	.acf-flexible-content .layout .acf-fc-layout-handle {
	    border-bottom: #6cd4be solid 1px;
	}
</style>
<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');
